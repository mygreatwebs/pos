﻿Public Class promo4soes
    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        Try
            xSet.Tables("listsoesdiskon").Clear()
            deletepromo(True, 0)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Dim letakrowpromo As Integer
    Private Sub promo4soes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If IsNothing(xSet.Tables("listsoesdiskon")) Then
                xSet.Tables.Add("listsoesdiskon")
                xSet.Tables("listsoesdiskon").Columns.Add("idBrg")
                xSet.Tables("listsoesdiskon").Columns.Add("namaBrg")
                xSet.Tables("listsoesdiskon").Columns.Add("namaKategori2")
                xSet.Tables("listsoesdiskon").Columns.Add("jmlh")
            End If
            SQLquery = "SELECT
  mb.idBrg,
  mb.namaBrg,
  mk.namaKategori2,
  0 `jmlh`
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.Inactive='0' AND mk.idKategori2='8'"
            xSet.Tables("listsoesdiskon").Clear()
            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listsoesdiskon")
            GridControl1.DataSource = xSet.Tables("listsoesdiskon")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub deletepromo(ByVal isdeletedpromo As Boolean, ByVal jumlahsebelumdel As Integer)
        Dim data As New DataTable
        data = xSet.Tables("setSales").Copy


        For Each dtrow In data.Rows
            'Dim a As Integer = FormPOSx.GridView1.RowCount
            'Dim b As String = FormPOSx.GridView1.GetRowCellValue(i, "namaBrg")
            If dtrow("namaBrg") = "Soes - Promo Kalender Apr 2017" And isdeletedpromo = True Then
                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    xSet.Tables("setsales").Rows.Remove(mdtrow_1)
                Next
            End If
            If dtrow("namaBrg") = "Soes - Promo Kalender Apr 2017" And isdeletedpromo = False Then

                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    mdtrow_1("jmlh") = jumlahsebelumdel
                Next
            End If
            Dim kategori As String = ExDb.Scalar("SELECT
  mk.idKategori2
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.idBrg='" & dtrow("idBrg") & "'", My.Settings.localconn)
            If kategori = "8" And dtrow("hrgpcs") = 2500 Then
                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    xSet.Tables("setSales").Rows.Remove(mdtrow_1)
                Next

            End If
        Next
        data.Clear()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Try
            Dim jumlahpotongan As Integer = FormPOSx.GridView1.GetRowCellValue(letakrowpromo, "jmlh")

            deletepromo(False, jumlahpotongan)
            Dim countsoes As Integer
            Dim selectedsoes() As DataRow = xSet.Tables("listsoesdiskon").Select("jmlh>0")

            For Each soes In selectedsoes
                countsoes = countsoes + soes.Item("jmlh")

            Next soes
            If countsoes > jumlahpotongan * 20 Then
                MessageBox.Show("Jumlah soes tidak sesuai ketentuan kupon kalender", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            For Each soes In selectedsoes
                xSet.Tables("setSales").Rows.Add(soes.Item("idBrg"),
                                                    soes.Item("namaBrg"),
                                                    soes.Item("jmlh"),
                                                    soes.Item("jmlh") * 2500,
                                                    2500,
                                                    0)
            Next soes
            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class