﻿Imports MySql.Data.MySqlClient

Public Class freeprodwholebread
    Dim TotalTrans As Double
    Dim jmlhrg10000 As Integer
    Dim jml5000 As Integer
    Dim isproduklain As Integer

    Private Sub isiGrid(ByVal isotherproduct As Boolean)
        If isotherproduct Then
            SQLquery = "select  a.idbrg,namaBrg,namaKategori2,0 jmlh,price hrg from mbarang  a  left join mkategoribrg2 c on a.idKategori2=c.idkategori2  where c.namaKategori2 Not in ('WHOLE BREAD') order by price asc"
        Else
            SQLquery = "select  a.idbrg,namaBrg,namaKategori2,0 jmlh,price hrg from mbarang  a  left join mkategoribrg2 c on a.idKategori2=c.idkategori2  where c.namaKategori2  in ('WHOLE BREAD') and  a.namaBrg not in ('Butter Roll','Burger Bun','Roti Sisir Polos') order by price Desc"
        End If

        If IsNothing(xSet.Tables("itemprod")) = False Then xSet.Tables("itemprod").Clear()
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "itemprod")
        GridControl1.DataSource = xSet.Tables("itemprod")

    End Sub

    Public Sub CallMe(ByVal v_TotalTrans As Double)
        TotalTrans = v_TotalTrans
        isiGrid(False)
        Me.ShowDialog()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        jml5000 = 0
        jmlhrg10000 = 0

        If TotalTrans >= 50000 And TotalTrans < 100000 Then
            If isproduklain Then
                jmlhrg10000 = 15
            Else
                jmlhrg10000 = 5
            End If

        End If
        If TotalTrans >= 100000 Then
            Dim tempjml As Double = TotalTrans / 100000
            If isproduklain Then
                jml5000 = 5 * Int(tempjml) * 3
            Else
                jml5000 = 5 * Int(tempjml)
            End If

            Dim sisakoma As Double

            sisakoma = tempjml - Int(tempjml)

            If sisakoma >= 0 Then

                sisakoma = sisakoma * 100000
                If sisakoma >= 50000 Then
                    jmlhrg10000 = 5
                End If
            End If
        End If


        Dim totqty As Integer
        For Each row As DataRow In xSet.Tables("itemprod").Select("jmlh>0")
            totqty += row("jmlh")
        Next

        'If totqty <> jml5000 + jmlhrg10000 Then
        '    MsgBox("Input Qty Tidak Tepat ! ")
        '    Exit Sub
        'End If
        Dim Diskonnua As Double
        Dim sisa5000 As Integer = jml5000
        Dim sisa10000 As Integer = jmlhrg10000
        Dim temphrg As Double = 0
        Dim itemke3 As Integer = 3
        Dim discountx As Integer
        If IsNothing(xSet.Tables("setSales")) Then Exit Sub
        For Each row As DataRow In xSet.Tables("itemprod").Rows
            'xSet.Tables("setSalesSwap").Rows.Add(drow.Item("idBrg"),
            '                                          drow.Item("namaBrg"),
            '                                          drow.Item("jmlh"),
            '                                          drow.Item("hrg"),
            '                                          drow.Item("hrgpcs"),
            '                                          drow.Item("disc"))
            'If isproduklain Then
            '    If sisa5000 > 0 Then
            '        If row("jmlh") = 3 Then
            '            Diskonnua = Int(5000 / 3)
            '            itemke3 = 0
            '        ElseIf row("jmlh") < 3 Then
            '            Diskonnua = row("hrg")
            '            itemke3 = itemke3 - row("jmlh")
            '            temphrg += (row("hrg") * row("jmlh"))
            '            If itemke3 = 0 Then
            '                If temphrg >= 5000 Then
            '                    Diskonnua = row("hrg")
            '                    discountx = 100
            '                Else
            '                    Diskonnua = 5000 - temphrg
            '                End If
            '                temphrg = 0
            '            ElseIf itemke3 = 1 Then
            '                If temphrg > 5000 Then
            '                    If row("jmlh") = 2 Then
            '                        Diskonnua = 5000 / 2
            '                    Else
            '                        Diskonnua = temphrg - 5000
            '                    End If
            '                End If
            '            ElseIf itemke3 = 2 Then
            '                If temphrg > 5000 Then
            '                    Diskonnua = temphrg - 5000
            '                Else
            '                    Diskonnua = row("hrg")
            '                End If
            '            End If
            '        End If


            '    End If
            If row("Jmlh") > 0 Then
                If sisa5000 > 0 Then
                    discountx = 0
                    Diskonnua = 0
                    discountx = ((row("hrg") - 5000) / row("hrg")) * 100
                    Diskonnua = 5000 * row("jmlh")
                    sisa5000 -= row("jmlh")
                    xSet.Tables("setSales").Rows.Add(row("idBrg"),
                                                    row("namaBrg"), row("jmlh"), Diskonnua, Diskonnua / row("jmlh"), 0)
                    If sisa5000 = 0 Then GoTo nextaja
                End If

                If sisa10000 > 0 And sisa5000 = 0 Then
                    discountx = 0
                    Diskonnua = 0
                    discountx = ((row("hrg") - 10000) / row("hrg")) * 100
                    Diskonnua = 10000 * row("jmlh")
                    sisa10000 -= row("jmlh")
                    xSet.Tables("setSales").Rows.Add(row("idBrg"),
                                                    row("namaBrg"), row("jmlh"), Diskonnua, Diskonnua / row("jmlh"), 0)
                End If
                
            End If
Nextaja:
        Next

        Close()

        AktifPromo = 7
    End Sub


    Private Sub freeprodshowcase_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("itemprod")
        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub freeprodshowcase_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        AktifPromo = 0
        G_ketpromo = ""
        Close()
    End Sub

    Private Sub freeprodshowcase_Load(sender As Object, e As EventArgs) Handles Me.Load
        PanelControl1.Appearance.BackColor = Color.PaleVioletRed
        PanelControl1.Appearance.BackColor2 = Color.PaleVioletRed
        PanelControl1.Appearance.Options.UseBackColor = True

        PanelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        PanelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
    End Sub

    Private Sub tbprodshowcase_Click(sender As Object, e As EventArgs) Handles tbprodshowcase.Click
        isproduklain = False
        isiGrid(False)
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        isproduklain = True
        isiGrid(True)
    End Sub
End Class