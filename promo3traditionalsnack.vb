﻿Public Class promo3traditionalsnack
    Private Sub promo3traditionalsnack_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If IsNothing(xSet.Tables("listtsdiskon")) Then
                xSet.Tables.Add("listtsdiskon")
                xSet.Tables("listtsdiskon").Columns.Add("idBrg")
                xSet.Tables("listtsdiskon").Columns.Add("namaBrg")
                xSet.Tables("listtsdiskon").Columns.Add("namaKategori2")
                xSet.Tables("listtsdiskon").Columns.Add("jmlh")
            End If
            SQLquery = "SELECT
  mb.idBrg,
  mb.namaBrg,
  mk.namaKategori2,
  0 `jmlh`
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.Inactive='0' AND mk.idKategori2='11'"
            xSet.Tables("listtsdiskon").Clear()
            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listtsdiskon")
            GridControl1.DataSource = xSet.Tables("listtsdiskon")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        Try
            xSet.Tables("listtsdiskon").Clear()
            deletepromo(True, 0)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Dim letakrowpromo As Integer
    Private Sub deletepromo(ByVal isdeletedpromo As Boolean, ByVal jumlahsebelumdel As Integer)
        Dim data As New DataTable
        data = xSet.Tables("setSales").Copy


        For Each dtrow In data.Rows
            'Dim a As Integer = FormPOSx.GridView1.RowCount
            'Dim b As String = FormPOSx.GridView1.GetRowCellValue(i, "namaBrg")
            If dtrow("namaBrg") = "Promo Kalender Juli 2017" And isdeletedpromo = True Then
                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    xSet.Tables("setsales").Rows.Remove(mdtrow_1)
                Next
            End If
            If dtrow("namaBrg") = "Promo Kalender Juli 2017" And isdeletedpromo = False Then

                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    mdtrow_1("jmlh") = jumlahsebelumdel
                Next
            End If
            Dim kategori As String = ExDb.Scalar("SELECT
  mk.idKategori2
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.idBrg='" & dtrow("idBrg") & "'", My.Settings.localconn)
            If kategori = "11" And dtrow("hrgpcs") = 0 Then
                For Each mdtrow_1 As DataRow In xSet.Tables("setsales").Select("idbrg=" & dtrow("idBrg"))
                    xSet.Tables("setSales").Rows.Remove(mdtrow_1)
                Next

            End If
        Next
        data.Clear()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Try
            Dim jumlahpotongan As Integer = qttInteger

            deletepromo(False, jumlahpotongan)
            Dim countsoes As Integer = 0
            Dim selectedsoes() As DataRow = xSet.Tables("listtsdiskon").Select("jmlh>0")

            For Each soes In selectedsoes
                countsoes = countsoes + soes.Item("jmlh")

            Next soes

            If countsoes > 15 * jumlahpotongan Then
                MessageBox.Show("Jumlah item tidak sesuai ketentuan kupon kalender", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            If countsoes Mod 3 <> 0 Then
                MessageBox.Show("Jumlah item harus kelipatan 3", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            For Each soes In selectedsoes
                xSet.Tables("setSales").Rows.Add(soes.Item("idBrg"),
                                                    soes.Item("namaBrg"),
                                                    soes.Item("jmlh"),
                                                    soes.Item("jmlh") * 0,
                                                    0,
                                                    0)
            Next soes

            Dim hargaakhir As Double = (countsoes / 3) * 10000
            For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                If xSet.Tables("setSales").Rows(i).Item("namaBrg").ToString = "Promo Kalender Juli 2017" Then
                    xSet.Tables("setSales").Rows(i).Item("hrg") = hargaakhir
                    xSet.Tables("setSales").Rows(i).Item("hrgpcs") = hargaakhir / jumlahpotongan
                    Exit For
                End If
            Next i
            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class