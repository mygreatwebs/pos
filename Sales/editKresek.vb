﻿Public Class editKresek
    Dim DiskonRP As Boolean

    Private Sub editKresek_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        textName.Text = selected_row.Item("namaBrg")
        textQtt.Text = CInt(selected_row.Item("jmlh"))
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        xSet.Tables("setkresbox").Rows(selected_idxRow).Item("jmlh") = CInt(textQtt.Text)
        selected_idxRow = 0
        selected_row = Nothing
        Close()
    End Sub

    Private Sub butDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butDelete.Click
        xSet.Tables("setkresbox").Rows.Remove(selected_row)
        Close()
    End Sub

    Private Sub textQtt_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles textQtt.Enter
        virNumpad.ShowDialog()
        textQtt.Text = qttInteger
    End Sub

End Class