﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmboxkresek
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmboxkresek))
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.butDiskon = New DevExpress.XtraEditors.SimpleButton()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.pageText = New DevExpress.XtraEditors.TextEdit()
        Me.nextButton = New DevExpress.XtraEditors.SimpleButton()
        Me.prevButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Level1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Level2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu16 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu15 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu14 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu13 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu12 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu11 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu10 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu9 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu8 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu7 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu6 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu5 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu4 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu3 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pageText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Controls.Add(Me.butDiskon)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 491)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(825, 87)
        Me.PanelControl1.TabIndex = 0
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Appearance.Options.UseTextOptions = True
        Me.SimpleButton1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.SimpleButton1.Location = New System.Drawing.Point(485, 14)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(155, 61)
        Me.SimpleButton1.TabIndex = 49
        Me.SimpleButton1.Text = "Tanpa Kresek   /   Box"
        Me.SimpleButton1.Visible = False
        '
        'butDiskon
        '
        Me.butDiskon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butDiskon.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDiskon.Appearance.Options.UseFont = True
        Me.butDiskon.Image = CType(resources.GetObject("butDiskon.Image"), System.Drawing.Image)
        Me.butDiskon.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDiskon.Location = New System.Drawing.Point(671, 14)
        Me.butDiskon.Name = "butDiskon"
        Me.butDiskon.Size = New System.Drawing.Size(104, 61)
        Me.butDiskon.TabIndex = 2
        Me.butDiskon.Text = "Simpan"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Size = New System.Drawing.Size(660, 404)
        Me.ShapeContainer1.TabIndex = 4
        Me.ShapeContainer1.TabStop = False
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Controls.Add(Me.pageText)
        Me.PanelControl2.Controls.Add(Me.nextButton)
        Me.PanelControl2.Controls.Add(Me.prevButton)
        Me.PanelControl2.Controls.Add(Me.Level1)
        Me.PanelControl2.Controls.Add(Me.Level2)
        Me.PanelControl2.Controls.Add(Me.Menu16)
        Me.PanelControl2.Controls.Add(Me.Menu15)
        Me.PanelControl2.Controls.Add(Me.Menu14)
        Me.PanelControl2.Controls.Add(Me.Menu13)
        Me.PanelControl2.Controls.Add(Me.Menu12)
        Me.PanelControl2.Controls.Add(Me.Menu11)
        Me.PanelControl2.Controls.Add(Me.Menu10)
        Me.PanelControl2.Controls.Add(Me.Menu9)
        Me.PanelControl2.Controls.Add(Me.Menu8)
        Me.PanelControl2.Controls.Add(Me.Menu7)
        Me.PanelControl2.Controls.Add(Me.Menu6)
        Me.PanelControl2.Controls.Add(Me.Menu5)
        Me.PanelControl2.Controls.Add(Me.Menu4)
        Me.PanelControl2.Controls.Add(Me.Menu3)
        Me.PanelControl2.Controls.Add(Me.Menu2)
        Me.PanelControl2.Controls.Add(Me.Menu1)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(825, 491)
        Me.PanelControl2.TabIndex = 1
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(13, 13)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(300, 424)
        Me.GridControl1.TabIndex = 48
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsFind.AllowFindPanel = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'pageText
        '
        Me.pageText.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pageText.EditValue = "1"
        Me.pageText.Location = New System.Drawing.Point(713, 454)
        Me.pageText.Name = "pageText"
        Me.pageText.Properties.Appearance.Options.UseTextOptions = True
        Me.pageText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.pageText.Properties.Mask.EditMask = "n0"
        Me.pageText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.pageText.Properties.MaxLength = 2
        Me.pageText.Size = New System.Drawing.Size(43, 20)
        Me.pageText.TabIndex = 47
        '
        'nextButton
        '
        Me.nextButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nextButton.Location = New System.Drawing.Point(760, 450)
        Me.nextButton.Name = "nextButton"
        Me.nextButton.Size = New System.Drawing.Size(53, 27)
        Me.nextButton.TabIndex = 46
        Me.nextButton.Text = "Next"
        '
        'prevButton
        '
        Me.prevButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prevButton.Location = New System.Drawing.Point(656, 450)
        Me.prevButton.Name = "prevButton"
        Me.prevButton.Size = New System.Drawing.Size(53, 27)
        Me.prevButton.TabIndex = 45
        Me.prevButton.Text = "Prev"
        '
        'Level1
        '
        Me.Level1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Level1.Appearance.Options.UseFont = True
        Me.Level1.Location = New System.Drawing.Point(569, 12)
        Me.Level1.Name = "Level1"
        Me.Level1.Size = New System.Drawing.Size(244, 61)
        Me.Level1.TabIndex = 43
        Me.Level1.Text = "Box"
        '
        'Level2
        '
        Me.Level2.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Level2.Appearance.Options.UseFont = True
        Me.Level2.Location = New System.Drawing.Point(320, 12)
        Me.Level2.Name = "Level2"
        Me.Level2.Size = New System.Drawing.Size(244, 61)
        Me.Level2.TabIndex = 44
        Me.Level2.Text = "Kresek"
        '
        'Menu16
        '
        Me.Menu16.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu16.Appearance.Options.UseFont = True
        Me.Menu16.Appearance.Options.UseTextOptions = True
        Me.Menu16.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu16.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu16.Location = New System.Drawing.Point(680, 358)
        Me.Menu16.Name = "Menu16"
        Me.Menu16.Size = New System.Drawing.Size(104, 84)
        Me.Menu16.TabIndex = 42
        Me.Menu16.Visible = False
        '
        'Menu15
        '
        Me.Menu15.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu15.Appearance.Options.UseFont = True
        Me.Menu15.Appearance.Options.UseTextOptions = True
        Me.Menu15.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu15.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu15.Location = New System.Drawing.Point(570, 358)
        Me.Menu15.Name = "Menu15"
        Me.Menu15.Size = New System.Drawing.Size(104, 84)
        Me.Menu15.TabIndex = 41
        Me.Menu15.Visible = False
        '
        'Menu14
        '
        Me.Menu14.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu14.Appearance.Options.UseFont = True
        Me.Menu14.Appearance.Options.UseTextOptions = True
        Me.Menu14.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu14.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu14.Location = New System.Drawing.Point(460, 359)
        Me.Menu14.Name = "Menu14"
        Me.Menu14.Size = New System.Drawing.Size(104, 84)
        Me.Menu14.TabIndex = 40
        Me.Menu14.Visible = False
        '
        'Menu13
        '
        Me.Menu13.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu13.Appearance.Options.UseFont = True
        Me.Menu13.Appearance.Options.UseTextOptions = True
        Me.Menu13.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu13.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu13.Location = New System.Drawing.Point(350, 360)
        Me.Menu13.Name = "Menu13"
        Me.Menu13.Size = New System.Drawing.Size(104, 84)
        Me.Menu13.TabIndex = 39
        Me.Menu13.Visible = False
        '
        'Menu12
        '
        Me.Menu12.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu12.Appearance.Options.UseFont = True
        Me.Menu12.Appearance.Options.UseTextOptions = True
        Me.Menu12.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu12.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu12.Location = New System.Drawing.Point(680, 267)
        Me.Menu12.Name = "Menu12"
        Me.Menu12.Size = New System.Drawing.Size(104, 84)
        Me.Menu12.TabIndex = 38
        Me.Menu12.Visible = False
        '
        'Menu11
        '
        Me.Menu11.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu11.Appearance.Options.UseFont = True
        Me.Menu11.Appearance.Options.UseTextOptions = True
        Me.Menu11.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu11.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu11.Location = New System.Drawing.Point(570, 267)
        Me.Menu11.Name = "Menu11"
        Me.Menu11.Size = New System.Drawing.Size(104, 84)
        Me.Menu11.TabIndex = 37
        Me.Menu11.Visible = False
        '
        'Menu10
        '
        Me.Menu10.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu10.Appearance.Options.UseFont = True
        Me.Menu10.Appearance.Options.UseTextOptions = True
        Me.Menu10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu10.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu10.Location = New System.Drawing.Point(460, 267)
        Me.Menu10.Name = "Menu10"
        Me.Menu10.Size = New System.Drawing.Size(104, 84)
        Me.Menu10.TabIndex = 36
        Me.Menu10.Visible = False
        '
        'Menu9
        '
        Me.Menu9.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu9.Appearance.Options.UseFont = True
        Me.Menu9.Appearance.Options.UseTextOptions = True
        Me.Menu9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu9.Location = New System.Drawing.Point(350, 267)
        Me.Menu9.Name = "Menu9"
        Me.Menu9.Size = New System.Drawing.Size(104, 84)
        Me.Menu9.TabIndex = 35
        Me.Menu9.Visible = False
        '
        'Menu8
        '
        Me.Menu8.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu8.Appearance.Options.UseFont = True
        Me.Menu8.Appearance.Options.UseTextOptions = True
        Me.Menu8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu8.Location = New System.Drawing.Point(680, 177)
        Me.Menu8.Name = "Menu8"
        Me.Menu8.Size = New System.Drawing.Size(104, 84)
        Me.Menu8.TabIndex = 34
        Me.Menu8.Visible = False
        '
        'Menu7
        '
        Me.Menu7.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu7.Appearance.Options.UseFont = True
        Me.Menu7.Appearance.Options.UseTextOptions = True
        Me.Menu7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu7.Location = New System.Drawing.Point(570, 177)
        Me.Menu7.Name = "Menu7"
        Me.Menu7.Size = New System.Drawing.Size(104, 84)
        Me.Menu7.TabIndex = 33
        Me.Menu7.Visible = False
        '
        'Menu6
        '
        Me.Menu6.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu6.Appearance.Options.UseFont = True
        Me.Menu6.Appearance.Options.UseTextOptions = True
        Me.Menu6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu6.Location = New System.Drawing.Point(460, 177)
        Me.Menu6.Name = "Menu6"
        Me.Menu6.Size = New System.Drawing.Size(104, 84)
        Me.Menu6.TabIndex = 32
        Me.Menu6.Visible = False
        '
        'Menu5
        '
        Me.Menu5.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu5.Appearance.Options.UseFont = True
        Me.Menu5.Appearance.Options.UseTextOptions = True
        Me.Menu5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu5.Location = New System.Drawing.Point(350, 177)
        Me.Menu5.Name = "Menu5"
        Me.Menu5.Size = New System.Drawing.Size(104, 84)
        Me.Menu5.TabIndex = 31
        Me.Menu5.Visible = False
        '
        'Menu4
        '
        Me.Menu4.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu4.Appearance.Options.UseFont = True
        Me.Menu4.Appearance.Options.UseTextOptions = True
        Me.Menu4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu4.Location = New System.Drawing.Point(680, 86)
        Me.Menu4.Name = "Menu4"
        Me.Menu4.Size = New System.Drawing.Size(104, 84)
        Me.Menu4.TabIndex = 30
        Me.Menu4.Visible = False
        '
        'Menu3
        '
        Me.Menu3.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu3.Appearance.Options.UseFont = True
        Me.Menu3.Appearance.Options.UseTextOptions = True
        Me.Menu3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu3.Location = New System.Drawing.Point(570, 86)
        Me.Menu3.Name = "Menu3"
        Me.Menu3.Size = New System.Drawing.Size(104, 84)
        Me.Menu3.TabIndex = 29
        Me.Menu3.Visible = False
        '
        'Menu2
        '
        Me.Menu2.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu2.Appearance.Options.UseFont = True
        Me.Menu2.Appearance.Options.UseTextOptions = True
        Me.Menu2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu2.Location = New System.Drawing.Point(460, 86)
        Me.Menu2.Name = "Menu2"
        Me.Menu2.Size = New System.Drawing.Size(104, 84)
        Me.Menu2.TabIndex = 28
        Me.Menu2.Visible = False
        '
        'Menu1
        '
        Me.Menu1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu1.Appearance.Options.UseFont = True
        Me.Menu1.Appearance.Options.UseTextOptions = True
        Me.Menu1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.Menu1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu1.Location = New System.Drawing.Point(350, 86)
        Me.Menu1.Name = "Menu1"
        Me.Menu1.Size = New System.Drawing.Size(104, 84)
        Me.Menu1.TabIndex = 27
        Me.Menu1.Visible = False
        '
        'frmboxkresek
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(825, 578)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmboxkresek"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pageText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butDiskon As DevExpress.XtraEditors.SimpleButton
    Private WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents pageText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nextButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents prevButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Level1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Level2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu12 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
