﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class printNota
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butPrint = New DevExpress.XtraEditors.SimpleButton()
        Me.butTutup = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.textKembalian = New DevExpress.XtraEditors.TextEdit()
        Me.textBayar = New DevExpress.XtraEditors.TextEdit()
        Me.textTotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.textKembalian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textBayar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'butPrint
        '
        Me.butPrint.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPrint.Appearance.Options.UseFont = True
        Me.butPrint.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Document_Print_32
        Me.butPrint.Location = New System.Drawing.Point(259, 269)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(101, 54)
        Me.butPrint.TabIndex = 7
        Me.butPrint.Text = "Print"
        '
        'butTutup
        '
        Me.butTutup.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butTutup.Appearance.Options.UseFont = True
        Me.butTutup.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butTutup.Location = New System.Drawing.Point(12, 269)
        Me.butTutup.Name = "butTutup"
        Me.butTutup.Size = New System.Drawing.Size(101, 54)
        Me.butTutup.TabIndex = 8
        Me.butTutup.Text = "Tutup"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.textKembalian)
        Me.PanelControl1.Controls.Add(Me.textBayar)
        Me.PanelControl1.Controls.Add(Me.textTotal)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.ShapeContainer1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 122)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(348, 141)
        Me.PanelControl1.TabIndex = 9
        '
        'textKembalian
        '
        Me.textKembalian.EditValue = "999999"
        Me.textKembalian.Location = New System.Drawing.Point(154, 91)
        Me.textKembalian.Name = "textKembalian"
        Me.textKembalian.Properties.AllowFocused = False
        Me.textKembalian.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.textKembalian.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!)
        Me.textKembalian.Properties.Appearance.Options.UseBackColor = True
        Me.textKembalian.Properties.Appearance.Options.UseFont = True
        Me.textKembalian.Properties.Appearance.Options.UseTextOptions = True
        Me.textKembalian.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textKembalian.Properties.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent
        Me.textKembalian.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.textKembalian.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.textKembalian.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.textKembalian.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.textKembalian.Properties.Mask.EditMask = "n0"
        Me.textKembalian.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textKembalian.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textKembalian.Properties.ReadOnly = True
        Me.textKembalian.Size = New System.Drawing.Size(178, 34)
        Me.textKembalian.TabIndex = 5
        '
        'textBayar
        '
        Me.textBayar.EditValue = "999999"
        Me.textBayar.Location = New System.Drawing.Point(154, 41)
        Me.textBayar.Name = "textBayar"
        Me.textBayar.Properties.AllowFocused = False
        Me.textBayar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.textBayar.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!)
        Me.textBayar.Properties.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.textBayar.Properties.Appearance.Options.UseBackColor = True
        Me.textBayar.Properties.Appearance.Options.UseFont = True
        Me.textBayar.Properties.Appearance.Options.UseForeColor = True
        Me.textBayar.Properties.Appearance.Options.UseTextOptions = True
        Me.textBayar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textBayar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.textBayar.Properties.Mask.EditMask = "n0"
        Me.textBayar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textBayar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textBayar.Properties.ReadOnly = True
        Me.textBayar.Size = New System.Drawing.Size(178, 34)
        Me.textBayar.TabIndex = 4
        '
        'textTotal
        '
        Me.textTotal.EditValue = "999999"
        Me.textTotal.Location = New System.Drawing.Point(154, 5)
        Me.textTotal.Name = "textTotal"
        Me.textTotal.Properties.AllowFocused = False
        Me.textTotal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.textTotal.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!)
        Me.textTotal.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.textTotal.Properties.Appearance.Options.UseBackColor = True
        Me.textTotal.Properties.Appearance.Options.UseFont = True
        Me.textTotal.Properties.Appearance.Options.UseForeColor = True
        Me.textTotal.Properties.Appearance.Options.UseTextOptions = True
        Me.textTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textTotal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.textTotal.Properties.Mask.EditMask = "n0"
        Me.textTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textTotal.Properties.ReadOnly = True
        Me.textTotal.Size = New System.Drawing.Size(178, 34)
        Me.textTotal.TabIndex = 3
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(5, 93)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(143, 29)
        Me.LabelControl4.TabIndex = 2
        Me.LabelControl4.Text = "Kembalian :"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Appearance.ForeColor = System.Drawing.Color.Maroon
        Me.LabelControl3.Location = New System.Drawing.Point(61, 43)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(87, 29)
        Me.LabelControl3.TabIndex = 1
        Me.LabelControl3.Text = "Bayar :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LabelControl2.Location = New System.Drawing.Point(70, 8)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 29)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "Total :"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(2, 2)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(344, 137)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 154
        Me.LineShape1.X2 = 332
        Me.LineShape1.Y1 = 82
        Me.LineShape1.Y2 = 82
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 36.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Green
        Me.LabelControl1.Location = New System.Drawing.Point(61, 5)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(232, 59)
        Me.LabelControl1.TabIndex = 10
        Me.LabelControl1.Text = "L U N A S"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl5)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(348, 104)
        Me.PanelControl2.TabIndex = 11
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 16.0!)
        Me.LabelControl5.Location = New System.Drawing.Point(93, 70)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(165, 25)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "SO/12/09/9999"
        '
        'printNota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 335)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.butPrint)
        Me.Controls.Add(Me.butTutup)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "printNota"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.textKembalian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textBayar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents butPrint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butTutup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents textKembalian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textBayar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
End Class
