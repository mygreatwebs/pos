﻿Public Class inputDisc 

    Private Sub inputDisc_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        TextEdit2.Text = totDisc
        TextEdit1.Text = TotalDisc
    End Sub

    Private Sub TextEdit2_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles TextEdit2.Enter
        virNumpad.ShowDialog()
        If qttInteger > 99 Then
            TextEdit2.Text = 100
            TextEdit1.EditValue = FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
        Else
            TextEdit2.Text = qttInteger
            TextEdit1.EditValue = qttInteger / 100 * FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
        End If

        FormPOSx.DiskonPersen = True

    End Sub

    Private Sub but10_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but10.Click
        TextEdit2.Text = 10
        TextEdit1.EditValue = 10 / 100 * FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
        FormPOSx.DiskonPersen = True
    End Sub

    Private Sub but50_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but50.Click
        TextEdit2.Text = 50
        TextEdit1.EditValue = 50 / 100 * FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
        FormPOSx.DiskonPersen = True
    End Sub

    Private Sub but100_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but100.Click
        TextEdit2.Text = 100
        TextEdit1.EditValue = FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
        FormPOSx.DiskonPersen = True
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        totDisc = TextEdit2.Text
        TotalDisc = TextEdit1.Text
        Close()
    End Sub

    Private Sub TextEdit1_Enter(sender As Object, e As EventArgs) Handles TextEdit1.Enter
        virNumpad.ShowDialog()
        If qttInteger >= FormPOSx.TotalMkn Then
            TextEdit1.Text = FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue
            TextEdit2.Text = 100
        Else
            TextEdit1.Text = qttInteger
            TextEdit2.Text = qttInteger / FormPOSx.GridView1.Columns("hrg").SummaryItem.SummaryValue * 100
        End If

        FormPOSx.DiskonPersen = False

    End Sub

   
    Private Sub TextEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles TextEdit1.EditValueChanged

    End Sub
End Class