﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class nilaivoucher
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.textnilaivoucher = New DevExpress.XtraEditors.TextEdit()
        Me.butBayar = New DevExpress.XtraEditors.SimpleButton()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.textnilaivoucher.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'textnilaivoucher
        '
        Me.textnilaivoucher.EditValue = "0"
        Me.textnilaivoucher.Location = New System.Drawing.Point(77, 55)
        Me.textnilaivoucher.Name = "textnilaivoucher"
        Me.textnilaivoucher.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textnilaivoucher.Properties.Appearance.Options.UseFont = True
        Me.textnilaivoucher.Properties.Appearance.Options.UseTextOptions = True
        Me.textnilaivoucher.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textnilaivoucher.Properties.Mask.EditMask = "n0"
        Me.textnilaivoucher.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textnilaivoucher.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textnilaivoucher.Size = New System.Drawing.Size(198, 24)
        Me.textnilaivoucher.TabIndex = 13
        '
        'butBayar
        '
        Me.butBayar.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBayar.Appearance.Options.UseFont = True
        Me.butBayar.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butBayar.Location = New System.Drawing.Point(59, 101)
        Me.butBayar.Name = "butBayar"
        Me.butBayar.Size = New System.Drawing.Size(101, 54)
        Me.butBayar.TabIndex = 14
        Me.butBayar.Text = "Ok"
        '
        'butBatal
        '
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butBatal.Location = New System.Drawing.Point(190, 101)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(101, 54)
        Me.butBatal.TabIndex = 15
        Me.butBatal.Text = "Batal"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(101, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(159, 18)
        Me.LabelControl4.TabIndex = 16
        Me.LabelControl4.Text = "Jumlah Roti Gratis"
        '
        'nilaivoucher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(378, 187)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.butBatal)
        Me.Controls.Add(Me.butBayar)
        Me.Controls.Add(Me.textnilaivoucher)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "nilaivoucher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nilai Voucher"
        CType(Me.textnilaivoucher.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents textnilaivoucher As DevExpress.XtraEditors.TextEdit
    Friend WithEvents butBayar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
End Class
