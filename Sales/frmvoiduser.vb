﻿Public Class frmvoiduser

    Private Sub butLogin_Click(sender As Object, e As EventArgs) Handles butLogin.Click
        If IsNothing(xSet.Tables("cekuser")) = False Then xSet.Tables("cekuser").Clear()
        SQLquery = String.Format("SELECT idStaff, Nama, a.idSecurityGroup, IFNULL(baca,0) AS baca, a.idCabang,namaCabang FROM mstaff a " _
                                                                     & "LEFT JOIN msecuritygroup b ON a.idSecurityGroup=b.idSecurityGroup " _
                                                                     & "LEFT JOIN progpermissions c ON a.idSecurityGroup=c.idSecurityGroup AND idform=31 left join mcabang d on a.idCabang=d.idCabang " _
                                                                     & "WHERE username='{0}' AND password=AES_ENCRYPT('{1}', '{0}')",
                                                                     TextEdit1.Text, TextEdit2.Text)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "cekuser")
        If xSet.Tables("cekuser").Rows.Count > 0 Then
            'SECURITY PERMISSIONS!
            If Not xSet.Tables("getLoginPermissions") Is Nothing Then xSet.Tables("getLoginPermissions").Clear()
            SQLquery = "SELECT namaform, IFNULL( baca, FALSE ) AS Baca, IFNULL( tambah, FALSE ) AS Tambah, " & _
                        "IFNULL( edit, FALSE ) AS Edit, IFNULL( batal, FALSE ) AS Batal, IFNULL( cetak, FALSE ) AS Cetak " & _
                        "FROM mform a LEFT JOIN progpermissions b ON a.idform = b.idform AND b.idsecuritygroup =" & xSet.Tables("cekuser").Rows(0)("idSecurityGroup")
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getLoginPermissions")
            userVoid = xSet.Tables("cekuser").Rows(0)("idSecurityGroup")
            isUserCanVoid = xSet.Tables("getLoginPermissions").Select("namaform='POS'")(0).Item("Batal")
            If xSet.Tables("cekuser").Rows(0)("idSecurityGroup") = 1 Then isUserCanVoid = True
            If isUserCanVoid = False Then
                MsgBox("Ups ! User tidak mempunyai hak untuk Void !")
            End If
            Close()
        Else
            MsgBox("Ups ! User/Password Salah !")
        End If
    End Sub

    Private Sub frmvoiduser_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("cekuser")
        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub frmvoiduser_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub butClose_Click(sender As Object, e As EventArgs) Handles butClose.Click
        userVoid = 0
        isUserCanVoid = 0
        Close()
    End Sub
End Class