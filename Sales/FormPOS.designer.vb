﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPOSx
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormPOSx))
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.ImageCollection2 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.page1 = New DevExpress.XtraTab.XtraTabPage()
        Me.page2 = New DevExpress.XtraTab.XtraTabPage()
        Me.page3 = New DevExpress.XtraTab.XtraTabPage()
        Me.ImageCollection1 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.pageText = New DevExpress.XtraEditors.TextEdit()
        Me.nextButton = New DevExpress.XtraEditors.SimpleButton()
        Me.prevButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu9 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu8 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu7 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu6 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu5 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu4 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu3 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Menu1 = New DevExpress.XtraEditors.SimpleButton()
        Me.backButton = New DevExpress.XtraEditors.SimpleButton()
        Me.Level1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Level2 = New DevExpress.XtraEditors.SimpleButton()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.txtDiscPercent = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtTotalBeli = New DevExpress.XtraEditors.TextEdit()
        Me.txtDisc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.check_saldo = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.swap = New DevExpress.XtraEditors.SimpleButton()
        Me.butBayar = New DevExpress.XtraEditors.SimpleButton()
        Me.butClear = New DevExpress.XtraEditors.SimpleButton()
        Me.butDiskon = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.ConnStatus = New DevExpress.XtraBars.BarStaticItem()
        Me.BarStaticItem2 = New DevExpress.XtraBars.BarStaticItem()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        CType(Me.ImageCollection2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageCollection1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.pageText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txtDiscPercent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalBeli.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDisc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(16, 15)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(300, 468)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsFind.AllowFindPanel = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.XtraTabControl1.Images = Me.ImageCollection2
        Me.XtraTabControl1.Location = New System.Drawing.Point(324, 16)
        Me.XtraTabControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.[False]
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.page1
        Me.XtraTabControl1.Size = New System.Drawing.Size(822, 473)
        Me.XtraTabControl1.TabIndex = 1
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.page1, Me.page2, Me.page3})
        '
        'ImageCollection2
        '
        Me.ImageCollection2.ImageSize = New System.Drawing.Size(48, 48)
        Me.ImageCollection2.ImageStream = CType(resources.GetObject("ImageCollection2.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ImageCollection2.Images.SetKeyName(0, "beverage.png")
        Me.ImageCollection2.Images.SetKeyName(1, "breads.png")
        Me.ImageCollection2.Images.SetKeyName(2, "other.png")
        '
        'page1
        '
        Me.page1.ImageIndex = 1
        Me.page1.Margin = New System.Windows.Forms.Padding(4)
        Me.page1.Name = "page1"
        Me.page1.Size = New System.Drawing.Size(814, 408)
        Me.page1.Text = "Food, Cake && Snacks"
        '
        'page2
        '
        Me.page2.ImageIndex = 0
        Me.page2.Margin = New System.Windows.Forms.Padding(4)
        Me.page2.Name = "page2"
        Me.page2.Size = New System.Drawing.Size(814, 408)
        Me.page2.Text = "Beverages && Drinks"
        '
        'page3
        '
        Me.page3.ImageIndex = 2
        Me.page3.Margin = New System.Windows.Forms.Padding(4)
        Me.page3.Name = "page3"
        Me.page3.Size = New System.Drawing.Size(814, 408)
        Me.page3.Text = "Other Products"
        '
        'ImageCollection1
        '
        Me.ImageCollection1.ImageSize = New System.Drawing.Size(64, 64)
        Me.ImageCollection1.ImageStream = CType(resources.GetObject("ImageCollection1.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        '
        'PanelControl2
        '
        Me.PanelControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl2.Controls.Add(Me.pageText)
        Me.PanelControl2.Controls.Add(Me.nextButton)
        Me.PanelControl2.Controls.Add(Me.prevButton)
        Me.PanelControl2.Controls.Add(Me.Menu9)
        Me.PanelControl2.Controls.Add(Me.Menu8)
        Me.PanelControl2.Controls.Add(Me.Menu7)
        Me.PanelControl2.Controls.Add(Me.Menu6)
        Me.PanelControl2.Controls.Add(Me.Menu5)
        Me.PanelControl2.Controls.Add(Me.Menu4)
        Me.PanelControl2.Controls.Add(Me.Menu3)
        Me.PanelControl2.Controls.Add(Me.Menu2)
        Me.PanelControl2.Controls.Add(Me.Menu1)
        Me.PanelControl2.Controls.Add(Me.backButton)
        Me.PanelControl2.Controls.Add(Me.Level1)
        Me.PanelControl2.Controls.Add(Me.Level2)
        Me.PanelControl2.Controls.Add(Me.ShapeContainer1)
        Me.PanelControl2.Location = New System.Drawing.Point(340, 76)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(797, 404)
        Me.PanelControl2.TabIndex = 5
        '
        'pageText
        '
        Me.pageText.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pageText.EditValue = "1"
        Me.pageText.Location = New System.Drawing.Point(687, 374)
        Me.pageText.Name = "pageText"
        Me.pageText.Properties.Appearance.Options.UseTextOptions = True
        Me.pageText.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.pageText.Properties.Mask.EditMask = "n0"
        Me.pageText.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.pageText.Properties.MaxLength = 2
        Me.pageText.Size = New System.Drawing.Size(43, 20)
        Me.pageText.TabIndex = 16
        '
        'nextButton
        '
        Me.nextButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.nextButton.Location = New System.Drawing.Point(736, 370)
        Me.nextButton.Name = "nextButton"
        Me.nextButton.Size = New System.Drawing.Size(53, 27)
        Me.nextButton.TabIndex = 15
        Me.nextButton.Text = "Next"
        '
        'prevButton
        '
        Me.prevButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prevButton.Location = New System.Drawing.Point(628, 370)
        Me.prevButton.Name = "prevButton"
        Me.prevButton.Size = New System.Drawing.Size(53, 27)
        Me.prevButton.TabIndex = 14
        Me.prevButton.Text = "Prev"
        '
        'Menu9
        '
        Me.Menu9.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu9.Appearance.Options.UseFont = True
        Me.Menu9.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu9.Location = New System.Drawing.Point(377, 280)
        Me.Menu9.Name = "Menu9"
        Me.Menu9.Size = New System.Drawing.Size(176, 84)
        Me.Menu9.TabIndex = 13
        Me.Menu9.Text = "9"
        '
        'Menu8
        '
        Me.Menu8.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu8.Appearance.Options.UseFont = True
        Me.Menu8.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu8.Location = New System.Drawing.Point(195, 280)
        Me.Menu8.Name = "Menu8"
        Me.Menu8.Size = New System.Drawing.Size(176, 84)
        Me.Menu8.TabIndex = 12
        Me.Menu8.Text = "8"
        '
        'Menu7
        '
        Me.Menu7.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu7.Appearance.Options.UseFont = True
        Me.Menu7.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu7.Location = New System.Drawing.Point(13, 279)
        Me.Menu7.Name = "Menu7"
        Me.Menu7.Size = New System.Drawing.Size(176, 84)
        Me.Menu7.TabIndex = 11
        Me.Menu7.Text = "7"
        '
        'Menu6
        '
        Me.Menu6.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu6.Appearance.Options.UseFont = True
        Me.Menu6.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu6.Location = New System.Drawing.Point(377, 190)
        Me.Menu6.Name = "Menu6"
        Me.Menu6.Size = New System.Drawing.Size(176, 84)
        Me.Menu6.TabIndex = 10
        Me.Menu6.Text = "6"
        '
        'Menu5
        '
        Me.Menu5.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu5.Appearance.Options.UseFont = True
        Me.Menu5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu5.Location = New System.Drawing.Point(195, 190)
        Me.Menu5.Name = "Menu5"
        Me.Menu5.Size = New System.Drawing.Size(176, 84)
        Me.Menu5.TabIndex = 9
        Me.Menu5.Text = "5"
        '
        'Menu4
        '
        Me.Menu4.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu4.Appearance.Options.UseFont = True
        Me.Menu4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu4.Location = New System.Drawing.Point(13, 189)
        Me.Menu4.Name = "Menu4"
        Me.Menu4.Size = New System.Drawing.Size(176, 84)
        Me.Menu4.TabIndex = 8
        Me.Menu4.Text = "4"
        '
        'Menu3
        '
        Me.Menu3.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu3.Appearance.Options.UseFont = True
        Me.Menu3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu3.Location = New System.Drawing.Point(377, 99)
        Me.Menu3.Name = "Menu3"
        Me.Menu3.Size = New System.Drawing.Size(176, 84)
        Me.Menu3.TabIndex = 7
        Me.Menu3.Text = "3"
        '
        'Menu2
        '
        Me.Menu2.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu2.Appearance.Options.UseFont = True
        Me.Menu2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu2.Location = New System.Drawing.Point(195, 99)
        Me.Menu2.Name = "Menu2"
        Me.Menu2.Size = New System.Drawing.Size(176, 84)
        Me.Menu2.TabIndex = 6
        Me.Menu2.Text = "2"
        '
        'Menu1
        '
        Me.Menu1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Menu1.Appearance.Options.UseFont = True
        Me.Menu1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.Menu1.Location = New System.Drawing.Point(13, 99)
        Me.Menu1.Name = "Menu1"
        Me.Menu1.Size = New System.Drawing.Size(176, 84)
        Me.Menu1.TabIndex = 5
        Me.Menu1.Text = "1"
        '
        'backButton
        '
        Me.backButton.Location = New System.Drawing.Point(13, 13)
        Me.backButton.Name = "backButton"
        Me.backButton.Size = New System.Drawing.Size(49, 61)
        Me.backButton.TabIndex = 0
        Me.backButton.Text = "..."
        '
        'Level1
        '
        Me.Level1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Level1.Appearance.Options.UseFont = True
        Me.Level1.Location = New System.Drawing.Point(68, 13)
        Me.Level1.Name = "Level1"
        Me.Level1.Size = New System.Drawing.Size(155, 61)
        Me.Level1.TabIndex = 1
        Me.Level1.Text = "Lv1"
        Me.Level1.Visible = False
        '
        'Level2
        '
        Me.Level2.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Level2.Appearance.Options.UseFont = True
        Me.Level2.Location = New System.Drawing.Point(229, 13)
        Me.Level2.Name = "Level2"
        Me.Level2.Size = New System.Drawing.Size(155, 61)
        Me.Level2.TabIndex = 2
        Me.Level2.Text = "Lv2"
        Me.Level2.Visible = False
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(797, 404)
        Me.ShapeContainer1.TabIndex = 4
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 10
        Me.LineShape1.X2 = 788
        Me.LineShape1.Y1 = 86
        Me.LineShape1.Y2 = 86
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.txtDiscPercent)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.txtTotalBeli)
        Me.GroupControl1.Controls.Add(Me.txtDisc)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(16, 491)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(300, 94)
        Me.GroupControl1.TabIndex = 2
        Me.GroupControl1.Text = "Total  Order"
        '
        'txtDiscPercent
        '
        Me.txtDiscPercent.EditValue = 0
        Me.txtDiscPercent.Location = New System.Drawing.Point(82, 25)
        Me.txtDiscPercent.Name = "txtDiscPercent"
        Me.txtDiscPercent.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscPercent.Properties.Appearance.Options.UseFont = True
        Me.txtDiscPercent.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDiscPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDiscPercent.Properties.Mask.EditMask = "n0"
        Me.txtDiscPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDiscPercent.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDiscPercent.Properties.MaxLength = 3
        Me.txtDiscPercent.Properties.ReadOnly = True
        Me.txtDiscPercent.Size = New System.Drawing.Size(42, 24)
        Me.txtDiscPercent.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(5, 28)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(71, 18)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Disc % :"
        '
        'txtTotalBeli
        '
        Me.txtTotalBeli.EditValue = 0
        Me.txtTotalBeli.Location = New System.Drawing.Point(187, 57)
        Me.txtTotalBeli.Name = "txtTotalBeli"
        Me.txtTotalBeli.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalBeli.Properties.Appearance.Options.UseFont = True
        Me.txtTotalBeli.Properties.Appearance.Options.UseTextOptions = True
        Me.txtTotalBeli.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtTotalBeli.Properties.Mask.EditMask = "n0"
        Me.txtTotalBeli.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtTotalBeli.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtTotalBeli.Properties.ReadOnly = True
        Me.txtTotalBeli.Size = New System.Drawing.Size(100, 24)
        Me.txtTotalBeli.TabIndex = 3
        '
        'txtDisc
        '
        Me.txtDisc.EditValue = 0
        Me.txtDisc.Location = New System.Drawing.Point(187, 25)
        Me.txtDisc.Name = "txtDisc"
        Me.txtDisc.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisc.Properties.Appearance.Options.UseFont = True
        Me.txtDisc.Properties.Appearance.Options.UseTextOptions = True
        Me.txtDisc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtDisc.Properties.Mask.EditMask = "n0"
        Me.txtDisc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtDisc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtDisc.Properties.ReadOnly = True
        Me.txtDisc.Size = New System.Drawing.Size(100, 24)
        Me.txtDisc.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(125, 60)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(56, 18)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Total :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(135, 28)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 18)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Disc :"
        '
        'PanelControl1
        '
        Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl1.Controls.Add(Me.SimpleButton4)
        Me.PanelControl1.Controls.Add(Me.swap)
        Me.PanelControl1.Controls.Add(Me.butBayar)
        Me.PanelControl1.Controls.Add(Me.butClear)
        Me.PanelControl1.Controls.Add(Me.butDiskon)
        Me.PanelControl1.Controls.Add(Me.butClose)
        Me.PanelControl1.Location = New System.Drawing.Point(326, 497)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(815, 88)
        Me.PanelControl1.TabIndex = 3
        '
        'check_saldo
        '
        Me.check_saldo.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.check_saldo.Appearance.ForeColor = System.Drawing.Color.Black
        Me.check_saldo.Appearance.Options.UseFont = True
        Me.check_saldo.Appearance.Options.UseForeColor = True
        Me.check_saldo.Image = CType(resources.GetObject("check_saldo.Image"), System.Drawing.Image)
        Me.check_saldo.Location = New System.Drawing.Point(654, 5)
        Me.check_saldo.Name = "check_saldo"
        Me.check_saldo.Size = New System.Drawing.Size(129, 47)
        Me.check_saldo.TabIndex = 19
        Me.check_saldo.Text = "Check Saldo"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Image = CType(resources.GetObject("SimpleButton4.Image"), System.Drawing.Image)
        Me.SimpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.SimpleButton4.Location = New System.Drawing.Point(302, 5)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(109, 78)
        Me.SimpleButton4.TabIndex = 5
        Me.SimpleButton4.Text = "Promo Online"
        '
        'swap
        '
        Me.swap.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.swap.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.swap.Appearance.Options.UseFont = True
        Me.swap.Image = CType(resources.GetObject("swap.Image"), System.Drawing.Image)
        Me.swap.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.swap.Location = New System.Drawing.Point(417, 5)
        Me.swap.Name = "swap"
        Me.swap.Size = New System.Drawing.Size(88, 78)
        Me.swap.TabIndex = 4
        Me.swap.Text = "Swap"
        '
        'butBayar
        '
        Me.butBayar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butBayar.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBayar.Appearance.Options.UseFont = True
        Me.butBayar.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butBayar.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butBayar.Location = New System.Drawing.Point(669, 5)
        Me.butBayar.Name = "butBayar"
        Me.butBayar.Size = New System.Drawing.Size(134, 78)
        Me.butBayar.TabIndex = 3
        Me.butBayar.Text = "Bayar"
        '
        'butClear
        '
        Me.butClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butClear.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClear.Appearance.Options.UseFont = True
        Me.butClear.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butClear.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butClear.Location = New System.Drawing.Point(590, 5)
        Me.butClear.Name = "butClear"
        Me.butClear.Size = New System.Drawing.Size(73, 78)
        Me.butClear.TabIndex = 2
        Me.butClear.Text = "Clear"
        '
        'butDiskon
        '
        Me.butDiskon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.butDiskon.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDiskon.Appearance.Options.UseFont = True
        Me.butDiskon.Image = CType(resources.GetObject("butDiskon.Image"), System.Drawing.Image)
        Me.butDiskon.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDiskon.Location = New System.Drawing.Point(511, 5)
        Me.butDiskon.Name = "butDiskon"
        Me.butDiskon.Size = New System.Drawing.Size(73, 78)
        Me.butDiskon.TabIndex = 1
        Me.butDiskon.Text = "Diskon"
        '
        'butClose
        '
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Image = Global.Laritta_POS.My.Resources.Resources.left_32
        Me.butClose.Location = New System.Drawing.Point(14, 5)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(132, 78)
        Me.butClose.TabIndex = 0
        Me.butClose.Text = "Kembali"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton3.Appearance.ForeColor = System.Drawing.Color.DarkViolet
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Appearance.Options.UseForeColor = True
        Me.SimpleButton3.Image = CType(resources.GetObject("SimpleButton3.Image"), System.Drawing.Image)
        Me.SimpleButton3.Location = New System.Drawing.Point(969, 5)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(172, 47)
        Me.SimpleButton3.TabIndex = 2
        Me.SimpleButton3.Text = "Script Up-Sell"
        '
        'ConnStatus
        '
        Me.ConnStatus.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.ConnStatus.Caption = "Offline"
        Me.ConnStatus.Enabled = False
        Me.ConnStatus.Id = 42
        Me.ConnStatus.ImageIndex = 1
        Me.ConnStatus.ImageIndexDisabled = 0
        Me.ConnStatus.ItemAppearance.Disabled.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConnStatus.ItemAppearance.Disabled.Options.UseFont = True
        Me.ConnStatus.ItemAppearance.Normal.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConnStatus.ItemAppearance.Normal.Options.UseFont = True
        Me.ConnStatus.Name = "ConnStatus"
        Me.ConnStatus.ShowImageInToolbar = False
        Me.ConnStatus.Size = New System.Drawing.Size(100, 0)
        Me.ConnStatus.TextAlignment = System.Drawing.StringAlignment.Center
        Me.ConnStatus.Width = 100
        '
        'BarStaticItem2
        '
        Me.BarStaticItem2.Glyph = CType(resources.GetObject("BarStaticItem2.Glyph"), System.Drawing.Image)
        Me.BarStaticItem2.Id = 49
        Me.BarStaticItem2.Name = "BarStaticItem2"
        Me.BarStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.check_saldo)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.SimpleButton3)
        Me.PanelControl3.Controls.Add(Me.SimpleButton1)
        Me.PanelControl3.Controls.Add(Me.SimpleButton2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl3.Location = New System.Drawing.Point(0, 586)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1159, 62)
        Me.PanelControl3.TabIndex = 8
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Appearance.ForeColor = System.Drawing.Color.OrangeRed
        Me.LabelControl4.Location = New System.Drawing.Point(353, 26)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(68, 16)
        Me.LabelControl4.TabIndex = 18
        Me.LabelControl4.Text = "dd-MM-yyyy"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.ForeColor = System.Drawing.Color.DodgerBlue
        Me.SimpleButton1.Appearance.Options.UseBackColor = True
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Appearance.Options.UseForeColor = True
        Me.SimpleButton1.Appearance.Options.UseTextOptions = True
        Me.SimpleButton1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SimpleButton1.AutoSize = True
        Me.SimpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.SimpleButton1.Image = CType(resources.GetObject("SimpleButton1.Image"), System.Drawing.Image)
        Me.SimpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.SimpleButton1.Location = New System.Drawing.Point(21, 18)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(244, 32)
        Me.SimpleButton1.TabIndex = 8
        Me.SimpleButton1.Text = "User Cabang Shif"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton2.Appearance.ForeColor = System.Drawing.Color.Red
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Appearance.Options.UseForeColor = True
        Me.SimpleButton2.Image = CType(resources.GetObject("SimpleButton2.Image"), System.Drawing.Image)
        Me.SimpleButton2.Location = New System.Drawing.Point(789, 5)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(174, 47)
        Me.SimpleButton2.TabIndex = 1
        Me.SimpleButton2.Text = "Promo Yang Aktif"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'FormPOSx
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1159, 648)
        Me.ControlBox = false
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.GridControl1)
        Me.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FormPOSx"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GridControl1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GridView1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.XtraTabControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.XtraTabControl1.ResumeLayout(false)
        CType(Me.ImageCollection2,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ImageCollection1,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PanelControl2,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl2.ResumeLayout(false)
        CType(Me.pageText.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.GroupControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupControl1.ResumeLayout(false)
        Me.GroupControl1.PerformLayout
        CType(Me.txtDiscPercent.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtTotalBeli.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtDisc.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PanelControl1,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl1.ResumeLayout(false)
        CType(Me.PanelControl3,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelControl3.ResumeLayout(false)
        Me.PanelControl3.PerformLayout
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents page1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents page2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents page3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ImageCollection1 As DevExpress.Utils.ImageCollection
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents backButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Level1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Level2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents pageText As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nextButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents prevButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Menu1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butBayar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClear As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butDiskon As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtTotalBeli As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDisc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtDiscPercent As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ImageCollection2 As DevExpress.Utils.ImageCollection
    Friend WithEvents swap As DevExpress.XtraEditors.SimpleButton
    Private WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Private WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents ConnStatus As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarStaticItem2 As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Public WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents check_saldo As DevExpress.XtraEditors.SimpleButton
End Class
