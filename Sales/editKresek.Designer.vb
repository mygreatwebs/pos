﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class editKresek
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.textQtt = New DevExpress.XtraEditors.TextEdit()
        Me.textName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.textQtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.butOK)
        Me.GroupControl1.Controls.Add(Me.butDelete)
        Me.GroupControl1.Controls.Add(Me.textQtt)
        Me.GroupControl1.Controls.Add(Me.textName)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(440, 305)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Koreksi Detail Produk"
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butOK.Location = New System.Drawing.Point(300, 202)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(114, 70)
        Me.butOK.TabIndex = 16
        Me.butOK.Text = "Simpan"
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDelete.Location = New System.Drawing.Point(12, 202)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(114, 70)
        Me.butDelete.TabIndex = 15
        Me.butDelete.Text = "Hapus"
        '
        'textQtt
        '
        Me.textQtt.EditValue = "0"
        Me.textQtt.Location = New System.Drawing.Point(92, 69)
        Me.textQtt.Name = "textQtt"
        Me.textQtt.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textQtt.Properties.Appearance.Options.UseFont = True
        Me.textQtt.Properties.Appearance.Options.UseTextOptions = True
        Me.textQtt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textQtt.Properties.Mask.EditMask = "n0"
        Me.textQtt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textQtt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textQtt.Properties.MaxLength = 3
        Me.textQtt.Size = New System.Drawing.Size(79, 42)
        Me.textQtt.TabIndex = 14
        '
        'textName
        '
        Me.textName.Location = New System.Drawing.Point(92, 31)
        Me.textName.Name = "textName"
        Me.textName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textName.Properties.Appearance.Options.UseFont = True
        Me.textName.Properties.ReadOnly = True
        Me.textName.Size = New System.Drawing.Size(322, 32)
        Me.textName.TabIndex = 12
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(44, 86)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(42, 18)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Qty :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 18)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Produk :"
        '
        'editKresek
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 305)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "editKresek"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.textQtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textQtt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textName As DevExpress.XtraEditors.TextEdit
End Class
