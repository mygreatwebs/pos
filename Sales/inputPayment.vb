﻿Imports DevExpress.XtraReports.UI
Imports MySql.Data.MySqlClient
Imports System.Math
Imports Laritta_POS.RFID_Tools
Imports System.ComponentModel

Public Class inputPayment
    Dim totqty, totitem, discperc As Integer
    Dim totmintransaksi As Double = 0
    Dim klikvoucher As Boolean = False
    Dim klikkartukredit As Boolean = False
    Dim optprintout As XtraReport = New Nota
    Dim opttoolprintout As DevExpress.XtraReports.UI.ReportPrintTool
    Dim DoneAddPoint As Boolean
    Dim disRpBeforeBRI As Double
    Dim disRpBeforeBRI_2 As Double
    Dim MSCount As Integer
    Private ReadOnly bw As BackgroundWorker = New BackgroundWorker()
    Public Sub callMe(ByVal totBuy As Integer, ByVal totDisc As Integer, ByVal tqty As Integer, ByVal titem As Integer, ByVal tdisc As Integer, ishowdialog As Boolean)
        textBiaya.EditValue = totBuy
        textDisc.EditValue = totDisc
        textTotalBiaya.EditValue = totBuy - totDisc
        totqty = tqty
        totitem = titem
        discperc = tdisc
        isbayarshow = True
        If ishowdialog = True Then
            If kodemember.Length > 0 And isambilpaketpromo = False And checktopup(False) = False Then
                masterpromo("8")
            End If
            ShowDialog()
        End If
    End Sub

    Private Sub gridConfig()
        xSet.Tables.Add("gridPembayaran")
        xSet.Tables("gridPembayaran").Columns.Add("idPayment", Type.GetType("System.Decimal"))
        xSet.Tables("gridPembayaran").Columns.Add("namaPayment", Type.GetType("System.String"))
        xSet.Tables("gridPembayaran").Columns.Add("amount", Type.GetType("System.Decimal"))
        xSet.Tables("gridPembayaran").Columns.Add("chargeFee", Type.GetType("System.Decimal"))
        xSet.Tables("gridPembayaran").Columns.Add("numName", Type.GetType("System.String"))
        xSet.Tables("gridPembayaran").Columns.Add("totPayment", Type.GetType("System.Decimal"))
        xSet.Tables("gridPembayaran").Columns.Add("idCOAEdc", Type.GetType("System.String"))
        xSet.Tables("gridPembayaran").Columns.Add("idEDC", Type.GetType("System.Decimal"))

        GridControl1.DataSource = xSet.Tables("gridPembayaran").DefaultView

        With GridView1
            .Columns("idPayment").Visible = False
            .Columns("numName").Visible = False
            .Columns("idCOAEdc").Visible = False
            .Columns("idEDC").Visible = False

            .Columns("namaPayment").Caption = "Jenis Pembayaran"
            .Columns("amount").Caption = "Jumlah"
            .Columns("chargeFee").Caption = "Card Charge"
            .Columns("totPayment").Caption = "Sub Total"

            .Columns("amount").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("amount").DisplayFormat.FormatString = "n0"
            .Columns("amount").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

            .Columns("chargeFee").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("chargeFee").DisplayFormat.FormatString = "n0"
            .Columns("chargeFee").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

            .Columns("totPayment").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("totPayment").DisplayFormat.FormatString = "n0"
            .Columns("totPayment").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum

            .BestFitColumns()
        End With

        'SQLquery = "SELECT idPayment, namaPayment, idKategoriPayment, minCharge, cardCharge FROM mpayment WHERE inactive=0"
        SQLquery = "SELECT mp.idPayment, namaPayment, idKategoriPayment, minCharge, cardCharge,idCOA,is_creditCard FROM mpayment mp left JOIN m_coa_payment mc ON mc.idPayment= mp.idPayment WHERE mp.inactive=0 and mc.idCabang='" & My.Settings.idcabang & "'"
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDataMPayment")

        If IsNothing(xSet.Tables("dateEDC")) Then
            xSet.Tables.Add("dateEDC")
        Else
            xSet.Tables("dateEDC").Clear()
        End If
        SQLquery = "SELECT idEDC,namaEDC `Nama EDC`,idCOA FROM mtedc me WHERE inactive=0"
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "dateEDC")
        LookUpEdit1.Properties.DataSource = xSet.Tables("dateEDC")
        LookUpEdit1.Properties.DisplayMember = "Nama EDC"
        LookUpEdit1.Properties.ValueMember = "idEDC"
    End Sub

    Private Sub getMPayment(ByVal idx As Integer)
        ListBoxControl1.Items.Clear()
        ListBoxControl1.Dock = DockStyle.Left
        LookUpEdit1.Visible = False
        'cek klik voucher atau tidak
        If idx = 3 Then
            klikvoucher = True
        Else
            klikvoucher = False
        End If

        For Each row As DataRow In xSet.Tables("getDataMPayment").Select("idKategoriPayment=" & idx )
            ListBoxControl1.Items.Add(row.Item("namaPayment"))
        Next
        ListBoxControl1.SelectedItem = 0
        PanelControl3.Visible = False

        PanelControl2.Visible = True
    End Sub

    Private Sub getMPayment2(ByVal idx As Integer, ByVal isbri As Boolean)
        ListBoxControl1.Dock = DockStyle.Left
        LookUpEdit1.Visible = False
        Dim strwhere As String = ""
        klikkartukredit = False
        If isbri = False Then
            strwhere = " And namaPayment Not In ('BRIZI','DEBIT BRI','KARTU KREDIT BRI')"
        Else
            strwhere = " and namaPayment in ('BRIZI','DEBIT BRI','KARTU KREDIT BRI')"
        End If

        'cek klik voucher atau tidak
        If idx = 3 Then
            klikvoucher = True
        ElseIf idx = 2 Then
            klikkartukredit = True
            Dim datakartukredit() As DataRow = xSet.Tables("getDataMPayment").Select("is_creditCard='1'")
            Dim listpayment As String = ""
            For Each payment In datakartukredit
                listpayment = listpayment & "'" & payment("namaPayment").ToString & "',"
            Next payment
            listpayment = listpayment.Substring(0, listpayment.Count - 1)
            strwhere = " and namaPayment in (" & listpayment & ")"
            ListBoxControl1.Dock = DockStyle.None
            ListBoxControl1.Height = 335
            LookUpEdit1.Visible = True
        ElseIf idx = 4 Then
            idx = 2
            Dim datakartukredit() As DataRow = xSet.Tables("getDataMPayment").Select("is_creditCard='0'")
            Dim listpayment As String = ""
            For Each payment In datakartukredit
                listpayment = listpayment & "'" & payment("namaPayment").ToString & "',"
            Next payment
            listpayment = listpayment.Substring(0, listpayment.Count - 1)
            strwhere = " and namaPayment in (" & listpayment & ")"

        Else
            klikvoucher = False
        End If

        ListBoxControl1.Items.Clear()
        For Each row As DataRow In xSet.Tables("getDataMPayment").Select("idKategoriPayment=" & idx & strwhere)
            ListBoxControl1.Items.Add(row.Item("namaPayment"))
        Next
        ListBoxControl1.SelectedItem = 0
        PanelControl3.Visible = False
        PanelControl2.Visible = True
    End Sub

    Private Sub inputPayment_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        PanelControl2.Visible = False
        PanelControl1.Visible = True
        jmlBayar.Text = 0
        textCardNo.Text = ""

        xSet.Tables.Remove("gridPembayaran")
        xSet.Tables.Remove("getDataMPayment")
        xSet.Tables.Remove("getNewNum")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub inputPayment_FormClosed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.FormClosed
        TotalDisc = 0
        totDisc = 0
        isbayarshow = False
        Dispose()
    End Sub

    Private Sub ReaderStarter()
        Try
            If Not MyACR.ReaderEstablished Then
                MyACR.Establish(GlobalReaderContext)
            End If
            MyACR.ReaderName = DefReaderName
            MyACR.EasyConnection()
        Catch ex As RFID_Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function KeySetupCheck() As Boolean
        If DefReaderName = "" Then
            Return False
        End If
        If IsNothing(MyMiFareKey.KeyA) Then
            Return False
        End If

        Return True
    End Function

    'Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
    '    On Error Resume Next
    '    MSCount = 10

    '    Do While CardProgressBar.EditValue <> 100
    '        Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
    '        If bw.CancellationPending Or ReaderIsBusy Then
    '            e.Cancel = True
    '            Exit Do
    '        End If

    '        System.Threading.Thread.Sleep(100)
    '        bw.ReportProgress(0)

    '        If MSCount < 10 Then MSCount += 1

    '        'check if the reader available
    '        If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
    '        If Not MyCard.ReaderEstablished Then
    '            SetLabelText_ThreadSafe(CardProgressStatus, "Reader not found")             'Cant found any reader
    '            MSCount = 0
    '            Continue Do
    '        End If
    '        bw.ReportProgress(1)
    '        SetLabelText_ThreadSafe(CardProgressStatus, "Waiting..")                        'Successfully connected to reader

    '        'check is any available card
    '        MyCard.Connect(GlobalReaderContext, DefReaderName)
    '        If Not MyCard.CardConnected Then
    '            Continue Do
    '        End If
    '        'the shit is coming
    '        bw.ReportProgress(30)
    '        SetLabelText_ThreadSafe(CardProgressStatus, "Authenticating..")                 'Successfully connected to card

    '        'try to authenticate card
    '        MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.KeyA)
    '        If Not MyCard.ReturnCode = 0 Then
    '            'shit happen
    '            SetLabelText_ThreadSafe(CardProgressStatus, "Load failed..")                'Failed to load keys
    '            MSCount = 0
    '            Continue Do
    '        Else
    '            bw.ReportProgress(60)
    '            MyCard.Authentication(4, MiFareKey.KeyType.TypeA)                          'auth block no.4 where the key stored
    '            If Not MyCard.ReturnCode = 0 Then
    '                'shit!
    '                SetLabelText_ThreadSafe(CardProgressStatus, "Authenticate failed..")    'Failed to authenticate
    '                MSCount = 0
    '                Continue Do
    '            End If
    '        End If
    '        bw.ReportProgress(75)

    '        'get card uid
    '        CardUID = MyCard.GetUID
    '        If CardUID = "" Then
    '            SetLabelText_ThreadSafe(CardProgressStatus, "Card not recognized!")         'IMPOSIBRO!
    '            MSCount = 0
    '            Return
    '        End If
    '        bw.ReportProgress(85)
    '        SetLabelText_ThreadSafe(CardProgressStatus, "Reading..")                        'Trying to read

    '        'try to get kodemember
    '        MyCard.BlockRead(4, 16)                                                        'read block no.4 where the key stored
    '        If Not MyCard.ReturnCode = 0 Then
    '            'holy shit!
    '            SetLabelText_ThreadSafe(CardProgressStatus, "Read failed..")                'Failed to read
    '            MSCount = 0
    '            Continue Do
    '        Else
    '            bw.ReportProgress(100)
    '            SetLabelText_ThreadSafe(CardProgressStatus, "Success..")                    'Godspeed..
    '            Return
    '        End If
    '    Loop
    'End Sub

    ' The delegate
    Delegate Sub SetLabelText_Delegate(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)

    ' The delegates subroutine.
    Private Sub SetLabelText_ThreadSafe(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)
        ' InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        If MSCount >= 10 Or [text] = "Success.." Then
            If InvokeRequired Then
                Dim MyDelegate As New SetLabelText_Delegate(AddressOf SetLabelText_ThreadSafe)
                Invoke(MyDelegate, New Object() {[TargetObject], [text]})
            Else
                [TargetObject].Caption = [text]
            End If
        End If
    End Sub

    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        'CardProgressBar.EditValue = e.ProgressPercentage
    End Sub

    Private Sub inputPayment_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        gridConfig()

    End Sub

    Private Sub bw_RunWorkerCompleted()
        'done

        Dim TempKodeMember As String = ""
        For i = 0 To 16
            If MyCard.RecvBuff(i) = &H0 Then Continue For
            TempKodeMember = TempKodeMember + Convert.ToChar(MyCard.RecvBuff(i))
        Next
        MyCard.Disconnect()
        'kodemember.EditValue = Trim(TempKodeMember) 'Godspeed..
        hasilread = Trim(TempKodeMember)
    End Sub
    Dim hasilread As String
    Private Sub butCash_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butCash.Click
        'cash
        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            selectedvoucher = 0
            LabelControl6.Visible = False
            textCardNo.Visible = False
            getMPayment(1)
        End If
    End Sub

    Private Sub butCreditCard_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butCreditCard.Click
        'credit card
        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            selectedvoucher = 0
            LabelControl6.Visible = True
            textCardNo.Visible = True
            getMPayment2(2, False)
        End If
    End Sub

    Private Sub butVouc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVouc.Click
        'Voc
        If AktifPromo = 3 Then
            MsgBox("Promo tidak dapat dibayar dengan voucher")
            Exit Sub
        End If

        ''cek top up atau ga
        'If checktopup(True) = True Then
        '    MsgBox("Pembayaran Top up tidak bisa menggunakan voucher!")
        '    Exit Sub
        'End If

        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            LabelControl6.Visible = True
            textCardNo.Visible = True
            selectedvoucher = 0
            klikvoucher = True
            frmpilihvoucher.ShowDialog()

            getMPayment(3)
            If batalnilaivoucher = True Then
                butHide_Click(sender, e)
            End If
        End If
    End Sub

    Private Sub butDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butDelete.Click
        If GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "namaPayment").ToString = "VOUCHER TOP UP" Then
            pakaivouchertopup = False
        End If
        GridView1.DeleteSelectedRows()

        If GridView1.RowCount > 0 Then
            textTotalBayar.Text = GridView1.Columns("amount").SummaryItem.SummaryValue
            If CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue) < 0 Then
                textKembalian.Text = 0
            Else
                textKembalian.Text = CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue)
            End If
        Else
            textTotalBayar.Text = 0
            textKembalian.Text = 0
        End If
    End Sub

    Private Sub SimpleButton18_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butReset.Click
        jmlBayar.Text = 0
        textCardNo.Text = ""
    End Sub

    Private Sub butHide_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butHide.Click
        Dim v_total As Double

        PanelControl2.Visible = False
        PanelControl3.Visible = True
        jmlBayar.Text = 0
        textCardNo.Text = ""
        'With FormPOSx
        '    .txtDisc.EditValue = 0
        '    .txtDiscPercent.EditValue = 0
        'End With 
        'textDisc.EditValue=0
        'textTotalBiaya.EditValue=textBiaya.EditValue-textDisc.EditValue
        'With FormPosX    
        '    .txtTotalBeli.EditValue = .GridView1.Columns("hrg").SummaryItem.SummaryValue - .txtDisc.EditValue
        'End with
        With FormPOSx
            v_total = textBiaya.EditValue
            If disRpBeforeBRI = disRpBeforeBRI_2 Then
                disRpBeforeBRI = 0
            End If
            If disRpBeforeBRI = 0 Then
                disRpBeforeBRI = .txtDisc.EditValue
                disRpBeforeBRI_2 = .txtDisc.EditValue
            Else
                .txtDisc.EditValue = Abs(disRpBeforeBRI - .txtDisc.EditValue)
                disRpBeforeBRI = .txtDisc.EditValue
            End If
            If disRpBeforeBRI > 0 Then
                .txtDiscPercent.EditValue = (disRpBeforeBRI / v_total) * 100
            Else
                .txtDiscPercent.EditValue = 0
            End If
        End With
        textDisc.EditValue = disRpBeforeBRI
        textTotalBiaya.EditValue = textBiaya.EditValue - textDisc.EditValue
        With FormPOSx
            .txtTotalBeli.EditValue = .GridView1.Columns("hrg").SummaryItem.SummaryValue - .txtDisc.EditValue
        End With
    End Sub

    Private Sub butProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butProceed.Click
        Try
            If textCardNo.Text.Contains("'") Or textCardNo.Text.Contains(";") Then
                MessageBox.Show("Huruf (') tidak diterima sebagai inputan", "Perhatian", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If


            If checktopup(False) And klikvoucher = True Then
                If ListBoxControl1.SelectedItem.ToString = "VOUCHER TOP UP" Then
                    novoucher = textCardNo.Text
                    pakaivouchertopup = True
                Else
                    MessageBox.Show("Voucher tidak bisa digunakan untuk transaksi TOP UP!", "Voucher Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If

            'cek voucher yang di input

            If ListBoxControl1.SelectedItem.ToString = "GEBYAR VOUCHER BELANJA" Then
                novoucher = textCardNo.Text
            ElseIf ListBoxControl1.SelectedItem.ToString = "LARITTA PESTA VOUCHER" Then
                novoucher = textCardNo.Text
            End If

            If ListBoxControl1.SelectedItem.ToString = "GEBYAR VOUCHER BELANJA" Or ListBoxControl1.SelectedItem.ToString = "LARITTA PESTA VOUCHER" Or ListBoxControl1.SelectedItem.ToString = "VOUCHER TOP UP" Or ListBoxControl1.SelectedItem.ToString = "VOUCHER NOMINAL" Then
                If TestOnline() Then
                    If textCardNo.Text <> "" Then
                        Dim cek As String
                        If textCardNo.Text.Length > 8 Then
                            If textCardNo.Text.Substring(4, 3).ToUpper = "BOM" Then
                                SQLquery = "select kode_voucher from app_promo.voucher_retail vr where vr.kode_voucher='" & textCardNo.Text & "'"
                                cek = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                            ElseIf textCardNo.Text.Substring(4, 3).ToUpper = "PES" Then
                                SQLquery = "select kode_voucher from app_promo.voucher_pesanan vp where vp.kode_voucher='" & textCardNo.Text & "'"
                                cek = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                            ElseIf textCardNo.Text.Substring(4, 3).ToUpper = "ECA" Then
                                SQLquery = "select kode_voucher,nominal_voucher,namaBrg,idBrg,mb.price from app_promo.voucher_emoney ve JOIN mbarang mb ON mb.idBrg=ve.idbrg_topup where ve.kode_voucher='" & textCardNo.Text & "' AND is_use=0 and del='0'"
                                If IsNothing(xSet.Tables("datavoucher")) Then
                                    xSet.Tables.Add("datavoucher")
                                Else
                                    xSet.Tables("datavoucher").Clear()
                                End If

                                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "datavoucher")
                                If xSet.Tables("datavoucher").Rows.Count = 0 Then
                                    MessageBox.Show("voucher yang diinput salah atau sudah tidak berlaku!")
                                    Exit Sub
                                Else
                                    jmlBayar.EditValue = xSet.Tables("datavoucher").Rows(0).Item("price")
                                End If
                                If checkvalidvouchertoptup() Then
                                    'nominaltopupvoucher = CDbl(xSet.Tables("datavoucher").Rows(0).Item("nominal_voucher"))
                                    cek = xSet.Tables("datavoucher").Rows(0).Item("kode_voucher")
                                Else
                                    MessageBox.Show("Voucher tidak bisa digunakan untuk transaksi ini!", "Voucher Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                    Exit Sub
                                End If
                            ElseIf textCardNo.Text.Substring(4, 3).ToUpper = "NOM" Then
                                SQLquery = "select kode_voucher,nominal_voucher,minimal_nominal_transaksi from app_promo.voucher_nominal vr where vr.kode_voucher='" & textCardNo.Text & "'"
                                If IsNothing(xSet.Tables("datavoucher")) Then
                                    xSet.Tables.Add("datavoucher")
                                Else
                                    xSet.Tables("datavoucher").Clear()
                                End If
                                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "datavoucher")

                                If xSet.Tables("datavoucher").Rows.Count = 0 Then
                                    MessageBox.Show("voucher yang diinput salah atau sudah tidak berlaku!")
                                    Exit Sub
                                ElseIf CDbl(xSet.Tables("datavoucher").Rows(0).Item("minimal_nominal_transaksi")) > CDbl(textTotalBiaya.Text) Then
                                    MessageBox.Show("Minimal transaksi Rp. " & Format(CDbl(xSet.Tables("datavoucher").Rows(0).Item("minimal_nominal_transaksi")), "#,###") & ".-")
                                    Exit Sub
                                Else
                                    novoucher = xSet.Tables("datavoucher").Rows(0).Item("kode_voucher")
                                    cek = xSet.Tables("datavoucher").Rows(0).Item("kode_voucher")
                                    If CDbl(textTotalBiaya.Text) > CDbl(xSet.Tables("datavoucher").Rows(0).Item("nominal_voucher")) Then
                                        jmlBayar.EditValue = xSet.Tables("datavoucher").Rows(0).Item("nominal_voucher")
                                    Else
                                        jmlBayar.EditValue = CDbl(textTotalBiaya.Text)
                                    End If

                                End If
                            End If
                        End If

                        If cek = "" Or IsNothing(cek) = True Then
                            MessageBox.Show("voucher yang diinput salah!")
                            Exit Sub
                        End If
                    End If
                End If
                If CDbl(jmlBayar.EditValue) + CDbl(textTotalBayar.Text) > CDbl(textTotalBiaya.Text) Then
                    Exit Sub
                End If
            End If



            If ListBoxControl1.SelectedItem.ToString.ToUpper = "promo kalender mei 2017 - Voucher Rp 50.000".ToUpper Then
                Dim jumlahvoucher As Integer = CDbl(jmlBayar.Text) \ 50000
                If CDbl(textTotalBiaya.Text) < 100000 * jumlahvoucher Then
                    MessageBox.Show("Penggunaan voucher tidak sesuai total biaya")
                    Exit Sub
                End If
                If CDbl(jmlBayar.Text) Mod 50000 <> 0 Then
                    MessageBox.Show("Nilai voucher harus kelipatan 100.000")
                    Exit Sub
                End If
            End If

            If ListBoxControl1.SelectedItem.ToString.ToUpper = "Voucher Lebaran Rp 25.000,-".ToUpper Then
                Dim jumlahvoucher As Integer = CDbl(jmlBayar.Text) \ 25000
                If CDbl(textTotalBiaya.Text) < 50000 * jumlahvoucher Then
                    MessageBox.Show("Penggunaan voucher tidak sesuai total biaya")
                    Exit Sub
                End If
                'cek input nominal sesuai dengan nominal voucher atau gak
                If CDbl(jmlBayar.Text) Mod 25000 <> 0 Then
                    MessageBox.Show("Nilai voucher harus kelipatan 25.000")
                    Exit Sub
                End If
            End If

            If selectedvoucher = 1 Then
                If Trim$(textCardNo.Text) = "" And textCardNo.Enabled = True Then
                    MessageBox.Show("Kode Voucher Wajib Diisi !", "Informasi")
                    Exit Sub
                End If
            End If

sukses:     If GridView1.RowCount = 0 Then
                GoTo insertRow
            Else
                For Each row As DataRow In xSet.Tables("gridPembayaran").Rows
                    If row.Item("namaPayment") = ListBoxControl1.SelectedItem Then
                        row.Item("amount") = jmlBayar.Text
                        row.Item("numName") = textCardNo.Text
                        row.Item("chargeFee") = 0
                        row.Item("totPayment") = row.Item("amount")

                        textTotalBayar.Text = GridView1.Columns("amount").SummaryItem.SummaryValue
                        If CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue) < 0 Then
                            textKembalian.Text = 0
                        Else
                            textKembalian.Text = CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue)
                        End If

                        GoTo endOfSub
                    End If
                Next
                GoTo insertRow
            End If

            'idPayment, namaPayment, amount, chargeFee, numName
insertRow:  xSet.Tables("gridPembayaran").Rows.Add(xSet.Tables("getDataMPayment").Select("namaPayment = '" & ListBoxControl1.SelectedItem & "'")(0).Item("idPayment"),
                                    ListBoxControl1.SelectedItem, jmlBayar.Text, 0, textCardNo.Text, jmlBayar.Text, GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "idCOA"), LookUpEdit1.EditValue)
            LookUpEdit1.EditValue = Nothing
endOfSub:   PanelControl2.Visible = False
            PanelControl3.Visible = True
            jmlBayar.Text = 0
            textCardNo.Text = ""
            'klikvoucher = False

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            msgboxErrorDev()
        End Try
    End Sub

    Private Sub but100_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but100.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 100000 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 100000
    End Sub

    Private Sub but50_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but50.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 50000 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 50000
    End Sub

    Private Sub but10_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but10.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 10000 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 10000
    End Sub

    Private Sub but5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but5.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 5000 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 5000
    End Sub

    Private Sub but1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but1.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 1000 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 1000
    End Sub

    Private Sub butMinim_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butMinim.Click
        'cek laritta cash tidak bisa kembalian
        If ListBoxControl1.SelectedItem.ToString = "LARITTA CASH" Then
            If CDbl(jmlBayar.Text) + CDbl(textTotalBayar.Text) + 500 > CDbl(textTotalBiaya.Text) Then
                Exit Sub
            End If
        End If
        jmlBayar.Text = CInt(jmlBayar.Text) + 500
    End Sub

    Private Sub butPayAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butPayAll.Click
        jmlBayar.Text = CDbl(textTotalBiaya.EditValue) - CDbl(textTotalBayar.EditValue)
    End Sub

    Private Sub GridView1_RowCountChanged(ByVal sender As Object, ByVal e As EventArgs) Handles GridView1.RowCountChanged
        If GridView1.RowCount <= 0 Then Return
        If GridView1.RowCount = 0 Then
            butDelete.Enabled = False
            butBayar.Enabled = False
        Else
            butDelete.Enabled = True
            butBayar.Enabled = True
        End If

        Try
            GridView1.UpdateSummary()
            textTotalBayar.Text = GridView1.Columns("amount").SummaryItem.SummaryValue
            If CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue) < 0 Then
                textKembalian.Text = 0
            Else
                textKembalian.Text = CInt(textTotalBayar.Text) - CInt(textTotalBiaya.EditValue)
            End If
        Catch ex As Exception
            msgboxErrorDev()
        End Try
    End Sub

    Private Sub butBatal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBatal.Click
        Close()
    End Sub

    Private Sub TambahPointOffline(ByVal v_idOrder As String)
        SQLquery = String.Format("insert into app_logoffline.mlogaddpoint (idOrder,kodekartu) VALUES ({0},'{1}')", v_idOrder, kodemember)
        ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
        '    V_cmd.CommandText = SQLquery
        '    V_cmd.ExecuteNonQuery()
        '    SQLquery = ""
    End Sub

    Private Function TambahPointCloud(ByVal v_idOrder As String) As Boolean
        Dim XConCloud As MySqlConnection
        Dim vartrCloud As MySql.Data.MySqlClient.MySqlTransaction
        Dim adaTranCloud As Boolean = False
        XConCloud = New MySqlConnection(My.Settings.cloudconn)
        Dim commandCloud As New MySqlCommand()
        'Dim command2 As New MySqlCommand

        If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
        SQLquery = String.Format("SELECT idOrder ID, totalQTY totalqtt, disc Diskon, subTotal-disc TotalTrans, createDate TglTrans, isVoid,CONCAT(DATE_FORMAT(CURDATE(), 'SO/%y/%m/'), LPAD(numorder, 6, '0')) NoNota FROM mtpos WHERE idOrder={0} ", v_idOrder)
        ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "This_mtpos")
        If xSet.Tables("This_mtpos").Rows.Count = 0 Then
            Return False
        End If

        If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
        SQLquery = String.Format("SELECT a.idBrg ID, b.namaBrg Nama, a.quantity Jml, a.price Hrg, a.disc Disc, a.totalPrice Total FROM dtpos a INNER JOIN mbarang b ON a.idBrg=b.idBrg AND a.status=1 WHERE idOrder={0};", v_idOrder)
        ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "This_dtpos")
        Try
            XConCloud.Open()
            commandCloud.Connection = XConCloud
            vartrCloud = XConCloud.BeginTransaction()
            commandCloud.Transaction = vartrCloud
            adaTranCloud = True

            SQLquery = String.Format("CALL InsertTransOutlet ({0}, {1}, {2}, '{3}', '{4}', {5}, {6}, {7}, '",
                                     idCust,
                                     id_cabang,
                                     v_idOrder,
                                     Format(xSet.Tables("This_mtpos").Rows(0).Item("TglTrans"), "yyyy-MM-dd HH:mm:ss"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("NoNota"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("totalqtt"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("TotalTrans"),
                                     staff_id)
            For Each drow As DataRow In xSet.Tables("This_dtpos").Rows
                SQLquery += String.Format("({0}, {1}, {2}, {3}, {4}, {5}, {6}), ", v_idOrder, id_cabang, drow.Item("ID"), drow.Item("Jml"), drow.Item("Hrg"), drow.Item("Disc"), drow.Item("Total"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "'); "
            'ExDb.ExecData(1, SQLquery)
            commandCloud.CommandText = SQLquery
            commandCloud.ExecuteNonQuery()
            SQLquery = ""
            vartrCloud.Commit()
            XConCloud.Close()
            adaTranCloud = False
            Return True
        Catch exCloud As Exception
            If adaTranCloud = True Then
                vartrCloud.Rollback()
            End If
            Return False
        End Try
    End Function

    Private Function kategoribrg(idbrg As String)
        Dim hasil As String = ExDb.Scalar("SELECT mk.namaKategori2 FROM mkategoribrg2 mk 
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
WHERE mb.idBrg='" & idbrg & "'", conn_string_local)
        Return hasil
    End Function

    Private Function idkategori2barang(idbrg As String)
        Dim hasil As String = ExDb.Scalar("SELECT mk.idKategori2 FROM mkategoribrg2 mk 
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
WHERE mb.idBrg='" & idbrg & "'", conn_string_local)
        Return hasil
    End Function


    Dim waktusaldo As New Timer
    Public Function checktopupcard()
        Get_MembercardSettings()
        'If Not MyMiFareKey.Stats Then Return

        If Not MyMiFareKey.Stats Then
            Return False
            Exit Function
        End If

        Dim state As String = ""

        Try
            state = "Reader"
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            'StatusMessage.Text = state & " is ready"

            'try to connect to card
            state = "Card"
            MyCard.Connect(GlobalReaderContext, DefReaderName)
            'StatusMessage.Text = state & " is ready"
        Catch ex As RFID_Exception
            'StatusMessage.Text = state & " not found"
        End Try

        Try

            '============================================================using sector 1 as hq, trailer=7, block=4,5,6
            'try to load DEFAULT keys to buffer
            state = "Load Default Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.DefaultKey)

            'try to authenticate with DEFAULT keys into trailer sector 2 (block7)
            state = "Auth"
            MyCard.Authentication(7, MiFareKey.KeyType.TypeA)

            'if success > do binarywrite | if no > skip

            'write secret keys to trailer sector
            MyCard.BinaryKeys_Write(1, MyMiFareKey.KeyA, MyMiFareKey.KeyB)
            '============================================================
        Catch ex As Exception
            'StatusMessage.Text = "Default key not accepted"
        End Try


        Try
            'try to load custom key B (to write purpose)
            state = "Load Custom Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeB, MyMiFareKey.KeyB)

            'try to authenticate with CUSTOM keys into block 4 where keys are going to be stored
            state = "Auth"
            MyCard.Authentication(4, MiFareKey.KeyType.TypeB)

            '============================================================
            'get uid
            state = "UID"
            CardUID = MyCard.GetUID()
            Return True
            'StatusMessage.Text = "Card is ready"
        Catch ex As Exception
            'StatusMessage.Text = "Card is not recognized"
        End Try
    End Function



    Sub processtopup()
        LoadSprite.CloseLoading()
        If IsNothing(xSet.Tables("listopup")) Then
            xSet.Tables.Add("listopup")
            xSet.Tables("listopup").Columns.Add("idbrg")
            xSet.Tables("listopup").Columns.Add("Top up")
            xSet.Tables("listopup").Columns.Add("Jumlah")
            xSet.Tables("listopup").Columns.Add("Harga", Type.GetType("System.Double"))
            xSet.Tables("listopup").Columns.Add("Total", Type.GetType("System.Double"))
            xSet.Tables("listopup").Columns.Add("Nominal Top up", Type.GetType("System.Double"))
            xSet.Tables("listopup").Columns.Add("Total Top up", Type.GetType("System.Double"))
        Else
            xSet.Tables("listopup").Clear()
        End If

        'data top up
        If IsNothing(xSet.Tables("datanominaltopup")) Then
            xSet.Tables.Add("datanominaltopup")
        Else
            xSet.Tables("datanominaltopup").Clear()
        End If

        'data promo top up
        If IsNothing(xSet.Tables("listpromoemoney")) Then
            xSet.Tables.Add("listpromoemoney")
            xSet.Tables("listpromoemoney").Columns.Add("idpromo")
        Else
            xSet.Tables("listpromoemoney").Clear()
        End If

        xSet.Tables("listopup").Columns("idbrg").ColumnMapping = MappingType.Hidden
        jumlahjenisbarangtopupdenganvoucher = 0
        membertopup = kodemember

        'variable warning tanpa bonus top up (beli pertama kali)
        Dim tidakdapatpromo As Integer = 0

        For i = 0 To FormPOSx.GridView1.RowCount - 1
            Dim kategori As String = kategoribrg(FormPOSx.GridView1.GetRowCellValue(i, "idBrg")).ToString.ToUpper
            xSet.Tables("datanominaltopup").Clear()
            If kategori = "TOP UP" Then
                Dim topup As Double = 0
                Dim topupnormal As Boolean = True
                Dim jumlahbrgtopup As Integer = 0

                SQLquery = "SELECT mn.nominal_topup,mn.idnominal_topup,jenis_topup FROM mnominal_topup mn WHERE mn.void=0 AND mn.idbrg='" & FormPOSx.GridView1.GetRowCellValue(i, "idBrg") & "'"
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "datanominaltopup")

                'cek apakah ada top up yang tidak dijual (di bayar dengan voucher)
                Dim result() As DataRow = xSet.Tables("datanominaltopup").Select("jenis_topup=1")
                If result.Count > 0 Then
                    adatopupdenganvoucher = True
                    jumlahjenisbarangtopupdenganvoucher = jumlahjenisbarangtopupdenganvoucher + 1
                    If xSet.Tables("gridPembayaran").Select("namaPayment='VOUCHER TOP UP'").Count = 0 Then
                        'syarattopupvoucher = False
                        Exit Sub
                    End If
                Else
                    'cek apakah ada top up yang di jual dan bisa pakai voucher
                    'result = xSet.Tables("datanominaltopup").Select("jenis_topup=0")
                    If xSet.Tables("datanominaltopup").Select("jenis_topup=0").Count > 0 Then
                        adatopupdenganvoucher = True
                        jumlahjenisbarangtopupdenganvoucher = jumlahjenisbarangtopupdenganvoucher + 1
                        'adajenistopupvoucherdanreguler = True
                        If xSet.Tables("gridPembayaran").Select("namaPayment<>'VOUCHER TOP UP'").Count > 0 Then
                            jumlahjenisbarangtopupdenganvoucher = jumlahjenisbarangtopupdenganvoucher - 1
                            If jumlahjenisbarangtopupdenganvoucher = 0 Then
                                adatopupdenganvoucher = False
                            End If

                        End If
                    Else
                        'syarattopupvoucher = True
                        adatopupdenganvoucher = False
                        'adajenistopupvoucherdanreguler = False
                    End If
                End If

                topup = CDbl(xSet.Tables("datanominaltopup").Rows(0).Item("nominal_topup"))
                jumlahbrgtopup = CInt(FormPOSx.GridView1.GetRowCellValue(i, "jmlh"))

                If TestOnline() And adatopupdenganvoucher = False Then

                    'If CBool(cekpernahpakailarittacash(kodemember)) = False Then
                    'cek apakah sudah pake laritta cash
                    '        sqlquery = string.format("select idparameter_promo,value from mpromo_topup mt join parameter_promo pp on mt.idpromo=pp.idpromo where mt.idnominal_topup='{0}' and tipe_promo=0 and mt.inactive=0 and mt.tanggal_aktif_awal<=date (now()) and mt.tanggal_aktif_akhir>=date (now())", xset.tables("datanominaltopup").rows(0).item("idnominal_topup").tostring())
                    '        exdb.execquery(my.settings.localconn, sqlquery, xset, "listparameterpromotopup")
                    '        if xset.tables("listparameterpromotopup").rows.count > 0 then
                    '            topup = cdbl(xset.tables("listparameterpromotopup").rows(0).item("value"))
                    '        end if
                    '    end if

                    'data promo top up
                    If IsNothing(xSet.Tables("listparameterpromotopup")) Then
                        xSet.Tables.Add("listparameterpromotopup")
                    Else
                        xSet.Tables("listparameterpromotopup").Clear()
                    End If

                    SQLquery = String.Format("select idparameter_promo,value,idjenis_promo,mt.idpromo from mpromo_topup mt join parameter_promo pp on mt.idpromo=pp.idpromo where mt.idnominal_topup='{0}' and tipe_promo=0 and mt.inactive=0 and mt.tanggal_aktif_awal<=date (now()) and mt.tanggal_aktif_akhir>=date (now())", xSet.Tables("datanominaltopup").Rows(0).Item("idnominal_topup").ToString())
                    ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listparameterpromotopup")


                    For k = 0 To xSet.Tables("listparameterpromotopup").Rows.Count - 1


                        If xSet.Tables("listparameterpromotopup").Rows(k).Item("idjenis_promo").ToString = "3" Then
                            For Each itemtopup In listitemtopup.Select(String.Format("idBrg='{0}'", FormPOSx.GridView1.GetRowCellValue(i, "idBrg")))
                                'cek pernah beli atau tidak
                                If CDbl(cekpernahbeliitem(idCust, itemtopup("idBrg"))) = False Then
                                    If xSet.Tables("listparameterpromotopup").Rows(k).Item("idparameter_promo").ToString = "1" Then
                                        If CInt(FormPOSx.GridView1.GetRowCellValue(i, "jmlh")) = 1 Then
                                            topup = CDbl(xSet.Tables("listparameterpromotopup").Rows(k).Item("value"))
                                        Else
                                            topupnormal = False
                                            'input listtopup dengan jumlah nominal top up normal
                                            jumlahbrgtopup = FormPOSx.GridView1.GetRowCellValue(i, "jmlh") - 1
                                            xSet.Tables("listopup").Rows.Add(FormPOSx.GridView1.GetRowCellValue(i, "idBrg"), FormPOSx.GridView1.GetRowCellValue(i, "namaBrg"), jumlahbrgtopup, FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs"), FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs") * jumlahbrgtopup, topup, topup * jumlahbrgtopup)

                                            'input listtopup dengan harga khusus
                                            topup = CDbl(xSet.Tables("listparameterpromotopup").Rows(k).Item("value"))
                                            jumlahbrgtopup = 1
                                            xSet.Tables("listopup").Rows.Add(FormPOSx.GridView1.GetRowCellValue(i, "idBrg"), FormPOSx.GridView1.GetRowCellValue(i, "namaBrg"), jumlahbrgtopup, FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs"), FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs") * jumlahbrgtopup, topup, topup * jumlahbrgtopup)
                                        End If
                                        'input list promo
                                        xSet.Tables("listpromoemoney").Rows.Add(xSet.Tables("listparameterpromotopup").Rows(k).Item("idpromo"))
                                    End If
                                Else
                                    tidakdapatpromo = tidakdapatpromo + 1
                                End If
                            Next itemtopup

                        End If
                    Next k
                End If

                If topupnormal = True Then
                    xSet.Tables("listopup").Rows.Add(FormPOSx.GridView1.GetRowCellValue(i, "idBrg"), FormPOSx.GridView1.GetRowCellValue(i, "namaBrg"), FormPOSx.GridView1.GetRowCellValue(i, "jmlh"), FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs"), FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs") * jumlahbrgtopup, topup, topup * jumlahbrgtopup)
                End If

            End If
        Next i

        If tidakdapatpromo > 0 Then
            MessageBox.Show("Top up ini tidak mendapatkan promo!", "Top up Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
        'Dim x As New topup_confirmation
        'x.ShowDialog()
    End Sub

    Private Function cekItemBS(idBrg As String) As Boolean
        SQLquery = String.Format("SELECT idBrg FROM msetupitembs WHERE idBrgBS='{0}';", idBrg)
        If IsNothing(ExDb.Scalar(SQLquery, conn_string_local)) = False Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function cekNilaiInventory(idBrg As String, isItemBS As Boolean) As Boolean
        SQLquery = String.Format("SELECT idBrg FROM msetupitembs WHERE idBrgBS='{0}';", idBrg)
        If IsNothing(ExDb.Scalar(SQLquery, conn_string_local)) = False Then
            Return True
        Else
            Return False
        End If
    End Function

    Dim idOrder, numOrder As Integer

    'variable jurnal public
    Dim listjurnal As New DataTable
    Dim countjurnal As Integer = 0
    Private Sub butBayar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBayar.Click
        'cek apakah sudah ada barang yang di pilih
        If IsNothing(xSet.Tables("setSales")) = False Then
            If xSet.Tables("setSales").Rows.Count = 0 Then
                msgboxErrorDev()
                Exit Sub
            End If
        End If

        Dim nilaiInventory As Decimal = 0
        Dim diskonPenjualan As Double = 0
        diskonPenjualan = diskonPenjualan + CDbl(FormPOSx.txtDisc.EditValue)
        'Dim issaved As Boolean = False
        Dim ketpromo As String = ""
        Dim Zcon As MySqlConnection
        Dim vartr As MySql.Data.MySqlClient.MySqlTransaction
        Dim adatrans As Boolean = False
        Dim arrybln() As String = {"Januari", "Pebuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"}
        useemoney = False
        'cek apakah total bayar sudah sesuai dengan total biaya
        If CInt(textTotalBiaya.EditValue) > CInt(textTotalBayar.Text) Then
            Exit Sub
        End If

        If AktifPromo = 1 Then
            ketpromo = "Promo PROCHIZ"
        ElseIf AktifPromo = 2 Then
            ketpromo = "Promo " & arrybln(Month(Now) - 1) & " " & Year(Now()) & " Laritta"
        ElseIf AktifPromo = 3 Then
            ketpromo = G_ketpromo
        End If

        If CInt(textTotalBiaya.EditValue) = 0 And CInt(textTotalBayar.Text) <> 0 Then
            MsgBox("Input Pembayaran Salah !, Total Order 0, Total Bayar <> 0 !")
            Exit Sub
        End If

        'cek penggunaan voucher pesanan dan voucher top up
        Dim hitungvc As Integer = 0
        'pakaivouchertopup = False
        For i = 0 To GridView1.RowCount - 1
            If GridView1.GetRowCellValue(i, "namaPayment") = "LARITTA PESTA VOUCHER" Then
                hitungvc = GridView1.GetRowCellValue(i, "totPayment") / 25000
                'ElseIf GridView1.GetRowCellValue(i, "namaPayment") = "VOUCHER TOP UP" Then
                '    xSet.Tables("setSales").Rows.Add(xSet.Tables("datavoucher").Rows(0).Item("idBrg"), xSet.Tables("datavoucher").Rows(0).Item("namaBrg"), 1, xSet.Tables("datavoucher").Rows(0).Item("price"), xSet.Tables("datavoucher").Rows(0).Item("price"), 0)
                '    pakaivouchertopup = True
                Exit For
            End If
        Next i


        If hitungvc > (CDbl(textTotalBiaya.Text) \ 50000) Then
            MsgBox("Penggunaan Voucher tidak valid")
            Exit Sub
        End If

        'proses top up
        'For i = 0 To GridView1.RowCount - 1
        Dim listpembayaran() As DataRow = xSet.Tables("gridPembayaran").Select("namaPayment='Laritta Cash'")
        If listpembayaran.Count > 0 Then
            'jumlahpembayaranemoney = CDbl(GridView1.GetRowCellValue(i, "totPayment"))
            For Each row As DataRow In listpembayaran
                jumlahpembayaranemoney = CDbl(row("totPayment"))
            Next
            bayarpakaiemoney = True
            useemoney = True
            'Exit For
        Else
            bayarpakaiemoney = False
        End If


        'Next i

        'cek pembayaran kartu kredit
        'For i = 0 To GridView1.RowCount - 1
        listpembayaran = xSet.Tables("gridPembayaran").Select("namaPayment='BCA CARD' or namaPayment='VISA' or namaPayment='MASTER' or namaPayment='KARTU KREDIT BRI' or namaPayment='KARTU KREDIT MAYBANK'")
        'If GridView1.GetRowCellValue(i, "namaPayment") = "BCA CARD" Or GridView1.GetRowCellValue(i, "namaPayment") = "VISA" Or GridView1.GetRowCellValue(i, "namaPayment") = "MASTER" Or GridView1.GetRowCellValue(i, "namaPayment") = "KARTU KREDIT BRI" Or GridView1.GetRowCellValue(i, "namaPayment") = "KARTU KREDIT MAYBANK" Then
        If listpembayaran.Count > 0 Then
            bayarpakaikartukredit = True
        Else
            bayarpakaikartukredit = False
        End If
        listpembayaran = xSet.Tables("gridPembayaran").Select("namaPayment like '%VOUCHER%'")
        If listpembayaran.Count > 0 Then
            If listpembayaran.FirstOrDefault()("namaPayment").ToString.Contains("VOUCHER") Then
                klikvoucher = True
            Else
                klikvoucher = False
            End If
        End If

        'Next i

        'cek pembayaran voucher atau gak
        'For i = 0 To GridView1.RowCount - 1
        'If GridView1.GetRowCellValue(i, "namaPayment").ToString = "GEBYAR VOUCHER BELANJA" Or GridView1.GetRowCellValue(i, "namaPayment").ToString = "LARITTA PESTA VOUCHER" Or GridView1.GetRowCellValue(i, "namaPayment").ToString = "GIFT VOUCHER" Or GridView1.GetRowCellValue(i, "namaPayment").ToString = "PROMO KALENDER MEI 2017 - VOUCHER Rp 50.000" Then
        '        klikvoucher = True
        '    'Exit For
        'End If
        'Next i

        'cek Top up
        If checktopup(True) = True Then
            If bayarpakaiemoney = True Or bayarpakaikartukredit = True Then
                MessageBox.Show("Top up tidak bisa menggunakan saldo Laritta Cash, Kartu Kredit!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If

            LoadSprite.ShowLoading("please wait", "checking card..", Me)
            If checktopupcard() = True Then
                ceksalahsaldo(readidmemeber().ToString)
                If ishavemember = False Then
                    LoadSprite.CloseLoading()
                    MessageBox.Show("Pembayaran harus menggunakan kartu member!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

                processtopup()

                If adatopupdenganvoucher = True And pakaivouchertopup = False Then
                    MessageBox.Show("Top up ini harus menggunakan Voucher Top Up!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                ElseIf adatopupdenganvoucher = False And pakaivouchertopup = True Then
                    MessageBox.Show("Tidak ada transaksi Top Up dengan voucher pada transaksi ini!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                ElseIf jumlahjenisbarangtopupdenganvoucher > 1 Then
                    MessageBox.Show("Transaksi dengan voucher tidak valid!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

                useemoney = True
                saldokartu = CDbl(readsaldo(True))
                saldosesudah = saldokartu
                'saldoawal.Text = saldokartu.ToString
            Else
                LoadSprite.CloseLoading()
                MessageBox.Show("kartu tidak dikenali" & vbCrLf & "silahkan periksa kembali kartu member!", "card top up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        'mengurangi saldo dari pembayaran e-money
        If bayarpakaiemoney = True Then
            If checktopupcard() = True Then

                ceksalahsaldo(readidmemeber.ToString)

                'pengecekan punya member atau tida
                If ishavemember = False Then
                    MessageBox.Show("Pembayaran harus menggunakan kartu member!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If

                'promo pemakaian laritta cash

                'pemakaian promo berlaku bila menggunakan laritta cash saja
                If GridView1.RowCount = 1 And xSet.Tables("gridPembayaran").Select("namaPayment='Laritta Cash'").Count > 0 Then
                    'promoemoney()
                    masterpromo("2")
                End If

                saldokartu = CDbl(readsaldo(False))
                saldosebelum = saldokartu
                If saldokartu < jumlahpembayaranemoney Then
                    MessageBox.Show("Saldo tidak mencukupi!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
                'saldokartu = saldokartu - jumlahpembayaranemoney
                ''saldokartu = 0
                'inputsaldo(saldokartu.ToString)
                'saldosesudah = saldokartu
                useemoney = True
            Else
                MessageBox.Show("kartu tidak dikenali" & vbCrLf & "silahkan periksa kembali kartu member!", "card top up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        End If

        'promo member
        masterpromo("7")

        'promo non member
        masterpromo("6")

        Try
            '-----------------------------ORDERAN


            Zcon = New MySqlConnection(My.Settings.db_conn_mysql)
            Dim command As New MySqlCommand()
            'Dim command2 As New MySqlCommand
            Zcon.Open()
            command.Connection = Zcon
            vartr = Zcon.BeginTransaction()
            command.Transaction = vartr
            adatrans = True
            'select dl num id pos terbaru
            'Dim idOrder, numOrder As Integer
            SQLquery = "SELECT IFNULL( MAX( numOrder ) , 0 ) +1 getNewNum FROM mtpos WHERE MONTH(createDate)=MONTH(CURDATE()) AND YEAR(createDate)=YEAR(CURDATE()) AND idCabang=" & id_cabang
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
            numOrder = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
            xSet.Tables("getNewNum").Clear()

            Dim transaksigojek As Integer = 0
            If isgojek.Checked = True Then
                transaksigojek = 1
            End If
            'insert ke mtpos
            SQLquery = String.Format("INSERT INTO mtpos(numOrder, idCabang, totalQTY, totalItem, subTotal, discPercent, disc, updateBy, createDate, shiftKe,promodeskripsi,jenis_transaksi) " &
                       "VALUES({0},{1},{2},{3},'{4}','{5}','{6}',{7},CURRENT_TIMESTAMP(),{8},'{9}','{10}')",
                       numOrder, id_cabang, totqty, totitem, CInt(textBiaya.Text), totDisc, CInt(textDisc.EditValue), staff_id, shiftLogin, ketpromo, transaksigojek)
            'ExDb.ExecData(SQLquery, conn_string_local)
            command.CommandText = SQLquery
            command.ExecuteNonQuery()

            'promo voucher (10.000)
            If checktopup(False) = False And klikvoucher = False Then
                masterpromo("5")
            End If


            If cekvalidbuyonegetone() Then
                'cek customer membawa member atau tidak
                Dim bawakartumember As Boolean = False
                If kodemember <> "" And klikvoucher = False Then
                    Dim result As Integer = MessageBox.Show("Apakah customer membawa member?", "Konfirmasi Member", MessageBoxButtons.YesNoCancel)
                    If result = DialogResult.Yes Then
                        bawakartumember = True
                    End If
                End If

                'print voucher
                If klikvoucher = False Then
                    If kodemember <> "" Then
                        If bawakartumember = True Then
                            'cek item BS
                            Dim nominaldapatvoucher As Double = 0
                            If IsNothing(xSet.Tables("listbs")) = True Then
                                gridlistbs()
                            End If

                            Dim tampung As String

                            For i = 0 To FormPOSx.GridView1.RowCount - 1
                                tampung = tampung & "'" & FormPOSx.GridView1.GetRowCellValue(i, "idBrg") & "'"
                                If i <> FormPOSx.GridView1.RowCount - 1 Then
                                    tampung = tampung & ","
                                End If
                            Next i
                            If IsNothing(xSet.Tables("listbs")) = False Then
                                xSet.Tables("listbs").Clear()
                            End If
                            SQLquery = "select namaBrg FROM mbarang mb
join mkategoribrg2 mk on mk.idKategori2=mb.idKategori2
WHERE idBrg IN (" & tampung & ") and 
mk.namakategori2 in ('BS','BS TART','BS TART SLICE','BS DONAT UPIN/IPIN','BS WHOLE BREAD','BS TRADITIONAL SNACK','BS WHOLE CAKE','BS SOES','BS PASTRY & PIE','SOFT DRINK ','SOFT DRINK KONSINYASI','PARCEL')
order by price asc"

                            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "listbs")

                            'mengurangi nominal untuk dapat voucher
                            Dim nominaltampung As Double = 0
                            For i = 0 To xSet.Tables("listbs").Rows.Count - 1
                                For j = 0 To FormPOSx.GridView1.RowCount - 1
                                    'Dim a As String = xSet.Tables("pricelist").Rows(i).Item("namaBrg")
                                    'Dim b As String = FormPOSx.GridView1.GetRowCellValue(j, "namaBrg")

                                    If FormPOSx.GridView1.GetRowCellValue(j, "namaBrg").ToString.ToUpper = xSet.Tables("listbs").Rows(i).Item("namaBrg").ToString.ToUpper Then
                                        nominaltampung = nominaltampung + FormPOSx.GridView1.GetRowCellValue(j, "hrg")
                                        Exit For
                                    End If
                                Next j
                            Next i
                            nominaldapatvoucher = CDbl(textTotalBiaya.Text) - nominaltampung
                            If CDbl(nominaldapatvoucher) >= 25000 Then
                                Dim voucher As String = ""
                                'Dim total As String = textTotalBiaya.Text
                                Dim total As String = nominaldapatvoucher
                                total = total.Replace(",", "")
                                Dim countvc As Integer = CDbl(total) \ 25000
                                Dim jumlahprint As Integer = 0
                                Dim jumlahvoucher() As String
                                Dim a As String
                                If countvc > 10 Then
                                    Dim s As Double = countvc / 10
                                    For i = 1 To Int(s)
                                        a = a & "10;"
                                    Next i
                                    Dim z As Integer = Int(s)
                                    Dim koma As Decimal = (s - z) * 10
                                    a = a & koma
                                    jumlahvoucher = Split(a, ";")
                                Else
                                    jumlahvoucher = Split(countvc.ToString & ";", ";")
                                End If
                                For i = 0 To jumlahvoucher.Count - 1

                                    If jumlahvoucher(i).ToString = "" Then
                                        Exit For
                                    End If
                                    If TestOnline() = True Then
                                        voucher = autogenerate("VCR", "BOM", "select kode_voucher from app_promo.voucher_retail ")
                                        SQLquery = "insert into app_promo.voucher_retail (kode_voucher,idCabang,is_use,createdate,idOrder,del,jumlah_voucher,jenis_voucher,cetakanke) values ('" & voucher & "','" & My.Settings.idcabang & "','0',now(),'" & numOrder & "','0'," & jumlahvoucher(i) & ",'1',1)"
                                        ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                                    Else
                                        voucher = ExDb.Scalar("select kode_voucher from app_promo.voucher_retail where jenis_voucher='2'", My.Settings.localconn)
                                        SQLquery = "update app_promo.voucher_retail set jenis_voucher='3',jumlah_voucher='" & jumlahvoucher(i) & "' where kode_voucher='" & voucher & "'"
                                        ExDb.ExecData(SQLquery, My.Settings.localconn)
                                    End If

                                    'printing voucher
                                    Dim printvc As XtraReport = New voucher_retail
                                    printvc.ShowPrintStatusDialog = False
                                    printvc.Parameters("reward").Value = "Free " & jumlahvoucher(i) & " pcs product"
                                    printvc.Parameters("alamat").Value = ExDb.Scalar("Select alamatCabang from mcabang where idCabang='" & My.Settings.idcabang & "'", My.Settings.localconn)
                                    printvc.Parameters("kodevoucher").Value = voucher
                                    printvc.Parameters("cetakanke").Value = "Cetakan ke-1"
                                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printvc)
                                        'tool.PreviewForm.WindowState = FormWindowState.Maximized
                                        'tool.ShowPreviewDialog()
                                        tool.Print()
                                    End Using
                                Next i
                            End If
                        End If
                    End If
                Else

                    If TestOnline() Then
                        If novoucher <> "" Then
                            If novoucher.Substring(4, 3) = "BOM" Then
                                SQLquery = "UPDATE app_promo.voucher_retail SET usedate=now(),is_use='1',updateDate=now(),idCabangUse='" & My.Settings.idcabang & "',jumlah_voucher='" & nilaivcbom & "' WHERE kode_voucher='" & novoucher & "'"
                                ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                            ElseIf novoucher.Substring(4, 3) = "PES" Then
                                SQLquery = "UPDATE app_promo.voucher_pesanan SET usedate=now(),is_use='1',updateDate=now(),idCabangUse='" & My.Settings.idcabang & "',nilai_voucher='" & CDbl(jmlBayar.Text) & "' WHERE kode_voucher='" & novoucher & "'"
                                ExDb.ExecData(SQLquery, My.Settings.cloudconn)

                            End If
                        End If
                    Else
                        Dim voucherapril As Boolean = False
                        For i = 0 To GridView1.RowCount - 1
                            If GridView1.GetRowCellValue(i, "namaPayment") = "LARITTA PESTA VOUCHER" Or GridView1.GetRowCellValue(i, "namaPayment") = "GEBYAR VOUCHER BELANJA" Then
                                voucherapril = True
                                Exit For
                            End If
                        Next i
                        If voucherapril = True Then
                            SQLquery = "insert into app_promo.listupdate (kode_voucher,done,createDate,jumlah_voucher) values('" & novoucher & "','0',now()," & nilaivcbom & ")"
                            ExDb.ExecData(SQLquery, My.Settings.localconn)
                        End If

                    End If

                End If
            End If

            'pemakaian offline voucher nominal
            If xSet.Tables("gridPembayaran").Select("namaPayment='VOUCHER NOMINAL'").Count > 0 Then
                If TestOnline() Then
                    If novoucher.Substring(4, 3) = "NOM" Then
                        SQLquery = "UPDATE app_promo.voucher_nominal SET usedate=now(),is_use='1',updateDate=now(),idCabangUse='" & My.Settings.idcabang & "' WHERE kode_voucher='" & novoucher & "'"
                        ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                    End If
                Else
                    If novoucher.Substring(4, 3) = "NOM" Then
                        SQLquery = "insert into app_promo.listupdate (kode_voucher,done,createDate) values('" & novoucher & "','0',now())"
                        ExDb.ExecData(SQLquery, My.Settings.localconn)
                    End If
                End If
            End If


            'select idorder yg baru aja di insert
            'SQLquery = String.Format("SELECT idOrder getNewNum FROM mtpos WHERE MONTH(createDate)=MONTH(CURDATE()) AND YEAR(createDate)=YEAR(CURDATE()) AND idCabang={0} AND numOrder={1}", id_cabang, numOrder)
            SQLquery = String.Format("SELECT idOrder getNewNum FROM mtpos WHERE MONTH(createDate)=MONTH(CURDATE()) AND YEAR(createDate)=YEAR(CURDATE()) AND idCabang={0} AND numOrder={1}", id_cabang, numOrder)
            'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
            command.CommandText = SQLquery
            ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
            idOrder = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
            xSet.Tables("getNewNum").Clear()

            'insert ke dtpos
            For Each rows As DataRow In xSet.Tables("setSales").Rows
                SQLquery = String.Format("INSERT INTO dtpos(idOrder, idBrg, quantity, price, discPercent, disc, totalPrice) " &
                           "VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                           idOrder, rows.Item("idBrg"),
                           CInt(rows.Item("jmlh")),
                           (100 / (100 - CInt(rows.Item("disc"))) * CInt(rows.Item("hrg"))) / CInt(rows.Item("jmlh")),
                           CInt(rows.Item("disc")),
                            (100 / (100 - CInt(rows.Item("disc"))) * CInt(rows.Item("hrg"))) / CInt(rows.Item("jmlh")) * (CInt(rows.Item("disc")) / 100),
                           (CInt(rows.Item("hrg"))))
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()

                SQLquery = String.Format("SELECT dn.nilai_inventory FROM
  (SELECT mn.idbrg,msBS.idBrg `idbrgNonBS`,nilai_inventory FROM app_logoffline.m_nilai_inventory mn
JOIN app_logoffline.d_nilai_inventory dn ON mn.id_nilai_inventory=dn.id_nilai_inventory
LEFT JOIN msetupitembs msBS ON msBS.idBrgBS=mn.idbrg
  WHERE dn.status=1 AND mn.idBrg='{0}') dataBarang
JOIN app_logoffline.m_nilai_inventory mni ON if(idbrgNonBS IS NULL ,dataBarang.idBrg=mni.idBrg,mni.idBrg=dataBarang.idbrgNonBS)
JOIN app_logoffline.d_nilai_inventory dn ON mni.id_nilai_inventory=dn.id_nilai_inventory;", rows.Item("idBrg"))

                nilaiInventory = nilaiInventory + ExDb.Scalar(SQLquery, conn_string_local)

                If cekItemBS(rows.Item("idBrg").ToString) = True Then
                    diskonPenjualan = diskonPenjualan + CDbl((100 / (100 - CInt(rows.Item("disc"))) * CInt(rows.Item("hrg"))) / CInt(rows.Item("jmlh")))
                End If
            Next rows


            'insert ke tstok
            For Each rows As DataRow In xSet.Tables("setSales").Rows
                'cek apakah barang adalah kartu signature atau bukan
                If rows.Item("idBrg") = "1175" Or rows.Item("idBrg") = "1176" Or rows.Item("idBrg") = "1179" Or rows.Item("idBrg") = "1180" Or rows.Item("idBrg") = "1197" Or rows.Item("idBrg") = "1198" Or rows.Item("idBrg") = "1201" Or rows.Item("idBrg") = "1202" Or rows.Item("idBrg") = "1446" Then
                    SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) " &
                           "VALUES(4, {0},{1},{2},{3}*-1)",
                           id_cabang, idOrder, 728, CInt(rows.Item("jmlh")))
                Else
                    SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) " &
                          "VALUES(4, {0},{1},{2},{3}*-1)",
                          id_cabang, idOrder, rows.Item("idBrg"), CInt(rows.Item("jmlh")))
                End If

                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
            Next

            '-----------------------------PEMBAYARAN

            'insert ke mtbayarpos
            SQLquery = String.Format("INSERT INTO mtbayarpos(idOrder, idCabang, totalOrder, totalPayment, totalChange, updateBy, updateDate) " &
                       "VALUES({0},{1},'{2}','{3}','{4}',{5},CURRENT_TIMESTAMP())", idOrder, id_cabang, CInt(textTotalBiaya.EditValue), CInt(textTotalBayar.Text), CInt(textKembalian.Text), staff_id)
            'ExDb.ExecData(SQLquery, conn_string_local)
            command.CommandText = SQLquery
            command.ExecuteNonQuery()
            'select idpaymentorder yg br aj di insert
            Dim idPaymentOrder As String
            SQLquery = String.Format("SELECT idPaymentOrder getNewNum FROM mtbayarpos WHERE idOrder={0}", idOrder)
            'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
            command.CommandText = SQLquery
            ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
            idPaymentOrder = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
            xSet.Tables("getNewNum").Clear()

            'insert ke dtbayarpos
            For Each row As DataRow In xSet.Tables("gridPembayaran").Rows
                SQLquery = String.Format("INSERT INTO dtbayarpos(idPaymentOrder, idPayment, amount, chargeFee, numName) VALUES({0},{1},'{2}','{3}','{4}')",
                                         idPaymentOrder, row.Item("idPayment"), CInt(row.Item("amount")), 0, row.Item("numName"))
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
            Next row
            'insert ke dtkodevoucher
            'If IsNothing(xSet.Tables("kodevoucher")) = False Then
            'For Each row As DataRow In xSet.Tables("kodevoucher").Rows
            'SQLquery = String.Format("INSERT INTO dtkodevoucher(idOrder,kodevoucher) VALUES({0},'{1}')",
            '                         idOrder, row.Item(0))
            ''ExDb.ExecData(SQLquery, conn_string_local)
            'command.CommandText = SQLquery
            'command.ExecuteNonQuery()
            'Next
            'End If
            'insert ke mtposmember
            If Trim(kodemember) <> "" Then

                'DoneAddPoint = TambahPointCloud(idOrder)
                'If DoneAddPoint = False Then
                '    TambahPointOffline(idOrder, command)
                '    MsgBox("Gagal menambahkan Point, Koneksi Offline")
                'Else
                '    MsgBox("Point Berhasil Ditambahkan")
                '    frmInputKodeMember.callme_2()
                'End If
                SQLquery = String.Format("INSERT INTO mtposmember(idOrder, kodemember,pointawal,pointakhir,kuponawal,kuponakhir) " &
                           "VALUES('{0}','{1}','{2}','{3}','{4}','{5}')", idOrder, kodemember, v_pointawal, v_pointakhir, v_kuponawal, v_kuponakhir)
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
                'cek ada brg free apa tidak
                If xSet.Tables("setSales").Select("namaBrg='Kalender Laritta 2017 (FREE)'").Count > 0 Then
                    SQLquery = String.Format("INSERT INTO mtfreekalender(idCust, kodemember) " &
                                "VALUES({0},'{1}')", idCust, kodemember)
                    'ExDb.ExecData(SQLquery, conn_string_local)
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()
                End If
            End If

            If AktifPromo = 3 And G_kodekupon <> "" Then
                If TestOnline() Then
                    Try
                        SQLquery = String.Format("INSERT INTO app_promo.mt_tukar_kupon (idPromo,idOrder,idCabang,kodekupon) VALUES ({0},{1},{2},'{3}') ", G_idpromo, idOrder, id_cabang, G_kodekupon)
                        ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                    Catch excloud As Exception
                        'do nothing 
                    End Try
                Else
                    SQLquery = String.Format("INSERT INTO app_logoffline.mt_tukar_kuponoffline (idPromo,idOrder,idCabang,kodekupon) VALUES ({0},{1},{2},'{3}') ", G_idpromo, idOrder, id_cabang, G_kodekupon)
                    'ExDb.ExecData(SQLquery, conn_string_local)
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()
                End If
            End If
            If AktifPromo = 5 And Trim$(G_kuponkalender) <> "" Then
                If TestOnline() Then
                    SQLquery = String.Format("update app_kupon.mkuponkalender set idOrder={0},idCabang={1} where kodekupon='{2}'", idOrder, id_cabang, G_kuponkalender)
                    ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                Else
                    vartr.Rollback()
                    msgboxErrorOffline()
                    Exit Sub
                End If
            End If
            'finance
            'variable jurnal
            countjurnal = 0
            listjurnal = New DataTable
            listjurnal.Columns.Add("dtjurnal")
            Dim dtjurnal As String = ""
            Dim datajurnal As New DataTable

            'show form konfirmasi top up
            If checktopup(False) Then
                Dim x As New topup_confirmation
                x.ShowDialog()
                'testing server
                Dim nominalselisih As Double = totaltopup - nominalhargatopup
                Dim xdtjurnal As String
                If nominalselisih > 0 Then
                    countjurnal = countjurnal + 1
                    xdtjurnal = countjurnal & "//402//0//" & nominalselisih
                    listjurnal.Rows.Add(xdtjurnal)
                End If

                countjurnal = countjurnal + 1
                xdtjurnal = countjurnal & "//433//1//" & totaltopup
                listjurnal.Rows.Add(xdtjurnal)
            End If

            If bataltopup = True Then
                Exit Sub
            End If


            'simpan jurnal finance

            'jurnal untuk masing-masing list payment
            If IsNothing(xSet.Tables("coaBayar")) Then
                xSet.Tables.Add("coaBayar")
                xSet.Tables("coaBayar").Columns.Add("idCOA")
                xSet.Tables("coaBayar").Columns.Add("nominalCOA")
            Else
                xSet.Tables("coaBayar").Clear()
            End If

            For i = 0 To GridView1.RowCount - 1
                Dim idCOApayment As String
                Dim totalpayment As Decimal
                If GridView1.GetRowCellValue(i, "idPayment").ToString = "1" Then
                    If CDbl(GridView1.GetRowCellValue(i, "totPayment")) > CDbl(textTotalBiaya.EditValue) Then
                        totalpayment = textTotalBiaya.EditValue
                    Else
                        totalpayment = GridView1.GetRowCellValue(i, "totPayment")
                    End If
                Else
                    totalpayment = GridView1.GetRowCellValue(i, "totPayment")
                End If

                If IsDBNull(GridView1.GetRowCellValue(i, "idCOAEdc")) Then
                    'input COA dengan pembayaran non kartu kredit
                    idCOApayment = xSet.Tables("mCoaPayment").Select("idPayment='" & GridView1.GetRowCellValue(i, "idPayment") & "'").FirstOrDefault()("idCOA")
                Else
                    'input COA dengan pembayaran kartu kredit
                    Dim potonganpersen As Decimal
                    idCOApayment = GridView1.GetRowCellValue(i, "idCOAEdc")
                    SQLquery = String.Format("SELECT potonganPersen FROM mtedc me JOIN dtedc de ON me.idEDC=de.idEDC WHERE me.idEDC='{0}' AND de.idPayment='{1}'", GridView1.GetRowCellValue(i, "idEDC"), GridView1.GetRowCellValue(i, "idPayment"))
                    potonganpersen = CDec(ExDb.Scalar(SQLquery, conn_string_local))
                    countjurnal = countjurnal + 1
                    Dim totalpaymentawal As Decimal = totalpayment
                    totalpayment = totalpayment - (totalpayment * (potonganpersen / 100))

                    Dim sdtjurnal As String = countjurnal & "//"
                    sdtjurnal = sdtjurnal & "448" & "//0//" & CStr(totalpaymentawal * (potonganpersen / 100))
                    listjurnal.Rows.Add(sdtjurnal)
                End If

                'input jurnal payment
                countjurnal = countjurnal + 1
                Dim xdtjurnal As String = countjurnal & "//"
                xdtjurnal = xdtjurnal & idCOApayment & "//0//" & totalpayment
                listjurnal.Rows.Add(xdtjurnal)

                If diskonPenjualan > 0 Then
                    'input jurnal diskon penjualan
                    countjurnal = countjurnal + 1
                    xdtjurnal = countjurnal & "//123//0//" & diskonPenjualan
                    listjurnal.Rows.Add(xdtjurnal)
                End If
            Next i

            'input jurnal COGS pada nilai invetory outlet

            countjurnal = countjurnal + 1
            dtjurnal = countjurnal & "//420//0//" & nilaiInventory.ToString
            listjurnal.Rows.Add(dtjurnal)

            countjurnal = countjurnal + 1
            dtjurnal = countjurnal & "//123//1//" & nilaiInventory.ToString
            listjurnal.Rows.Add(dtjurnal)

            inputJurnalPenjualan(CDbl(textTotalBiaya.EditValue) + diskonPenjualan)

            'input jurnal
            For o = 0 To listjurnal.Rows.Count - 1
                dtjurnal = dtjurnal & listjurnal.Rows(o).Item("dtjurnal")
                If o <> listjurnal.Rows.Count - 1 Then
                    dtjurnal = dtjurnal & "|"
                End If
            Next o

            SQLquery = String.Format("INSERT INTO {2}mt_journal_offline (idOrder, dtjurnal,isJurnalNotaVoid) VALUES ('{0}','{1}','0');", idOrder, dtjurnal, "app_logoffline.")

            command.CommandText = SQLquery
            command.ExecuteNonQuery()

            'commit untuk data transaksi lokal
            vartr.Commit()

            'simpan data cloud
            Dim koneksiemoney As String
            Dim schemaoffline As String = ""
            If TestOnline() Then
                koneksiemoney = My.Settings.cloudconn
            Else
                koneksiemoney = My.Settings.localconn
                schemaoffline = "app_logoffline."
            End If

            conemoney = New MySqlConnection(koneksiemoney)
            commandemoney = New MySqlCommand
            Dim vartremoney As MySql.Data.MySqlClient.MySqlTransaction

            conemoney.Open()
            commandemoney.Connection = conemoney
            vartremoney = conemoney.BeginTransaction()


            'simpan log history penggunaan saldo e-money
            If useemoney = True And bayarpakaiemoney = True Then
                'mengurangi saldo pemakaian e-money
                saldokartu = saldokartu - jumlahpembayaranemoney

                inputsaldo(saldokartu.ToString)
                While readsaldo(False).ToString <> saldokartu.ToString
                    MessageBox.Show("Input saldo gagal!" & vbCrLf & "Pastikan alat terpasang dengan benar kemudian klik OK untuk melakukan input ulang saldo", "Top Up Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    If checktopupcard() Then
                        inputsaldo(saldokartu.ToString)
                    End If

                End While
                saldosesudah = saldokartu

                'eksekusi update saldo di mcustomer
                If schemaoffline = "" Then
                    'update saldo di mcustomer
                    SQLquery = String.Format("UPDATE mcustomer m SET m.emoney={0} WHERE m.idCust='{1}'", saldosesudah, idCust)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()
                Else
                    SQLquery = String.Format("INSERT INTO {2}saldo_offline (kode_member, saldo) VALUES ('{0}','{1}')", kodemember, saldosesudah, schemaoffline)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()
                End If

                'insert ke mtrans_emoney
                SQLquery = String.Format("INSERT INTO {3}mtrans_emoney (idOrder, idCabang, tanggal_input, kode_member, isVoid, jenis_transaksi,nominal) VALUES ('{0}','{1}',now(),'{2}','0',1,{4})", idOrder, My.Settings.idcabang, kodemember, schemaoffline, jumlahpembayaranemoney * -1)
                commandemoney.CommandText = SQLquery
                commandemoney.ExecuteNonQuery()

                'insert ke mbst_dmoney
                'SQLquery = String.Format("INSERT INTO {2}mbst_demoney (id_trans_emoney,idOrder, nominal, isVoid) VALUES (LAST_INSERT_ID(),'{0}',{1},'0')", idOrder, jumlahpembayaranemoney * -1, schemaoffline)
                'commandemoney.CommandText = SQLquery
                'commandemoney.ExecuteNonQuery()

                'insert ke detail promo emoney
                If IsNothing(xSet.Tables("listpromoemoney")) = False Then
                    If xSet.Tables("listpromoemoney").Rows.Count > 0 Then
                        For i = 0 To xSet.Tables("listpromoemoney").Rows.Count - 1
                            SQLquery = String.Format("INSERT INTO {1}dtrans_emoney_promo (id_trans_emoney, idpromo, isvoid) VALUES (LAST_INSERT_ID(),'{0}',0)", xSet.Tables("listpromoemoney").Rows(i).Item("idpromo"), schemaoffline)
                            commandemoney.CommandText = SQLquery
                            commandemoney.ExecuteNonQuery()
                        Next i
                    End If
                End If
                'vartremoney.Commit()

                'Dim xdtjurnal As String
                'countjurnal = countjurnal + 1
                'xdtjurnal = countjurnal & "//" & getAccountChild("4110100", xSet.Tables("mcoa")).Select("idCabang='" & My.Settings.idcabang & "'").FirstOrDefault()("idCOA") & "//1//" & jumlahpembayaranemoney
                'listjurnal.Rows.Add(xdtjurnal)


            ElseIf useemoney = True And checktopup(False) = True Then
                'Dim koneksiemoney As String
                'Dim schemaoffline As String = ""
                'If TestOnline() Then
                '    koneksiemoney = My.Settings.cloudconn
                '    'update saldo di mcustomer
                '    SQLquery = String.Format("UPDATE mcustomer m SET m.emoney={0} WHERE m.idCust='{1}'", saldosesudah, idCust)
                'Else
                '    koneksiemoney = My.Settings.localconn
                '    schemaoffline = "app_logoffline."
                'End If

                'Dim conemoney As New MySqlConnection(koneksiemoney)
                'Dim commandemoney As New MySqlCommand
                'Dim vartremoney As MySql.Data.MySqlClient.MySqlTransaction
                'conemoney.Open()
                'commandemoney.Connection = conemoney
                'vartremoney = conemoney.BeginTransaction()

                'testing server
                'Dim nominalselisih As Double = totaltopup - nominalhargatopup
                'Dim xdtjurnal As String
                'If nominalselisih > 0 Then
                '    countjurnal = countjurnal + 1
                '    xdtjurnal = countjurnal & "//402//0//" & nominalselisih
                '    listjurnal.Rows.Add(xdtjurnal)
                'End If

                'countjurnal = countjurnal + 1
                'xdtjurnal = countjurnal & "//433//1//" & totaltopup
                'listjurnal.Rows.Add(xdtjurnal)

                'eksekusi update saldo di mcustomer
                If schemaoffline = "" Then
                    'update saldo di mcustomer
                    SQLquery = String.Format("UPDATE mcustomer m SET m.emoney={0} WHERE m.idCust='{1}'", saldosesudah, idCust)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()

                    'insert ke mbst_posm dan mbst_posd
                    If IsNothing(xSet.Tables("listitemtransaksipos")) = True Then
                        xSet.Tables.Add("listitemtransaksipos")
                    Else
                        xSet.Tables("listitemtransaksipos").Clear()
                    End If

                    SQLquery = String.Format("SELECT mp.idOrder,subTotal-mp.disc `totalNominal`,quantity,idBrg,price,dp.totalPrice `totalPriceDetail`,date_format(createDate,'%Y%m%d%k%i%s') `createDate`,dp.disc `detaildisc`,totalQTY,CONCAT(DATE_FORMAT(CURDATE(), 'SO/%y/%m/'), LPAD(numorder, 6, '0')) NoNota FROM mtpos mp JOIN dtpos dp ON mp.idOrder=dp.idOrder WHERE mp.idOrder='{0}'", idOrder)
                    ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listitemtransaksipos")

                    'insert ke mbst_posm
                    SQLquery = String.Format("INSERT INTO mbst_posm(id_pos, idCabang, tgl_pos, nomor_nota, idCust, total_qty, total_price, isVoid, tgl_input, point_gained, stamp_gained, use_promo, use_gift)
	VALUES ({0}, {1}, {2}, '{3}', {4}, {5}, {6}, 0, now(), 0, 0, 0, 0)",
    idOrder,
                                                 My.Settings.idcabang,
                                                 xSet.Tables("listitemtransaksipos").Rows(0).Item("createDate"),
                             xSet.Tables("listitemtransaksipos").Rows(0).Item("NoNota"),
                        idCust,
    xSet.Tables("listitemtransaksipos").Rows(0).Item("totalQTY"),
                                                 xSet.Tables("listitemtransaksipos").Rows(0).Item("totalNominal")
    )
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()

                    'insert ke mbst_posd
                    For j = 0 To xSet.Tables("listitemtransaksipos").Rows.Count - 1
                        SQLquery = String.Format("INSERT INTO mbst_posd (id_pos, idCabang, idBrg, quantity, price, disc, totalPrice) VALUES ({0},{1},{2},{3},{4},{5},{6})",
                               idOrder,
                                         My.Settings.idcabang,
                                                  xSet.Tables("listitemtransaksipos").Rows(j).Item("idBrg"),
                                                  xSet.Tables("listitemtransaksipos").Rows(j).Item("quantity"),
                                                     xSet.Tables("listitemtransaksipos").Rows(j).Item("price"),
                                                     xSet.Tables("listitemtransaksipos").Rows(j).Item("detaildisc"),
                                                     xSet.Tables("listitemtransaksipos").Rows(j).Item("totalPriceDetail")
                            )
                        commandemoney.CommandText = SQLquery
                        commandemoney.ExecuteNonQuery()
                    Next j
                Else
                    SQLquery = String.Format("INSERT INTO {2}saldo_offline (kode_member, saldo) VALUES ('{0}','{1}')", kodemember, saldosesudah, schemaoffline)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()
                    SQLquery = String.Format("INSERT INTO {1}topup_offline (idOrder,kode_member) VALUES ('{0}','{2}')", idOrder, schemaoffline, kodemember)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()
                End If

                'insert ke mtrans_emoney
                SQLquery = String.Format("INSERT INTO {3}mtrans_emoney (idOrder, idCabang, tanggal_input, kode_member, isVoid, jenis_transaksi,nominal) VALUES ('{0}','{1}',now(),'{2}','0',1,{4})", idOrder, My.Settings.idcabang, kodemember, schemaoffline, totaltopup)
                commandemoney.CommandText = SQLquery
                commandemoney.ExecuteNonQuery()

                'insert ke mbst_dmoney
                'SQLquery = String.Format("INSERT INTO {2}mbst_demoney (id_trans_emoney,idOrder, nominal, isVoid) VALUES (LAST_INSERT_ID(),'{0}',{1},'0')", idOrder, totaltopup, schemaoffline)
                'commandemoney.CommandText = SQLquery
                'commandemoney.ExecuteNonQuery()

                'insert ke detail promo
                If xSet.Tables("listpromoemoney").Rows.Count > 0 Then
                    For i = 0 To xSet.Tables("listpromoemoney").Rows.Count - 1
                        SQLquery = String.Format("INSERT INTO {1}dtrans_emoney_promo (id_trans_emoney, idpromo, isvoid) VALUES (LAST_INSERT_ID(),'{0}',0)", xSet.Tables("listpromoemoney").Rows(i).Item("idpromo"), schemaoffline)
                        commandemoney.CommandText = SQLquery
                        commandemoney.ExecuteNonQuery()
                    Next i
                End If

                'update voucher yang digunakan
                If pakaivouchertopup Then
                    SQLquery = String.Format("UPDATE app_promo.voucher_emoney SET is_use=1,updateDate=now(),useDate=now(),idCabangUse='{1}' WHERE kode_voucher='{0}'", xSet.Tables("datavoucher").Rows(0).Item("kode_voucher"), My.Settings.idcabang)
                    commandemoney.CommandText = SQLquery
                    commandemoney.ExecuteNonQuery()
                End If

                'vartremoney.Commit()
            End If
            'testing server
            'inputJurnalPenjualan(CDbl(textTotalBiaya.EditValue))
            ''input jurnal
            'For o = 0 To listjurnal.Rows.Count - 1
            '    dtjurnal = dtjurnal & listjurnal.Rows(o).Item("dtjurnal")
            '    If o <> listjurnal.Rows.Count - 1 Then
            '        dtjurnal = dtjurnal & "|"
            '    End If
            'Next o
            'If schemaoffline = "" Then
            '    SQLquery = String.Format("CALL {4}.insert_journal(DATE(now()),'12','{0}','{1}' ,'Penjualan Retail',NULL,'{2}',now(),'{3}')", idOrder, numOrder, staff_id, dtjurnal, My.Settings.db_finance)
            'Else
            '    SQLquery = String.Format("INSERT INTO {2}mt_journal_offline (idOrder, dtjurnal,isJurnalNotaVoid) VALUES ('{0}','{1}','0');", idOrder, dtjurnal, schemaoffline)
            'End If
            'commandemoney.CommandText = SQLquery
            'commandemoney.ExecuteNonQuery()

            'commit cloud
            vartremoney.Commit()

            Zcon.Close()
            adatrans = False
            xSet.Tables("setSales").Clear()

            ispromoproces = False
            idfrmconfirm_1 = 0
            'MsgBox("Data tersimpan")
            'If IsNothing(xSet.Tables("kodevoucher")) = False Then
            'xSet.Tables("kodevoucher").Clear()
            'End If

            'MsgBox("query header")
            'Try

            'Tambah poin customer
            If istopup = False Then
                If Trim(kodemember) <> "" Then
                    DoneAddPoint = TambahPointCloud(idOrder)
                    If DoneAddPoint = False Then
                        TambahPointOffline(idOrder)
                        MsgBox("Gagal menambahkan Point, Koneksi Offline")
                    Else
                        MsgBox("Point Berhasil Ditambahkan")
                        frmInputKodeMember.callme_2()

                        'ambil point customer setelah transaksi
                        'If IsNothing(xSet.Tables("datapointkupon")) Then
                        '    xSet.Tables.Add("infopoint")
                        'Else
                        '    xSet.Tables("infopoint").Clear()
                        'End If
                        'SQLquery String.Format("")
                        'v_pointakhir = 0
                        'v_kuponakhir = 0
                    End If
                    SQLquery = String.Format("update mtposmember set pointakhir='{0}',kuponakhir='{1}' where idorder='{2}'", v_pointakhir, v_kuponakhir, idOrder)
                    ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                End If

            End If

            'proses print

            selected_idxRow = idOrder
            isReprint = False
            QryHeaderNota()
            Dim printout As XtraReport = New Nota
            printout.ShowPrintStatusDialog = False
            'input parameter nota
            printout.Parameters("namamember").Value = namapemilikkartu


            'printout.CreateDocument(True)
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                'tool.ShowRibbonPreview()
                tool.Print()
                'tool.ShowPreviewDialog()
            End Using

            'isprinted = True
            'Catch ex As Exception
            '   isprinted = False
            'End Try
            'optprintout = New Nota
            'opttoolprintout = New DevExpress.XtraReports.UI.ReportPrintTool(optprintout)
            'MsgBox("here 1")
            'opttoolprintout.Print("PDF Converter Elite 3.0 Printer")
            'opttoolprintout.ShowRibbonPreview()



            printNota.callMe(textTotalBiaya.EditValue, textTotalBayar.Text, textKembalian.Text, idOrder, numOrder)

            'sync data offline (voucher dan e-money)
            If TestOnline() Then
                SQLquery = "select done,kode_voucher,DATE_FORMAT(createDate,'%Y-%m-%d %T') `createDate`,jumlah_voucher from app_promo.listupdate where done='0'"
                If IsNothing(xSet.Tables("listvoucheroffline")) = True Then
                    gridvoucheroffline()
                End If
                xSet.Tables("listvoucheroffline").Clear()
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listvoucheroffline")
                For i = 0 To xSet.Tables("listvoucheroffline").Rows.Count - 1
                    If xSet.Tables("listvoucheroffline").Rows(i).Item("kode_voucher").ToString.Substring(4, 3) = "NOM" Then
                        SQLquery = "UPDATE app_promo.voucher_nominal SET usedate='" & xSet.Tables("listvoucheroffline").Rows(i).Item("createDate") & "',is_use='1',updateDate=now(),cetakanke='1' WHERE kode_voucher='" & xSet.Tables("listvoucheroffline").Rows(i).Item("kode_voucher") & "'"
                    Else
                        SQLquery = "UPDATE app_promo.voucher_retail SET usedate='" & xSet.Tables("listvoucheroffline").Rows(i).Item("createDate") & "',is_use='1',updateDate=now(),jenis_voucher='3',jumlah_voucher='" & xSet.Tables("listvoucheroffline").Rows(i).Item("jumlah_voucher") & "',cetakanke='1' WHERE kode_voucher='" & xSet.Tables("listvoucheroffline").Rows(i).Item("kode_voucher") & "'"
                    End If
                    ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                    SQLquery = "update app_promo.listupdate set done='1' where kode_voucher='" & xSet.Tables("listvoucheroffline").Rows(i).Item("kode_voucher") & "'"
                    ExDb.ExecData(SQLquery, My.Settings.localconn)
                    'command.CommandText = SQLquery
                    'command.ExecuteNonQuery()
                Next i
                'testing server
                updatedataofflineemoney(False)

            End If

            'show saldo
            If useemoney = True Then
                waktusaldo.Interval = 2000
                waktusaldo.Start()
                AddHandler waktusaldo.Tick, AddressOf waktusaldo_event
            End If
            'reset promo7rb
            konfirmasipromo7rb = False
            saldosebelum = 0
            saldosesudah = 0
            saldokartu = 0
            AktifPromo = 0
            ketpromo = ""
            kodemember = ""
            namapemilikkartu = ""
            G_kodekupon = ""
            G_idpromo = 0
            G_ketpromo = ""
            idCust = 0
            G_kuponkalender = 0
            bataltopup = False
            batalnilaivoucher = False
            istopup = False
            isinputkodemember = False
            listitemtopup.Clear()
            FormPOSx.txtTotalBeli.Text = "0"
            isDapatPromo = False
            pakaivouchertopup = False
            If IsNothing(xSet.Tables("datavoucher")) = False Then
                xSet.Tables("datavoucher").Clear()
            End If
            adatopupdenganvoucher = False
            pakaivouchertopup = False
            custbawakartumember = False
            sudahtanyabawamember = False
            isambilpaketpromo = False
            Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            If adatrans = True Then
                If Zcon.State = ConnectionState.Closed Then
                    Zcon.Open()
                End If
                vartr.Rollback()
            End If
        Finally
            conemoney.Dispose()
        End Try
    End Sub

    Private Sub inputJurnalPenjualan(nominalPayment As Double)
        Try
            If checktopup(False) = True Then
                Exit Sub
            End If
            Dim xdtjurnal As String
            countjurnal = countjurnal + 1
            xdtjurnal = countjurnal & "//" & getAccountChild("4110100", xSet.Tables("mcoa")).Select("idCabang='" & My.Settings.idcabang & "'").FirstOrDefault()("idCOA") & "//1//" & nominalPayment
            listjurnal.Rows.Add(xdtjurnal)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub waktusaldo_event(sender As Object, e As EventArgs)
        ReaderStarter()
        If MyACR.CardConnected Then
            MyACR.ClearLCD()
            MyACR.Backlight(False)
            MyACR.Disconnect()
        End If
        waktusaldo.Stop()
    End Sub
    Public Function GenerateRandomString(ByRef iLength As Integer) As String
        Dim rdm As New Random()
        Dim allowChrs() As Char = "ABCDEFGHIJKLMNOPQRSTU0123456789".ToCharArray()
        Dim sResult As String = ""

        For i As Integer = 0 To iLength - 1
            sResult += allowChrs(rdm.Next(0, allowChrs.Length))
        Next

        Return sResult
    End Function


    Function autogenerate(ByVal prefix As String, ByVal subprefix As String, ByVal cekkode As String)
        Try
            Dim syarat As Boolean = False
            Dim cek As String
            Dim kode As String = ""
            While syarat = False


                'If kode <> "" Then
                '    urutan = kode.Substring(kode.Length - 5)
                'Else
                '    urutan = 0
                'End If
                Dim fix As String = GenerateRandomString(5)
                Dim cabang As String = My.Settings.idcabang.ToString
                If My.Settings.idcabang.ToString.Length = 1 Then
                    cabang = "0" & My.Settings.idcabang.ToString
                End If


                'urutan = kode.Substring(kode.Length - 5)
                'urutan = urutan + 1
                'For i = 0 To 4 - urutan.ToString.Length
                '    fix = fix & "0"5
                'Next i
                ' fix = fix & urutan.ToString
                kode = prefix & "/" & subprefix & "/" & cabang & "/" & "01" & "/" & fix

                cek = cekkode & "where kode_voucher='" & kode & "'"
                Dim hasilcek As String = ExDb.Scalar(cek, My.Settings.cloudconn)
                If hasilcek = "" Or hasilcek Is Nothing Then
                    syarat = True
                End If
            End While

            Return kode
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Function

    Private Sub textCardNo_GotFocus(sender As Object, e As EventArgs) Handles textCardNo.GotFocus
        'If klikvoucher = True Then
        'frminputvc.ShowDialog()
        'End If
    End Sub

    Private Sub textTotalBiaya_EditValueChanged(sender As Object, e As EventArgs) Handles textTotalBiaya.EditValueChanged

    End Sub

    Private Sub promobri()
        Dim v_total As Double
        If My.Settings.idWilayah <> "MLG" Then Exit Sub
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where '{0}' >= date(TglMulai) and '{0}' <= date(TglAkhir) and idpromo={1}", Format(Now(), "yyyy-MM-dd"), 4)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            'MsgBox("Promo sudah Expired !")
            AktifPromo = 0
            Exit Sub
        End If

        SQLquery = String.Format("select count(*) from app_promo.mpromo where aktif=1 and idpromo={0}", 4)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            AktifPromo = 0
            MsgBox("Promo sudah Tidak Aktif !")
            Exit Sub
        End If
        If ListBoxControl1.Text = "" Then Exit Sub
        If textBiaya.EditValue = 0 Then Exit Sub

        Select Case ListBoxControl1.Text
            Case "BRIZI", "DEBIT BRI", "KARTU KREDIT BRI"
                MessageBox.Show("TEST")
            Case Else
                Exit Sub
        End Select
        Dim dptdiskon As Boolean = False
        Dim maxdiskon As Integer = (150000 * (10 / 100))
        Select Case ListBoxControl1.Text
            Case "BRIZI"
                dptdiskon = True
            Case Else
                If textBiaya.EditValue < 50000 Then
                    dptdiskon = False
                Else
                    dptdiskon = True
                End If
        End Select

        With FormPOSx
            v_total = textBiaya.EditValue
            If disRpBeforeBRI = disRpBeforeBRI_2 Then
                disRpBeforeBRI = 0
            End If
            If disRpBeforeBRI = 0 Then
                disRpBeforeBRI = .txtDisc.EditValue
                disRpBeforeBRI_2 = .txtDisc.EditValue
            Else
                .txtDisc.EditValue = Abs(disRpBeforeBRI - .txtDisc.EditValue)
                disRpBeforeBRI = .txtDisc.EditValue
            End If
            If disRpBeforeBRI > 0 Then
                .txtDiscPercent.EditValue = (disRpBeforeBRI / v_total) * 100
            Else
                .txtDiscPercent.EditValue = 0
            End If
        End With
        textDisc.EditValue = disRpBeforeBRI
        textTotalBiaya.EditValue = textBiaya.EditValue - textDisc.EditValue
        With FormPOSx
            .txtTotalBeli.EditValue = .GridView1.Columns("hrg").SummaryItem.SummaryValue - .txtDisc.EditValue
        End With
        If dptdiskon Then

            If textTotalBiaya.EditValue > 150000 Then
                textDisc.EditValue = maxdiskon + disRpBeforeBRI
                With FormPOSx
                    .txtDisc.EditValue = maxdiskon + disRpBeforeBRI
                    .txtDiscPercent.EditValue = (.txtDisc.EditValue / textBiaya.EditValue) * 100
                    disRpBeforeBRI = maxdiskon
                End With
            Else
                With FormPOSx
                    .txtDisc.EditValue = (textTotalBiaya.EditValue * (10 / 100)) + disRpBeforeBRI
                    .txtDiscPercent.EditValue = (.txtDisc.EditValue / textBiaya.EditValue) * 100
                    textDisc.EditValue = .txtDisc.EditValue
                    disRpBeforeBRI = (textTotalBiaya.EditValue * (10 / 100))
                End With

            End If
            textTotalBiaya.EditValue = textBiaya.EditValue - textDisc.EditValue
            With FormPOSx
                .txtTotalBeli.EditValue = .GridView1.Columns("hrg").SummaryItem.SummaryValue - .txtDisc.EditValue
            End With
        End If
    End Sub
    Public nilaivcbom As String
    Private Sub ListBoxControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxControl1.SelectedIndexChanged
        Try
            textCardNo.Enabled = True
            butPayAll.Enabled = True
            jmlBayar.Enabled = True
            If IsNothing(xSet.Tables("pricelist")) = True Then
                gridConfigautofree()
            End If

            If IsNothing(ListBoxControl1.SelectedItem) = True Then
                Exit Sub
            End If

            If ListBoxControl1.SelectedItem.ToString = "GEBYAR VOUCHER BELANJA" Then
                Dim x As New nilaivoucher
                x.ShowDialog()


                Dim tampung As String

                For i = 0 To FormPOSx.GridView1.RowCount - 1
                    tampung = tampung & "'" & FormPOSx.GridView1.GetRowCellValue(i, "idBrg") & "'"
                    If i <> FormPOSx.GridView1.RowCount - 1 Then
                        tampung = tampung & ","
                    End If
                Next i

                If IsNothing(xSet.Tables("pricelist")) = False Then
                    xSet.Tables("pricelist").Clear()
                End If
                SQLquery = "select idBrg,namaBrg,price,mk.namakategori2 FROM mbarang mb
join mkategoribrg2 mk on mk.idKategori2=mb.idKategori2
WHERE idBrg IN (" & tampung & ") and 
mk.namakategori2 in ('BREAD','CAKE','DOUGHNUT','PASTRY & PIE','PUDDING','SOES','TRADITIONAL SNACK','MUFFIN','TART SLICE')
order by price asc"

                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "PriceList")

                Dim countpakaivc As Integer = 0
                Dim jumlahitem As Integer = 0
                Dim jumlahbayarvc As Decimal = 0
                For i = 0 To xSet.Tables("pricelist").Rows.Count - 1
                    For j = 0 To FormPOSx.GridView1.RowCount - 1
                        Dim a As String = xSet.Tables("pricelist").Rows(i).Item("namaBrg")
                        Dim b As String = FormPOSx.GridView1.GetRowCellValue(j, "namaBrg")
                        If FormPOSx.GridView1.GetRowCellValue(j, "idBrg").ToString.ToUpper = xSet.Tables("pricelist").Rows(i).Item("idBrg").ToString.ToUpper Then

                            xSet.Tables("pricelist").Rows(i).Item("quantity") = FormPOSx.GridView1.GetRowCellValue(j, "jmlh")
                            jumlahitem = jumlahitem + FormPOSx.GridView1.GetRowCellValue(j, "jmlh")
                            Exit For
                        End If
                    Next j
                Next i
                'cari harga barang terkecil
                countpakaivc = jumlahitem \ 2
                If countpakaivc > nilaivcbom Then
                    countpakaivc = nilaivcbom
                End If
                For i = 0 To xSet.Tables("pricelist").Rows.Count - 1
                    countpakaivc = countpakaivc - xSet.Tables("pricelist").Rows(i).Item("quantity")
                    If countpakaivc <= 0 Then
                        If countpakaivc = 0 Then
                            jumlahbayarvc = jumlahbayarvc + (xSet.Tables("pricelist").Rows(i).Item("price") * (xSet.Tables("pricelist").Rows(i).Item("quantity")))
                        Else
                            jumlahbayarvc = jumlahbayarvc + (xSet.Tables("pricelist").Rows(i).Item("price") * (xSet.Tables("pricelist").Rows(i).Item("quantity") - (countpakaivc * -1)))
                        End If
                        Exit For
                    Else
                        jumlahbayarvc = jumlahbayarvc + (xSet.Tables("pricelist").Rows(i).Item("price") * xSet.Tables("pricelist").Rows(i).Item("quantity"))
                    End If
                Next i
                jmlBayar.Text = jumlahbayarvc
                If CDbl(jmlBayar.Text) = 0 Then
                    MessageBox.Show("Pembayaran dengan voucher tidak valid")
                    butHide_Click(sender, e)
                    Exit Sub
                End If
            ElseIf ListBoxControl1.SelectedItem.ToString = "LARITTA PESTA VOUCHER" Then
                If CDbl(textTotalBiaya.Text) < 50000 Or CDbl(textDisc.Text) > 0 Then
                    MessageBox.Show("Vocher tidak berlaku pada diskon atau nominal di bawah Rp 50.000,-")
                    butHide_Click(sender, e)
                End If
            ElseIf ListBoxControl1.SelectedItem.ToString.ToUpper = "promo kalender mei 2017 - Voucher Rp 50.000".ToUpper Then
                If CDbl(textTotalBiaya.Text) < 100000 Or CDbl(textDisc.Text) > 0 Then
                    MessageBox.Show("Vocher tidak berlaku pada nominal pembelian dibawah Rp 100.000,-")
                    butHide_Click(sender, e)
                End If
                textCardNo.Enabled = False
            ElseIf ListBoxControl1.SelectedItem.ToString.ToUpper = "Voucher Lebaran Rp 25.000,-".ToUpper Then
                If CDbl(textTotalBiaya.Text) < 50000 Or CDbl(textDisc.Text) > 0 Then
                    MessageBox.Show("Vocher tidak berlaku pada nominal pembelian dibawah Rp 50.000,-")
                    butHide_Click(sender, e)
                End If
            ElseIf ListBoxControl1.SelectedItem.ToString.ToUpper = "Discount 10% Cookies - Promo Kalender Juni 2017".ToUpper Then
                textCardNo.Enabled = False
                butPayAll.Enabled = False
                Dim totaltransaksicookies As Double = 0
                For i = 0 To FormPOSx.GridView1.RowCount - 1
                    If kategoribrg(FormPOSx.GridView1.GetRowCellValue(i, "idBrg")) = "COOKIES".ToUpper Then
                        totaltransaksicookies = totaltransaksicookies + (FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs") * FormPOSx.GridView1.GetRowCellValue(i, "jmlh"))
                    End If
                Next i
                jmlBayar.Text = totaltransaksicookies * (10 / 100)
            ElseIf ListBoxControl1.SelectedItem.ToString.ToUpper = "PROMO DISCOUNT COOKIES LEBARAN 2017".ToUpper Then
                butPayAll.Enabled = False
                textCardNo.Enabled = False
                Dim totaltransaksicookies As Double = 0
                Dim jumlahcookies As Integer = 0
                For i = 0 To FormPOSx.GridView1.RowCount - 1
                    If kategoribrg(FormPOSx.GridView1.GetRowCellValue(i, "idBrg")) = "COOKIES".ToUpper Then
                        jumlahcookies = jumlahcookies + FormPOSx.GridView1.GetRowCellValue(i, "jmlh")
                        totaltransaksicookies = totaltransaksicookies + (FormPOSx.GridView1.GetRowCellValue(i, "hrgpcs") * FormPOSx.GridView1.GetRowCellValue(i, "jmlh"))
                    End If
                Next i
                If jumlahcookies > 2 And jumlahcookies < 5 Then
                    jmlBayar.Text = totaltransaksicookies * (5 / 100)
                ElseIf jumlahcookies > 2 And jumlahcookies >= 5 Then
                    jmlBayar.Text = totaltransaksicookies * (10 / 100)
                Else
                    jmlBayar.Text = 0
                End If
            ElseIf ListBoxControl1.SelectedItem.ToString.ToUpper = "VOUCHER TOP UP".ToUpper Then
                butPayAll.Enabled = False
                jmlBayar.Enabled = False

            End If

            'cek apakah pembayaran top up ada yang menggunakan voucher selain voucher top up
            'If klikvoucher = True Then

            '    If ListBoxControl1.SelectedItem.ToString.ToUpper <> "VOUCHER TOP UP".ToUpper Then
            '        MessageBox.Show("Transaksi tidak dapat dilakukan dengan voucher ini!", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '        butHide_Click(sender, e)
            '        Exit Sub
            '    End If
            'End If



        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try


    End Sub

    'grid untuk update voucher offline
    Private Sub gridvoucheroffline()
        xSet.Tables.Add("listvoucheroffline")
        xSet.Tables("listvoucheroffline").Columns.Add("createDate", Type.GetType("System.String"))
        xSet.Tables("listvoucheroffline").Columns.Add("done", Type.GetType("System.String"))
        xSet.Tables("listvoucheroffline").Columns.Add("kode_voucher", Type.GetType("System.String"))
    End Sub

    'grid untuk auto
    Private Sub gridConfigautofree()
        xSet.Tables.Add("PriceList")
        xSet.Tables("PriceList").Columns.Add("namaBrg", Type.GetType("System.String"))
        xSet.Tables("PriceList").Columns.Add("price", Type.GetType("System.Decimal"))
        xSet.Tables("PriceList").Columns.Add("quantity", Type.GetType("System.Decimal"))
    End Sub

    'grid untuk cek item BS
    Private Sub gridlistbs()
        xSet.Tables.Add("listbs")
        xSet.Tables("listbs").Columns.Add("namaBrg", Type.GetType("System.String"))
    End Sub

    Private Sub paymenttopuup_Click(sender As Object, e As EventArgs) Handles paymenttopuup.Click
        'e-money
        If ishavemember = False Then
            MessageBox.Show("Pembayaran Menggunakan Laritta Cash Harus Menggunakan Kartu Member", "Payment Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If
        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            selectedvoucher = 0
            LabelControl6.Visible = False
            textCardNo.Visible = False
            getMPayment(6)
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If My.Settings.idWilayah = "MLG" Then
            Dim result As Integer = MessageBox.Show("Apakah transaksi menggunakan promo?", "Konfirmasi Promo", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                Dim x As New promo10rbmalang
                x.ShowDialog()
            End If
        End If

        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            selectedvoucher = 0
            LabelControl6.Visible = True
            textCardNo.Visible = True
            getMPayment2(2, True)
            promobri()
        End If
    End Sub

    Private Sub isgojek_CheckedChanged(sender As Object, e As EventArgs) Handles isgojek.CheckedChanged
        Try
            If isgojek.Checked = True Then
                isgojek.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
                isgojek.Appearance.BackColor = Color.GreenYellow
            Else
                isgojek.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ListBoxControl1_Click(sender As Object, e As EventArgs) Handles ListBoxControl1.Click
        promobri()
    End Sub

    Private Sub debitCard_Click(sender As Object, e As EventArgs) Handles debitCard.Click
        'debit card
        If CInt(textTotalBayar.Text) <= CInt(textTotalBiaya.EditValue) Then
            selectedvoucher = 0
            LabelControl6.Visible = True
            textCardNo.Visible = True
            getMPayment2(4, False)
        End If
    End Sub

    Public Sub masterpromo(jenisPromo As String)
        If isDapatPromo Then
            Exit Sub
        End If

        'cek apakah customer membawa kartu member
        If kodemember <> "" And klikvoucher = False And sudahtanyabawamember = False Then
            sudahtanyabawamember = True
            Dim result As Integer = MessageBox.Show("Apakah customer membawa member?", "Konfirmasi Member", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                custbawakartumember = True
            End If
        End If

        'data promo top up
        If IsNothing(xSet.Tables("listparameterpromotopup")) Then
            xSet.Tables.Add("listparameterpromotopup")
        Else
            xSet.Tables("listparameterpromotopup").Clear()
        End If

        If IsNothing(xSet.Tables("listpromoaktif")) Then
            xSet.Tables.Add("listpromoaktif")
        Else
            xSet.Tables("listpromoaktif").Clear()
        End If

        'data detail promo penggunaan top up
        If IsNothing(xSet.Tables("listpromoemoney")) Then
            xSet.Tables.Add("listpromoemoney")
            xSet.Tables("listpromoemoney").Columns.Add("idpromo")
        Else
            xSet.Tables("listpromoemoney").Clear()
        End If

        'data syarat item promo
        If IsNothing(xSet.Tables("listrewardsyarat")) Then
            xSet.Tables.Add("listrewardsyarat")
        Else
            xSet.Tables("listrewardsyarat").Clear()
        End If

        'variable umum
        Dim totalpembayaranbaru As Double = 0
        Dim jumlahitemsesuaisyarat As Integer = 0
        Dim nominalitemsesuaisyarat As Double = 0
        Dim stringbrg As String = ""
        Dim perubahanharga As Boolean = False
        Dim perubahanhargatotaltransaksi As Boolean = False

        SQLquery = String.Format("select mt.idpromo from mpromo_topup mt where idjenis_promo='{0}' and mt.inactive=0 AND mt.tanggal_aktif_awal<=DATE (now()) AND mt.tanggal_aktif_akhir>=DATE (now())", jenisPromo)
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listpromoaktif")

        If xSet.Tables("listpromoaktif").Rows.Count = 0 Then
            Exit Sub
        End If

        'cek setiap parameter
        For a = 0 To xSet.Tables("listpromoaktif").Rows.Count - 1
            stringbrg = ""
            Dim syaratselesai As Integer = 0
            perubahanharga = False
            SQLquery = String.Format("select idparameter_promo,value,tipe_parameter from parameter_promo pp where idpromo={0}", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))
            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listparameterpromotopup")

            'sortir apakah promo perlu membawa member
            If xSet.Tables("listparameterpromotopup").Select("tipe_parameter='3' and idparameter_promo='30' and value='1'").Count > 0 And custbawakartumember = False Then
                Continue For
            End If

            'sortir parameter syarat
            Dim listparametertopup() As DataRow = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=0")
            'cek parameter syarat

            'variable untuk cek cabang
            Dim cekparametercabang As Boolean = False
            For Each parameter As DataRow In listparametertopup
                'cek parameter cabang
                If parameter("idparameter_promo").ToString = "8" And cekparametercabang = False Then
                    cekparametercabang = True
                    If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='8' and value='" & My.Settings.idcabang & "'").Count > 0 Then
                        syaratselesai = syaratselesai + 1
                    End If
                ElseIf parameter("idparameter_promo").ToString = "5" Then
                    If parameter("value").ToString = "1" Then
                        Dim syaratawal As Integer = syaratselesai
                        If IsNothing(xSet.Tables("listitemsyarat")) Then
                            xSet.Tables.Add("listitemsyarat")
                        Else
                            xSet.Tables("listitemsyarat").Clear()
                        End If

                        'tabel untuk menyimpan data barang yang memenuhi syarat promo
                        If IsNothing(xSet.Tables("listbarangpromopaket")) Then
                            xSet.Tables.Add("listbarangpromopaket")
                        Else
                            xSet.Tables("listbarangpromopaket").Clear()
                        End If

                        jumlahitemsesuaisyarat = 0
                        nominalitemsesuaisyarat = 0

                        'list barang yang dibeli
                        If stringbrg.Length = 0 Then
                            For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                                stringbrg = stringbrg & xSet.Tables("setSales").Rows(i).Item("idBrg").ToString
                                If i < xSet.Tables("setSales").Rows.Count - 1 Then
                                    stringbrg = stringbrg & ","
                                End If
                            Next i
                        End If

                        SQLquery = String.Format("SELECT *,if(jenis_item=0,idKategori2,idBrg) `iditem`
FROM (
  SELECT jenis_item,jenis_value,value_minimum,idBrg,namaBrg,mb.price
FROM syarat_item_transaksi si
JOIN mbarang mb ON mb.idBrg=si.iditem AND jenis_item=1
    WHERE idpromo='{0}'
UNION ALL
SELECT jenis_item,jenis_value,value_minimum,idBrg,namaBrg,mb.price
FROM syarat_item_transaksi si
JOIN mkategoribrg2 mk ON mk.idKategori2=si.iditem AND jenis_item=0
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
  WHERE idpromo='{0}'
) z WHERE idBrg IN ({1})", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"), stringbrg)
                        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarangpromopaket")
                        'cek apakah value minimum sesuai dengan jumlah item

                        'list syarat
                        SQLquery = String.Format("SELECT iditem,jenis_item,jenis_value,value_minimum,0 `JumlahNota` FROM syarat_item_transaksi si WHERE idpromo='{0}'", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo").ToString)
                        If IsNothing(xSet.Tables("syaratitem")) Then
                            xSet.Tables.Add("syaratitem")
                        Else
                            xSet.Tables("syaratitem").Clear()
                        End If
                        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "syaratitem")

                        Dim jumlahitempaketmemenuhisyarat As Integer = 0
                        Dim paketitem As Integer = ExDb.Scalar("SELECT count(*) FROM syarat_item_transaksi si WHERE idpromo='" & xSet.Tables("listpromoaktif").Rows(a).Item("idpromo").ToString & "'", conn_string_local)
                        For y = 0 To xSet.Tables("listbarangpromopaket").Rows.Count - 1
                            Dim databarangpromo As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangpromo").Rows(y).Item("idBrg").ToString & "'").FirstOrDefault()
                            Dim datasyarat() As DataRow
                            If xSet.Tables("listbarangpromo").Rows(y).Item("jenis_item").ToString = "0" Then
                                datasyarat = xSet.Tables("syaratitem").Select("jenis_item='0' and iditem='" & xSet.Tables("listbarangpromo").Rows(y).Item("iditem").ToString & "'")
                                datasyarat.FirstOrDefault()("JumlahNota") = CInt(datasyarat.FirstOrDefault()("JumlahNota")) + CInt(databarangpromo("jmlh"))
                            ElseIf xSet.Tables("listbarangpromo").Rows(y).Item("jenis_item").ToString = "1" Then
                                datasyarat = xSet.Tables("syaratitem").Select("jenis_item='1' and iditem='" & xSet.Tables("listbarangpromo").Rows(y).Item("iditem").ToString & "'")
                                datasyarat.FirstOrDefault()("JumlahNota") = CInt(datasyarat.FirstOrDefault()("JumlahNota")) + CInt(databarangpromo("jmlh"))
                            End If

                            If xSet.Tables("listbarangpromopaket").Select("JumlahNota < value_minimum").Count = 0 Then
                                syaratselesai = syaratselesai + 1
                            End If
                        Next y

                        'cek syarat minimal (nominal atau jumlah item)
                        If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 Then
                            'cek minimal berdasarkan jumlah item
                            For y = 0 To xSet.Tables("listbarangpromo").Rows.Count - 1
                                Dim databarangpromo As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangpromo").Rows(y).Item("idBrg").ToString & "'").FirstOrDefault()
                                jumlahitemsesuaisyarat = jumlahitemsesuaisyarat + CInt(databarangpromo("jmlh"))
                            Next y
                            If jumlahitemsesuaisyarat >= CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value")) Then
                                syaratselesai = syaratselesai + 1
                            End If
                        ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='23' and value='1'").Count > 0 Then
                            'cek minimal berdasarkan nominal
                            For y = 0 To xSet.Tables("listbarangpromo").Rows.Count - 1
                                Dim databarangpromo As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangpromo").Rows(y).Item("idBrg").ToString & "'").FirstOrDefault()
                                nominalitemsesuaisyarat = nominalitemsesuaisyarat + CInt(databarangpromo("hrg"))
                            Next y
                            If nominalitemsesuaisyarat >= CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value")) Then
                                syaratselesai = syaratselesai + 1
                            End If
                        Else
                            If xSet.Tables("listbarangpromo").Rows.Count > 0 Then
                                syaratselesai = syaratselesai + 1
                            End If
                        End If
                    End If
                ElseIf parameter("idparameter_promo").ToString = "6" Then
                    If parameter("value").ToString = "1" Then
                        Dim syaratawal As Integer = syaratselesai
                        If IsNothing(xSet.Tables("listitemsyarat")) Then
                            xSet.Tables.Add("listitemsyarat")
                        Else
                            xSet.Tables("listitemsyarat").Clear()
                        End If

                        'tabel untuk menyimpan data barang yang memenuhi syarat promo
                        If IsNothing(xSet.Tables("listbarangpromo")) Then
                            xSet.Tables.Add("listbarangpromo")
                            xSet.Tables("listbarangpromo").Columns.Add("iditem")
                            xSet.Tables("listbarangpromo").Columns.Add("jenis_item")
                            xSet.Tables("listbarangpromo").Columns.Add("qty")
                            xSet.Tables("listbarangpromo").Columns.Add("hargapcs")
                        Else
                            xSet.Tables("listbarangpromo").Clear()
                        End If

                        jumlahitemsesuaisyarat = 0
                        nominalitemsesuaisyarat = 0

                        'list barang yang dibeli
                        If stringbrg.Length = 0 Then
                            For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                                stringbrg = stringbrg & xSet.Tables("setSales").Rows(i).Item("idBrg").ToString
                                If i < xSet.Tables("setSales").Rows.Count - 1 Then
                                    stringbrg = stringbrg & ","
                                End If
                            Next i
                        End If

                        SQLquery = String.Format("SELECT *
FROM (
  SELECT jenis_item,jenis_value,value_minimum,idBrg,namaBrg,mb.price
FROM syarat_item_transaksi si
JOIN mbarang mb ON mb.idBrg=si.iditem AND jenis_item=1
    WHERE idpromo='{0}'
UNION ALL
SELECT jenis_item,jenis_value,value_minimum,idBrg,namaBrg,mb.price
FROM syarat_item_transaksi si
JOIN mkategoribrg2 mk ON mk.idKategori2=si.iditem AND jenis_item=0
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
  WHERE idpromo='{0}'
) z WHERE idBrg IN ({1})", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"), stringbrg)
                        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarangpromo")

                        'cek syarat minimal (nominal atau jumlah item)
                        If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 Then
                            'cek minimal berdasarkan jumlah item
                            For y = 0 To xSet.Tables("listbarangpromo").Rows.Count - 1
                                Dim databarangpromo As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangpromo").Rows(y).Item("idBrg").ToString & "'").FirstOrDefault()
                                jumlahitemsesuaisyarat = jumlahitemsesuaisyarat + CInt(databarangpromo("jmlh"))
                            Next y
                            If jumlahitemsesuaisyarat >= CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value")) Then
                                syaratselesai = syaratselesai + 1
                            End If
                        ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='23' and value='1'").Count > 0 Then
                            'cek minimal berdasarkan nominal
                            For y = 0 To xSet.Tables("listbarangpromo").Rows.Count - 1
                                Dim databarangpromo As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangpromo").Rows(y).Item("idBrg").ToString & "'").FirstOrDefault()
                                nominalitemsesuaisyarat = nominalitemsesuaisyarat + CInt(databarangpromo("hrg"))
                            Next y
                            If nominalitemsesuaisyarat >= CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value")) Then
                                syaratselesai = syaratselesai + 1
                            End If
                        Else
                            If xSet.Tables("listbarangpromo").Rows.Count > 0 Then
                                syaratselesai = syaratselesai + 1
                            End If
                        End If
                    End If
                ElseIf parameter("idparameter_promo").ToString = "11" Then 'nominal transaksi minimum
                    totalvalid = 0
                    'sortir item BS
                    If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='21'").Count > 0 Then
                        For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                            If kategoribrg(xSet.Tables("setSales").Rows(i).Item("idBrg").ToString).ToString.Substring(0, 3) <> "BS " Then
                                totalvalid = totalvalid + CDbl(xSet.Tables("setSales").Rows(i).Item("hrg"))
                            End If
                        Next i
                    Else
                        totalvalid = CDbl(textTotalBiaya.Text)
                    End If

                    If totalvalid >= CDbl(parameter("value")) Then
                        syaratselesai = syaratselesai + 1
                    End If
                ElseIf parameter("idparameter_promo").ToString = "27" Then
                    Dim range() As String = parameter("value").ToString.Split("-")
                    'sortir item BS
                    Dim totalvalid As Double = 0
                    If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='21'").Count > 0 Then
                        For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                            If kategoribrg(xSet.Tables("setSales").Rows(i).Item("idBrg").ToString).ToString.Substring(0, 3) <> "BS " Then
                                totalvalid = totalvalid + CDbl(xSet.Tables("setSales").Rows(i).Item("hrg"))
                            End If
                        Next i
                    Else
                        totalvalid = CDbl(textTotalBiaya.Text)
                    End If

                    If totalvalid >= CDbl(range(0)) And totalvalid <= CDbl(range(1)) Then
                        syaratselesai = syaratselesai + xSet.Tables("listparameterpromotopup").Select("idparameter_promo='27'").Count
                    End If
                ElseIf parameter("idparameter_promo").ToString = "30" Then
                    Dim result As Integer = MessageBox.Show("Apakah customer membawa potongan kalender?", "Promo Kalender", MessageBoxButtons.YesNo)
                    If result = DialogResult.Yes Then
                        syaratselesai = syaratselesai + 1
                    End If
                ElseIf parameter("idparameter_promo").ToString = "31" Then
                    If xSet.Tables("gridPembayaran").Select("idPayment='" & parameter("idparameter_promo").ToString & "'").Count > 0 Then
                        syaratselesai = syaratselesai + 1
                    End If
                ElseIf parameter("idparameter_promo").ToString = "36" Then
                    totalvalid = 0
                    SQLquery = String.Format("SELECT iditem,jenis_value FROM syarat_item_transaksi si WHERE idpromo='{0}'", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))
                    ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listrewardsyarat")
                    For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                        For k = 0 To xSet.Tables("listrewardsyarat").Rows.Count - 1
                            If idkategori2barang(xSet.Tables("setSales").Rows(i).Item("idBrg").ToString).ToString = xSet.Tables("listrewardsyarat").Rows(k).Item("iditem").ToString And xSet.Tables("listrewardsyarat").Rows(k).Item("jenis_value").ToString = "0" Then
                                totalvalid = totalvalid + CDbl(xSet.Tables("setSales").Rows(i).Item("hrg"))
                            ElseIf xSet.Tables("setSales").Rows(i).Item("idBrg").ToString = xSet.Tables("listrewardsyarat").Rows(k).Item("iditem").ToString And xSet.Tables("listrewardsyarat").Rows(k).Item("jenis_value").ToString = "1" Then
                                totalvalid = totalvalid + CDbl(xSet.Tables("setSales").Rows(i).Item("hrg"))
                            End If
                        Next k
                    Next i
                    If parameter("value") <= totalvalid Then
                        syaratselesai = syaratselesai + 1
                    End If
                End If
            Next parameter

            'list parameter reward

            If syaratselesai = listparametertopup.Count Then
                'sortir parameter reward
                listparametertopup = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=1")
                'cek parameter reward
                For Each parameter As DataRow In listparametertopup
                    'eksekusi tiap parameter (parameter pemakaian laritta cash)

                    'eksekusi parameter reward
                    If parameter("idparameter_promo").ToString = "2" And parameter("value").ToString = "1" Then
                        'proses memberi diskon
                        Dim kelipatan As Integer = 0
                        If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").Count > 0 Then
                            'mendiskon barang yang termurah
                            Dim itemyangdidiskon As Integer
                            If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then
                                kelipatan = jumlahitemsesuaisyarat \ CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value"))
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                                itemyangdidiskon = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value")) * kelipatan
                            ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                kelipatan = CDbl(textTotalBiaya.EditValue) \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value"))
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                                itemyangdidiskon = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value")) * kelipatan
                            Else
                                itemyangdidiskon = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value"))
                            End If
                            If stringbrg.Length = 0 Then
                                For i = 0 To xSet.Tables("setSales").Rows.Count - 1
                                    stringbrg = stringbrg & xSet.Tables("setSales").Rows(i).Item("idBrg").ToString
                                    If i < xSet.Tables("setSales").Rows.Count - 1 Then
                                        stringbrg = stringbrg & ","
                                    End If
                                Next i
                            End If

                            SQLquery = String.Format("SELECT jenis_item,if(substring(harga_item,length(harga_item),1)='%',(price*(replace(harga_item,'%','')/100)),harga_item) `harga_item`,idBrg,namaBrg,price
FROM (
  SELECT jenis_item,harga_item ,idBrg,namaBrg,mb.price
FROM reward_item_promo si
JOIN mbarang mb ON mb.idBrg=si.iditem AND jenis_item=1
    WHERE idpromo='{0}'
UNION ALL
SELECT jenis_item,harga_item ,idBrg,namaBrg,mb.price
FROM reward_item_promo si
JOIN mkategoribrg2 mk ON mk.idKategori2=si.iditem AND jenis_item=0
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
  WHERE idpromo='{0}'
) z WHERE idBrg IN ({1}) ORDER BY price ASC", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"), stringbrg)

                            If IsNothing(xSet.Tables("listbarangreward")) Then
                                xSet.Tables.Add("listbarangreward")
                            Else
                                xSet.Tables("listbarangreward").Clear()
                            End If
                            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarangreward")

                            For k = 0 To xSet.Tables("listbarangreward").Rows.Count - 1
                                Dim barangdiskonyangdibeli As DataRow = xSet.Tables("setSales").Select("idBrg='" & xSet.Tables("listbarangreward").Rows(k).Item("idBrg").ToString & "'").FirstOrDefault()

                                Dim jumlahitemhargaasli As Integer = 0
                                'input item dengan harga diskon
                                If itemyangdidiskon >= CInt(barangdiskonyangdibeli("jmlh")) Then
                                    jumlahitemhargaasli = 0
                                    itemyangdidiskon = itemyangdidiskon - CInt(barangdiskonyangdibeli("jmlh"))
                                    xSet.Tables("setSales").Rows.Add(barangdiskonyangdibeli("idBrg"), barangdiskonyangdibeli("namaBrg"), barangdiskonyangdibeli("jmlh"), CDbl(xSet.Tables("listbarangreward").Rows(k).Item("harga_item")) * CInt(barangdiskonyangdibeli("jmlh")), CDbl(xSet.Tables("listbarangreward").Rows(k).Item("harga_item")), 0)
                                ElseIf itemyangdidiskon < CInt(barangdiskonyangdibeli("jmlh")) Then
                                    jumlahitemhargaasli = CInt(barangdiskonyangdibeli("jmlh")) - itemyangdidiskon
                                    xSet.Tables("setSales").Rows.Add(barangdiskonyangdibeli("idBrg"), barangdiskonyangdibeli("namaBrg"), itemyangdidiskon, CDbl(xSet.Tables("listbarangreward").Rows(k).Item("harga_item")) * itemyangdidiskon, CDbl(xSet.Tables("listbarangreward").Rows(k).Item("harga_item")), 0)
                                    itemyangdidiskon = 0
                                End If

                                'input item dengan harga normal
                                If jumlahitemhargaasli = 0 Then
                                    'delete item yang di diskon semua
                                    xSet.Tables("setSales").Rows.Remove(barangdiskonyangdibeli)
                                Else
                                    barangdiskonyangdibeli("jmlh") = jumlahitemhargaasli
                                    barangdiskonyangdibeli("hrg") = jumlahitemhargaasli * CInt(barangdiskonyangdibeli("hrgpcs"))
                                End If

                                If itemyangdidiskon = 0 Then
                                    Exit For
                                End If
                            Next k
                        Else
                            'ambil barang reward
                            If IsNothing(xSet.Tables("listreward")) Then
                                xSet.Tables.Add("listreward")
                            Else
                                xSet.Tables("listreward").Clear()
                            End If
                            SQLquery = String.Format("SELECT iditem,if(jenis_item=0,mk.namaKategori2,mb.namaBrg) `RewardItem`,jenis_item,harga_item
                FROM reward_item_promo ri
                 LEFT JOIN mbarang mb ON mb.idBrg=ri.iditem
                LEFT JOIN mkategoribrg2 mk ON mk.idKategori2=ri.iditem
                WHERE ri.idpromo='{0}'", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))

                            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listreward")

                            For j = 0 To xSet.Tables("setSales").Rows.Count - 1
                                For k = 0 To xSet.Tables("listreward").Rows.Count - 1
                                    'membuat harga baru dari item barang
                                    If xSet.Tables("setSales").Rows(j).Item("idBrg").ToString = xSet.Tables("listreward").Rows(k).Item("iditem").ToString And xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "1" Then
                                        'If xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                        If xSet.Tables("setSales").Rows(j).Item("idBrg").ToString = xSet.Tables("listreward").Rows(k).Item("iditem").ToString Then
                                            If xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Contains("%") Then
                                                Dim nilaipersen() As String = xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Split("%")
                                                xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * (CInt(nilaipersen(0)) / 100)
                                                xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                            Else
                                                xSet.Tables("setSales").Rows(j).Item("hrgpcs") = xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.repPointSep
                                                xSet.Tables("setSales").Rows(j).Item("hrg") = xSet.Tables("setSales").Rows(j).Item("hrgpcs").ToString.repPointSep * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                            End If
                                        End If

                                        'membuat harga baru dari item kategori
                                    ElseIf kategoribarang(xSet.Tables("setSales").Rows(j).Item("idBrg").ToString).ToString = xSet.Tables("listreward").Rows(k).Item("iditem").ToString And xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                        'If xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                        If kategoribrg(xSet.Tables("setSales").Rows(j).Item("idBrg").ToString).ToString = xSet.Tables("listreward").Rows(k).Item("RewardItem").ToString Then
                                            If xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Contains("%") Then
                                                Dim nilaipersen() As String = xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Split("%")
                                                xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * (CInt(nilaipersen(0)) / 100)
                                                xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                            Else
                                                xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("listreward").Rows(k).Item("harga_item"))
                                                xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                            End If

                                        End If
                                    End If
                                Next k
                                totalpembayaranbaru = totalpembayaranbaru + CDbl(xSet.Tables("setSales").Rows(j).Item("hrg"))
                                perubahanharga = True
                            Next j
                        End If
                        isDapatPromo = True
                    ElseIf parameter("idparameter_promo").ToString = "29" And parameter("value").ToString = "1" Then 'print voucher emoney
                        If TestOnline() = True Then
                            Dim voucher As String
                            Dim nominalvoucher As Double
                            Dim kelipatan As Integer = 0
                            If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                kelipatan = CDbl(textTotalBiaya.EditValue) \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value"))
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                            ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                kelipatan = jumlahitemsesuaisyarat \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value"))
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                            End If

                            Dim datavoucher() As DataRow = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=4")
                            'printing voucher

                            Dim barangtoptup As String = ""
                            'print voucher sesuai syarat
                            For y = 0 To kelipatan - 1
                                Dim printvc As XtraReport = New voucher_retail
                                printvc.ShowPrintStatusDialog = False
                                For Each paramvc As DataRow In datavoucher
                                    If paramvc("idparameter_promo") = "17" Then
                                        printvc.Parameters("tipevoucher").Value = paramvc("value")
                                    ElseIf paramvc("idparameter_promo") = "20" Then
                                        printvc.Parameters("keteranganVoucher").Value = paramvc("value")
                                    ElseIf paramvc("idparameter_promo") = "9" Then
                                        barangtoptup = paramvc("value").ToString
                                        nominalvoucher = ExDb.Scalar(String.Format("SELECT nominal_topup FROM mnominal_topup WHERE idbrg='{0}'", paramvc("value").ToString), conn_string_local)
                                    End If
                                Next paramvc

                                voucher = autogenerate("VCR", "ECA", "select kode_voucher from app_promo.voucher_emoney ")
                                SQLquery = String.Format("INSERT INTO app_promo.voucher_emoney (kode_voucher, idCabang, is_use, createdate, updateDate, idOrder, del, nominal_voucher, jenis_voucher, useDate, cetakanke, idCabangUse, idbrg_topup, uniqueID)
                                VALUES('{0}','{1}','0',now(),NULL ,'{2}',0,'{3}',1,NULL ,1,NULL ,'{4}',NULL)", voucher, My.Settings.idcabang, idOrder, nominalvoucher, barangtoptup)
                                ExDb.ExecData(SQLquery, My.Settings.cloudconn)

                                'ambil data voucher yang sudah di insert
                                SQLquery = String.Format("SELECT * FROM app_promo.voucher_emoney WHERE kode_voucher='{0}'", voucher)
                                If IsNothing(xSet.Tables("datavoucher")) Then
                                    xSet.Tables.Add("datavoucher")
                                Else
                                    xSet.Tables("datavoucher").Clear()
                                End If
                                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "datavoucher")


                                printvc.Parameters("kodevoucher").Value = voucher
                                printvc.Parameters("cetakanke").Value = "Cetakan ke-1"
                                printvc.Parameters("reward").Value = "Voucher Top Up Rp." & Format(CDbl(xSet.Tables("datavoucher").Rows(0).Item("nominal_voucher")), "#,###") & ",-"
                                printvc.Parameters("alamat").Value = ExDb.Scalar("Select alamatCabang from mcabang where idCabang='" & My.Settings.idcabang & "'", My.Settings.db_conn_mysql)
                                Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printvc)
                                    'tool.PreviewForm.WindowState = FormWindowState.Maximized
                                    'tool.ShowPreviewDialog()
                                    tool.Print()
                                End Using
                            Next y
                        End If
                        isDapatPromo = True
                    ElseIf parameter("idparameter_promo").ToString = "13" And parameter("value").ToString = "1" Then
                        If TestOnline() = True Then
                            Dim voucher As String
                            Dim nominalvoucher As Double
                            Dim kelipatan As Integer = 0
                            If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                kelipatan = CDbl(textTotalBiaya.EditValue) \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value"))
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                            ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then
                                Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                                kelipatan = jumlahitemsesuaisyarat \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value"))
                                If querydt.Count > 0 Then
                                    If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                        kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                    End If
                                End If
                            End If

                            If xSet.Tables("listparameterpromotopup").Select("tipe_parameter='1' and idparameter_promo='16' and value='1'").Count > 0 Then

                                Dim datavoucher() As DataRow = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=1 and idparameter_promo<>'13'")
                                'printing voucher

                                Dim tanggalexp As String = ""
                                Dim minimumtransaksi As Double = 0

                                'print voucher sesuai syarat
                                For y = 0 To kelipatan - 1
                                    Dim printvc As XtraReport = New voucher_retail
                                    printvc.ShowPrintStatusDialog = False
                                    For Each paramvc As DataRow In datavoucher
                                        If paramvc("idparameter_promo") = "14" Then
                                            nominalvoucher = CDbl(CDbl(paramvc("value")))
                                            printvc.Parameters("reward").Value = "Voucher Rp." & Format(nominalvoucher, "#,###") & ",-"
                                        ElseIf paramvc("idparameter_promo") = "15" Then
                                            printvc.Parameters("expvoucher").Value = "Valid until " & Date.Now.AddDays(CDbl(paramvc("value"))).ToString("dd-MM-yyyy")
                                            tanggalexp = Date.Now.AddDays(CDbl(paramvc("value"))).ToString("yyyyMMdd")
                                        ElseIf paramvc("idparameter_promo") = "17" Then
                                            printvc.Parameters("tipevoucher").Value = paramvc("value")
                                        ElseIf paramvc("idparameter_promo") = "19" Then
                                            minimumtransaksi = CDbl(paramvc("value"))
                                        ElseIf paramvc("idparameter_promo") = "20" Then
                                            printvc.Parameters("keteranganVoucher").Value = paramvc("value")
                                        End If
                                    Next paramvc

                                    voucher = autogenerate("VCR", "NOM", "select kode_voucher from app_promo.voucher_nominal ")
                                    printvc.Parameters("kodevoucher").Value = voucher
                                    printvc.Parameters("cetakanke").Value = "Cetakan ke-1"
                                    printvc.Parameters("alamat").Value = ExDb.Scalar("Select alamatCabang from mcabang where idCabang='" & My.Settings.idcabang & "'", My.Settings.localconn)
                                    SQLquery = "insert into app_promo.voucher_nominal (kode_voucher,idCabang,is_use,createdate,idOrder,del,nominal_voucher,jenis_voucher,cetakanke,exp_date,minimal_nominal_transaksi) values ('" & voucher & "','" & My.Settings.idcabang & "','0',now(),'" & numOrder & "','0'," & nominalvoucher & ",'1',1,'" & tanggalexp & "','" & minimumtransaksi & "')"
                                    ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printvc)
                                        'tool.PreviewForm.WindowState = FormWindowState.Maximized
                                        tool.ShowPreviewDialog()
                                        'tool.Print()
                                    End Using
                                Next y

                            End If
                        End If
                        isDapatPromo = True
                    ElseIf parameter("idparameter_promo").ToString = "32" And parameter("value").ToString = "1" Then
                        Dim x As New listBarangPromo
                        x.ShowDialog()
                        If cancelambilbarangpromo = False Then
                            isDapatPromo = True
                        End If

                    ElseIf parameter("idparameter_promo").ToString = "34" And parameter("value").ToString = "1" Then
                        listBarangPromo.GenerateReward(String.Format("SELECT *
FROM m_reward_promo mrp
WHERE mrp.idpromo='{0}' AND inactive=0", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo").ToString), xSet.Tables("listpromoaktif").Rows(a).Item("idpromo").ToString)
                        If cancelambilbarangpromo = False Then
                            isDapatPromo = True
                        End If
                    ElseIf parameter("idparameter_promo").ToString = "38" Then
                        Dim totaldiskontransaksi As Double
                        Dim kelipatan As Double
                        If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then
                            kelipatan = totalvalid \ CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='22'").FirstOrDefault()("value"))
                            Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                            If querydt.Count > 0 Then
                                If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                    kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                End If
                            End If
                            totaldiskontransaksi = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value")) * kelipatan
                        ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                            Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                            kelipatan = totalvalid \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value"))
                            If querydt.Count > 0 Then
                                If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                    kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                End If
                            End If
                            totaldiskontransaksi = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value")) * kelipatan
                        ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='36'").Count > 0 And xSet.Tables("listparameterpromotopup").Select("idparameter_promo='37'").Count > 0 Then
                            Dim querydt() As DataRow = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'")
                            kelipatan = totalvalid \ CDbl(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='36'").FirstOrDefault()("value"))
                            If querydt.Count > 0 Then
                                If kelipatan > CInt(querydt.FirstOrDefault()("value")) Then
                                    kelipatan = CInt(querydt.FirstOrDefault()("value"))
                                End If
                            End If
                            totaldiskontransaksi = parameter("value") * kelipatan
                        Else
                            totaldiskontransaksi = CInt(xSet.Tables("listparameterpromotopup").Select("idparameter_promo='24'").FirstOrDefault()("value"))
                        End If
                        textDisc.EditValue = totaldiskontransaksi
                        perubahanhargatotaltransaksi = True
                        isDapatPromo = True
                    End If

                Next parameter
            End If
            'input promo yang digunakan
            xSet.Tables("listpromoemoney").Rows.Add(xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))

            'reset parameter promo yang aktif
            xSet.Tables("listparameterpromotopup").Clear()
        Next a

        If xSet.Tables("listpromoaktif").Rows.Count > 0 And perubahanharga = True And jenisPromo = "2" Then
            jumlahpembayaranemoney = totalpembayaranbaru
            textTotalBiaya.EditValue = totalpembayaranbaru
            textBiaya.EditValue = totalpembayaranbaru
            textTotalBayar.EditValue = totalpembayaranbaru
            Dim kembalian As Double = CDbl(textTotalBayar.EditValue) - totalpembayaranbaru
            textKembalian.EditValue = kembalian
            xSet.Tables("gridPembayaran").Rows(0).Item("amount") = textTotalBiaya.EditValue
        ElseIf perubahanhargatotaltransaksi = True And xSet.Tables("listpromoaktif").Rows.Count > 0 And jenisPromo = "2" Then
            textTotalBiaya.EditValue = textBiaya.EditValue - textDisc.EditValue
            textTotalBayar.EditValue = textTotalBiaya.EditValue
            xSet.Tables("gridPembayaran").Rows(0).Item("amount") = textTotalBiaya.EditValue
            jumlahpembayaranemoney = textTotalBiaya.EditValue
        End If

        'reset variable list promo
        If IsNothing(xSet.Tables("listpromoaktif")) = False Then
            xSet.Tables("listpromoaktif").Rows.Clear()
        End If
    End Sub

    Private Sub promoreguler()
        'data promo top up
        If IsNothing(xSet.Tables("listparameterpromotopup")) Then
            xSet.Tables.Add("listparameterpromotopup")
        Else
            xSet.Tables("listparameterpromotopup").Clear()
        End If

        If IsNothing(xSet.Tables("listpromoaktif")) Then
            xSet.Tables.Add("listpromoaktif")
        Else
            xSet.Tables("listpromoaktif").Clear()
        End If


        'data detail promo penggunaan top up
        If IsNothing(xSet.Tables("listpromoemoney")) Then
            xSet.Tables.Add("listpromoemoney")
            xSet.Tables("listpromoemoney").Columns.Add("idpromo")
        Else
            xSet.Tables("listpromoemoney").Clear()
        End If

        Dim totalpembayaranbaru As Double = 0
        Dim jumlahitemsesuaisyarat As Integer = 0
        Dim perubahanharga As Boolean = False

        SQLquery = String.Format("select mt.idpromo from mpromo_topup mt where mt.idnominal_topup='0' and tipe_promo=0 and idjenis_promo=5 and mt.inactive=0 AND mt.tanggal_aktif_awal<=DATE (now()) AND mt.tanggal_aktif_akhir>=DATE (now())")
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listpromoaktif")

        'cek setiap parameter
        For a = 0 To xSet.Tables("listpromoaktif").Rows.Count - 1
            Dim syaratselesai As Integer = 0
            totalpembayaranbaru = 0
            perubahanharga = False
            SQLquery = String.Format("select idparameter_promo,value,tipe_parameter from parameter_promo pp where idpromo={0}", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))
            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listparameterpromotopup")

            'cek apakah promo harus member
            If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='18' and value='1'").Count > 0 And Trim(kodemember) = "" And klikvoucher = False Then
                Exit Sub
            End If

            'sortir parameter syarat
            Dim listparametertopup() As DataRow = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=0")
            'cek parameter syarat

            'variable untuk cek cabang
            Dim cekparametercabang As Boolean = False
            For Each parameter As DataRow In listparametertopup
                'cek parameter cabang
                If parameter("idparameter_promo").ToString = "8" And cekparametercabang = False Then
                    cekparametercabang = True
                    If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='8' and value='" & My.Settings.idcabang & "'").Count > 0 Then
                        syaratselesai = syaratselesai + 1
                    End If
                ElseIf parameter("idparameter_promo").ToString = "5" Then
                    If parameter("value").ToString = "1" Then
                        If IsNothing(xSet.Tables("listitemsyarat")) Then
                            xSet.Tables.Add("listitemsyarat")
                        Else
                            xSet.Tables("listitemsyarat").Clear()
                        End If

                        jumlahitemsesuaisyarat = 0

                        SQLquery = String.Format("SELECT iditem,jenis_item,jenis_value,value_minimum FROM syarat_item_transaksi WHERE idpromo='{0}'", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))
                        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listitemsyarat")

                        'cek item yang di beli apakah sesuai dengan syarat
                        For j = 0 To xSet.Tables("setSales").Rows.Count - 1
                            For k = 0 To xSet.Tables("listitemsyarat").Rows.Count - 1
                                If xSet.Tables("setSales").Rows(j).Item("idBrg").ToString = xSet.Tables("listitemsyarat").Rows(k).Item("iditem").ToString And xSet.Tables("listitemsyarat").Rows(k).Item("jenis_item").ToString = "1" Then
                                    If xSet.Tables("listitemsyarat").Rows(k).Item("jenis_value").ToString = "0" Then
                                        'cek jika item barang adalah jumlah item
                                        If CInt(xSet.Tables("setSales").Rows(j).Item("jmlh")) Mod CInt(xSet.Tables("listitemsyarat").Rows(k).Item("value_minimum")) = 0 Then
                                            jumlahitemsesuaisyarat = jumlahitemsesuaisyarat + 1
                                        End If
                                    ElseIf xSet.Tables("listitemsyarat").Rows(k).Item("jenis_value").ToString = "1" Then
                                        'cek jika item barang adalah nominal item

                                    End If
                                End If
                            Next k
                        Next j
                    End If
                ElseIf parameter("idparameter_promo").ToString = "11" Then
                    If CDbl(textTotalBiaya.EditValue) >= CDbl(parameter("value")) Then
                        If xSet.Tables("listparameterpromotopup").Select("tipe_parameter=3 and idparameter_promo='7' and value='1'").Count > 0 Then
                            jumlahitemsesuaisyarat = CInt(CDbl(textTotalBiaya.EditValue) \ CDbl(parameter("value")))
                        Else
                            jumlahitemsesuaisyarat = 1
                        End If
                        syaratselesai = syaratselesai + 1
                    End If
                End If
            Next parameter

            If syaratselesai = listparametertopup.Count Then
                'sortir parameter reward
                listparametertopup = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=1")
                'cek parameter reward
                For Each parameter As DataRow In listparametertopup
                    'eksekusi tiap parameter (parameter pemakaian laritta cash)

                    'eksekusi parameter reward
                    If parameter("idparameter_promo").ToString = "2" And parameter("value").ToString = "1" Then
                        If IsNothing(xSet.Tables("listreward")) Then
                            xSet.Tables.Add("listreward")
                        Else
                            xSet.Tables("listreward").Clear()
                        End If
                        SQLquery = String.Format("SELECT iditem,if(jenis_item=0,mk.namaKategori2,mb.namaBrg) `RewardItem`,jenis_item,harga_item
                FROM reward_item_promo ri
                 LEFT JOIN mbarang mb ON mb.idBrg=ri.iditem
                LEFT JOIN mkategoribrg2 mk ON mk.idKategori2=ri.iditem
                WHERE ri.idpromo='{0}'", xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))

                        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listreward")

                        For j = 0 To xSet.Tables("setSales").Rows.Count - 1
                            For k = 0 To xSet.Tables("listreward").Rows.Count - 1
                                'membuat harga baru dari item barang
                                If xSet.Tables("setSales").Rows(j).Item("idBrg").ToString = xSet.Tables("listreward").Rows(k).Item("iditem").ToString And xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "1" Then
                                    'If xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                    If xSet.Tables("setSales").Rows(j).Item("idBrg").ToString = xSet.Tables("listreward").Rows(k).Item("RewardItem").ToString Then
                                        If xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Contains("%") Then
                                            Dim nilaipersen() As String = xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Split("%")
                                            xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * (CInt(nilaipersen(0)) / 100)
                                            xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                        Else
                                            xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("listreward").Rows(k).Item("harga_item"))
                                            xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                        End If
                                    End If

                                    'membuat harga baru dari item kategori
                                ElseIf kategoribarang(xSet.Tables("setSales").Rows(j).Item("idBrg").ToString).ToString = xSet.Tables("listreward").Rows(k).Item("iditem").ToString And xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                    'If xSet.Tables("listreward").Rows(k).Item("jenis_item").ToString = "0" Then
                                    If kategoribrg(xSet.Tables("setSales").Rows(j).Item("idBrg").ToString).ToString = xSet.Tables("listreward").Rows(k).Item("RewardItem").ToString Then
                                        If xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Contains("%") Then
                                            Dim nilaipersen() As String = xSet.Tables("listreward").Rows(k).Item("harga_item").ToString.Split("%")
                                            xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * (CInt(nilaipersen(0)) / 100)
                                            xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                        Else
                                            xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("listreward").Rows(k).Item("harga_item"))
                                            xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
                                        End If

                                    End If
                                End If
                            Next k
                            totalpembayaranbaru = totalpembayaranbaru + CDbl(xSet.Tables("setSales").Rows(j).Item("hrg"))
                            perubahanharga = True
                        Next j
                    ElseIf parameter("idparameter_promo").ToString = "13" And parameter("value").ToString = "1" Then
                        If TestOnline() = True Then
                            Dim voucher As String
                            Dim nominalvoucher As Double
                            If xSet.Tables("listparameterpromotopup").Select("tipe_parameter='1' and idparameter_promo='16' and value='1'").Count > 0 Then

                                Dim datavoucher() As DataRow = xSet.Tables("listparameterpromotopup").Select("tipe_parameter=1 and idparameter_promo<>'13'")
                                'printing voucher

                                Dim tanggalexp As String = ""
                                Dim minimumtransaksi As Double = 0

                                'print voucher sesuai syarat
                                For y = 0 To jumlahitemsesuaisyarat - 1
                                    Dim printvc As XtraReport = New voucher_retail
                                    printvc.ShowPrintStatusDialog = False
                                    For Each paramvc As DataRow In datavoucher
                                        If paramvc("idparameter_promo") = "14" Then
                                            nominalvoucher = CDbl(CDbl(paramvc("value")))
                                            printvc.Parameters("reward").Value = "Voucher Rp." & Format(nominalvoucher, "#,###") & ",-"
                                        ElseIf paramvc("idparameter_promo") = "15" Then
                                            printvc.Parameters("expvoucher").Value = "Valid until " & Date.Now.AddDays(CDbl(paramvc("value"))).ToString("dd-MM-yyyy")
                                            tanggalexp = Date.Now.AddDays(CDbl(paramvc("value"))).ToString("yyyyMMdd")
                                        ElseIf paramvc("idparameter_promo") = "17" Then
                                            printvc.Parameters("tipevoucher").Value = paramvc("value")
                                        ElseIf paramvc("idparameter_promo") = "19" Then
                                            minimumtransaksi = CDbl(paramvc("value"))
                                        ElseIf paramvc("idparameter_promo") = "20" Then
                                            printvc.Parameters("keteranganVoucher").Value = paramvc("value")
                                        End If
                                    Next paramvc

                                    voucher = autogenerate("VCR", "NOM", "select kode_voucher from app_promo.voucher_nominal ")
                                    printvc.Parameters("kodevoucher").Value = voucher
                                    printvc.Parameters("cetakanke").Value = "Cetakan ke-1"
                                    printvc.Parameters("alamat").Value = ExDb.Scalar("Select alamatCabang from mcabang where idCabang='" & My.Settings.idcabang & "'", My.Settings.localconn)
                                    SQLquery = "insert into app_promo.voucher_nominal (kode_voucher,idCabang,is_use,createdate,idOrder,del,nominal_voucher,jenis_voucher,cetakanke,exp_date,minimal_nominal_transaksi) values ('" & voucher & "','" & My.Settings.idcabang & "','0',now(),'" & numOrder & "','0'," & nominalvoucher & ",'1',1,'" & tanggalexp & "','" & minimumtransaksi & "')"
                                    ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printvc)
                                        'tool.PreviewForm.WindowState = FormWindowState.Maximized
                                        'tool.ShowPreviewDialog()
                                        tool.Print()
                                    End Using
                                Next y

                            End If
                        End If
                    ElseIf parameter("idparameter_promo").ToString = "9" And parameter("value").ToString = "1" Then

                    End If
                Next parameter

            End If
            'input promo yang digunakan
            xSet.Tables("listpromoemoney").Rows.Add(xSet.Tables("listpromoaktif").Rows(a).Item("idpromo"))

            'reset parameter promo yang aktif
            xSet.Tables("listparameterpromotopup").Clear()
        Next a
        If xSet.Tables("listpromoaktif").Rows.Count > 0 And perubahanharga = True Then
            jumlahpembayaranemoney = totalpembayaranbaru
            textTotalBayar.Text = totalpembayaranbaru.ToString
            textTotalBiaya.Text = totalpembayaranbaru.ToString
            textBiaya.Text = totalpembayaranbaru.ToString
            xSet.Tables("gridPembayaran").Rows(0).Item("amount") = textTotalBiaya.Text
        End If
        'reset variable list promo
        If IsNothing(xSet.Tables("listpromoaktif")) = False Then
            xSet.Tables("listpromoaktif").Rows.Clear()
        End If

    End Sub

    Private Function insertdatatopup() As Boolean
        Return True
    End Function

End Class