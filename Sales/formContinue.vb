﻿Public Class formContinue 

    Public Sub callMe(ByVal newsText As String)
        LabelControl1.Text = newsText
        LabelControl2.Visible = False
        MemoEdit1.Visible = False
        ShowDialog()
    End Sub

    Public Sub callMe2(ByVal newsText As String)
        LabelControl1.Text = newsText
        LabelControl2.Visible = True
        MemoEdit1.Text = ""
        MemoEdit1.Visible = True
        ShowDialog()
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        isContinue = True
        ketvoidpos = MemoEdit1.EditValue
        If Trim(ketvoidpos) = "" And MemoEdit1.Visible = True Then
            MessageBox.Show("Keterangan Void Wajib Di isi", "POS Laritta")
            Exit Sub
        End If
        Close()
    End Sub

    Private Sub butDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butDelete.Click
        isContinue = False
        ketvoidpos = ""
        Close()
    End Sub
End Class