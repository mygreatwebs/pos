﻿Public Class virNumpad
    Dim hasPushed As Boolean

    Private Function cekTotal()
        Try
            If CInt(TextEdit1.EditValue) >= 99999999 Then
                TextEdit1.Text = 99999999
                Return False
            End If
            If hasPushed = False Then hasPushed = True
            Return True
        Catch ex As Exception
            TextEdit1.Text = 99999999
            Return False
        End Try
    End Function

    Private Sub virNumpad_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim xLoc As Point = New Point() With {.X = Screen.PrimaryScreen.WorkingArea.Width - Size.Width, .Y = Screen.PrimaryScreen.WorkingArea.Height - Size.Height}
        Location = New Point(xLoc)

        hasPushed = False
        qttInteger = 0
        TextEdit1.Text = 1
        TextEdit1.Select(0, TextEdit1.Text.Length)
        TextEdit1.Focus()
    End Sub

    Private Sub butNum1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum1.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 1
        hasPushed = True
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum2.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 2
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum3.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 3
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum4.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 4
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum5.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 5
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum6.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 6
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum7.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 7
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum8.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 8
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum9_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum9.Click
        If hasPushed = False Then
            If TextEdit1.Text = 1 Then TextEdit1.Text = 0
        End If
        TextEdit1.Text = TextEdit1.Text * 10 + 9
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum0.Click
        TextEdit1.Text = TextEdit1.Text * 10
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum00_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum00.Click
        TextEdit1.Text = TextEdit1.Text * 100
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butReset.Click
        TextEdit1.Text = 0
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        qttInteger = TextEdit1.Text
        Close()
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        qttInteger = 0
        Close()
    End Sub
End Class