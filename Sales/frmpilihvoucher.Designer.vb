﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmpilihvoucher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.vckhusus = New DevExpress.XtraEditors.SimpleButton()
        Me.vc_promo = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.vckhusus)
        Me.PanelControl1.Controls.Add(Me.vc_promo)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(422, 222)
        Me.PanelControl1.TabIndex = 0
        '
        'vckhusus
        '
        Me.vckhusus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vckhusus.Appearance.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.vckhusus.Appearance.Options.UseFont = True
        Me.vckhusus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.vckhusus.Location = New System.Drawing.Point(42, 119)
        Me.vckhusus.Name = "vckhusus"
        Me.vckhusus.Size = New System.Drawing.Size(338, 78)
        Me.vckhusus.TabIndex = 6
        Me.vckhusus.Text = "Voucher Kalender"
        '
        'vc_promo
        '
        Me.vc_promo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.vc_promo.Appearance.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.vc_promo.Appearance.Options.UseFont = True
        Me.vc_promo.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.vc_promo.Location = New System.Drawing.Point(42, 22)
        Me.vc_promo.Name = "vc_promo"
        Me.vc_promo.Size = New System.Drawing.Size(338, 78)
        Me.vc_promo.TabIndex = 5
        Me.vc_promo.Text = "Voucher Promo/Khusus"
        '
        'frmpilihvoucher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 222)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmpilihvoucher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmpilihvoucher"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents vckhusus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents vc_promo As DevExpress.XtraEditors.SimpleButton
End Class
