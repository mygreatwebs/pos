﻿Public Class frmInputKodeMember
    Dim isCall_2 As Boolean = False

    Public Sub callme()
        isCall_2 = False
        If kodemember = "" Or TextEdit1.Text = "" Then
            If isambilpaketpromo Then
                ishavemember = True
            Else
                frmconfirmMember.ShowDialog()
            End If

            TextEdit1.ReadOnly = False
            refreshmember.Enabled = True

            If isvalidSpecialkemerdekaan = True Then
                showlistbarangpromo = True
            End If

            If ishavemember = False Then
                kodemember = ""
                Me.Close()
                Exit Sub
            End If
        Else
            TextEdit1.Text = kodemember
            ishavemember = True
            TextEdit1.ReadOnly = True
            refreshmember.Enabled = False
        End If
        Me.ShowDialog()
    End Sub

    Public Sub callme_2()
        isCall_2 = True
        If kodemember <> "" Then
            TextEdit1.Text = kodemember
        End If
        refreshmember.Enabled = False
        TextEdit1.ReadOnly = True
        Me.ShowDialog()
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        If isCekProdukPromo = True And Trim(TextEdit1.Text) = "" Then
            MsgBox("Kode Member Wajib Di Inputkan !")
            Exit Sub
        End If
        kodemember = ""
        ispromoproces = True
        Close()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        'isCekProdukPromo = True And
        If Trim(TextEdit1.Text) = "" Then
            MsgBox("Kode Member Wajib Di Inputkan !")
            Exit Sub
        End If
        isinputkodemember = True
        'If ispromoproces = True And Trim(TextEdit1.Text) = "" Then
        'MsgBox("Kode Member Wajib Di Inputkan !")
        'Exit Sub
        'End If
        If Trim(TextEdit1.Text) <> "" Then
            kodemember = TextEdit1.Text
            namapemilikkartu = TextEdit2.Text
        End If

        'promo menggunakan member

        'cek apakah menggunakan promo sepcial kemeredekaan 2017
        If isvalidSpecialkemerdekaan = False Or showlistbarangpromo = False Then
            Close()
            Exit Sub
        End If


        Dim syaratspecialkemeredekaan As Boolean = False
        Dim syaratnominal As Double = 0
        'filter barang yang tidak masuk syarat promo

        filterbarangdankategori("'825','824'", "'SOFT DRINK','BS','BS TART','BS TART SLICE','BS DONAT UPIN/IPIN','BS WHOLE BREAD','BS TRADITIONAL SNACK','BS WHOLE CAKE','BS SOES','BS PASTRY & PIE','TOP UP'")

        For i = 0 To xSet.Tables("setSales").Rows.Count - 1
            Dim ada As Boolean = False
            For j = 0 To xSet.Tables("listfilter").Rows.Count - 1
                If xSet.Tables("setSales").Rows(i).Item("namaBrg").ToString = xSet.Tables("listfilter").Rows(j).Item("namaBrg").ToString Then
                    ada = True
                    Exit For
                End If
            Next j
            If ada = False Then
                syaratnominal = syaratnominal + CDbl(xSet.Tables("setSales").Rows(i).Item("hrg"))
                If syaratnominal >= 72000 Then
                    syaratspecialkemeredekaan = True
                    Exit For
                End If
            End If
        Next i



        If syaratspecialkemeredekaan = True Then

            Dim promokemerdekaan As New listBarangPromo
            promokemerdekaan.callme("SELECT
  mb.idBrg,
  mb.namaBrg,
  mk.namaKategori2,
  0 `jmlh`
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.Inactive='0' AND (mb.idBrg='518' or mb.idBrg='515'
                           or mb.idBrg='598' or mb.idBrg='773'
                           or mb.idBrg='775' or mb.idBrg='64'
                           or mk.idKategori2='6'
                           or mb.idBrg='133' or mb.idBrg='29'
                           or mb.idBrg='159' or mb.idBrg='855'
                           or mb.idBrg='854' or mb.idBrg='858'
                           or mb.idBrg='860' or mb.idBrg='254'
                           or mb.idBrg='48' or mb.idBrg='18')", "promokemerdekaan")
        End If
        'showlistbarangpromo = False

        ''promo harga top up
        'If checktopup() = True Then
        '    'data top up
        '    If IsNothing(xSet.Tables("datanominaltopup")) Then
        '        xSet.Tables.Add("datanominaltopup")
        '    Else
        '        xSet.Tables("datanominaltopup").Clear()
        '    End If

        '    'data promo top up
        '    If IsNothing(xSet.Tables("listparameterpromotopup")) Then
        '        xSet.Tables.Add("listparameterpromotopup")
        '    Else
        '        xSet.Tables("listparameterpromotopup").Clear()
        '    End If

        '    xSet.Tables("listopup").Columns("idbrg").ColumnMapping = MappingType.Hidden
        '    If TestOnline() Then
        '        If CBool(cekpernahpakailarittacash(kodemember)) = False Then
        '            For k = 0 To 10
        '                SQLquery = String.Format("select idparameter_promo,value from mpromo_topup mt join parameter_promo pp on mt.idpromo=pp.idpromo where mt.idnominal_topup='{0}' and tipe_promo=0 and mt.inactive=0 and mt.tanggal_aktif_awal<=date (now()) and mt.tanggal_aktif_akhir>=date (now())", xSet.Tables("datanominaltopup").Rows(0).Item("idnominal_topup").ToString())
        '                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listparameterpromotopup")
        '                For i = 0 To xSet.Tables("listparameterpromotopup").Rows.Count - 1
        '                    If xSet.Tables("listparameterpromotopup").Rows(i).Item("idparameter_promo").ToString = "3" Then
        '                        'ubah harga sesuai promo di setSales
        '                        For j = 0 To xSet.Tables("setSales").Rows.Count - 1
        '                            If kategoribrg(xSet.Tables("setSales").Rows(j).Item("idBrg").ToString).ToString = "TOP UP" Then
        '                                xSet.Tables("setSales").Rows(j).Item("hrgpcs") = CDbl(xSet.Tables("listparameterpromotopup").Rows(i).Item("value"))
        '                                xSet.Tables("setSales").Rows(j).Item("hrg") = CDbl(xSet.Tables("setSales").Rows(j).Item("hrgpcs")) * CInt(xSet.Tables("setSales").Rows(j).Item("jmlh"))
        '                            End If
        '                        Next j
        '                    End If
        '                Next i
        '            Next k

        '        End If
        '    End If
        'End If


        Close()
    End Sub

    Private Sub frmInputKodeMember_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed

        Dispose()
    End Sub

    Private Sub TextEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles TextEdit1.EditValueChanged
        If TextEdit1.Text.Length = 12 Then
            'cek kode member dicloud
            MemoEdit1.Text = ""
            TextEdit2.Text = ""
            idCust = 0
            MemberPoint.EditValue = 0
            MemberStamp.EditValue = 0
            MemberEmoney.EditValue = readsaldo(False)
            TextEdit3.EditValue = 0

            Try
                SQLquery = String.Format("SELECT a.idCust, b.namaKategoriCust, a.id_tipemembership, c.nama_tipemembership, c.level_tipemembership, a.namaCust, BirthDate, homeAddress, d.nama_kota, Phone, PhoneMobile, Pekerjaan, kode_kartu, kode_kartuUID, " &
                                                 "emoney, points, stamp, sum_trans_outlet, sum_trans_order, sum_buy_outlet, sum_buy_order, a.notes, a.joinmembership_date,0 jumlahKupon " &
                                                 "FROM mcustomer a INNER JOIN mkategoricust b ON a.idKategoriCust=b.idKategoriCust INNER JOIN mbsm_tipemembership c ON a.id_tipemembership=c.id_tipemembership INNER JOIN mbsm_lockota d ON a.id_kota=d.id_kota " &
                                                 "LEFT JOIN app_kupon.bonusKupon e on a.idCust=e.idCust WHERE a.kode_member='{0}'", TextEdit1.Text)

                If IsNothing(xSet.Tables("infomember")) = False Then xSet.Tables("infomember").Clear()
                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "infomember")

                If xSet.Tables("infomember").Rows.Count = 0 Then
                    TextEdit2.Text = "Data Tidak Ditemukan"
                Else
                    idCust = xSet.Tables("infomember").Rows(0)("idCust")
                    TextEdit2.Text = xSet.Tables("infomember").Rows(0)("namaCust")
                    namapemilikkartu = xSet.Tables("infomember").Rows(0)("namaCust")
                    MemoEdit1.Text = xSet.Tables("infomember").Rows(0)("HomeAddress")

                    MemberPoint.EditValue = xSet.Tables("infomember").Rows(0).Item("points")
                    v_pointakhir = MemberPoint.EditValue
                    If isCall_2 = False Then
                        v_pointawal = MemberPoint.EditValue
                    End If
                    TextEdit4.EditValue = v_pointawal
                    'MsgBox("Point Awal " & v_pointawal & " - " & " Point Akhir " & v_pointakhir)
                    MemberStamp.EditValue = xSet.Tables("infomember").Rows(0).Item("stamp")
                    MemberEmoney.EditValue = xSet.Tables("infomember").Rows(0).Item("emoney")
                    TextEdit3.EditValue = xSet.Tables("infomember").Rows(0).Item("jumlahKupon")
                    'MemberJmlTransOrder.EditValue = xSet.Tables("infomember").Rows(0).Item("sum_trans_order")
                    'MemberJmlTransOutlet.EditValue = xSet.Tables("infomember").Rows(0).Item("sum_trans_outlet")
                    'MemberTotalTransOrder.EditValue = xSet.Tables("infomember").Rows(0).Item("sum_buy_order")
                    'MemberTotalTransOutlet.EditValue = xSet.Tables("infomember").Rows(0).Item("sum_buy_outlet")
                    'MemberJmlTrans.EditValue = MemberJmlTransOrder.EditValue + MemberJmlTransOutlet.EditValue
                    'MemberTotalTrans.EditValue = MemberTotalTransOrder.EditValue + MemberTotalTransOutlet.EditValue
                    SQLquery = String.Format("SELECT ifnull(app_kupon.f_GetTotalPoint2014To2016({0}),0) + ifnull(app_kupon.f_GetTotalPointPromoJan2017({0}),0) + ifnull(app_kupon.f_GetTotalPointPromoFeb17toNow({0}),0) as totpoint ", idCust)
                    'SQLquery = String.Format("SELECT ifnull(app_kupon.f_GetTotalPoint2014To2016Test({0}),0) + ifnull(app_kupon.f_GetTotalPointPromoJan2017Test({0}),0) + ifnull(app_kupon.f_GetTotalPointPromoFeb17toNowTest({0}),0) as totpoint ", idCust)
                    If IsNothing(xSet.Tables("infopoint")) = False Then xSet.Tables("infopoint").Clear()
                    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "infopoint")
                    If xSet.Tables("infopoint").Rows.Count > 0 Then
                        TextEdit3.EditValue = xSet.Tables("infopoint").Rows(0)(0)
                        v_kuponakhir = TextEdit3.EditValue
                        If isCall_2 = False Then
                            v_kuponawal = TextEdit3.EditValue
                        End If
                        'MsgBox("Kupon Awal " & v_kuponawal & " - " & " Kupon Akhir " & v_kuponakhir)
                    End If
                End If

            Catch ex As Exception
                If ex.HResult = -2147467259 Then
                    MsgBox(ex.Message)
                    TextEdit2.Text = "Koneksi Offline"
                Else
                    MsgBox(ex.Message)
                End If
            End Try
        End If
    End Sub

    Private Sub TextEdit1_LostFocus(sender As Object, e As EventArgs) Handles TextEdit1.LostFocus

    End Sub

    Private Sub frmInputKodeMember_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If isinputkodemember = False Then
                TextEdit1.EditValue = readidmemeber()
            Else
                TextEdit1.EditValue = kodemember
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub refreshmember_Click(sender As Object, e As EventArgs) Handles refreshmember.Click
        Try
            TextEdit1.EditValue = ""
            TextEdit1.EditValue = readidmemeber()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class