﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class virNumpad
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butNum1 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum2 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum3 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum4 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum5 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum6 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum7 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum8 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum9 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.butReset = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum0 = New DevExpress.XtraEditors.SimpleButton()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum00 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butNum1
        '
        Me.butNum1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum1.Appearance.Options.UseFont = True
        Me.butNum1.Location = New System.Drawing.Point(12, 60)
        Me.butNum1.Name = "butNum1"
        Me.butNum1.Size = New System.Drawing.Size(63, 62)
        Me.butNum1.TabIndex = 0
        Me.butNum1.Text = "1"
        '
        'butNum2
        '
        Me.butNum2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum2.Appearance.Options.UseFont = True
        Me.butNum2.Location = New System.Drawing.Point(81, 60)
        Me.butNum2.Name = "butNum2"
        Me.butNum2.Size = New System.Drawing.Size(63, 62)
        Me.butNum2.TabIndex = 1
        Me.butNum2.Text = "2"
        '
        'butNum3
        '
        Me.butNum3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum3.Appearance.Options.UseFont = True
        Me.butNum3.Location = New System.Drawing.Point(150, 60)
        Me.butNum3.Name = "butNum3"
        Me.butNum3.Size = New System.Drawing.Size(63, 62)
        Me.butNum3.TabIndex = 2
        Me.butNum3.Text = "3"
        '
        'butNum4
        '
        Me.butNum4.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum4.Appearance.Options.UseFont = True
        Me.butNum4.Location = New System.Drawing.Point(12, 128)
        Me.butNum4.Name = "butNum4"
        Me.butNum4.Size = New System.Drawing.Size(63, 62)
        Me.butNum4.TabIndex = 5
        Me.butNum4.Text = "4"
        '
        'butNum5
        '
        Me.butNum5.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum5.Appearance.Options.UseFont = True
        Me.butNum5.Location = New System.Drawing.Point(81, 128)
        Me.butNum5.Name = "butNum5"
        Me.butNum5.Size = New System.Drawing.Size(63, 62)
        Me.butNum5.TabIndex = 4
        Me.butNum5.Text = "5"
        '
        'butNum6
        '
        Me.butNum6.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum6.Appearance.Options.UseFont = True
        Me.butNum6.Location = New System.Drawing.Point(150, 128)
        Me.butNum6.Name = "butNum6"
        Me.butNum6.Size = New System.Drawing.Size(63, 62)
        Me.butNum6.TabIndex = 3
        Me.butNum6.Text = "6"
        '
        'butNum7
        '
        Me.butNum7.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum7.Appearance.Options.UseFont = True
        Me.butNum7.Location = New System.Drawing.Point(12, 196)
        Me.butNum7.Name = "butNum7"
        Me.butNum7.Size = New System.Drawing.Size(63, 62)
        Me.butNum7.TabIndex = 8
        Me.butNum7.Text = "7"
        '
        'butNum8
        '
        Me.butNum8.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum8.Appearance.Options.UseFont = True
        Me.butNum8.Location = New System.Drawing.Point(81, 196)
        Me.butNum8.Name = "butNum8"
        Me.butNum8.Size = New System.Drawing.Size(63, 62)
        Me.butNum8.TabIndex = 7
        Me.butNum8.Text = "8"
        '
        'butNum9
        '
        Me.butNum9.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum9.Appearance.Options.UseFont = True
        Me.butNum9.Location = New System.Drawing.Point(150, 196)
        Me.butNum9.Name = "butNum9"
        Me.butNum9.Size = New System.Drawing.Size(63, 62)
        Me.butNum9.TabIndex = 6
        Me.butNum9.Text = "9"
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "0"
        Me.TextEdit1.Location = New System.Drawing.Point(12, 12)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
        Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TextEdit1.Properties.Mask.EditMask = "n0"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 9
        Me.TextEdit1.Size = New System.Drawing.Size(331, 42)
        Me.TextEdit1.TabIndex = 9
        '
        'butReset
        '
        Me.butReset.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butReset.Appearance.Options.UseFont = True
        Me.butReset.Location = New System.Drawing.Point(219, 60)
        Me.butReset.Name = "butReset"
        Me.butReset.Size = New System.Drawing.Size(124, 62)
        Me.butReset.TabIndex = 10
        Me.butReset.Text = "C"
        '
        'butNum0
        '
        Me.butNum0.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum0.Appearance.Options.UseFont = True
        Me.butNum0.Location = New System.Drawing.Point(12, 264)
        Me.butNum0.Name = "butNum0"
        Me.butNum0.Size = New System.Drawing.Size(132, 62)
        Me.butNum0.TabIndex = 11
        Me.butNum0.Text = "0"
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Location = New System.Drawing.Point(219, 128)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(124, 130)
        Me.butOK.TabIndex = 12
        Me.butOK.Text = "OK"
        '
        'butClose
        '
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Location = New System.Drawing.Point(219, 264)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(124, 62)
        Me.butClose.TabIndex = 13
        Me.butClose.Text = "Cancel"
        '
        'butNum00
        '
        Me.butNum00.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum00.Appearance.Options.UseFont = True
        Me.butNum00.Location = New System.Drawing.Point(150, 264)
        Me.butNum00.Name = "butNum00"
        Me.butNum00.Size = New System.Drawing.Size(63, 62)
        Me.butNum00.TabIndex = 14
        Me.butNum00.Text = "00"
        '
        'virNumpad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 339)
        Me.ControlBox = False
        Me.Controls.Add(Me.butNum00)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.butOK)
        Me.Controls.Add(Me.butNum0)
        Me.Controls.Add(Me.butReset)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.butNum7)
        Me.Controls.Add(Me.butNum8)
        Me.Controls.Add(Me.butNum9)
        Me.Controls.Add(Me.butNum4)
        Me.Controls.Add(Me.butNum5)
        Me.Controls.Add(Me.butNum6)
        Me.Controls.Add(Me.butNum3)
        Me.Controls.Add(Me.butNum2)
        Me.Controls.Add(Me.butNum1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "virNumpad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TopMost = True
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents butNum1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents butReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum00 As DevExpress.XtraEditors.SimpleButton
End Class
