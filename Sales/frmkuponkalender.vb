﻿Public Class frmkuponkalender

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        'cek kodekupon dicloud
        Dim jmldata As Integer
        If TestOnline() Then
            SQLquery = String.Format("select count(*) from app_kupon.mkuponkalender where isDimenangkan=1 and idOrder=0 and kodekupon={0}", TextEdit1.Text)
            jmldata = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
            SQLquery = ""
            If jmldata = 0 Then
                MsgBox("Kupon Tidak ada di Database !")
                Exit Sub
            End If
        Else
            msgboxErrorOffline()
            G_kuponkalender = ""
            Exit Sub
        End If
        G_kuponkalender = TextEdit1.Text
        Close()
    End Sub

    Private Sub frmkuponkalender_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub frmkuponkalender_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Trim(G_kuponkalender) = "" Then
            e.Cancel = True
        End If
    End Sub
End Class