﻿Public Class frmboxkresek

    Dim currpage As Integer = 0
    Dim totalpage As Integer = 0
    Dim currindex As Integer = 0
    Dim savekresek As Boolean
    Dim iskresek As Boolean

    Public Function InitForm() As Boolean
        ShowDialog()
        Return True
    End Function

    Private Sub resetbutton()
        Menu1.Text = ""
        Menu1.Tag = ""
        Menu1.Visible = False
        Menu2.Text = ""
        Menu2.Tag = ""
        Menu2.Visible = False
        Menu3.Text = ""
        Menu3.Tag = ""
        Menu3.Visible = False
        Menu4.Text = ""
        Menu4.Tag = ""
        Menu4.Visible = False
        Menu5.Text = ""
        Menu5.Tag = ""
        Menu6.Visible = False
        Menu6.Text = ""
        Menu6.Tag = ""
        Menu6.Visible = False
        Menu7.Text = ""
        Menu7.Tag = ""
        Menu7.Visible = False
        Menu8.Text = ""
        Menu8.Tag = ""
        Menu8.Visible = False
        Menu9.Text = ""
        Menu9.Tag = ""
        Menu9.Visible = False
        Menu10.Text = ""
        Menu10.Tag = ""
        Menu10.Visible = False
        Menu11.Text = ""
        Menu11.Tag = ""
        Menu11.Visible = False
        Menu12.Text = ""
        Menu12.Tag = ""
        Menu12.Visible = False
        Menu13.Text = ""
        Menu13.Tag = ""
        Menu13.Visible = False
        Menu14.Text = ""
        Menu14.Tag = ""
        Menu14.Visible = False
        Menu15.Text = ""
        Menu15.Tag = ""
        Menu15.Visible = False
        Menu16.Text = ""
        Menu16.Tag = ""
        Menu16.Visible = False
    End Sub

    Private Sub drawbutton(ByVal mnuid As Integer, ByVal nmbrang As String, ByVal idbrg As String)
        Dim sudahtulistanpabox As Integer = 0
        While sudahtulistanpabox < 2
            Select Case mnuid
                Case 1
                    Menu1.Text = nmbrang
                    Menu1.Tag = idbrg
                    Menu1.Visible = True
                Case 2
                    Menu2.Text = nmbrang
                    Menu2.Tag = idbrg
                    Menu2.Visible = True
                Case 3
                    Menu3.Text = nmbrang
                    Menu3.Tag = idbrg
                    Menu3.Visible = True
                Case 4
                    Menu4.Text = nmbrang
                    Menu4.Tag = idbrg
                    Menu4.Visible = True
                Case 5
                    Menu5.Text = nmbrang
                    Menu5.Tag = idbrg
                    Menu5.Visible = True
                Case 6
                    Menu6.Text = nmbrang
                    Menu6.Tag = idbrg
                    Menu6.Visible = True
                Case 7
                    Menu7.Text = nmbrang
                    Menu7.Tag = idbrg
                    Menu7.Visible = True
                Case 8
                    Menu8.Text = nmbrang
                    Menu8.Tag = idbrg
                    Menu8.Visible = True
                Case 9
                    Menu9.Text = nmbrang
                    Menu9.Tag = idbrg
                    Menu9.Visible = True
                Case 10
                    Menu10.Text = nmbrang
                    Menu10.Tag = idbrg
                    Menu10.Visible = True
                Case 11
                    Menu11.Text = nmbrang
                    Menu11.Tag = idbrg
                    Menu11.Visible = True
                Case 12
                    Menu12.Text = nmbrang
                    Menu12.Tag = idbrg
                    Menu12.Visible = True
                Case 13
                    Menu13.Text = nmbrang
                    Menu13.Tag = idbrg
                    Menu13.Visible = True
                Case 14
                    Menu14.Text = nmbrang
                    Menu14.Tag = idbrg
                    Menu14.Visible = True
                Case 15
                    Menu15.Text = nmbrang
                    Menu15.Tag = idbrg
                    Menu15.Visible = True
                Case 16
                    Menu16.Text = nmbrang
                    Menu16.Tag = idbrg
                    Menu16.Visible = True
            End Select
            If mnuid < 16 Then
                nmbrang = "Tanpa Box/Kresek"
                idbrg = 0
                mnuid = mnuid + 1
                sudahtulistanpabox = sudahtulistanpabox + 1
            ElseIf mnuid = 16 Then
                sudahtulistanpabox = sudahtulistanpabox + 1
            End If

        End While



    End Sub

    Private Sub createKresBoxDataSet()
        xSet.Tables.Add("setkresbox")
        xSet.Tables("setkresbox").Columns.Add("idBrg", Type.GetType("System.Decimal"))
        xSet.Tables("setkresbox").Columns.Add("namaBrg", Type.GetType("System.String"))
        xSet.Tables("setkresbox").Columns.Add("jmlh", Type.GetType("System.Decimal"))
    End Sub

    Private Sub gridSettings()

        GridControl1.DataSource = xSet.Tables("setkresbox").DefaultView
        GridControl1.ForceInitialize()
        GridView1.RowHeight = 40

        With GridView1
            .OptionsView.EnableAppearanceEvenRow = True
            .OptionsView.EnableAppearanceOddRow = True
            .OptionsSelection.EnableAppearanceFocusedCell = False

            .Columns("idBrg").Visible = False

            .Columns("namaBrg").Caption = "Nama Item"
            .Columns("jmlh").Caption = "Jumlah"

            .Columns("jmlh").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric

            .Columns("jmlh").DisplayFormat.FormatString = "{0:n0}"

            .Columns("namaBrg").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            .Columns("jmlh").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("jmlh").SummaryItem.DisplayFormat = "{0:n0}"

            .Columns("namaBrg").Width = GridControl1.Width * 0.5
            .Columns("jmlh").Width = GridControl1.Width * 0.2
        End With

    End Sub

    Private Sub frmboxkresek_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("setkresbox")
        xSet.Tables.Remove("xkresek")

        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub frmboxkresek_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub frmboxkresek_Load(sender As Object, e As EventArgs) Handles Me.Load
        createKresBoxDataSet()
        gridSettings()

    End Sub

    Private Sub Level2_Click_1(sender As Object, e As EventArgs) Handles Level2.Click
        totalpage = ExDb.Scalar(String.Format("select count(idBrg) from mbarang where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))='{1}'", 20, "KRESEK"), My.Settings.db_conn_mysql.ToString)

        If totalpage = 0 Then
            totalpage = 1
        Else
            totalpage = CInt(totalpage / 16)
            If totalpage = 0 Then totalpage = 1
        End If
        iskresek = True
        currpage = 1
        pageText.Text = currpage
        currindex = 0
        resetbutton()
        Dim buttno As Integer = 1
        If IsNothing(xSet.Tables("xkresek")) = False Then xSet.Tables("xkresek").Clear()

        ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))='{1}' order by  ifnull(urutan,0) ", 20, "KRESEK"), xSet, "xkresek", currindex, 16)

        For Each row As DataRow In xSet.Tables("xkresek").Rows
            drawbutton(buttno, row("namaBrg"), row("idBrg"))
            buttno = buttno + 1
        Next
    End Sub

    Private Sub Level1_Click(sender As Object, e As EventArgs) Handles Level1.Click

        totalpage = ExDb.Scalar(String.Format("select count(idBrg) from mbarang where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))<>'{1}'", 20, "KRESEK"), My.Settings.db_conn_mysql.ToString)
        If totalpage = 0 Then
            totalpage = 1
        Else
            totalpage = CInt(totalpage / 16)
            If totalpage = 0 Then totalpage = 1
        End If

        iskresek = False
        currpage = 1
        pageText.Text = currpage
        currindex = 0
        resetbutton()
        Dim buttno As Integer = 1
        If IsNothing(xSet.Tables("xkresek")) = False Then xSet.Tables("xkresek").Clear()

        ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))<>'{1}' order by ifnull(urutan,0)", 20, "KRESEK"), xSet, "xkresek", currindex, 16)

        For Each row As DataRow In xSet.Tables("xkresek").Rows
            drawbutton(buttno, row("namaBrg"), row("idBrg"))
            buttno = buttno + 1
        Next
    End Sub

    Private Sub nextButton_Click(sender As Object, e As EventArgs) Handles nextButton.Click
        If currpage >= totalpage Then Exit Sub

        currpage += 1
        pageText.Text = currpage
        currindex += 16
        resetbutton()
        Dim buttno As Integer = 1
        If IsNothing(xSet.Tables("xkresek")) = False Then xSet.Tables("xkresek").Clear()

        If iskresek Then
            ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))='{1}' order by ifnull(urutan,0)", 20, "KRESEK"), xSet, "xkresek", currindex, 16)
        Else
            ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))<>'{1}' order by ifnull(urutan,0) ", 20, "KRESEK"), xSet, "xkresek", currindex, 16)
        End If

        For Each row As DataRow In xSet.Tables("xkresek").Rows
            drawbutton(buttno, row("namaBrg"), row("idBrg"))
            buttno = buttno + 1
        Next
    End Sub

    Private Sub AddData2Grid(ByVal mntag As String)
        'jika pilihan tanpa kresek dan box
        If mntag = "0" Then
            xSet.Tables("setkresbox").Rows.Add(0, "Tanpa Box/Kresek", 0)
            Exit Sub
        End If
        'tombol yang di klik merupakan jenis PRODUCT -> Show virtual numpad
        Dim selectstring As String = ""
        virNumpad.ShowDialog()
        If qttInteger = 0 Then Exit Sub
        'cek kalau dari select berhasil mendapatkan >=1row
        selectstring = String.Format("idBrg={0}", mntag)
        'If xSet.Tables("getDataButton").Select(selectString).Count > 0 Then
        If xSet.Tables("xkresek").Select(selectstring).Count > 0 Then
            'sudah ada row, update
            If GridView1.RowCount >= 1 Then
                For Each row As DataRow In xSet.Tables("setkresbox").Rows
                    'cek setiap row, sama dg yg skrg dipilih/tdk.
                    If row.Item("idBrg") = xSet.Tables("xkresek").Select(selectstring)(0).Item("idBrg") Then
                        row.Item("jmlh") += qttInteger
                        Exit Sub
                    End If
                Next
                xSet.Tables("setkresbox").Rows.Add(xSet.Tables("xkresek").Select(selectstring)(0).Item("idBrg"), xSet.Tables("xkresek").Select(selectstring)(0).Item("namaBrg"), qttInteger)
            Else
                xSet.Tables("setkresbox").Rows.Add(xSet.Tables("xkresek").Select(selectstring)(0).Item("idBrg"), xSet.Tables("xkresek").Select(selectstring)(0).Item("namaBrg"), qttInteger)
            End If
        Else
            'belum ada row, insert
            xSet.Tables("setkresbox").Rows.Add(xSet.Tables("xkresek").Select(selectstring)(0).Item("idBrg"), xSet.Tables("xkresek").Select(selectstring)(0).Item("namaBrg"), qttInteger)
        End If
    End Sub

    Private Sub Menu1_Click(sender As Object, e As EventArgs) Handles Menu1.Click
        AddData2Grid(Menu1.Tag.ToString)
    End Sub

    Private Sub Menu2_Click(sender As Object, e As EventArgs) Handles Menu2.Click
        AddData2Grid(Menu2.Tag.ToString)
    End Sub

    Private Sub prevButton_Click(sender As Object, e As EventArgs) Handles prevButton.Click
        If currpage = 1 Then Exit Sub
        currpage -= 1
        pageText.Text = currpage
        currindex -= 16
        resetbutton()
        Dim buttno As Integer = 1
        If IsNothing(xSet.Tables("xkresek")) = False Then xSet.Tables("xkresek").Clear()

        If iskresek Then
            ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))='{1}' order by ifnull(urutan,0)", 20, "KRESEK"), xSet, "xkresek", currindex, 16)
        Else
            ExDb.ExecQuery2(My.Settings.db_conn_mysql, String.Format("select a.namaBrg,a.idBrg from mbarang a left join mmenukresek b on a.idBrg=b.idBrg where inactive=0 and idkategori2={0} and upper(mid(namaBrg,1,6))<>'{1}' order by ifnull(urutan,0)", 20, "KRESEK"), xSet, "xkresek", currindex, 16)
        End If

        For Each row As DataRow In xSet.Tables("xkresek").Rows
            drawbutton(buttno, row("namaBrg"), row("idBrg"))
            buttno = buttno + 1
        Next
    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs) Handles GridView1.RowCellClick
        selected_idxRow = GridView1.FocusedRowHandle
        selected_row = GridView1.GetFocusedDataRow
        editKresek.ShowDialog()
    End Sub

    Private Sub butDiskon_Click(sender As Object, e As EventArgs) Handles butDiskon.Click
        'cek apakah sudah memilih box atau belum
        If GridView1.RowCount = 0 Then
            Exit Sub
        End If
        'cek apakah menggunakan kresek
        If xSet.Tables("setkresbox").Select("idBrg=0").Count > 0 Then
            Close()
            Exit Sub
        End If

        'belum ada row, insert
        Dim stringselect As String
        If IsNothing(xSet.Tables("setSales")) Then Exit Sub
        For Each row As DataRow In xSet.Tables("setkresbox").Rows
            stringselect = String.Format("idBrg={0}", row("idBrg"))
            'MsgBox(xSet.Tables("setSales").Select(stringselect).Count.ToString)
            If xSet.Tables("setSales").Select(stringselect).Count > 0 Then

                xSet.Tables("setSales").Select(stringselect)(0)("jmlh") += row("jmlh")
            Else
                xSet.Tables("setSales").Rows.Add(row("idBrg"),
                                                    row("namaBrg"), row("jmlh"), 0, 0, 0)
            End If
        Next
        Close()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Close()
    End Sub

    Private Sub Menu3_Click(sender As Object, e As EventArgs) Handles Menu3.Click
        AddData2Grid(Menu3.Tag.ToString)
    End Sub

    Private Sub Menu4_Click(sender As Object, e As EventArgs) Handles Menu4.Click
        AddData2Grid(Menu4.Tag.ToString)
    End Sub

    Private Sub Menu5_Click(sender As Object, e As EventArgs) Handles Menu5.Click
        AddData2Grid(Menu5.Tag.ToString)
    End Sub

    Private Sub Menu6_Click(sender As Object, e As EventArgs) Handles Menu6.Click
        AddData2Grid(Menu6.Tag.ToString)
    End Sub

    Private Sub Menu7_Click(sender As Object, e As EventArgs) Handles Menu7.Click
        AddData2Grid(Menu7.Tag.ToString)
    End Sub

    Private Sub Menu8_Click(sender As Object, e As EventArgs) Handles Menu8.Click
        AddData2Grid(Menu8.Tag.ToString)
    End Sub

    Private Sub Menu9_Click(sender As Object, e As EventArgs) Handles Menu9.Click
        AddData2Grid(Menu9.Tag.ToString)
    End Sub

    Private Sub Menu10_Click(sender As Object, e As EventArgs) Handles Menu10.Click
        AddData2Grid(Menu10.Tag.ToString)
    End Sub

    Private Sub Menu11_Click(sender As Object, e As EventArgs) Handles Menu11.Click
        AddData2Grid(Menu11.Tag.ToString)
    End Sub

    Private Sub Menu12_Click(sender As Object, e As EventArgs) Handles Menu12.Click
        AddData2Grid(Menu12.Tag.ToString)
    End Sub

    Private Sub Menu13_Click(sender As Object, e As EventArgs) Handles Menu13.Click
        AddData2Grid(Menu13.Tag.ToString)
    End Sub

    Private Sub Menu14_Click(sender As Object, e As EventArgs) Handles Menu14.Click
        AddData2Grid(Menu14.Tag.ToString)
    End Sub

    Private Sub Menu15_Click(sender As Object, e As EventArgs) Handles Menu15.Click
        AddData2Grid(Menu15.Tag.ToString)
    End Sub

    Private Sub Menu16_Click(sender As Object, e As EventArgs) Handles Menu16.Click
        AddData2Grid(Menu16.Tag.ToString)
    End Sub
End Class