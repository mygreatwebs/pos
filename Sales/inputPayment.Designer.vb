﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class inputPayment
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inputPayment))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.textKembalian = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.textTotalBayar = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.textTotalBiaya = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.textDisc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.textBiaya = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ListBoxControl1 = New DevExpress.XtraEditors.ListBoxControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.but1 = New DevExpress.XtraEditors.SimpleButton()
        Me.textCardNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.butHide = New DevExpress.XtraEditors.SimpleButton()
        Me.butProceed = New DevExpress.XtraEditors.SimpleButton()
        Me.butReset = New DevExpress.XtraEditors.SimpleButton()
        Me.butPayAll = New DevExpress.XtraEditors.SimpleButton()
        Me.but5 = New DevExpress.XtraEditors.SimpleButton()
        Me.butMinim = New DevExpress.XtraEditors.SimpleButton()
        Me.but10 = New DevExpress.XtraEditors.SimpleButton()
        Me.but50 = New DevExpress.XtraEditors.SimpleButton()
        Me.but100 = New DevExpress.XtraEditors.SimpleButton()
        Me.jmlBayar = New DevExpress.XtraEditors.TextEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.idEDC = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.namaedc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.idCOA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.debitCard = New DevExpress.XtraEditors.SimpleButton()
        Me.isgojek = New DevExpress.XtraEditors.CheckButton()
        Me.paymenttopuup = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.butBayar = New DevExpress.XtraEditors.SimpleButton()
        Me.butCash = New DevExpress.XtraEditors.SimpleButton()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.butCreditCard = New DevExpress.XtraEditors.SimpleButton()
        Me.butVouc = New DevExpress.XtraEditors.SimpleButton()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.textKembalian.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textTotalBayar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textTotalBiaya.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textDisc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textBiaya.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textCardNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.jmlBayar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.Controls.Add(Me.textKembalian)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.textTotalBayar)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.textTotalBiaya)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.textDisc)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.textBiaya)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(623, 193)
        Me.PanelControl1.TabIndex = 0
        '
        'textKembalian
        '
        Me.textKembalian.EditValue = "0"
        Me.textKembalian.Location = New System.Drawing.Point(181, 161)
        Me.textKembalian.Name = "textKembalian"
        Me.textKembalian.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textKembalian.Properties.Appearance.Options.UseFont = True
        Me.textKembalian.Properties.Appearance.Options.UseTextOptions = True
        Me.textKembalian.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textKembalian.Properties.Mask.EditMask = "n0"
        Me.textKembalian.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textKembalian.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textKembalian.Properties.ReadOnly = True
        Me.textKembalian.Size = New System.Drawing.Size(432, 24)
        Me.textKembalian.TabIndex = 12
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(84, 164)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(91, 18)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "Kembalian"
        '
        'textTotalBayar
        '
        Me.textTotalBayar.EditValue = "0"
        Me.textTotalBayar.Location = New System.Drawing.Point(181, 119)
        Me.textTotalBayar.Name = "textTotalBayar"
        Me.textTotalBayar.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textTotalBayar.Properties.Appearance.Options.UseFont = True
        Me.textTotalBayar.Properties.Appearance.Options.UseTextOptions = True
        Me.textTotalBayar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textTotalBayar.Properties.Mask.EditMask = "n0"
        Me.textTotalBayar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textTotalBayar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textTotalBayar.Properties.ReadOnly = True
        Me.textTotalBayar.Size = New System.Drawing.Size(432, 24)
        Me.textTotalBayar.TabIndex = 10
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(13, 122)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(162, 18)
        Me.LabelControl4.TabIndex = 9
        Me.LabelControl4.Text = "Total Pembayaran"
        '
        'textTotalBiaya
        '
        Me.textTotalBiaya.EditValue = "0"
        Me.textTotalBiaya.Location = New System.Drawing.Point(181, 87)
        Me.textTotalBiaya.Name = "textTotalBiaya"
        Me.textTotalBiaya.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textTotalBiaya.Properties.Appearance.Options.UseFont = True
        Me.textTotalBiaya.Properties.Appearance.Options.UseTextOptions = True
        Me.textTotalBiaya.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textTotalBiaya.Properties.Mask.EditMask = "n0"
        Me.textTotalBiaya.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textTotalBiaya.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textTotalBiaya.Properties.ReadOnly = True
        Me.textTotalBiaya.Size = New System.Drawing.Size(432, 24)
        Me.textTotalBiaya.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(71, 90)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(104, 18)
        Me.LabelControl3.TabIndex = 7
        Me.LabelControl3.Text = "Grand Total"
        '
        'textDisc
        '
        Me.textDisc.EditValue = "0"
        Me.textDisc.Location = New System.Drawing.Point(181, 46)
        Me.textDisc.Name = "textDisc"
        Me.textDisc.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textDisc.Properties.Appearance.Options.UseFont = True
        Me.textDisc.Properties.Appearance.Options.UseTextOptions = True
        Me.textDisc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textDisc.Properties.Mask.EditMask = "n0"
        Me.textDisc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textDisc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textDisc.Properties.ReadOnly = True
        Me.textDisc.Size = New System.Drawing.Size(432, 24)
        Me.textDisc.TabIndex = 6
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(140, 49)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(35, 18)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "Disc"
        '
        'textBiaya
        '
        Me.textBiaya.EditValue = "0"
        Me.textBiaya.Location = New System.Drawing.Point(181, 14)
        Me.textBiaya.Name = "textBiaya"
        Me.textBiaya.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textBiaya.Properties.Appearance.Options.UseFont = True
        Me.textBiaya.Properties.Appearance.Options.UseTextOptions = True
        Me.textBiaya.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textBiaya.Properties.Mask.EditMask = "n0"
        Me.textBiaya.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textBiaya.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textBiaya.Properties.ReadOnly = True
        Me.textBiaya.Size = New System.Drawing.Size(432, 24)
        Me.textBiaya.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(73, 17)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(102, 18)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Total Order"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ListBoxControl1)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.but1)
        Me.PanelControl2.Controls.Add(Me.textCardNo)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.butHide)
        Me.PanelControl2.Controls.Add(Me.butProceed)
        Me.PanelControl2.Controls.Add(Me.butReset)
        Me.PanelControl2.Controls.Add(Me.butPayAll)
        Me.PanelControl2.Controls.Add(Me.but5)
        Me.PanelControl2.Controls.Add(Me.butMinim)
        Me.PanelControl2.Controls.Add(Me.but10)
        Me.PanelControl2.Controls.Add(Me.but50)
        Me.PanelControl2.Controls.Add(Me.but100)
        Me.PanelControl2.Controls.Add(Me.jmlBayar)
        Me.PanelControl2.Controls.Add(Me.LookUpEdit1)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 211)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(623, 375)
        Me.PanelControl2.TabIndex = 17
        '
        'ListBoxControl1
        '
        Me.ListBoxControl1.ItemHeight = 25
        Me.ListBoxControl1.Location = New System.Drawing.Point(2, 38)
        Me.ListBoxControl1.Name = "ListBoxControl1"
        Me.ListBoxControl1.Size = New System.Drawing.Size(322, 335)
        Me.ListBoxControl1.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(330, 15)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl7.TabIndex = 40
        Me.LabelControl7.Text = "Jumlah"
        '
        'but1
        '
        Me.but1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but1.Appearance.Options.UseFont = True
        Me.but1.Location = New System.Drawing.Point(479, 192)
        Me.but1.Name = "but1"
        Me.but1.Size = New System.Drawing.Size(134, 58)
        Me.but1.TabIndex = 32
        Me.but1.TabStop = False
        Me.but1.Text = "1,000"
        '
        'textCardNo
        '
        Me.textCardNo.Location = New System.Drawing.Point(411, 37)
        Me.textCardNo.Name = "textCardNo"
        Me.textCardNo.Size = New System.Drawing.Size(202, 20)
        Me.textCardNo.TabIndex = 2
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(330, 40)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(43, 13)
        Me.LabelControl6.TabIndex = 39
        Me.LabelControl6.Text = "Card No."
        '
        'butHide
        '
        Me.butHide.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butHide.Appearance.Options.UseFont = True
        Me.butHide.Location = New System.Drawing.Point(541, 59)
        Me.butHide.Name = "butHide"
        Me.butHide.Size = New System.Drawing.Size(72, 63)
        Me.butHide.TabIndex = 5
        Me.butHide.Text = "Batal"
        '
        'butProceed
        '
        Me.butProceed.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butProceed.Appearance.Options.UseFont = True
        Me.butProceed.Location = New System.Drawing.Point(411, 59)
        Me.butProceed.Name = "butProceed"
        Me.butProceed.Size = New System.Drawing.Size(124, 63)
        Me.butProceed.TabIndex = 4
        Me.butProceed.Text = "OK"
        '
        'butReset
        '
        Me.butReset.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butReset.Appearance.Options.UseFont = True
        Me.butReset.Location = New System.Drawing.Point(330, 59)
        Me.butReset.Name = "butReset"
        Me.butReset.Size = New System.Drawing.Size(75, 63)
        Me.butReset.TabIndex = 3
        Me.butReset.Text = "C"
        '
        'butPayAll
        '
        Me.butPayAll.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPayAll.Appearance.Options.UseFont = True
        Me.butPayAll.Location = New System.Drawing.Point(330, 320)
        Me.butPayAll.Name = "butPayAll"
        Me.butPayAll.Size = New System.Drawing.Size(283, 52)
        Me.butPayAll.TabIndex = 6
        Me.butPayAll.Text = "Pay All"
        '
        'but5
        '
        Me.but5.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but5.Appearance.Options.UseFont = True
        Me.but5.Location = New System.Drawing.Point(479, 128)
        Me.but5.Name = "but5"
        Me.but5.Size = New System.Drawing.Size(134, 58)
        Me.but5.TabIndex = 34
        Me.but5.TabStop = False
        Me.but5.Text = "5,000"
        '
        'butMinim
        '
        Me.butMinim.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butMinim.Appearance.Options.UseFont = True
        Me.butMinim.Location = New System.Drawing.Point(479, 256)
        Me.butMinim.Name = "butMinim"
        Me.butMinim.Size = New System.Drawing.Size(134, 58)
        Me.butMinim.TabIndex = 31
        Me.butMinim.TabStop = False
        Me.butMinim.Text = "500"
        '
        'but10
        '
        Me.but10.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but10.Appearance.Options.UseFont = True
        Me.but10.Location = New System.Drawing.Point(330, 256)
        Me.but10.Name = "but10"
        Me.but10.Size = New System.Drawing.Size(134, 58)
        Me.but10.TabIndex = 28
        Me.but10.TabStop = False
        Me.but10.Text = "10,000"
        '
        'but50
        '
        Me.but50.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but50.Appearance.Options.UseFont = True
        Me.but50.Location = New System.Drawing.Point(330, 192)
        Me.but50.Name = "but50"
        Me.but50.Size = New System.Drawing.Size(134, 58)
        Me.but50.TabIndex = 26
        Me.but50.TabStop = False
        Me.but50.Text = "50,000"
        '
        'but100
        '
        Me.but100.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but100.Appearance.Options.UseFont = True
        Me.but100.Location = New System.Drawing.Point(330, 128)
        Me.but100.Name = "but100"
        Me.but100.Size = New System.Drawing.Size(134, 58)
        Me.but100.TabIndex = 25
        Me.but100.TabStop = False
        Me.but100.Text = "100,000"
        '
        'jmlBayar
        '
        Me.jmlBayar.EditValue = "0"
        Me.jmlBayar.Location = New System.Drawing.Point(411, 8)
        Me.jmlBayar.Name = "jmlBayar"
        Me.jmlBayar.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.jmlBayar.Properties.Appearance.Options.UseFont = True
        Me.jmlBayar.Properties.Appearance.Options.UseTextOptions = True
        Me.jmlBayar.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.jmlBayar.Properties.Mask.EditMask = "n0"
        Me.jmlBayar.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.jmlBayar.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.jmlBayar.Size = New System.Drawing.Size(202, 24)
        Me.jmlBayar.TabIndex = 24
        Me.jmlBayar.TabStop = False
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(2, 6)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.NullText = "[Pilih Mesin EDC]"
        Me.LookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.LookUpEdit1.Size = New System.Drawing.Size(322, 26)
        Me.LookUpEdit1.TabIndex = 41
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.idEDC, Me.namaedc, Me.idCOA})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'idEDC
        '
        Me.idEDC.Caption = "idEDC"
        Me.idEDC.FieldName = "idEDC"
        Me.idEDC.Name = "idEDC"
        '
        'namaedc
        '
        Me.namaedc.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaedc.AppearanceCell.Options.UseFont = True
        Me.namaedc.Caption = "Nama EDC"
        Me.namaedc.FieldName = "Nama EDC"
        Me.namaedc.Name = "namaedc"
        Me.namaedc.Visible = True
        Me.namaedc.VisibleIndex = 0
        '
        'idCOA
        '
        Me.idCOA.Caption = "idCOA"
        Me.idCOA.FieldName = "idCOA"
        Me.idCOA.Name = "idCOA"
        '
        'PanelControl3
        '
        Me.PanelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl3.Controls.Add(Me.debitCard)
        Me.PanelControl3.Controls.Add(Me.isgojek)
        Me.PanelControl3.Controls.Add(Me.paymenttopuup)
        Me.PanelControl3.Controls.Add(Me.GridControl1)
        Me.PanelControl3.Controls.Add(Me.SimpleButton1)
        Me.PanelControl3.Controls.Add(Me.butBayar)
        Me.PanelControl3.Controls.Add(Me.butCash)
        Me.PanelControl3.Controls.Add(Me.butBatal)
        Me.PanelControl3.Controls.Add(Me.butCreditCard)
        Me.PanelControl3.Controls.Add(Me.butVouc)
        Me.PanelControl3.Controls.Add(Me.butDelete)
        Me.PanelControl3.Location = New System.Drawing.Point(12, 211)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(623, 375)
        Me.PanelControl3.TabIndex = 18
        '
        'debitCard
        '
        Me.debitCard.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.debitCard.Appearance.Options.UseFont = True
        Me.debitCard.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.debitCard.Location = New System.Drawing.Point(198, 5)
        Me.debitCard.Name = "debitCard"
        Me.debitCard.Size = New System.Drawing.Size(88, 60)
        Me.debitCard.TabIndex = 20
        Me.debitCard.Text = "Debit Card"
        '
        'isgojek
        '
        Me.isgojek.Appearance.BackColor = System.Drawing.Color.GreenYellow
        Me.isgojek.Appearance.Options.UseBackColor = True
        Me.isgojek.Image = CType(resources.GetObject("isgojek.Image"), System.Drawing.Image)
        Me.isgojek.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.isgojek.Location = New System.Drawing.Point(407, 316)
        Me.isgojek.Name = "isgojek"
        Me.isgojek.Size = New System.Drawing.Size(100, 53)
        Me.isgojek.TabIndex = 19
        '
        'paymenttopuup
        '
        Me.paymenttopuup.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.paymenttopuup.Appearance.Options.UseFont = True
        Me.paymenttopuup.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.paymenttopuup.Location = New System.Drawing.Point(480, 5)
        Me.paymenttopuup.Name = "paymenttopuup"
        Me.paymenttopuup.Size = New System.Drawing.Size(87, 60)
        Me.paymenttopuup.TabIndex = 17
        Me.paymenttopuup.Text = "E-Money"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(9, 69)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(605, 241)
        Me.GridControl1.TabIndex = 15
        Me.GridControl1.TabStop = False
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.EnableAppearanceOddRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.SimpleButton1.Location = New System.Drawing.Point(292, 5)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(88, 60)
        Me.SimpleButton1.TabIndex = 16
        Me.SimpleButton1.Text = "BRI"
        '
        'butBayar
        '
        Me.butBayar.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBayar.Appearance.Options.UseFont = True
        Me.butBayar.Enabled = False
        Me.butBayar.Image = CType(resources.GetObject("butBayar.Image"), System.Drawing.Image)
        Me.butBayar.Location = New System.Drawing.Point(513, 316)
        Me.butBayar.Name = "butBayar"
        Me.butBayar.Size = New System.Drawing.Size(101, 54)
        Me.butBayar.TabIndex = 5
        Me.butBayar.Text = "Bayar"
        '
        'butCash
        '
        Me.butCash.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCash.Appearance.Options.UseFont = True
        Me.butCash.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.butCash.Location = New System.Drawing.Point(11, 5)
        Me.butCash.Name = "butCash"
        Me.butCash.Size = New System.Drawing.Size(87, 60)
        Me.butCash.TabIndex = 1
        Me.butCash.Text = "Cash"
        '
        'butBatal
        '
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Image = CType(resources.GetObject("butBatal.Image"), System.Drawing.Image)
        Me.butBatal.Location = New System.Drawing.Point(9, 313)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(101, 54)
        Me.butBatal.TabIndex = 6
        Me.butBatal.Text = "Batal"
        '
        'butCreditCard
        '
        Me.butCreditCard.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCreditCard.Appearance.Options.UseFont = True
        Me.butCreditCard.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.butCreditCard.Location = New System.Drawing.Point(104, 5)
        Me.butCreditCard.Name = "butCreditCard"
        Me.butCreditCard.Size = New System.Drawing.Size(88, 60)
        Me.butCreditCard.TabIndex = 2
        Me.butCreditCard.Text = "Credit Card"
        '
        'butVouc
        '
        Me.butVouc.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butVouc.Appearance.Options.UseFont = True
        Me.butVouc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.butVouc.Location = New System.Drawing.Point(386, 5)
        Me.butVouc.Name = "butVouc"
        Me.butVouc.Size = New System.Drawing.Size(88, 60)
        Me.butVouc.TabIndex = 3
        Me.butVouc.Text = "Voucher"
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Enabled = False
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.butDelete.Location = New System.Drawing.Point(573, 33)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(41, 32)
        Me.butDelete.TabIndex = 4
        Me.butDelete.Text = "X"
        '
        'inputPayment
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 593)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "inputPayment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.textKembalian.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textTotalBayar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textTotalBiaya.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textDisc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textBiaya.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textCardNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.jmlBayar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents textKembalian As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textTotalBayar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textTotalBiaya As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textDisc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textBiaya As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butHide As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butProceed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butPayAll As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butMinim As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but50 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but100 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents jmlBayar As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ListBoxControl1 As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butBayar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butCash As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butCreditCard As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents butVouc As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textCardNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents paymenttopuup As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents isgojek As DevExpress.XtraEditors.CheckButton
    Friend WithEvents debitCard As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents idEDC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents namaedc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents idCOA As DevExpress.XtraGrid.Columns.GridColumn
End Class
