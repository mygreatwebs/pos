﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class editSales
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.textQtt = New DevExpress.XtraEditors.TextEdit()
        Me.textPrice = New DevExpress.XtraEditors.TextEdit()
        Me.textName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.textDisc = New DevExpress.XtraEditors.TextEdit()
        Me.but50 = New DevExpress.XtraEditors.SimpleButton()
        Me.but20 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.but10 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textQtt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textPrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textDisc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.butOK)
        Me.GroupControl1.Controls.Add(Me.butDelete)
        Me.GroupControl1.Controls.Add(Me.textQtt)
        Me.GroupControl1.Controls.Add(Me.textPrice)
        Me.GroupControl1.Controls.Add(Me.textName)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.textDisc)
        Me.GroupControl1.Controls.Add(Me.but50)
        Me.GroupControl1.Controls.Add(Me.but20)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.but10)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(441, 384)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Koreksi Detail Produk"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(92, 233)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(50, 32)
        Me.LabelControl6.TabIndex = 18
        Me.LabelControl6.Text = "Rp. "
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "0"
        Me.TextEdit1.Location = New System.Drawing.Point(161, 230)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
        Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.TextEdit1.Properties.Mask.EditMask = "n0"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(217, 38)
        Me.TextEdit1.TabIndex = 17
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butOK.Location = New System.Drawing.Point(300, 298)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(114, 70)
        Me.butOK.TabIndex = 16
        Me.butOK.Text = "Simpan"
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDelete.Location = New System.Drawing.Point(12, 298)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(114, 70)
        Me.butDelete.TabIndex = 15
        Me.butDelete.Text = "Hapus"
        '
        'textQtt
        '
        Me.textQtt.EditValue = "0"
        Me.textQtt.Location = New System.Drawing.Point(92, 69)
        Me.textQtt.Name = "textQtt"
        Me.textQtt.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textQtt.Properties.Appearance.Options.UseFont = True
        Me.textQtt.Properties.Appearance.Options.UseTextOptions = True
        Me.textQtt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textQtt.Properties.Mask.EditMask = "n0"
        Me.textQtt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textQtt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textQtt.Properties.MaxLength = 3
        Me.textQtt.Size = New System.Drawing.Size(79, 42)
        Me.textQtt.TabIndex = 14
        '
        'textPrice
        '
        Me.textPrice.EditValue = "0"
        Me.textPrice.Location = New System.Drawing.Point(92, 117)
        Me.textPrice.Name = "textPrice"
        Me.textPrice.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textPrice.Properties.Appearance.Options.UseFont = True
        Me.textPrice.Properties.Appearance.Options.UseTextOptions = True
        Me.textPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textPrice.Properties.Mask.EditMask = "n0"
        Me.textPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textPrice.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textPrice.Properties.ReadOnly = True
        Me.textPrice.Size = New System.Drawing.Size(201, 32)
        Me.textPrice.TabIndex = 13
        '
        'textName
        '
        Me.textName.Location = New System.Drawing.Point(92, 31)
        Me.textName.Name = "textName"
        Me.textName.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textName.Properties.Appearance.Options.UseFont = True
        Me.textName.Properties.ReadOnly = True
        Me.textName.Size = New System.Drawing.Size(322, 32)
        Me.textName.TabIndex = 12
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(384, 169)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(34, 32)
        Me.LabelControl5.TabIndex = 11
        Me.LabelControl5.Text = "%"
        '
        'textDisc
        '
        Me.textDisc.EditValue = "0"
        Me.textDisc.Location = New System.Drawing.Point(299, 164)
        Me.textDisc.Name = "textDisc"
        Me.textDisc.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textDisc.Properties.Appearance.Options.UseFont = True
        Me.textDisc.Properties.Appearance.Options.UseTextOptions = True
        Me.textDisc.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.textDisc.Properties.Mask.EditMask = "n0"
        Me.textDisc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textDisc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.textDisc.Properties.MaxLength = 3
        Me.textDisc.Properties.ReadOnly = True
        Me.textDisc.Size = New System.Drawing.Size(79, 42)
        Me.textDisc.TabIndex = 10
        '
        'but50
        '
        Me.but50.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but50.Appearance.Options.UseFont = True
        Me.but50.Location = New System.Drawing.Point(230, 155)
        Me.but50.Name = "but50"
        Me.but50.Size = New System.Drawing.Size(63, 62)
        Me.but50.TabIndex = 7
        Me.but50.Text = "50%"
        '
        'but20
        '
        Me.but20.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but20.Appearance.Options.UseFont = True
        Me.but20.Location = New System.Drawing.Point(161, 155)
        Me.but20.Name = "but20"
        Me.but20.Size = New System.Drawing.Size(63, 62)
        Me.but20.TabIndex = 6
        Me.but20.Text = "20%"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(40, 167)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(46, 18)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Disc :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(21, 126)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(65, 18)
        Me.LabelControl4.TabIndex = 4
        Me.LabelControl4.Text = "Harga :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(44, 86)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(42, 18)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Qty :"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 18)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Produk :"
        '
        'but10
        '
        Me.but10.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.but10.Appearance.Options.UseFont = True
        Me.but10.Location = New System.Drawing.Point(92, 155)
        Me.but10.Name = "but10"
        Me.but10.Size = New System.Drawing.Size(63, 62)
        Me.but10.TabIndex = 1
        Me.but10.Text = "10%"
        '
        'editSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 384)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "editSales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textQtt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textPrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textDisc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents but10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents but50 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents but20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textDisc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents textQtt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textPrice As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
End Class
