﻿Public Class frmconfirm_1
    Public Sub callme(ByVal jmlitempromo As Integer, ByVal lbljudul As String)
        Dim pembagipromo As Integer

        If AktifPromo = 1 Then
            pembagipromo = 2
        ElseIf AktifPromo = 2 Then
            pembagipromo = 6
        End If
        Dim jmlgratis As Integer = Int(jmlitempromo / pembagipromo)
        Dim jmlbyar As Integer = jmlitempromo - jmlgratis
        Dim x As Integer = Convert.ToInt32(jmlitempromo)
        Dim checker As Integer = x Mod pembagipromo
        LabelControl1.Text = lbljudul
        txbJumlah.Text = jmlitempromo
        If AktifPromo = 1 Then
            txbTambah.Text = 1 - checker
        ElseIf AktifPromo = 2 Then
            txbTambah.Text = 5 - checker
        End If
        LabelControl2.Text += " " & jmlgratis
        LabelControl3.Text += " " & jmlbyar
        Me.ShowDialog()
    End Sub

    Private Sub frmconfirm_1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If idfrmconfirm_1 = 0 Then
            e.Cancel = True
            Exit Sub
        End If
        Dispose()
    End Sub

    Private Sub frmconfirm_1_Load(sender As Object, e As EventArgs) Handles Me.Load
        idfrmconfirm_1 = 0
    End Sub

    Private Sub vc_promo_Click(sender As Object, e As EventArgs) Handles vc_promo.Click
        idfrmconfirm_1 = 1
        AktifPromo = 0
        Close()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        idfrmconfirm_1 = 2
        Close()
    End Sub


End Class