﻿
Imports MySql.Data.MySqlClient
Imports DevExpress.XtraSplashScreen
Imports System.Threading
Imports System.Threading.Tasks
Imports System.IO

Public Class FormPOSx

    Dim idKategoriMenu, Depth, pageNo, idParent, buttonNo, idParentLv1, idParentLv2 As Integer
    Dim selectString As String
    Dim dv As DataView
    Dim idxgambar As Integer
    Dim disablemouse As Boolean
    Dim lagiprosestransfer As Boolean
    Public TotalMkn As Integer
    Public DiskonPersen As Boolean
    Dim IsShowingformbayar = True
    Dim isitem5gratis1 As Boolean = False


    Private Function totalpos_cloud(ByVal mtglawal As Date) As Double
        Dim totcabang As Double = 0
        If IsNothing(xSet.Tables("cekmcabang")) = False Then xSet.Tables("cekmcabang").Clear()

        SQLquery = "Select * from mcabang where idCabang not in (999," & id_cabang & ")"
        ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "cekmCabang")
        For Each dtRow In xSet.Tables("cekmCabang").Rows
            If IsNothing(xSet.Tables("mtotcabang")) = False Then xSet.Tables("mtotcabang").Clear()
            SQLquery = " select ifnull(sum(a.subtotal-disc),0) tot from " & dtRow("db_name") & ".mtpos a left join " & dtRow("db_name") & ".mtposmember b on a.idOrder  = b.idOrder where a.isVoid =0 " & _
                       " and date(a.createdate) between '" & Format(mtglawal, "yyyy-MM-dd") & "' and date(now()) and kodemember='" & kodemember & "'"
            ExDb.ExecQuery(SQLquery, SQLquery, xSet, "mtotcabang")
            If xSet.Tables("mtotcabang").Rows.Count > 0 Then totcabang = totcabang + xSet.Tables("mtotcabang")(0)("tot")
        Next
        xSet.Tables.Remove("mtotcabang")
        xSet.Tables.Remove("cekmcabang")
    End Function

    Private Function CekFreeKalender(ByVal qtyfree As Integer) As Boolean
        Dim jmlbyarpesanan As Double = 0
        Dim jmlpos As Double = 0
        Dim tglawal As Date

        'cek koneksi koneksi OL
        If TestOnline() Then
            Try
                'Cek Berlaku Free Kalender 
                If IsNothing(xSet.Tables("CekFreeKalender")) = False Then xSet.Tables("CekFreeKalender").Clear()
                SQLquery = "SELECT * from mcustomer where inactive=0 and kode_member='" & kodemember & "'"
                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "CekFreeKalender")
                If xSet.Tables("CekFreeKalender").Rows.Count = 0 Then
                    MsgBox("Data Member Tidak Di Temukan !")
                    kodemember = ""
                    Return False
                End If
                If IsNothing(xSet.Tables("CekFreeKalender")) = False Then xSet.Tables("CekFreeKalender").Clear()
                SQLquery = "SELECT * from app_promo.mpromo where aktif=1 and '" & Format(Now, "yyyy-MM-dd") & "' between tglmulai and tglakhir and namapromo='Free Kalender 2017'"
                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "CekFreeKalender")

                If xSet.Tables("CekFreeKalender").Rows.Count = 0 Then
                    MsgBox("Promo Free Kalender Sudah Berakhir")
                    Return False
                Else
                    tglawal = xSet.Tables("CekFreeKalender").Rows(0)("tglmulai")
                End If
                If qtyfree > 1 Then
                    MsgBox("Qty Free Member tidak boleh lebih dari 1")
                    Return False
                End If
                'cek apaudah pernah dapat free
                If IsNothing(xSet.Tables("CekFreeKalender")) = False Then xSet.Tables("CekFreeKalender").Clear()
                SQLquery = "SELECT * from mtfreekalender where   kodemember='" & kodemember & "'"
                ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "CekFreeKalender")
                If xSet.Tables("CekFreeKalender").Rows.Count > 0 Then
                    MsgBox("Ups! Member sudah pernah dapat free kalender")
                    Return False
                End If
                If IsNothing(xSet.Tables("CekFreeKalender")) = False Then xSet.Tables("CekFreeKalender").Clear()
                SQLquery = "select ifnull(sum(c.amount),0) tot from mtpesanan a left join mtbayarpesanan b on a.idPesanan = b.idPesanan " & _
                           " left join dtbayarpesanan c on b.idBayarPesanan =c.idBayarPesanan left join mcustomer d on a.idCust=d.idCust where c.idPayment not in (8,10) and date(b.tglPayment) between '" & Format(tglawal, "yyyy-MM-dd") & "' and date(now()) and kode_member='" & kodemember & "' and a.void=0"
                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "CekFreeKalender")
                If xSet.Tables("CekFreeKalender").Rows.Count > 0 Then jmlbyarpesanan = xSet.Tables("CekFreeKalender").Rows(0)("tot")
                SQLquery = " select ifnull(sum(a.subtotal-disc),0) tot from mtpos a left join mtposmember b on a.idOrder  = b.idOrder where a.isVoid =0 " & _
                           " and date(a.createdate) between '" & Format(tglawal, "yyyy-MM-dd") & "' and date(now()) and kodemember='" & kodemember & "'"
                ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "CekFreeKalender")

                If xSet.Tables("CekFreeKalender").Rows.Count > 0 Then jmlpos = xSet.Tables("CekFreeKalender").Rows(0)("tot")
                If (jmlbyarpesanan + jmlpos) >= 1000000 Then
                    'dpt gratis
                    MsgBox("Proses Checking Berhasil !")
                    Return True
                Else
                    MsgBox("Nominal Transaksi Belum Memenuhi Syarat !")
                    Return False
                End If

            Catch es As Exception
                MsgBox(es.Message)
                Return False
            End Try
        Else
            MsgBox("Transaksi Free Kalender Tidak Dapat Dilakukan Karena Koneksi Offline")
            Return False
        End If
    End Function

    Private Function isBrgKalenderJan2017(ByVal idBrgN As Integer) As Boolean
        Dim jmldata As Integer
        Dim retval As Boolean
        SQLquery = "select count(idBrg) a from mbarang a inner join mkategoribrg2 b on a.idkategori2=b.idkategori2 where a.idBrg=" & idBrgN & " and " & _
                " upper(b.namaKategori2)  in ('BREAD','DOUGHNUT','CAKE','TRADITIONAL SNACK','SOES') and a.price <>0 and namaBrg not like '%disc%'"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Function idxKategorinya(ByVal idBrgN As Integer) As Integer
        Dim jmldata As Integer
        SQLquery = "select case when upper(b.namaKategori2)='BREAD' OR upper(b.namaKategori2)='DOUGHNUT' THEN 1 when upper(b.namaKategori2)='CAKE' THEN 2 ELSE 3 END  a from mbarang a inner join mkategoribrg2 b on a.idkategori2=b.idkategori2 where a.idBrg=" & idBrgN & " and " & _
                " upper(b.namaKategori2)  in ('BREAD','DOUGHNUT','CAKE','TRADITIONAL SNACK','SOES') and a.price <>0 and namaBrg not like '%disc%'"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)

        Return jmldata
    End Function

    Private Function isBrgPromoBeli5Gratis1MLG(ByVal idBrgN As Integer) As Boolean
        Dim jmldata As Integer
        Dim retval As Boolean
        SQLquery = "select count(idBrg) a from mbarang a inner join mkategoribrg2 b on a.idkategori2=b.idkategori2 where a.idBrg=" & idBrgN & " and " & _
                " upper(b.namaKategori2) in ('PASTRY & PIE','TART','TART SLICE','PUDDING') and a.price <>0 and namaBrg not like '%disc%'"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Function isBrgPromoBeli5Gratis1(ByVal idBrgN As Integer) As Boolean
        Dim jmldata As Integer
        Dim retval As Boolean
        SQLquery = "select count(idBrg) a from mbarang a inner join mkategoribrg2 b on a.idkategori2=b.idkategori2 where a.idBrg=" & idBrgN & " and " & _
                " upper(b.namaKategori2) not in ('DONAT UPIN/IPIN','COOKIES','SOFT DRINK','SOFT DRINK KONSINYASI','TART','WHOLE CAKE','D & K','PRODUCT PROMO','BS','FREE PRODUCT','LAIN-LAIN','PARCEL','BIAYA TAMBAHAN','MERCHANDISE') and a.price <>0 and namaBrg not like '%disc%'"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Function isBrgPromoKeju(ByVal idBrgN As Integer) As Boolean
        Dim jmldata As Integer
        Dim retval As Boolean
        'coklat keju'
        SQLquery = "select count(idBrg) from mbarang where mbarang.namaBrg in ('donat keju','cheese cream cheese','double cheese','keju manis','cheese john','roti keju') and idbrg=" & idBrgN & ";"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Function isBrgPromoWholeBreadMLG(ByVal idBrgN As Integer) As Boolean
        Dim jmldata As Integer
        Dim retval As Boolean
        'coklat keju'
        SQLquery = "select count(idBrg) from mbarang a left join mkategoribrg2 b on a.idkategori2=b.idkategori2 where b.namamkategori2='WHOLE BREAD' and  a.namaBrg not in ('Butter Roll','Burger Bun','Roti Sisir Polos') and idbrg=" & idBrgN & ";"

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Sub promokeju()
        'beli satu gratis satu
        If isvalidpromokeju = False Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If
        ispromoproces = True
        isPromoKeju = False
        If IsNothing(xSet.Tables("xtemppromo")) = False Then xSet.Tables("xtemppromo").Clear()

        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg from mbarang where 1=0", xSet, "xtemppromo")

        'xSet.Tables.Add("xtemppromo_2")
        Dim jmlitempromo As Integer = 0
        Dim nourut As Integer = 1
        Dim jmlitemdiskon As Integer = 0
        '
        For Each rowdt As DataRow In xSet.Tables("setsales").Rows
            If isBrgPromoKeju(rowdt("idBrg")) Then
                isPromoKeju = True
                jmlitempromo += rowdt("jmlh")
                xSet.Tables("xtemppromo").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
                'xSet.Tables("xtemppromo_2").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
            End If
            nourut = nourut + 1
        Next
        nourut = 1
        Dim rowdiskon As Integer = 0
        Dim jmlqty As Integer = 0
        Dim jmlqydiskon As Integer = 0
        If jmlitempromo < 1 Then
            AktifPromo = 0
            ispromoproces = False
            Exit Sub
        End If
        'frmInputKodeMember.callme()
        AktifPromo = 1
        frmconfirm_1.callme(jmlitempromo, "Jumlah Item Promo Keju Beli 1 Gratis 1  yang dibawa ke kasir")

        'End If
        If idfrmconfirm_1 = 1 Then
            ispromoproces = False
            Exit Sub
        End If


        'cek apakah jumlah item diskon yang diskonnya 100 percen
        Dim jmldiskon100 As Integer = xSet.Tables("setsales").Select("disc=hrg and hrgpcs>0").Count
        jmlqydiskon = Int(jmlitempromo / 2)
        'If jmldiskon100 <> jmlqydiskon Then
        '    MsgBox("Jumlah Item Diskon tidak sama dengan  Item free !")
        '    Exit Sub
        'End If
        If jmlqydiskon = 0 Then
            ispromoproces = False
            Exit Sub
        End If
        'cari hargaminim
        Dim hrgmin As Integer = 0
        Dim deffilter As String = ""

hitunglagi:
        hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs)")(0)("hrgpcs")
        deffilter = "hrgpcs=" & hrgmin
        For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select(deffilter, "jmlh desc")
            rowdiskon = dtrow("rowid")

            If dtrow("jmlh") = jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
                If jmlqydiskon = 0 Then Exit For
            ElseIf dtrow("jmlh") > jmlqydiskon Then
                'dipecah
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") = xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") - jmlqydiskon
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = (xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") * xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrgpcs")) - xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc")
                Dim rowdtz As DataRow = xSet.Tables("setSales").Rows(rowdiskon - 1)
                xSet.Tables("setSales").Rows.Add(rowdtz.Item("idBrg"),
                                                           rowdtz.Item("namaBrg"),
                                                            jmlqydiskon,
                                                            0,
                                                          rowdtz.Item("hrgpcs"),
                                                            100)
                jmlqydiskon = 0
                Exit For
            ElseIf dtrow("jmlh") < jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
            End If
        Next
        If jmlqydiskon > 0 Then
            'MsgBox(xSet.Tables("xtemppromo").Rows.Count)
            For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select("hrgpcs=" & hrgmin)
                dtrow.Delete()
            Next

            xSet.Tables("xtemppromo").AcceptChanges()


            'hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs) and hrgpcs<>" & hrgmin)(0)("hrgpcs")
            GoTo hitunglagi
        End If
        disablemenu(False)
        calculateDiscTotal()
    End Sub

    Private Sub PromoBeli5Gratis1MLG()
        'untuk promo bulan depan
        'If isPromoKeju Then Exit Sub
        If isBeli5Gratis1MLG = False Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If

        ispromoproces = True
        If IsNothing(xSet.Tables("xtemppromo")) = False Then xSet.Tables("xtemppromo").Clear()
        If IsNothing(xSet.Tables("xtemppromo_2")) = False Then xSet.Tables("xtemppromo_2").Clear()
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg from mbarang where 1=0", xSet, "xtemppromo")
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg from mbarang where 1=0", xSet, "xtemppromo_2")
        'xSet.Tables.Add("xtemppromo_2")
        Dim jmlitempromo As Integer = 0
        Dim nourut As Integer = 1
        Dim jmlitemdiskon As Integer = 0

        'barang yg bukan termasuk promo DONAT UPIN/IPIN,cookies,soft drink,soft drink konsinyasi,whole bread,whole cake,D & K
        For Each rowdt As DataRow In xSet.Tables("setsales").Rows
            If isBrgPromoBeli5Gratis1MLG(rowdt("idBrg")) Then
                jmlitempromo += rowdt("jmlh")
                xSet.Tables("xtemppromo").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
                'xSet.Tables("xtemppromo_2").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
            End If
            nourut = nourut + 1
        Next
        nourut = 1
        Dim rowdiskon As Integer = 0
        Dim jmlqty As Integer = 0
        Dim jmlqydiskon As Integer = 0
        If jmlitempromo < 5 Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If

        'If kodemember = "" Then
        '    frmInputKodeMember.callme()
        'End If

        'If kodemember = "" Then
        '    ispromoproces = False
        '    AktifPromo = 0
        '    Exit Sub
        'End If

        'If idfrmconfirm_1 = 0 Then
        AktifPromo = 2
        frmconfirm_1.callme(jmlitempromo, "Jumlah Item Promo 5 Gratis 1  yang dibawa ke kasir")
        'End If
        If idfrmconfirm_1 = 1 Then
            ispromoproces = False
            Exit Sub
        End If


        'cek apakah jumlah item diskon yang diskonnya 100 percen
        Dim jmldiskon100 As Integer = xSet.Tables("setsales").Select("disc=hrg and hrgpcs>0").Count
        jmlqydiskon = Int(jmlitempromo / 6)
        'If jmldiskon100 <> jmlqydiskon Then
        '    MsgBox("Jumlah Item Diskon tidak sama dengan  Item free !")
        '    Exit Sub
        'End If
        If jmlqydiskon = 0 Then
            ispromoproces = False
            Exit Sub
        End If
        'cari hargaminim
        Dim hrgmin As Integer = 0
        Dim deffilter As String = ""

hitunglagi:
        hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs)")(0)("hrgpcs")
        deffilter = "hrgpcs=" & hrgmin
        For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select(deffilter, "jmlh desc")
            rowdiskon = dtrow("rowid")

            If dtrow("jmlh") = jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
                If jmlqydiskon = 0 Then Exit For
            ElseIf dtrow("jmlh") > jmlqydiskon Then
                'dipecah
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") = xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") - jmlqydiskon
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = (xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") * xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrgpcs")) - xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc")
                Dim rowdtz As DataRow = xSet.Tables("setSales").Rows(rowdiskon - 1)
                xSet.Tables("setSales").Rows.Add(rowdtz.Item("idBrg"),
                                                           rowdtz.Item("namaBrg"),
                                                            jmlqydiskon,
                                                            0,
                                                          rowdtz.Item("hrgpcs"),
                                                            100)
                jmlqydiskon = 0
                Exit For
            ElseIf dtrow("jmlh") < jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
            End If
        Next
        If jmlqydiskon > 0 Then
            'MsgBox(xSet.Tables("xtemppromo").Rows.Count)
            For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select("hrgpcs=" & hrgmin)
                dtrow.Delete()
            Next

            xSet.Tables("xtemppromo").AcceptChanges()


            'hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs) and hrgpcs<>" & hrgmin)(0)("hrgpcs")
            GoTo hitunglagi
        End If
        disablemenu(False)
        calculateDiscTotal()
        'ispromoproces = False
    End Sub

    Private Sub PromoBeli5Gratis1()
        'untuk promo bulan depan
        'If isPromoKeju Then Exit Sub
        If isBeli5Gratis1 = False Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If

        ispromoproces = True
        If IsNothing(xSet.Tables("xtemppromo")) = False Then xSet.Tables("xtemppromo").Clear()
        If IsNothing(xSet.Tables("xtemppromo_2")) = False Then xSet.Tables("xtemppromo_2").Clear()
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg from mbarang where 1=0", xSet, "xtemppromo")
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg from mbarang where 1=0", xSet, "xtemppromo_2")
        'xSet.Tables.Add("xtemppromo_2")
        Dim jmlitempromo As Integer = 0
        Dim nourut As Integer = 1
        Dim jmlitemdiskon As Integer = 0

        'barang yg bukan termasuk promo DONAT UPIN/IPIN,cookies,soft drink,soft drink konsinyasi,whole bread,whole cake,D & K
        For Each rowdt As DataRow In xSet.Tables("setsales").Rows
            If isBrgPromoBeli5Gratis1(rowdt("idBrg")) Then
                jmlitempromo += rowdt("jmlh")
                xSet.Tables("xtemppromo").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
                'xSet.Tables("xtemppromo_2").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"))
            End If
            nourut = nourut + 1
        Next
        nourut = 1
        Dim rowdiskon As Integer = 0
        Dim jmlqty As Integer = 0
        Dim jmlqydiskon As Integer = 0
        If jmlitempromo < 5 Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If

        'If kodemember = "" Then
        '    frmInputKodeMember.callme()
        'End If

        If kodemember = "" Then
            ispromoproces = False
            AktifPromo = 0
            Exit Sub
        End If

        'If idfrmconfirm_1 = 0 Then
        AktifPromo = 2
        frmconfirm_1.callme(jmlitempromo, "Jumlah Item Promo 5 Gratis 1  yang dibawa ke kasir")
        'End If
        If idfrmconfirm_1 = 1 Then
            ispromoproces = False
            Exit Sub
        End If


        'cek apakah jumlah item diskon yang diskonnya 100 percen
        Dim jmldiskon100 As Integer = xSet.Tables("setsales").Select("disc=hrg and hrgpcs>0").Count
        jmlqydiskon = Int(jmlitempromo / 6)
        'If jmldiskon100 <> jmlqydiskon Then
        '    MsgBox("Jumlah Item Diskon tidak sama dengan  Item free !")
        '    Exit Sub
        'End If
        If jmlqydiskon = 0 Then
            ispromoproces = False
            Exit Sub
        End If
        'cari hargaminim
        Dim hrgmin As Integer = 0
        Dim deffilter As String = ""

hitunglagi:
        hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs)")(0)("hrgpcs")
        deffilter = "hrgpcs=" & hrgmin
        For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select(deffilter, "jmlh desc")
            rowdiskon = dtrow("rowid")

            If dtrow("jmlh") = jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
                If jmlqydiskon = 0 Then Exit For
            ElseIf dtrow("jmlh") > jmlqydiskon Then
                'dipecah
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") = xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") - jmlqydiskon
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = (xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") * xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrgpcs")) - xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc")
                Dim rowdtz As DataRow = xSet.Tables("setSales").Rows(rowdiskon - 1)
                xSet.Tables("setSales").Rows.Add(rowdtz.Item("idBrg"),
                                                           rowdtz.Item("namaBrg"),
                                                            jmlqydiskon,
                                                            0,
                                                          rowdtz.Item("hrgpcs"),
                                                            100)
                jmlqydiskon = 0
                Exit For
            ElseIf dtrow("jmlh") < jmlqydiskon Then
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 100
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = 0
                jmlqydiskon = jmlqydiskon - dtrow("jmlh")
            End If
        Next
        If jmlqydiskon > 0 Then
            'MsgBox(xSet.Tables("xtemppromo").Rows.Count)
            For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select("hrgpcs=" & hrgmin)
                dtrow.Delete()
            Next

            xSet.Tables("xtemppromo").AcceptChanges()


            'hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs) and hrgpcs<>" & hrgmin)(0)("hrgpcs")
            GoTo hitunglagi
        End If
        disablemenu(False)
        calculateDiscTotal()
        'ispromoproces = False
    End Sub

    Private Sub disablemenu(ByVal isenables As Boolean)
        Menu1.Enabled = isenables
        Menu2.Enabled = isenables
        Menu3.Enabled = isenables
        Menu4.Enabled = isenables
        Menu5.Enabled = isenables
        Menu6.Enabled = isenables
        Menu7.Enabled = isenables
        Menu8.Enabled = isenables
        Menu9.Enabled = isenables
    End Sub
    Private Sub getDataButton()
        If Not xSet.Tables("getDataButton") Is Nothing Then xSet.Tables("getDataButton").Clear()
        SQLquery = "SELECT idMenu, idKategoriMenu, depth, pageNo, buttonNo, a.idBrg, displayText, isShow, " & _
            "isProduct, idParent, IFNULL(price, 0) AS price, length(imgfile) AS nullFile, imgFile FROM mmenu a LEFT JOIN mbarang b ON a.idBrg=b.idBrg WHERE isShow=1"

        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDataButton")
    End Sub

    'Private Sub function qtysblmtrans
    Private Sub printbeforemutasi()
        SqlAftermutasi = ""
        Sqlbeforemutasi = String.Format("SELECT a.idBrg,namaBrg, namaKategori2, SUM( IFNULL(qttIN,0)) AS qttIN, SUM( IFNULL(qttOUT,0)) AS qttOUT, SUM( IFNULL(qttIN,0)) + SUM(IFNULL(qttOUT,0)) AS Balance " & _
                              " FROM mbarang a INNER  JOIN (SELECT        idBrg, quantity AS qttIN, 0 AS qttOUT " & _
                               " FROM            tstok a WHERE        (idCabang = {0}) AND (isExpired = 0) AND (isVoid = 0) AND (quantity > 0)  " & _
                               " UNION ALL " & _
                               " SELECT        idBrg, 0 AS qttIN, quantity AS qttOUT FROM tstok b  " & _
                               " WHERE        (idCabang = {0}) AND (isExpired = 0) AND (isVoid = 0) AND (quantity < 0)) b_1 ON a.idBrg = b_1.idBrg LEFT OUTER JOIN " & _
                               " mkategoribrg2 c ON a.idKategori2 = c.idKategori2 WHERE  a.idBrg in (select idBrg from msetupitembs)  and (a.isToko = 1) AND (a.inactive=0)  GROUP BY a.idBrg " & _
                               " ORDER BY c.namaKategori2, a.namaBrg ", id_cabang)

        'simpan data ke tabeltemp
        If IsNothing(xSet.Tables("tsbeforemutasi")) = False Then xSet.Tables("tsbeforemutasi").Clear()
        ExDb.ExecQuery(My.Settings.db_conn_mysql, Sqlbeforemutasi, xSet, "tsbeforemutasi")

        'Using printout As New Stok_3
        'Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
        'tool.ShowPreviewDialog()
        'End Using
        'End Using
    End Sub

    Private Sub printaftermutasi()
        SqlAftermutasi = "SELECT namaBrg, namaKategori2, SUM( IFNULL(qttIN,0)) AS qttIN, SUM( IFNULL(qttOUT,0)) AS qttOUT, SUM( IFNULL(qttIN,0)) + SUM(IFNULL(qttOUT,0)) AS Balance " & _
                              " FROM mbarang a INNER  JOIN (SELECT        idBrg, quantity AS qttIN, 0 AS qttOUT " & _
                               " FROM            tstok a WHERE        (idCabang = @idCabang) AND (isExpired = 0) AND (isVoid = 0) AND (quantity > 0)  " & _
                               " UNION ALL " & _
                               " SELECT        idBrg, 0 AS qttIN, quantity AS qttOUT FROM tstok b  " & _
                               " WHERE        (idCabang = @idCabang) AND (isExpired = 0) AND (isVoid = 0) AND (quantity < 0)) b_1 ON a.idBrg = b_1.idBrg LEFT OUTER JOIN " & _
                               " mkategoribrg2 c ON a.idKategori2 = c.idKategori2 WHERE  a.idBrg in (select idBrgBS from msetupitembs)  and (a.isToko = 1) AND (a.inactive=0)  GROUP BY a.idBrg " & _
                               " ORDER BY c.namaKategori2, a.namaBrg "

        Using printout As New Stok_3
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                tool.ShowPreviewDialog()
            End Using
        End Using
    End Sub

    Private Function getqtybefore(ByVal vidBrg As String) As Double

        If IsNothing(xSet.Tables("tsbeforemutasi")) Then
            Return 0
            Exit Function
        End If
        If xSet.Tables("tsbeforemutasi").Select("idBrg=" & vidBrg).Count > 0 Then
            Return xSet.Tables("tsbeforemutasi").Select("idBrg=" & vidBrg)(0)("balance")
        Else
            Return 0
        End If

    End Function
    Private Function iskategoridonat(ByVal idBrg As Integer) As Boolean

        Dim jmldata As Integer
        Dim retval As Boolean
        'coklat keju'
        SQLquery = "select count(idBrg) from mbarang a left join mkategoribrg2 b on a.idkategori2=b.idkategori2 where b.namakategori2='DOUGHNUT' and idBrg=" & idBrg

        jmldata = ExDb.Scalar(SQLquery, conn_string_local)
        retval = (jmldata = 1)
        Return retval
    End Function

    Private Function cekjumlahkategoridonat() As Integer
        Dim jmld As Integer = 0
        For Each dtrow As DataRow In xSet.Tables("setSales").Select("hrg>0")
            If iskategoridonat(dtrow("idBrg")) = True Then
                jmld += dtrow("jmlh")
            End If
        Next
        Return jmld
    End Function
    Private Sub getDataButtonTRBS()
        Dim numToko As Integer = 0
        Dim id_adjdet As Integer = 0
        Dim qtytrans As Double
        Dim Zcon As MySqlConnection
        Dim vartr As MySql.Data.MySqlClient.MySqlTransaction

        lagiprosestransfer = True
        If IsNothing(xSet.Tables("getDataButton")) Then Exit Sub
        'idBrg,Displaytext
        If IsNothing(xSet.Tables("getDataButtonBS")) = False Then xSet.Tables("getDataButtonBS").Clear()
        If adadatatransfer = False Then
            'cetak sebelum mutasi
            printbeforemutasi()
            Try
                Zcon = New MySqlConnection(My.Settings.db_conn_mysql)
                Dim command As New MySqlCommand()
                'Dim command2 As New MySqlCommand
                Zcon.Open()
                command.Connection = Zcon
                vartr = Zcon.BeginTransaction()
                command.Transaction = vartr

                SQLquery = "SELECT a.idBrg,idBrgBS,namaBRG as namaBS,price from msetupitembs a inner join mbarang b on a.idBrgBS=b.idBrg"
                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDataButtonBS")

                'proses mutasi

                'insert baru
                SQLquery = "SELECT IFNULL( MAX( numAdj ) , 0 ) +1 getNewNum FROM mtadj WHERE MONTH(createDate)=MONTH(CURDATE()) AND YEAR(createDate)=YEAR(CURDATE()) AND idCabang=" & id_cabang
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                command.CommandText = SQLquery

                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
                If xSet.Tables("getNewNum").Rows(0).Item(0).ToString <> "" Then
                    numToko = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                Else
                    numToko = 1
                End If
                xSet.Tables("getNewNum").Clear()

                SQLquery = String.Format("INSERT INTO mtadj(numadj, idCabang, notes, updateBy, createDate, updateDate) " & _
                                        "VALUES({0},{1},'{2}',{3},CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())", numToko, id_cabang,
                                         "Mutasi TS ke BS by Software (+)", staff_id)

                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()

                SQLquery = String.Format("SELECT MAX(idAdj) getNewNum FROM mtadj WHERE idCabang={0} ", id_cabang)
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                command.CommandText = SQLquery
                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
                id_adjdet = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                xSet.Tables("getNewNum").Clear()

                For Each mrow As DataRow In xSet.Tables("getDataButtonBS").Rows
                    If xSet.Tables("getDataButton").Select("idBrg=" & mrow("idBrg")).Count > 0 Then

                        qtytrans = getqtybefore(mrow("idBrg"))
                        '6  + koreksi stock
                        SQLquery = String.Format("INSERT INTO dtAdj(idAdj, idBrg, idjnsadj, quantity) VALUES({0},{1},{2},{3})",
                                                     id_adjdet, mrow.Item("idBrgBS"),
                                                    6, qtytrans)
                        'ExDb.ExecData(SQLquery, conn_string_local)
                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()

                        SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) VALUES(2, {0},{1},{2},{3})",
                                                 id_cabang, id_adjdet, mrow.Item("idBrgBS"), qtytrans)

                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()
                    End If
                Next
                'proses mutasi
                xSet.Tables("getNewNum").Clear()
                'insert baru
                SQLquery = "SELECT IFNULL( MAX( numAdj ) , 0 ) +1 getNewNum FROM mtadj WHERE MONTH(createDate)=MONTH(CURDATE()) AND YEAR(createDate)=YEAR(CURDATE()) AND idCabang=" & id_cabang
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                command.CommandText = SQLquery
                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
                If xSet.Tables("getNewNum").Rows(0).Item(0).ToString <> "" Then
                    numToko = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                Else
                    numToko = 1
                End If
                xSet.Tables("getNewNum").Clear()

                SQLquery = String.Format("INSERT INTO mtadj(numadj, idCabang, notes, updateBy, createDate, updateDate) " & _
                                        "VALUES({0},{1},'{2}',{3},CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP())", numToko, id_cabang,
                                         "Mutasi TS ke BS by Software (-)", staff_id)

                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
                'MsgBox(id_adjdet)
                SQLquery = String.Format("SELECT MAX(idAdj) getNewNum FROM mtadj WHERE idCabang={0} ", id_cabang)
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                command.CommandText = SQLquery
                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
                id_adjdet = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                xSet.Tables("getNewNum").Clear()

                For Each mrow As DataRow In xSet.Tables("getDataButtonBS").Rows
                    If xSet.Tables("getDataButton").Select("idBrg=" & mrow("idBrg")).Count > 0 Then

                        qtytrans = getqtybefore(mrow("idBrg"))

                        '7  - koreksi stock
                        SQLquery = String.Format("INSERT INTO dtAdj(idAdj, idBrg, idjnsadj, quantity) VALUES({0},{1},{2},{3})",
                                     id_adjdet, mrow.Item("idBrg"),
                                    7, qtytrans)
                        'ExDb.ExecData(SQLquery, conn_string_local)
                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()

                        SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) VALUES(2, {0},{1},{2},{3})",
                                                 id_cabang, id_adjdet, mrow.Item("idBrg"), -1 * qtytrans)

                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()

                        For Each dtrow As DataRow In xSet.Tables("getDataButton").Select("idBrg=" & mrow("idBrg"))
                            dtrow("idBrg") = mrow("idBrgBS")
                            dtrow("price") = mrow("price")
                            dtrow("DisplayText") = mrow("namaBS")
                        Next

                    End If
                Next
                vartr.Commit()
                Zcon.Close()
                adadatatransfer = True
                'MessageBox.Show("Transfer Data Berhasil !", "Informasi")
            Catch ex As Exception
                adadatatransfer = False
                vartr.Rollback()

                MessageBox.Show("Proses Mutasi Gagal !" & vbCrLf & ex.Message, "Informasi")
                Exit Sub
            End Try
        Else
            SQLquery = "SELECT a.idBrg,idBrgBS,namaBRG as namaBS,price from msetupitembs a inner join mbarang b on a.idBrgBS=b.idBrg"
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDataButtonBS")

            For Each mrow As DataRow In xSet.Tables("getDataButtonBS").Rows
                For Each dtrow As DataRow In xSet.Tables("getDataButton").Select("idBrg=" & mrow("idBrg"))
                    dtrow("idBrg") = mrow("idBrgBS")
                    dtrow("price") = mrow("price")
                    dtrow("DisplayText") = mrow("namaBS")
                Next
            Next
            adadatatransfer = True
            'MessageBox.Show("Transfer Data Berhasil !", "Informasi")
        End If
        lagiprosestransfer = False
        'printaftermutasi()
    End Sub

    Private Sub selectDataButton(ByVal objButtonNo As Integer, ByVal objSkin As Boolean)
        Dim jmlfreekalender As Integer = 0
        Dim jmlsblm As Integer = 0
        If ShiftChecker() Then
            If Format(TglShiftLogin, "yyyy-MM-dd") = Format(TglShiftNow, "yyyy-MM-dd") Then
                If Not shiftLogin = shiftNow Then
                    msgboxInformation(String.Format("Shift {0} sudah selesai. Silahkan melakukan closing.", shiftLogin))
                    Close()
                End If
            Else
                msgboxWarning(String.Format("Tanggal login({0}) dan shift({1}) tidak sama, silahkan melakukan closing.", Format(TglShiftLogin, "dd/MMM/yy"), Format(TglShiftNow, "dd/MMM/yy")))
                Close()
            End If
        Else
            Return
        End If

        buttonNo = objButtonNo
        If objButtonNo > 0 Then
            selectString = String.Format("idKategoriMenu={0} AND Depth={1} AND pageNo={2} AND buttonNo={3}", idKategoriMenu, Depth, pageNo, objButtonNo)
            If Not objSkin Then
                'tombol yang di klik merupakan jenis MENU -> Masuk ke dalam MENU 
                If Depth = 0 Then
                    Level1.Text = xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText")
                    idParentLv1 = xSet.Tables("getDataButton").Select(selectString)(0).Item("idMenu")
                    Level1.Visible = True
                    idParent = idParentLv1
                ElseIf Depth = 1 Then
                    selectString = String.Format("idKategoriMenu={0} AND Depth={1} AND pageNo={2} AND buttonNo={3} AND idParent={4}", idKategoriMenu, Depth, pageNo, objButtonNo, idParentLv1)
                    Level2.Text = xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText")
                    idParentLv2 = xSet.Tables("getDataButton").Select(selectString)(0).Item("idMenu")
                    Level2.Visible = True
                    idParent = idParentLv2
                End If

                Depth += 1
                pageNo = 1
                selectString = String.Format("idKategoriMenu={0} AND Depth={1} AND pageNo={2} AND idParent={3}", idKategoriMenu, Depth, pageNo, idParent)

            Else
                'variable untuk menentukan virNumpad tampil atau tidak (untuk pilihan barang yang tidak ingin mengeluarkan virtual Num di tuliskan sebelum virnum dialog show)
                showvirtualnum = True





                'tombol yang di klik merupakan jenis PRODUCT -> Show virtual numpad
                If showvirtualnum = True Then
                    virNumpad.ShowDialog()
                End If

                If qttInteger = 0 Then Exit Sub

                selectString = String.Format("idKategoriMenu={0} AND Depth={1} AND pageNo={2} AND buttonNo={3} AND idParent={4}", idKategoriMenu, Depth, pageNo, objButtonNo, idParent)

                'cek kalau dari select berhasil mendapatkan >=1row
                If xSet.Tables("getDataButton").Select(selectString).Count > 0 Then
                    jmlfreekalender = 0
                    If GridView1.RowCount > 0 Then
                        'konfirmasi penggunaan promo 7000
                        If DateTime.Now.ToString("yyyy/MM/dd") = "2017/04/21" Then
                            If konfirmasipromo7rb = False Then
                                Dim result As Integer = MessageBox.Show("Apakah transaksi menggunakan PROMO SERBA Rp.  7.000,-?", "Konfirmasi pemakaian promo", MessageBoxButtons.YesNo)
                                If result = DialogResult.Yes Then
                                    promo7rb = True
                                Else
                                    promo7rb = False
                                End If
                                konfirmasipromo7rb = True
                            End If
                        Else
                            promo7rb = False
                        End If
                        'cek syarat promosi
                        Dim syarat As Boolean = cekkategori(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"), promo7rb, qttInteger)

                        'sudah ada row, update
                        For Each row As DataRow In xSet.Tables("setSales").Rows

                            'cek setiap row, sama dg yg skrg dipilih/tdk.
                            If row.Item("idBrg") = xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg") Then
                                'perhitungan max 5 item
                                If syarat = True And promo7rb = True Then
                                    Dim countallow As Integer = allowqty(qttInteger)
                                    'menambahkan harga promo
                                    For i = 0 To countallow - 1
                                        row.Item("hrg") = row.Item("hrg") + 7000
                                    Next i
                                    'menambahkan harga normal
                                    'For j = 0 To (qttInteger - countallow) - 1
                                    '    row.Item("hrg") = row.Item("hrg") + xSet.Tables("getDataButton").Select(selectString)(0).Item("price")
                                    'Next j
                                    'qty untuk promo
                                    row.Item("jmlh") += countallow
                                    If countallow < qttInteger Then
                                        xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                     xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger - countallow,
                                                     (qttInteger - countallow) * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                     xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                                    End If
                                    jmlfreekalender = row.Item("jmlh")
                                Else
                                    'qty untuk gak promo
                                    If konfirmasipromo7rb = True And syarat = True Then
                                        xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                      xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger,
                                                      qttInteger * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                      xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                                    Else
                                        row.Item("jmlh") += qttInteger
                                        jmlfreekalender = row.Item("jmlh")

                                        'row.Item("hrg") = row.Item("jmlh") * xSet.Tables("getDataButton").Select(selectString)(0).Item("price")
                                        row.Item("hrg") = row.Item("hrg") + (xSet.Tables("getDataButton").Select(selectString)(0).Item("price") * qttInteger)
                                        row.Item("hrgpcs") = xSet.Tables("getDataButton").Select(selectString)(0).Item("price")
                                    End If


                                End If



                                'row.Item("hrg") = row.Item("jmlh") * xSet.Tables("getDataButton").Select(selectString)(0).Item("price")
                                'row.Item("hrgpcs") = xSet.Tables("getDataButton").Select(selectString)(0).Item("price")

                                calculateDiscTotal()
                                If Level1.Text.ToString.Contains("PROMO (KALENDER)") = True And Depth = 1 Then
                                    If AktifPromo = 4 Then AktifPromo = 0
                                    If AktifPromo = 5 Then AktifPromo = 0
                                    If AktifPromo <> 0 Then Exit Sub
                                    Select Case xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText").ToString
                                        Case "Promo Kalender Jan 2017"
                                            kalenderJanuari2017()
                                        Case "Soes - Promo Kalender Apr 2017"

                                            If Format(jamts, "HH:mm:ss") <= Format(Now(), "HH:mm:ss") Then
                                                GridView1.DeleteRow(GridView1.RowCount - 1)
                                                MessageBox.Show("Promo tidak berlaku di atas jam 18:00")
                                                Exit Sub
                                            End If
                                            Dim x As New promo4soes
                                            x.ShowDialog()

                                        Case "PROMO GRATIS ROTI - MALANG"
                                            Dim x As New promo10rbmalang
                                            'x.ShowDialog()
                                        Case "Free Chocolate Bar - Promo Kalender Feb 2017"
                                            'cek nominal transaksinya 
                                            If isvalidfreekalender022017 = False Then Exit Sub
                                            'If row.Item("jmlh") > 1 Then
                                            '    GridView1.DeleteRow(GridView1.RowCount - 1)
                                            '    MsgBox("jml free coklat bar wajib hanya boleh 1 dalam 1 nota")
                                            '    Exit Sub
                                            'End If
                                            If txtTotalBeli.EditValue < 100000 Then

                                                GridView1.DeleteRow(GridView1.RowCount - 1)
                                                MsgBox("Mininal transaksi Rp.  100.000")
                                                Exit Sub
                                            End If
                                        Case Else
                                            kalender25()
                                            kuponkalender()
                                    End Select

                                End If
                                Exit Sub
                            End If
                        Next
                        'input barang promo serba 7000
                        If DateTime.Now.ToString("yyyy/MM/dd") = "2017/04/21" Then
                            If konfirmasipromo7rb = False Then
                                Dim result As Integer = MessageBox.Show("Apakah transaksi menggunakan PROMO SERBA Rp.  7.000,-?", "Konfirmasi pemakaian promo", MessageBoxButtons.YesNo)
                                If result = DialogResult.Yes Then
                                    promo7rb = True
                                Else
                                    promo7rb = False
                                End If
                                konfirmasipromo7rb = True
                            End If
                            syarat = cekkategori(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"), promo7rb, qttInteger)
                        Else
                            syarat = False
                        End If

                        If (syarat = True) Then
                            Dim jumlahboleh As Integer = allowqty(qttInteger)
                            'input yang kena diskon
                            xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                             xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), jumlahboleh,
                                                        (jumlahboleh * 7000), 7000, 0)

                            If jumlahboleh < qttInteger Then
                                xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                            xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger - jumlahboleh,
                                                       (qttInteger - jumlahboleh) * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                       xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                            End If
                        Else
                            xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                       xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger,
                                                       qttInteger * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                       xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                        End If

                        calculateDiscTotal()
                    Else

                        'belum ada row, insert

                        'input barang promo serba 7000
                        Dim syarat As Boolean
                        If DateTime.Now.ToString("yyyy/MM/dd") = "2017/04/21" Then
                            If konfirmasipromo7rb = False Then
                                Dim result As Integer = MessageBox.Show("Apakah transaksi menggunakan PROMO SERBA Rp.  7.000,-?", "Konfirmasi pemakaian promo", MessageBoxButtons.YesNo)
                                If result = DialogResult.Yes Then
                                    promo7rb = True
                                Else
                                    promo7rb = False
                                End If
                                konfirmasipromo7rb = True
                            End If
                            syarat = cekkategori(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"), promo7rb, qttInteger)
                        Else
                            syarat = False
                        End If

                        If (syarat = True) Then
                            Dim jumlahboleh As Integer = allowqty(qttInteger)
                            'input yang kena diskon
                            xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                             xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), jumlahboleh,
                                                        (jumlahboleh * 7000), 7000, 0)

                            If jumlahboleh < qttInteger Then
                                xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                            xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger - jumlahboleh,
                                                       (qttInteger - jumlahboleh) * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                       xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                            End If
                        Else
                            xSet.Tables("setSales").Rows.Add(xSet.Tables("getDataButton").Select(selectString)(0).Item("idBrg"),
                                                             xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText"), qttInteger,
                                                        qttInteger * xSet.Tables("getDataButton").Select(selectString)(0).Item("price"),
                                                        xSet.Tables("getDataButton").Select(selectString)(0).Item("price"), 0)
                        End If

                        calculateDiscTotal()
                            jmlfreekalender = qttInteger
                        End If
                    If Level1.Text.ToString.Contains("PROMO (KALENDER)") = True And Depth = 1 Then
                        If AktifPromo = 4 Then AktifPromo = 0
                        If AktifPromo = 5 Then AktifPromo = 0
                        If AktifPromo <> 0 Then Exit Sub
                        Select Case xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText").ToString
                            Case "Promo Kalender Jan 2017"
                                kalenderJanuari2017()
                            Case "Promo Kalender Juli 2017"
                                If Format(jamts, "HH:mm:ss") <= Format(Now(), "HH:mm:ss") Then
                                    GridView1.DeleteRow(GridView1.RowCount - 1)
                                    MessageBox.Show("promo tidak berlaku di atas jam 18:00")
                                    Exit Sub
                                End If
                                Dim s As New promo3traditionalsnack
                                s.ShowDialog()
                            Case "Soes - Promo Kalender Apr 2017"
                                'If Format(jamts, "HH:mm:ss") <= Format(Now(), "HH:mm:ss") Then
                                '    GridView1.DeleteRow(GridView1.RowCount - 1)
                                '    MessageBox.Show("Promo tidak berlaku di atas jam 18:00")
                                '    Exit Sub
                                'End If
                                Dim x As New promo4soes
                                x.ShowDialog()
                            Case "PROMO GRATIS ROTI - MALANG"
                                Dim x As New promo10rbmalang
                                'x.ShowDialog()
                            Case "Free Chocolate Bar - Promo Kalender Feb 2017"
                                'cek nominal transaksinya 
                                If isvalidfreekalender022017 = False Then Exit Sub
                                'If qttInteger > 1 Then
                                '    GridView1.DeleteRow(GridView1.RowCount - 1)
                                '    MsgBox("jml free coklat bar wajib hanya boleh 1 dalam 1 nota")
                                '    Exit Sub
                                'End If
                                If txtTotalBeli.EditValue < 100000 Then

                                    GridView1.DeleteRow(GridView1.RowCount - 1)
                                    MsgBox("Mininal transaksi Rp.  100.000")
                                    Exit Sub
                                End If
                            Case "Mini Donat - Promo Kalender Mar 2017"
                                If isvalidpromokalender032017 = False Then Exit Sub
                                If cekjumlahkategoridonat() < 10 Then
                                    GridView1.DeleteRow(GridView1.RowCount - 1)
                                    MsgBox("Mininal jumlah pembelian donat 10 biji")
                                    Exit Sub
                                End If
                                freeminidonat.CallMe()
                            Case Else
                                kalender25()
                                kuponkalender()
                        End Select

                    End If
                    If Level1.Text.ToString.Contains("PROMO MALANG") = True And Depth = 1 Then
                        Select Case xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText").ToString
                            Case "PROMO GRATIS ROTI - MALANG"
                                Dim x As New promo10rbmalang
                                'x.ShowDialog()
                        End Select
                    End If

                    If xSet.Tables("getDataButton").Select(selectString)(0).Item("displayText").ToString.Contains("Kalender Laritta 2017 (FREE)") Then
                        If kodemember <> "" Then
                            Exit Sub
                        End If
                        frmInputKodeMember.ShowDialog()

                        If Trim(kodemember) = "" Then
                            GridView1.DeleteRow(GridView1.FocusedRowHandle)
                        End If
                        If CekFreeKalender(jmlfreekalender) Then
                        Else
                            GridView1.DeleteRow(GridView1.FocusedRowHandle)
                            kodemember = ""
                            ishavemember = False
                        End If
                    End If
                    Exit Sub
                End If

            End If
        Else
            selectString = String.Format("idKategoriMenu={0} AND Depth={1} AND pageNo={2} AND idParent={3}", idKategoriMenu, Depth, pageNo, idParent)
        End If

        buttonResetStyler()
        For Each row As DataRow In xSet.Tables("getDataButton").Select(selectString)
            If Not IsDBNull(row.Item("imgfile")) Then
                If row.Item("nullFile") <> 0 Then
                    Dim arrPicture() As Byte = CType(row.Item("imgfile"), Byte())

                    Using ms As MemoryStream = New MemoryStream(arrPicture)
                        ImageCollection1.AddImage(Image.FromStream(ms))
                    End Using
                    buttonStyler(row.Item("buttonNo"), row.Item("isProduct"), row.Item("displayText"), row.Item("price"), row.Item("isShow"), idxgambar)
                    idxgambar += 1
                Else
                    buttonStyler(row.Item("buttonNo"), row.Item("isProduct"), row.Item("displayText"), row.Item("price"), row.Item("isShow"), -1)
                End If
            Else
                buttonStyler(row.Item("buttonNo"), row.Item("isProduct"), row.Item("displayText"), row.Item("price"), row.Item("isShow"), -1)
            End If
        Next

        If Level1.Text.ToString.ToUpper = "PRODUCT PROMO" Then
            If Trim(kodemember) = "" Then
                frmInputKodeMember.ShowDialog()

                If Trim(kodemember) = "" Then
                    pageText.Text = 1
                    Depth = 0
                    idParent = 0
                    idParentLv1 = 0
                    idParentLv2 = 0
                    Level1.Text = ""
                    selectDataButton(0, True)
                    Level1.Visible = False
                    Level2.Visible = False
                End If
            End If
        End If

        'MessageBox()
    End Sub
    'Cek kategori barang
    Private Function cekkategori(idbrg As String, pakepromo As Boolean, qty As Integer)
        If pakepromo = False Then
            Return False
            Exit Function
        End If
        If qty > 5 And xSet.Tables("setsales").Rows.Count > 0 Then
            Return False
            Exit Function
        End If
        SQLquery = "SELECT mk.idKategori2 FROM db_laritta.mkategoribrg2 mk
JOIN db_laritta.mbarang mb ON mk.idKategori2=mb.idKategori2
WHERE mb.idBrg='" & idbrg & "'"
        Dim kategori As Integer = ExDb.Scalar(SQLquery, My.Settings.localconn)
        If kategori = 26 Or kategori = 7 Or kategori = 24 Then
            If xSet.Tables("setsales").Rows.Count = 0 Then
                Return True
            End If
            Dim countpromo As Integer = 0
            For i = 0 To xSet.Tables("setsales").Rows.Count - 1
                SQLquery = "SELECT mk.idKategori2 FROM db_laritta.mkategoribrg2 mk
JOIN db_laritta.mbarang mb ON mk.idKategori2=mb.idKategori2
WHERE mb.idBrg='" & xSet.Tables("setsales").Rows(i).Item("idBrg") & "'"
                kategori = ExDb.Scalar(SQLquery, My.Settings.localconn)
                If (kategori = 26 Or kategori = 7 Or kategori = 24) And xSet.Tables("setsales").Rows(i).Item("hrgpcs") = 7000 Then
                    countpromo = countpromo + xSet.Tables("setsales").Rows(i).Item("jmlh")
                    If countpromo >= 5 Then
                        Return False
                        Exit Function
                    End If
                End If
            Next i
        Else
            Return False
            Exit Function
        End If
        Return True
    End Function

    Private Function allowqty(qty As Integer)
        Dim kategori As Integer = 0
        Dim countpromo As Integer = 0
        If xSet.Tables("setsales").Rows.Count = 0 Then
            If qty < 5 Then
                Return qty
            Else
                Return 5
            End If
        End If
        'cek untuk yang sudah terisi
        For i = 0 To xSet.Tables("setsales").Rows.Count - 1
            SQLquery = "SELECT mk.idKategori2 FROM db_laritta.mkategoribrg2 mk
JOIN db_laritta.mbarang mb ON mk.idKategori2=mb.idKategori2
WHERE mb.idBrg='" & xSet.Tables("setsales").Rows(i).Item("idBrg") & "'"
            kategori = ExDb.Scalar(SQLquery, My.Settings.localconn)
            If (kategori = 26 Or kategori = 7 Or kategori = 24) And xSet.Tables("setsales").Rows(i).Item("hrgpcs") = 7000 Then
                countpromo = countpromo + xSet.Tables("setsales").Rows(i).Item("jmlh")
                If countpromo >= 5 Then
                    Return 0
                    Exit Function
                End If
            End If
        Next i

        If qty <= (5 - countpromo) Then
            Return qty
        Else
            Return 5 - countpromo
        End If

    End Function

    Private Sub buttonResetStyler()
        Menu1.LookAndFeel.UseDefaultLookAndFeel = True
        Menu2.LookAndFeel.UseDefaultLookAndFeel = True
        Menu3.LookAndFeel.UseDefaultLookAndFeel = True
        Menu4.LookAndFeel.UseDefaultLookAndFeel = True
        Menu5.LookAndFeel.UseDefaultLookAndFeel = True
        Menu6.LookAndFeel.UseDefaultLookAndFeel = True
        Menu7.LookAndFeel.UseDefaultLookAndFeel = True
        Menu8.LookAndFeel.UseDefaultLookAndFeel = True
        Menu9.LookAndFeel.UseDefaultLookAndFeel = True

        Menu1.Text = "Menu1"
        Menu2.Text = "Menu2"
        Menu3.Text = "Menu3"
        Menu4.Text = "Menu4"
        Menu5.Text = "Menu5"
        Menu6.Text = "Menu6"
        Menu7.Text = "Menu7"
        Menu8.Text = "Menu8"
        Menu9.Text = "Menu9"

        Menu1.Visible = False
        Menu2.Visible = False
        Menu3.Visible = False
        Menu4.Visible = False
        Menu5.Visible = False
        Menu6.Visible = False
        Menu7.Visible = False
        Menu8.Visible = False
        Menu9.Visible = False

        ImageCollection1.Clear()
        idxgambar = 0
    End Sub

    Private Sub buttonStyler(ByVal button_num As Integer, ByVal button_type As Boolean, ByVal button_name As String, ByVal button_price As Integer, ByVal button_show As Boolean, ByVal idx As Integer)
        If Not button_type Then
            'button utk folder (isproduct=0)
            If button_num = 1 Then
                Menu1.LookAndFeel.UseDefaultLookAndFeel = False
                Menu1.LookAndFeel.SkinName = "Money Twins"
                Menu1.Text = button_name
                Menu1.Visible = True
            ElseIf button_num = 2 Then
                Menu2.LookAndFeel.UseDefaultLookAndFeel = False
                Menu2.LookAndFeel.SkinName = "Money Twins"
                Menu2.Text = button_name
                Menu2.Visible = True
            ElseIf button_num = 3 Then
                Menu3.LookAndFeel.UseDefaultLookAndFeel = False
                Menu3.LookAndFeel.SkinName = "Money Twins"
                Menu3.Text = button_name
                Menu3.Visible = True
            ElseIf button_num = 4 Then
                Menu4.LookAndFeel.UseDefaultLookAndFeel = False
                Menu4.LookAndFeel.SkinName = "Money Twins"
                Menu4.Text = button_name
                Menu4.Visible = True
            ElseIf button_num = 5 Then
                Menu5.LookAndFeel.UseDefaultLookAndFeel = False
                Menu5.LookAndFeel.SkinName = "Money Twins"
                Menu5.Text = button_name
                Menu5.Visible = True
            ElseIf button_num = 6 Then
                Menu6.LookAndFeel.UseDefaultLookAndFeel = False
                Menu6.LookAndFeel.SkinName = "Money Twins"
                Menu6.Text = button_name
                Menu6.Visible = True
            ElseIf button_num = 7 Then
                Menu7.LookAndFeel.UseDefaultLookAndFeel = False
                Menu7.LookAndFeel.SkinName = "Money Twins"
                Menu7.Text = button_name
                Menu7.Visible = True
            ElseIf button_num = 8 Then
                Menu8.LookAndFeel.UseDefaultLookAndFeel = False
                Menu8.LookAndFeel.SkinName = "Money Twins"
                Menu8.Text = button_name
                Menu8.Visible = True
            ElseIf button_num = 9 Then
                Menu9.LookAndFeel.UseDefaultLookAndFeel = False
                Menu9.LookAndFeel.SkinName = "Money Twins"
                Menu9.Text = button_name
                Menu9.Visible = True
            End If
        Else
            'button utk product (isproduct=1)
            button_name = button_name & vbCrLf & button_price
            If button_show = False Then Menu1.Visible = False

            If button_num = 1 Then
                If button_show = True Then Menu1.Visible = True
                Menu1.Text = button_name
                Menu1.ImageList = ImageCollection1
                Menu1.ImageIndex = idx
            ElseIf button_num = 2 Then
                If button_show = True Then Menu2.Visible = True
                Menu2.Text = button_name
                Menu2.ImageList = ImageCollection1
                Menu2.ImageIndex = idx
            ElseIf button_num = 3 Then
                If button_show = True Then Menu3.Visible = True
                Menu3.Text = button_name
                Menu3.ImageList = ImageCollection1
                Menu3.ImageIndex = idx
            ElseIf button_num = 4 Then
                If button_show = True Then Menu4.Visible = True
                Menu4.Text = button_name
                Menu4.ImageList = ImageCollection1
                Menu4.ImageIndex = idx
            ElseIf button_num = 5 Then
                If button_show = True Then Menu5.Visible = True
                Menu5.Text = button_name
                Menu5.ImageList = ImageCollection1
                Menu5.ImageIndex = idx
            ElseIf button_num = 6 Then
                If button_show = True Then Menu6.Visible = True
                Menu6.Text = button_name
                Menu6.ImageList = ImageCollection1
                Menu6.ImageIndex = idx
            ElseIf button_num = 7 Then
                If button_show = True Then Menu7.Visible = True
                Menu7.Text = button_name
                Menu7.ImageList = ImageCollection1
                Menu7.ImageIndex = idx
            ElseIf button_num = 8 Then
                If button_show = True Then Menu8.Visible = True
                Menu8.Text = button_name
                Menu8.ImageList = ImageCollection1
                Menu8.ImageIndex = idx
            ElseIf button_num = 9 Then
                If button_show = True Then Menu9.Visible = True
                Menu9.Text = button_name
                Menu9.ImageList = ImageCollection1
                Menu9.ImageIndex = idx
            End If
        End If
    End Sub

    Public Sub calculateDiscTotal()
        GridView1.UpdateSummary()
        Try
            If DiskonPersen = True Then
                txtDisc.EditValue = GridView1.Columns("hrg").SummaryItem.SummaryValue * (totDisc / 100)
                txtDiscPercent.EditValue = totDisc
            Else
                txtDisc.EditValue = TotalDisc
                txtDiscPercent.EditValue = TotalDisc / GridView1.Columns("hrg").SummaryItem.SummaryValue * 100
            End If
        Catch ex As Exception
            txtDisc.EditValue = 0
            txtDiscPercent.EditValue = 0
        End Try

        txtTotalBeli.EditValue = GridView1.Columns("hrg").SummaryItem.SummaryValue - txtDisc.EditValue
        TotalMkn = GridView1.Columns("hrg").SummaryItem.SummaryValue
    End Sub

    Private Sub defaultAll()
        txtDisc.EditValue = 0
        txtTotalBeli.EditValue = 0
        idKategoriMenu = 1
        Depth = 0
        pageNo = 1
        idParent = 0
    End Sub

    Private Sub gridSettings()
        dv = xSet.Tables("setSales").DefaultView
        GridControl1.DataSource = dv
        GridControl1.ForceInitialize()
        GridView1.RowHeight = 40

        With GridView1
            .OptionsView.EnableAppearanceEvenRow = True
            .OptionsView.EnableAppearanceOddRow = True
            .OptionsSelection.EnableAppearanceFocusedCell = False

            .Columns("idBrg").Visible = False
            .Columns("disc").Visible = False
            .Columns("hrgpcs").Visible = False
            .Columns("namaBrg").Caption = "Nama Item"
            .Columns("jmlh").Caption = "Jumlah"
            .Columns("hrg").Caption = "Harga"

            .Columns("jmlh").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            .Columns("hrg").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric

            .Columns("jmlh").DisplayFormat.FormatString = "{0:n0}"
            .Columns("hrg").DisplayFormat.FormatString = "{0:n0}"

            .Columns("namaBrg").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            .Columns("jmlh").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("jmlh").SummaryItem.DisplayFormat = "{0:n0}"

            .Columns("hrg").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            .Columns("hrg").SummaryItem.DisplayFormat = "{0:n0}"

            .Columns("namaBrg").Width = GridControl1.Width * 0.5
            .Columns("jmlh").Width = GridControl1.Width * 0.20000000000000001
            .Columns("hrg").Width = GridControl1.Width * 0.29999999999999999
        End With

    End Sub

    Private Sub resetMenuDesign()
        Dim butsize As Size = New Size() With {.Width = 176, .Height = 84}

        Menu1.Size = butsize
        Menu2.Size = butsize
        Menu3.Size = butsize
        Menu4.Size = butsize
        Menu5.Size = butsize
        Menu6.Size = butsize
        Menu7.Size = butsize
        Menu8.Size = butsize
        Menu9.Size = butsize
    End Sub

    Private Sub menusDesign()
        resetMenuDesign()
        'Dim qloc As Point = SimpleButton1.Location
        'qloc.X = Me.Size.Width - 280
        'SimpleButton1.Location = qloc

        Dim xLoc As Point = Menu1.Location
        Dim butsize As Size = New Size() With {.Width = (PanelControl2.Width - 561) / 3, .Height = (PanelControl2.Height - 404) / 3 - 5}
        butsize.Width += Menu1.Width
        butsize.Height += Menu1.Height


        '-----------------LEFT BUTTON
        Menu1.Size = butsize

        Menu4.Size = butsize
        xLoc.Y += Menu1.Height + 10
        Menu4.Location = xLoc

        Menu7.Size = butsize
        xLoc.Y += Menu4.Location.Y - Menu1.Location.Y
        Menu7.Location = xLoc

        '-----------------MID BUTTON
        Menu2.Size = butsize
        xLoc.X = Menu1.Width + 20
        xLoc.Y = Menu1.Location.Y
        Menu2.Location = xLoc

        Menu5.Size = butsize
        xLoc.Y = Menu4.Location.Y
        Menu5.Location = xLoc

        Menu8.Size = butsize
        xLoc.Y = Menu7.Location.Y
        Menu8.Location = xLoc

        '-----------------RIGHT BUTTON
        Menu3.Size = butsize
        xLoc.X += Menu2.Location.X - Menu1.Location.X
        xLoc.Y = Menu1.Location.Y
        Menu3.Location = xLoc

        Menu6.Size = butsize
        xLoc.Y = Menu4.Location.Y
        Menu6.Location = xLoc

        Menu9.Size = butsize
        xLoc.Y = Menu7.Location.Y
        Menu9.Location = xLoc

    End Sub

    Private Sub FormPOSx_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("getDataButton")
        xSet.Tables.Remove("xtemppromo")
        xSet.Tables.Remove("xtemppromo_2")
        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub FormPOSx_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub FormPOS_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        PanelControl2.BackColor = PanelControl1.BackColor
        menusDesign()

        defaultAll()
        getDataButton()
        selectDataButton(0, True)
        gridSettings()
        calculateDiscTotal()

        If status_login Then
            SimpleButton1.Text = staff_name & "   " & namacabang & "    Shift " & shiftLogin & "     "
        End If

    End Sub

    Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanged
        If e.Page.Name = "page1" Then
            Level1.Visible = False
            Level2.Visible = False

            idKategoriMenu = 1
            Depth = 0
            pageText.Text = 1
            idParent = 0
            selectDataButton(0, True)

        ElseIf e.Page.Name = "page2" Then
            Level1.Visible = False
            Level2.Visible = False

            idKategoriMenu = 2
            Depth = 0
            pageText.Text = 1
            idParent = 0
            selectDataButton(0, True)

        ElseIf e.Page.Name = "page3" Then
            Level1.Visible = False
            Level2.Visible = False
            idKategoriMenu = 3
            Depth = 0
            pageText.Text = 1
            idParent = 0
            selectDataButton(0, True)
        End If
    End Sub

    Private Sub backButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles backButton.Click
        pageText.Text = 1
        Depth = 0
        Level1.Text = ""
        idParent = 0
        idParentLv1 = 0
        idParentLv2 = 0
        selectDataButton(0, True)
        Level1.Visible = False
        Level2.Visible = False
    End Sub

    Private Sub Level1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Level1.Click
        pageText.Text = 1
        Depth = 1
        idParentLv2 = 0
        idParent = idParentLv1
        selectDataButton(0, True)
        Level2.Visible = False
    End Sub

    Private Sub Level2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Level2.Click
        pageText.Text = 1
        Depth = 2
        idParent = idParentLv2
        selectDataButton(0, True)
    End Sub

    Private Sub Menu1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu1.Click
        selectDataButton(1, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu2.Click
        selectDataButton(2, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu3.Click
        selectDataButton(3, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu4.Click
        selectDataButton(4, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu5.Click
        selectDataButton(5, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu6.Click
        selectDataButton(6, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu7.Click
        selectDataButton(7, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu8.Click
        selectDataButton(8, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub Menu9_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Menu9.Click
        selectDataButton(9, Menu1.LookAndFeel.UseDefaultLookAndFeel)
    End Sub

    Private Sub prevButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles prevButton.Click
        If pageText.EditValue > 1 Then
            pageText.EditValue = pageText.EditValue - 1
        Else
            Exit Sub
        End If
        selectDataButton(0, True)
    End Sub

    Private Sub nextButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles nextButton.Click
        If pageText.EditValue < 9 Then
            pageText.EditValue = pageText.EditValue + 1
        Else
            Exit Sub
        End If
        selectDataButton(0, True)
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        prosests = False
        Close()
    End Sub

    Private Sub butDiskon_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butDiskon.Click
        If Not AllowDiskon Then
            msgboxInformation("Tidak mempunyai hak untuk memberi diskon!")
        Else
            If GridView1.Columns("hrg").SummaryItem.SummaryValue > 0 Then
                inputDisc.ShowDialog()
                calculateDiscTotal()
            End If
        End If
    End Sub

    Private Sub butClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClear.Click
        If MsgBox("Yakin ingin menghapus daftar?", MsgBoxStyle.YesNo, "Yakin?") = vbYes Then
            xSet.Tables("setSales").Clear()
            totDisc = 0
            TotalDisc = 0
            kodemember = ""
            kodememberswap = ""
            AktifPromo = 0
            bataltopup = False
            G_idpromo = 0
            G_ketpromo = ""
            G_kodekupon = ""
            promo7rb = Nothing
            konfirmasipromo7rb = False
            IsShowingformbayar = True
            ispromoproces = False
            isambilpaketpromo = False
            disablemenu(True)
            idfrmconfirm_1 = 0
            isDapatPromo = False
            calculateDiscTotal()
        End If
    End Sub

    Private Function cekprodukpromo() As Boolean
        Dim idbrgprod As String = ""

        For Each rows As DataRow In xSet.Tables("setSales").Rows
            idbrgprod = idbrgprod & rows(0) & ","
        Next

        If Trim(idbrgprod) = "" Then Return False
        idbrgprod = Microsoft.VisualBasic.Left(idbrgprod, idbrgprod.ToString.Length - 1)
        If IsNothing(xSet.Tables("cekProdukPromo")) = False Then xSet.Tables("cekProdukPromo").Clear()
        SQLquery = "select count(idBrg) from mbarang inner join mkategoribrg2 on mbarang.idkategori2=mkategoribrg2.idKategori2 where idBrg in (" & idbrgprod & ") and mkategoribrg2.namakategori2='PRODUCT PROMO'"
        ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "cekProdukPromo")
        If xSet.Tables("cekProdukPromo").Rows(0)(0) > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub Diskonkalenderlaritta()
        'Kalender Laritta 2017
        If ishavemember Then
            For Each row In xSet.Tables("SetSales").Rows
                If row.Item("hrg") <> 0 And row.item("namaBrg").ToString.Contains("Kalender Laritta 2017") = True Then
                    row.Item("disc") = 40
                    row.Item("hrg") = (1 - (40 / 100)) * (CInt(row.Item("jmlh")) * CInt(row.Item("hrgpcs")))
                End If
            Next
            calculateDiscTotal()
        End If
    End Sub
    Private Sub PromoWholeBread()
        If AktifPromo <> 0 Then
            Exit Sub
        End If

        If isPromoWholeBreadMLG = False Or My.Settings.idWilayah <> "MLG" Then
            Exit Sub
        End If
        'cek nominal transaksi
        If txtTotalBeli.EditValue >= 50000 Then
            freeprodwholebread.CallMe(txtTotalBeli.EditValue)
        End If

    End Sub

    Private Sub PromoNewProduct()
        Dim urtbrg As Integer
        Dim brgnodiskon As Integer = 0
        Dim temdiskon As Double
        Dim listNewProduct As String = "'Cheese Ball', 'Milky Cheese', 'Rainbow Chips','Choco Jerrycho','Choco Boy','Purple Elmo','Flower Cheese Bread','Super Pizza','Strawberry Sweet Cake','Blueberry Sweet Cake','Brownies Double Choco','Brownies Cokelat Almond','Brownies Choco Peanut','Black Forest Roll (V.1)','Black Forest Roll (V.2)'"
        If AktifPromo <> 0 Then
            Exit Sub
        End If
        If isPromoNewProduct = False Then Exit Sub

        If txtTotalBeli.EditValue >= 50000 Then
            If xSet.Tables("setSales").Select("namaBrg in (" & listNewProduct & ")").Count = 0 Then
                Exit Sub
            End If
            urtbrg = 1
            For Each xdtrow In xSet.Tables("setSales").Rows
                If xSet.Tables("setSales").Select("namaBrg in (" & listNewProduct & ") and idBrg=" & xdtrow("idBrg")).Count = 1 Then
                    If urtbrg = 1 Then
                        If txtTotalBeli.EditValue - CInt(xdtrow("jmlh")) * CInt(xdtrow("hrgpcs")) * ((20) / 100) < 50000 Then
                            'MsgBox(CInt(xdtrow("jmlh")) * CInt(xdtrow("hrgpcs")) * ((100 - 20) / 100).ToString)
                            brgnodiskon = xdtrow("idBrg")
                        End If
                    End If
                    urtbrg = urtbrg + 1
                End If
            Next
            If brgnodiskon <> 0 Then
                If xSet.Tables("setSales").Select("namaBrg in (" & listNewProduct & ")").Count - 1 = 0 Then
                    Exit Sub
                Else
                    msgboxInformation("Selamat Pelanggan Mendapatkan Diskon 20% untuk New Product")
                End If
            End If
            For Each dtrow As DataRow In xSet.Tables("setSales").Select("namaBrg in (" & listNewProduct & ") and idBrg<>" & brgnodiskon)
                dtrow("disc") = 20
                dtrow("hrg") = CInt(dtrow("jmlh")) * CInt(dtrow("hrgpcs")) * ((100 - 20) / 100)
            Next
        End If
    End Sub

    Private Sub butBayar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBayar.Click
        byarpakaiVC = False
        If GridView1.RowCount < 1 Then Exit Sub
        'Dim totaldiscdetail As Double = xSet.Tables("setSales").Compute("Sum(Disc)", "")
        'If CInt(txtTotalBeli.EditValue) = 0 And CInt(txtDisc.EditValue) = 0 And totaldiscdetail = 0 Then
        'cek 
        'Exit Sub
        'End If

        Dim flagkresek As Boolean = frmboxkresek.InitForm()
        'If flagkresek Then
        'Exit Sub
        'End If
        'If kodemember = "" Then
        '    isCekProdukPromo = cekprodukpromo()
        '    'cek ada produk promo atau tidak
        '    If isCekProdukPromo = True Then

        frmInputKodeMember.callme()
        Diskonkalenderlaritta()
        comfirmbayarVC.ShowDialog()
        'End If
        '    End If
        'End If

        'If Trim(kodemember) <> "" Then
        '    isCekProdukPromo = cekprodukpromo()
        '    If isCekProdukPromo = False Then
        '        kodemember = ""
        '    End If
        '    isCekProdukPromo = False
        'End If

        If Menu1.Enabled = True And byarpakaiVC = False And AktifPromo = 0 Then
            If isvalidpromokeju = True Or isBeli5Gratis1 = True Or isBeli5Gratis1MLG = True Then
                AktifPromo = 0
                If ishavemember = True Then
                    For Each rowdt As DataRow In xSet.Tables("setsales").Rows
                        If isBrgPromoBeli5Gratis1(rowdt("idbrg")) = True Then
                            isitem5gratis1 = True
                        End If
                        If isBrgPromoKeju(rowdt("idbrg")) = True Then
                            isPromoKeju = True
                        End If
                    Next

                    If isPromoKeju = True And isitem5gratis1 = True Then
                        pilihpromo.ShowDialog()
                        If AktifPromo = 1 Then
                            promokeju()
                        ElseIf AktifPromo = 2 Then
                            PromoBeli5Gratis1()
                        End If
                    ElseIf isPromoKeju = True And isitem5gratis1 = False Then
                        promokeju()
                    ElseIf isPromoKeju = False And isitem5gratis1 = True Then
                        PromoBeli5Gratis1()
                    End If
                Else
                    promokeju()
                End If
                PromoBeli5Gratis1MLG()
            Else
                If isvalidBeliRotiGratisProdukshowcase = True Then

                    If kodemember <> "" And AktifPromo = 0 Then
                        If txtTotalBeli.EditValue >= 50000 And txtTotalBeli.EditValue < 75000 Or txtTotalBeli.EditValue >= 75000 And txtTotalBeli.EditValue < 100000 Or txtTotalBeli.EditValue >= 100000 Then
                            freeprodshowcase.CallMe()
                        End If
                    End If
                End If
                PromoWholeBread()
                PromoNewProduct()

            End If
        End If
        'MsgBox(AktifPromo)
        calculateDiscTotal()
        IsShowingformbayar = (idfrmconfirm_1 = 2) Or (ishavemember = False) Or (ishavemember = True)
        IsShowingformbayar = Not (idfrmconfirm_1 = 1)
        If IsShowingformbayar = True Then
            inputPayment.callMe(CInt(txtTotalBeli.EditValue) + CInt(txtDisc.EditValue), CInt(txtDisc.EditValue), GridView1.Columns("jmlh").SummaryItem.SummaryValue, GridView1.Columns("namaBrg").SummaryItem.SummaryValue, CInt(txtDiscPercent.EditValue), True)
        End If
        If ispromoproces = False Then disablemenu(True)
        idfrmconfirm_1 = 0
        totDisc = 0

    End Sub

    Private Sub pageText_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles pageText.TextChanged
        pageNo = pageText.Text
    End Sub

    Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        calculateDiscTotal()
    End Sub

    Private Sub GridView1_RowCellClick(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs) Handles GridView1.RowCellClick
        selected_idxRow = GridView1.FocusedRowHandle
        selected_row = GridView1.GetFocusedDataRow
        If ispromoproces = True Then Exit Sub

        If AktifPromo = 3 And GridView1.GetFocusedRowCellValue("disc") = 100 Then Exit Sub
        If AktifPromo = 4 And GridView1.GetFocusedRowCellValue("disc") <> 0 Then Exit Sub

        'cek apakah kategori barang adalah top up
        filterbarang(GridView1.GetFocusedRowCellValue("idBrg").ToString, "'TOP UP'")
        If xSet.Tables("listfilter").Rows.Count > 0 Then
            Exit Sub
        End If

        editSales.ShowDialog()
        calculateDiscTotal()
    End Sub

    Private Sub swap_Click(sender As Object, e As EventArgs) Handles swap.Click
        If swap.Text = "Swap" Then
            If ispromoproces = True Or AktifPromo = 3 Then
                MsgBox("Proses Ini Tidak Dapat dilakukan karena promo belum tersimpan !")
                Exit Sub
            End If
            If GridView1.RowCount > 0 Then
                If IsNothing(xSet.Tables("setSalesSwap")) Then
                    xSet.Tables.Add("setSalesSwap")

                    xSet.Tables("setSalesSwap").Columns.Add("idBrg", Type.GetType("System.Decimal"))
                    xSet.Tables("setSalesSwap").Columns.Add("namaBrg", Type.GetType("System.String"))
                    xSet.Tables("setSalesSwap").Columns.Add("jmlh", Type.GetType("System.Decimal"))
                    xSet.Tables("setSalesSwap").Columns.Add("hrg", Type.GetType("System.Decimal"))
                    xSet.Tables("setSalesSwap").Columns.Add("hrgpcs", Type.GetType("System.Decimal"))
                    xSet.Tables("setSalesSwap").Columns.Add("disc", Type.GetType("System.Decimal"))
                Else
                    xSet.Tables("setSalesSwap").Clear()
                End If

                For Each drow As DataRow In xSet.Tables("setSales").Rows
                    xSet.Tables("setSalesSwap").Rows.Add(drow.Item("idBrg"),
                                                        drow.Item("namaBrg"),
                                                        drow.Item("jmlh"),
                                                        drow.Item("hrg"),
                                                        drow.Item("hrgpcs"),
                                                        drow.Item("disc"))
                Next

                xSet.Tables("setSales").Clear()
                kodememberswap = kodemember
                kodemember = ""
                AktifPromo = 0
                swap.Text = "UnSwap"
            End If
        Else
            xSet.Tables("setSales").Clear()

            For Each drow As DataRow In xSet.Tables("setSalesSwap").Rows
                xSet.Tables("setSales").Rows.Add(drow.Item("idBrg"),
                                                    drow.Item("namaBrg"),
                                                    drow.Item("jmlh"),
                                                    drow.Item("hrg"),
                                                    drow.Item("hrgpcs"),
                                                    drow.Item("disc"))
            Next

            kodemember = kodememberswap
            kodememberswap = ""
            xSet.Tables.Remove("setSalesSwap")

            swap.Text = "Swap"
        End If
        calculateDiscTotal()
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        frmnotifkasir.InitForm("Promo Yang Aktif", "Promo Yang Aktif")
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        frmnotifkasir.InitForm("Scrip Up Sales", "Script  UpSales")
    End Sub

    Async Sub dotransfer()

        If IsNothing(xSet.Tables("setSales")) = False Then xSet.Tables("setSales").Clear()
        If IsNothing(xSet.Tables("setSalesSwap")) = False Then xSet.Tables("setSalesSwap").Clear()
        totDisc = 0
        TotalDisc = 0
        calculateDiscTotal()

        swap.Text = "Swap"
        SplashScreenManager.ShowForm(GetType(SplashScreen1))
        Me.Enabled = False
        'SplashScreen1.ShowDialog()
        prosests = True
        getDataButtonTRBS()
        Application.DoEvents()
        Await Task.Delay(5000)
        If lagiprosestransfer = False Then
            SplashScreenManager.CloseForm()
        End If
        Me.Enabled = True
        Timer1.Start()
    End Sub

    Private Sub check_saldo_Click(sender As Object, e As EventArgs) Handles check_saldo.Click
        Dim x As New tap_card
        x.ShowDialog()
    End Sub

    Private Async Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'Exit Sub
        Dim strhari As String = arrayhari(Now().DayOfWeek)
        LabelControl4.Text = strhari + Format(Now(), ", dd-MM-yyyy hh:mm:ss tt")
        'MsgBox(prosests.ToString)
        If IsGrandOpening = True Then Exit Sub
        If isbayarshow = True Then Exit Sub
        If prosests = True And adadatatransfer = True Then Exit Sub
        'Timer1.Stop()
        'MsgBox(Format(jamts, "HH:mm:ss"))
        'MsgBox(Format(Now, "HH:mm:ss"))
        'If Format(jamts, "HH:mm:ss") <= Format(Now(), "HH:mm:ss") Then
        'MsgBox("a")
        'End If
        ''If Format(jamts, "HH:mm:ss") > Format(Now(), "HH:mm:ss") Then
        ' MsgBox("b")
        'End If
        If Format(jamts, "HH:mm:ss") <= Format(Now(), "HH:mm:ss") Then
            Timer1.Stop()

            'confirmtransfer.Show()
            'Await Task.Delay(2000)
            'confirmtransfer.Close()

            dotransfer()
        End If
    End Sub

    Private Sub FormPOSx_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick
        'MsgBox(e.Button.ToString)
        'If e.Button.ToString <> "" Then
        'If disablemouse = True Then
        'SendKeys.Send("{Esc}")
        'End If
        'End If
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click

        If AktifPromo <> 0 Then Exit Sub
        AktifPromo = 0
        pilihpromoOL.ShowDialog()
    End Sub

    Private Sub kuponkalender()
        If isUndianKalender = False Then
            Exit Sub
        End If

        If GridView1.Columns("hrg").SummaryItem.SummaryValue = 0 Then
            Exit Sub
        End If
        AktifPromo = 5
        Dim strprosen As String
        frmkuponkalender.ShowDialog()
        'ambil 2 digit kiri dari kode kupon
        strprosen = Microsoft.VisualBasic.Left(G_kuponkalender, 2)
        If strprosen = "99" Then strprosen = (Val(strprosen) + 1).ToString
        If GridView1.Columns("hrg").SummaryItem.SummaryValue > 250000 Then
            DiskonPersen = False
            TotalDisc = (Val(strprosen)) / 100 * 250000
        Else
            DiskonPersen = True
            totDisc = Val(strprosen)
        End If
        calculateDiscTotal()
    End Sub

    Private Sub kalenderJanuari2017()
        Dim vartemp As Integer
        Dim jmlitempromo As Integer
        'Diskon Kalender Januari 2017 10000 4 Roti
        If IsNothing(xSet.Tables("xtemppromo")) = False Then xSet.Tables("xtemppromo").Clear()
        If IsNothing(xSet.Tables("xtemppromo_2")) = False Then xSet.Tables("xtemppromo_2").Clear()
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg,0 idxkat from mbarang where 1=0", xSet, "xtemppromo")
        ExDb.ExecQuery(conn_string_local, "select 0 idBrg, 0 hrgpcs, 0 rowid,0 jmlh,0 hrg,0 idxkat from mbarang where 1=0", xSet, "xtemppromo_2")

        SQLquery = String.Format("select count(*) from app_promo.mpromo where '{0}' >= date(TglMulai) and '{0}' <= date(TglAkhir) and namapromo='{1}'", Format(Now(), "yyyy-MM-dd"), "Diskon Kalender Januari 2017")
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Expired !")
            AktifPromo = 0
            GridView1.DeleteRow(GridView1.FocusedRowHandle)
            Exit Sub
        End If
        Dim rwtodelete As Integer = GridView1.FocusedRowHandle
        Dim nourut As Integer = 1
        Dim idxkatnya As Integer
        For Each rowdt As DataRow In xSet.Tables("setsales").Rows
            If isBrgKalenderJan2017(rowdt("idBrg")) Then
                jmlitempromo += rowdt("jmlh")
                idxkatnya = idxKategorinya(rowdt("idBrg"))
                xSet.Tables("xtemppromo").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"), idxkatnya)
                xSet.Tables("xtemppromo_2").Rows.Add(rowdt("idBrg"), rowdt("hrgpcs"), nourut, rowdt("jmlh"), rowdt("hrg"), idxkatnya)
            End If
            nourut = nourut + 1
        Next
        If jmlitempromo = 0 Then
            MsgBox("Tidak Ada Item yang memenuhi Kriteria Promo ini !")
            AktifPromo = 0
            GridView1.DeleteRow(GridView1.FocusedRowHandle)
            Exit Sub
        End If
        kalenderjan17.ShowDialog()
        nourut = 1
        jmlPaket = jmlPaket * 4
        'cari hargaminim
        Dim hrgmin As Integer = 0
        Dim deffilter As String = ""
        Dim rowdiskon As Integer = 0
        Dim ctrdiskon As Integer = 1
hitunglagi:
        hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs)")(0)("hrgpcs")
        deffilter = "hrgpcs=" & hrgmin
        For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select(deffilter, "idxkat asc,jmlh desc")
            If jmlPaket <= 0 Then
                GoTo hitungnom
            End If
            rowdiskon = dtrow("rowid")
            If dtrow("jmlh") <= jmlPaket Then
                'xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc") = 2500
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = (2500) * xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh")
                jmlPaket -= dtrow("jmlh")
            ElseIf dtrow("jmlh") > jmlPaket Then
                'dipecah
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") = xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") - jmlPaket
                xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrg") = (xSet.Tables("setSales").Rows(rowdiskon - 1).Item("jmlh") * xSet.Tables("setSales").Rows(rowdiskon - 1).Item("hrgpcs")) - xSet.Tables("setSales").Rows(rowdiskon - 1).Item("disc")
                Dim rowdtz As DataRow = xSet.Tables("setSales").Rows(rowdiskon - 1)
                xSet.Tables("setSales").Rows.Add(rowdtz.Item("idBrg"),
                                                           rowdtz.Item("namaBrg"),
                                                            jmlPaket,
                                                             jmlPaket * 2500,
                                                          rowdtz.Item("hrgpcs"),
                                                            0)
                jmlPaket = 0

                Exit For
            End If

            'ctrdiskon += dtrow("jmlh")
        Next
        'If jmlqydiskon > 0 Then
        '    'MsgBox(xSet.Tables("xtemppromo").Rows.Count)
        For Each dtrow As DataRow In xSet.Tables("xtemppromo").Select("hrgpcs=" & hrgmin)
            dtrow.Delete()
        Next

        xSet.Tables("xtemppromo").AcceptChanges()

        If xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs) and hrgpcs<>" & hrgmin).Count > 0 Then
            hrgmin = xSet.Tables("xtemppromo").Select("hrgpcs=min(hrgpcs) and hrgpcs<>" & hrgmin)(0)("hrgpcs")
            GoTo hitunglagi
        End If
        'End If
hitungnom:
        calculateDiscTotal()
    End Sub

    Private Sub kalender25()
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where '{0}' >= date(TglMulai) and '{0}' <= date(TglAkhir) and idpromo={1}", Format(Now(), "yyyy-MM-dd"), 3)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            'MsgBox("Promo sudah Expired !")
            AktifPromo = 0
            Exit Sub
        End If
        SQLquery = String.Format("select count(*) from app_promo.mpromo where aktif=1 and idpromo={0}", 3)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            AktifPromo = 0
            MsgBox("Promo sudah Tidak Aktif !")
            Exit Sub
        End If

        If IsNothing(xSet.Tables("discitem")) = False Then xSet.Tables("discitem").Clear()
        SQLquery = String.Format("select idBrg from app_promo.dtpromoitem where idpromo={0}", 3)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "discitem")
        For Each dtrow As DataRow In xSet.Tables("discitem").Rows
            For Each dtrowx As DataRow In xSet.Tables("setSales").Select("idBrg=" & dtrow("idBrg"))
                AktifPromo = 4
                dtrowx("disc") = 25
                dtrowx("hrg") = CInt(dtrowx("jmlh")) * CInt(dtrowx("hrgpcs")) * ((100 - 25) / 100)
            Next
        Next
        xSet.Tables("setSales").AcceptChanges()
    End Sub
    '


End Class
