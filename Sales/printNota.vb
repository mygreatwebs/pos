﻿Imports DevExpress.XtraReports.UI
Public Class printNota
    Dim isprinted As Boolean


    Public Sub callMe(ByVal totBuy As Integer, ByVal totPay As Integer, ByVal totChange As Integer, ByVal param1 As Integer, ByVal param2 As Integer)
        textTotal.Text = totBuy
        textBayar.Text = totPay
        textKembalian.Text = totChange
        selected_idxRow = param1
        LabelControl5.Text = String.Format("SO/{0}/{1}/{2}", Format(Now.Year, "00").Substring(2, 2), Format(Now.Month, "00"), Format(param2, "000000"))

        ShowDialog()
    End Sub

    Private Sub printNota_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs) Handles Me.FormClosed
        textTotal.Text = 0
        textBayar.Text = 0
        textKembalian.Text = 0
        LabelControl5.Text = 0
        If isprinted Then
            totDisc = 0
            TotalDisc = 0
        End If
    End Sub

    Private Sub butPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butPrint.Click
        isReprint = False

        Try
            QryHeaderNota()
            Dim printout As XtraReport = New Nota
            printout.ShowPrintStatusDialog = False
            printout.Parameters("namamember").Value = namapemilikkartu
            'printout.CreateDocument(True)
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                tool.Print()
                'tool.ShowPreviewDialog()
            End Using
            isprinted = True
        Catch ex As Exception
            isprinted = False
        End Try
    End Sub

    Private Sub butTutup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butTutup.Click
        Close()
    End Sub
End Class