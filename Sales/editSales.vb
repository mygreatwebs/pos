﻿Public Class editSales 
    Dim DiskonRP As Boolean

    Private Sub editSales_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        textName.Text = selected_row.Item("namaBrg")
        textQtt.Text = CInt(selected_row.Item("jmlh"))
        textDisc.Text = CInt(selected_row.Item("disc"))
        textPrice.Text = CInt(selected_row.Item("hrgpcs"))

        TextEdit1.Text = (CInt(selected_row.Item("jmlh")) * CInt(selected_row.Item("hrgpcs"))) - CInt(selected_row.Item("hrg"))

        If Not AllowDiskon Then
            but10.Enabled = False
            but20.Enabled = False
            but50.Enabled = False
            textDisc.Enabled = False
        End If
    End Sub

    Private Sub but10_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but10.Click
        textDisc.Text = 10
        TextEdit1.EditValue = 10 / 100 * CInt(textQtt.Text) * CInt(textPrice.Text)
        DiskonRP = False
    End Sub

    Private Sub but20_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but20.Click
        textDisc.Text = 20
        TextEdit1.EditValue = 20 / 100 * CInt(textQtt.Text) * CInt(textPrice.Text)
        DiskonRP = False
    End Sub

    Private Sub but50_Click(ByVal sender As Object, ByVal e As EventArgs) Handles but50.Click
        textDisc.Text = 50
        TextEdit1.EditValue = 50 / 100 * CInt(textQtt.Text) * CInt(textPrice.Text)
        DiskonRP = False
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        xSet.Tables("setSales").Rows(selected_idxRow).Item("jmlh") = CInt(textQtt.Text)

        If DiskonRP = False Then
            xSet.Tables("setSales").Rows(selected_idxRow).Item("hrg") = CInt(textQtt.Text) * CInt(textPrice.Text) * ((100 - textDisc.Text) / 100)
        Else
            xSet.Tables("setSales").Rows(selected_idxRow).Item("hrg") = (CInt(textQtt.Text) * CInt(textPrice.Text)) - TextEdit1.Text
        End If
     
        xSet.Tables("setSales").Rows(selected_idxRow).Item("disc") = textDisc.Text
        selected_row = Nothing
        Close()
    End Sub

    Private Sub butDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butDelete.Click
        xSet.Tables("setSales").Rows.Remove(selected_row)
        Close()
    End Sub

    Private Sub textQtt_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles textQtt.Enter
        virNumpad.ShowDialog()
        textQtt.Text = qttInteger
    End Sub

    Private Sub textDisc_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles textDisc.Enter
        virNumpad.ShowDialog()
        If qttInteger > 99 Then
            textDisc.Text = 100
            TextEdit1.EditValue = CInt(textQtt.Text) * CInt(textPrice.Text)
        Else
            textDisc.Text = qttInteger
            TextEdit1.EditValue = qttInteger / 100 * CInt(textQtt.Text) * CInt(textPrice.Text)
        End If
        DiskonRP = False
    End Sub

    Private Sub TextEdit1_Enter(sender As Object, e As EventArgs) Handles TextEdit1.Enter
        virNumpad.ShowDialog()
        If qttInteger > CInt(textQtt.Text) * CInt(textPrice.Text) Then
            textDisc.Text = 100
            TextEdit1.EditValue = CInt(textQtt.Text) * CInt(textPrice.Text)
        Else
            TextEdit1.Text = qttInteger
            textDisc.Text = qttInteger / (CInt(textQtt.Text) * CInt(textPrice.Text)) * 100
        End If
        DiskonRP = True
    End Sub
End Class