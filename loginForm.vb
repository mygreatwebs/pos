﻿Imports MySql.Data.MySqlClient

Public Class loginForm

    Private Sub loginForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        TextEdit1.Text = ""
        TextEdit2.Text = ""
        LabelControl3.Visible = False
        status_login = False
        TextEdit1.Focus()
    End Sub

    Private Sub TextEdit1_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles TextEdit1.GotFocus
        TextEdit1.Select(0, TextEdit1.Text.Length)
    End Sub

    Private Sub TextEdit1_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles TextEdit1.KeyDown
        If Not My.Computer.Name = "NYOTO-PC" And Not My.Computer.Name = "PHILBERT-BASE" And Not My.Computer.Name = "PHILBERT-LAPTOP" Then Exit Sub
        If e.KeyCode = Keys.F5 Then
            TextEdit1.Text = "admin"
            TextEdit2.Text = "703011585"
            tryLogin()
        End If
    End Sub

    Private Sub TextEdit1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TextEdit1.KeyPress
        If e.KeyChar = ChrW(13) Then
            TextEdit2.Focus()
        End If
    End Sub

    Private Sub TextEdit2_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles TextEdit2.GotFocus
        TextEdit2.Select(0, TextEdit2.Text.Length)
    End Sub

    Private Sub TextEdit2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles TextEdit2.KeyPress
        If e.KeyChar = ChrW(13) Then
            butLogin.Focus()
            tryLogin()
        End If
    End Sub

    Private Function getnamacab(ByVal idcab As String) As String
        Dim dset As New DataSet
        dset = ExDb.ExecQuery(My.Settings.db_conn_mysql, "select namaCabang from mcabang where idCabang=" & idcab, dset, "mcabang")
        If dset.Tables(0).Rows.Count > 0 Then
            Return dset.Tables(0).Rows(0)("namaCabang")
        Else
            Return ""
        End If
        dset.Dispose()
    End Function

    Private Sub tryLogin()
        LabelControl3.Visible = False
        If id_cabang IsNot Nothing Then
            If TextEdit1.Text <> "" And TextEdit2.Text <> "" Then
                Try
                    Dim dts As New DataTable
                    Using xCon = New MySqlConnection(conn_string_local)

                        Using objcomm = New MySqlCommand(String.Format("SELECT idStaff, Nama, a.idSecurityGroup, IFNULL(baca,0) AS baca, a.idCabang,namaCabang FROM mstaff a " _
                                                                     & "LEFT JOIN msecuritygroup b ON a.idSecurityGroup=b.idSecurityGroup " _
                                                                     & "LEFT JOIN progpermissions c ON a.idSecurityGroup=c.idSecurityGroup AND idform=31 left join mcabang d on a.idCabang=d.idCabang " _
                                                                     & "WHERE username='{0}' AND password=AES_ENCRYPT('{1}', '{0}')",
                                                                     TextEdit1.Text, TextEdit2.Text), xCon) With {.CommandType = CommandType.Text}
                            'Using objcomm = New MySqlCommand(String.Format("SELECT idStaff, Nama, a.idSecurityGroup, IFNULL(baca,0) AS baca, a.idCabang,namaCabang FROM mstaff a " _
                            '                                             & "LEFT JOIN db_laritta.msecuritygroup b ON a.idSecurityGroup=b.idSecurityGroup " _
                            '                                             & "LEFT JOIN db_laritta.progpermissions c ON a.idSecurityGroup=c.idSecurityGroup  left join mcabang d on a.idCabang=d.idCabang " _
                            '                                             & "WHERE username='{0}' and idform=31",
                            '                                             TextEdit1.Text), xCon) With {.CommandType = CommandType.Text}
                            xCon.Open()

                            Using dr = objcomm.ExecuteReader
                                dts.Load(dr, LoadOption.OverwriteChanges)

                                If Not dts.Rows(0).Item("baca").ToString = True Then
                                    status_login = True
                                    Throw New Exception
                                End If
                                staff_id = dts.Rows(0).Item("idStaff").ToString
                                staff_name = dts.Rows(0).Item("Nama").ToString
                                staff_group = dts.Rows(0).Item("idSecurityGroup").ToString
                                namacabang = getnamacab(id_cabang)
                                'MessageBox.Show(namacabang)
                                If Not staff_group <= 3 Then
                                    If Not dts.Rows(0).Item("idCabang").ToString = id_cabang Then
                                        LabelControl3.Text = "Tidak bisa login di cabang ini"
                                        LabelControl3.Visible = True
                                        status_login = False
                                        Exit Sub
                                    End If
                                End If
                                status_login = True
                            End Using
                        End Using
                    End Using
                    dts.Dispose()

                    If status_login = True Then
                        'ifexist id telah login -> ambil shift pada saat login then compare dg shift skrg.
                        If Not ShiftChecker() Then
                            Return
                        End If
                        'SQLquery = String.Format("SELECT COUNT(shiftKe) num, IFNULL(MAX(shiftKe),0) shiftNow, DATE(MAX(startTime)) TglShiftNow, CURDATE() TglShiftLogin FROM mtswitchuser WHERE idCabang={0} AND idStaff={1} AND endTime='00:00:00' AND status=1", id_cabang, staff_id)
                        'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "CekShiftNow")
                        'shiftNow = xSet.Tables("CekShiftNow").Rows(0).Item("shiftNow")
                        'TglShiftNow = xSet.Tables("CekShiftNow").Rows(0).Item("TglShiftNow")

                        If xSet.Tables("CekShiftNow").Rows(0).Item("num") = 0 Then
                            'belum pernah login -> input modal
                            modalForm.ShowDialog()
                            If start_modal = 0 Then
                                TextEdit1.Text = ""
                                TextEdit2.Text = ""
                                status_login = False
                                Exit Sub
                            End If
                        Else
                            'udah pernah login tp blm closing -> lanjut
                            shiftLogin = shiftNow
                            TglShiftLogin = xSet.Tables("CekShiftNow").Rows(0).Item("TglShiftLogin")

                            msgboxInformation("Berhasil login kembali, Shift saat ini : " & shiftNow)
                        End If

                        'SECURITY PERMISSIONS!
                        If Not xSet.Tables("getLoginPermissions") Is Nothing Then xSet.Tables("getLoginPermissions").Clear()
                        SQLquery = "SELECT namaform, IFNULL( baca, FALSE ) AS Baca, IFNULL( tambah, FALSE ) AS Tambah, " & _
                                    "IFNULL( edit, FALSE ) AS Edit, IFNULL( batal, FALSE ) AS Batal, IFNULL( cetak, FALSE ) AS Cetak " & _
                                    "FROM mform a LEFT JOIN progpermissions b ON a.idform = b.idform AND b.idsecuritygroup =" & staff_group
                        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getLoginPermissions")
                        AllowDiskon = xSet.Tables("getLoginPermissions").Select("namaform='Diskon POS'")(0).Item("Baca")
                        AllowVoid = xSet.Tables("getLoginPermissions").Select("namaform='POS'")(0).Item("Batal")
                        If staff_group = 1 Then AllowVoid = True

                        'input coa payment
                        If IsNothing(xSet.Tables("mCoaPayment")) Then
                            xSet.Tables.Add("mCoaPayment")
                        Else
                            xSet.Tables("mCoaPayment").Clear()
                        End If
                        SQLquery = String.Format("SELECT * FROM m_coa_payment WHERE inactive=0 AND idCabang={0}", My.Settings.idcabang)
                        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "mCoaPayment")

                        'input mcoa
                        If IsNothing(xSet.Tables("mcoa")) Then
                            xSet.Tables.Add("mcoa")
                        Else
                            xSet.Tables("mcoa").Clear()
                        End If
                        SQLquery = "SELECT accCode,idCOA, accName, accParent, idAccType, isLowest, createBy, createDate, modifBy, modifDate, description, idCabang FROM m_coa WHERE inactive=0 AND isVoid=0"
                        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "mcoa")

                        Close()
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                    If status_login = True Then
                        LabelControl3.Text = "User tidak dapat menggunakan POS"
                    Else
                        LabelControl3.Text = "Username/password salah"
                    End If
                    LabelControl3.Visible = True
                    status_login = False
                End Try
            End If
        Else
            MsgBox("Aplikasi tidak dapat berjalan di komputer ini", MsgBoxStyle.OkOnly = MsgBoxStyle.Critical, "Warning!")
        End If
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butLogin.Click
        tryLogin()
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        Close()
    End Sub

    Private Sub butSettings_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butSettings.Click
        status_login = True
        staff_name = "setting pengaturan"
        Close()
    End Sub
End Class