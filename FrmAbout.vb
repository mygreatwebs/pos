﻿Public Class FrmAbout

    Private Sub FrmAbout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        VersionNum.Text = My.Application.Info.Version.ToString
        LastUpdate.EditValue = CDate("2016-10-17")
    End Sub

    Private Sub butClose_Click(sender As Object, e As EventArgs) Handles butClose.Click
        Close()
    End Sub
End Class