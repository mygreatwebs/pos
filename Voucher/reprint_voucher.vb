﻿Public Class reprint_voucher
    Private Sub reprint_voucher_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RadioGroup1.SelectedIndex = 0
    End Sub

    Private Sub butBatal_Click(sender As Object, e As EventArgs) Handles butBatal.Click
        Me.Close()
    End Sub

    Private Sub reprint_Click(sender As Object, e As EventArgs) Handles reprint.Click
        Try
            SQLquery = String.Format("SELECT vr.kode_voucher,mc.idCabang,mc.alamatCabang,jumlah_voucher
FROM app_promo.voucher_retail vr
JOIN db_laritta.mcabang mc ON mc.idCabang=vr.idCabang
WHERE {0}", If(RadioGroup1.SelectedIndex = 0, "vr.idOrder='" & kodevoucher.Text & "'", "vr.kode_voucher = '" & kodevoucher.Text & "'"))
            ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "listvoucher")

            Dim reprintvc As New voucher_retail
            If xSet.Tables("listvoucher").Rows.Count = 0 Then
                MessageBox.Show("Kode yang diinputkan tidak sesuai!")
                Exit Sub
            End If
            reprintvc.Parameters("kodevoucher").Value = xSet.Tables("listvoucher").Rows(0).Item("kode_voucher")
            reprintvc.Parameters("alamat").Value = xSet.Tables("listvoucher").Rows(0).Item("alamatCabang")
            reprintvc.Parameters("reward").Value = "Free " & xSet.Tables("listvoucher").Rows(0).Item("jumlah_voucher") & " pcs product"

            SQLquery = String.Format("Update app_promo.voucher_retail SET cetakanke=cetakanke+1 WHERE kode_voucher='{0}'", xSet.Tables("listvoucher").Rows(0).Item("kode_voucher"))
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(reprintvc)

                ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                SQLquery = String.Format("SELECT vr.cetakanke FROM app_promo.voucher_retail vr WHERE kode_voucher='{0}'", xSet.Tables("listvoucher").Rows(0).Item("kode_voucher"))
                reprintvc.Parameters("cetakanke").Value = "Cetakan ke-" & ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                'tool.PreviewForm.WindowState = FormWindowState.Maximized
                'tool.ShowPreviewDialog()
                tool.Print()
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub gridvoucher()
        If IsNothing(xSet.Tables("listvoucher")) = True Then
            xSet.Tables.Add("listvoucher")
            xSet.Tables("listvoucher").Columns.Add("kode_voucher", Type.GetType("System.String"))
            xSet.Tables("listvoucher").Columns.Add("idCabang", Type.GetType("System.String"))
            xSet.Tables("listvoucher").Columns.Add("jumlah_voucher", Type.GetType("System.String"))
        End If
    End Sub
End Class