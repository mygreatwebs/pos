﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reprint_voucher
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(reprint_voucher))
        Me.kodevoucher = New System.Windows.Forms.TextBox()
        Me.reprint = New DevExpress.XtraEditors.SimpleButton()
        Me.butBatal = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'kodevoucher
        '
        Me.kodevoucher.Font = New System.Drawing.Font("Verdana", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kodevoucher.Location = New System.Drawing.Point(12, 70)
        Me.kodevoucher.Name = "kodevoucher"
        Me.kodevoucher.Size = New System.Drawing.Size(397, 37)
        Me.kodevoucher.TabIndex = 0
        Me.kodevoucher.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'reprint
        '
        Me.reprint.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.reprint.Appearance.Options.UseFont = True
        Me.reprint.Image = CType(resources.GetObject("reprint.Image"), System.Drawing.Image)
        Me.reprint.Location = New System.Drawing.Point(201, 113)
        Me.reprint.Name = "reprint"
        Me.reprint.Size = New System.Drawing.Size(101, 54)
        Me.reprint.TabIndex = 7
        Me.reprint.Text = "Re-Print"
        '
        'butBatal
        '
        Me.butBatal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBatal.Appearance.Options.UseFont = True
        Me.butBatal.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butBatal.Location = New System.Drawing.Point(308, 113)
        Me.butBatal.Name = "butBatal"
        Me.butBatal.Size = New System.Drawing.Size(101, 54)
        Me.butBatal.TabIndex = 8
        Me.butBatal.Text = "Batal"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.RadioGroup1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(254, 52)
        Me.GroupControl1.TabIndex = 9
        Me.GroupControl1.Text = "Input Kode"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadioGroup1.Location = New System.Drawing.Point(2, 20)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("nota", "Nomor Nota"), New DevExpress.XtraEditors.Controls.RadioGroupItem("voucher", "Kode Voucher Retail")})
        Me.RadioGroup1.Size = New System.Drawing.Size(250, 30)
        Me.RadioGroup1.TabIndex = 0
        '
        'reprint_voucher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 176)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.reprint)
        Me.Controls.Add(Me.butBatal)
        Me.Controls.Add(Me.kodevoucher)
        Me.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "reprint_voucher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Re-print Voucher"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents kodevoucher As TextBox
    Friend WithEvents reprint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butBatal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
End Class
