﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frminputvc
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frminputvc))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.tbSimpan = New DevExpress.XtraEditors.SimpleButton()
        Me.tbkalender = New DevExpress.XtraEditors.SimpleButton()
        Me.tbvcGF = New DevExpress.XtraEditors.SimpleButton()
        Me.tbvcPR = New DevExpress.XtraEditors.SimpleButton()
        Me.tbKeluar = New DevExpress.XtraEditors.SimpleButton()
        Me.tbHapus = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.numpadpanel = New DevExpress.XtraEditors.PanelControl()
        Me.butNum00 = New DevExpress.XtraEditors.SimpleButton()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum0 = New DevExpress.XtraEditors.SimpleButton()
        Me.butReset = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum7 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum8 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum9 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum4 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum5 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum6 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum3 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum2 = New DevExpress.XtraEditors.SimpleButton()
        Me.butNum1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.txtcounter = New DevExpress.XtraEditors.TextEdit()
        Me.txtprefix = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colkodeVC = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.numpadpanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.numpadpanel.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.txtcounter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtprefix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.tbSimpan)
        Me.PanelControl1.Controls.Add(Me.tbkalender)
        Me.PanelControl1.Controls.Add(Me.tbvcGF)
        Me.PanelControl1.Controls.Add(Me.tbvcPR)
        Me.PanelControl1.Controls.Add(Me.tbKeluar)
        Me.PanelControl1.Controls.Add(Me.tbHapus)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl1.Location = New System.Drawing.Point(459, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(197, 378)
        Me.PanelControl1.TabIndex = 1
        '
        'tbSimpan
        '
        Me.tbSimpan.Image = CType(resources.GetObject("tbSimpan.Image"), System.Drawing.Image)
        Me.tbSimpan.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbSimpan.Location = New System.Drawing.Point(17, 222)
        Me.tbSimpan.Name = "tbSimpan"
        Me.tbSimpan.Size = New System.Drawing.Size(75, 65)
        Me.tbSimpan.TabIndex = 5
        Me.tbSimpan.Text = "Simpan"
        '
        'tbkalender
        '
        Me.tbkalender.Image = CType(resources.GetObject("tbkalender.Image"), System.Drawing.Image)
        Me.tbkalender.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbkalender.Location = New System.Drawing.Point(100, 75)
        Me.tbkalender.Name = "tbkalender"
        Me.tbkalender.Size = New System.Drawing.Size(75, 65)
        Me.tbkalender.TabIndex = 4
        Me.tbkalender.Text = "Kalender"
        '
        'tbvcGF
        '
        Me.tbvcGF.Image = CType(resources.GetObject("tbvcGF.Image"), System.Drawing.Image)
        Me.tbvcGF.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbvcGF.Location = New System.Drawing.Point(17, 147)
        Me.tbvcGF.Name = "tbvcGF"
        Me.tbvcGF.Size = New System.Drawing.Size(75, 65)
        Me.tbvcGF.TabIndex = 3
        Me.tbvcGF.Text = "GF"
        '
        'tbvcPR
        '
        Me.tbvcPR.Image = CType(resources.GetObject("tbvcPR.Image"), System.Drawing.Image)
        Me.tbvcPR.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbvcPR.Location = New System.Drawing.Point(17, 75)
        Me.tbvcPR.Name = "tbvcPR"
        Me.tbvcPR.Size = New System.Drawing.Size(75, 65)
        Me.tbvcPR.TabIndex = 2
        Me.tbvcPR.Text = "PR"
        '
        'tbKeluar
        '
        Me.tbKeluar.Image = CType(resources.GetObject("tbKeluar.Image"), System.Drawing.Image)
        Me.tbKeluar.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbKeluar.Location = New System.Drawing.Point(98, 222)
        Me.tbKeluar.Name = "tbKeluar"
        Me.tbKeluar.Size = New System.Drawing.Size(75, 65)
        Me.tbKeluar.TabIndex = 1
        Me.tbKeluar.Text = "Batal"
        '
        'tbHapus
        '
        Me.tbHapus.Image = CType(resources.GetObject("tbHapus.Image"), System.Drawing.Image)
        Me.tbHapus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.tbHapus.Location = New System.Drawing.Point(99, 147)
        Me.tbHapus.Name = "tbHapus"
        Me.tbHapus.Size = New System.Drawing.Size(75, 65)
        Me.tbHapus.TabIndex = 0
        Me.tbHapus.Text = "Hapus"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.numpadpanel)
        Me.PanelControl2.Controls.Add(Me.PanelControl3)
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(459, 378)
        Me.PanelControl2.TabIndex = 2
        '
        'numpadpanel
        '
        Me.numpadpanel.Controls.Add(Me.butNum00)
        Me.numpadpanel.Controls.Add(Me.butClose)
        Me.numpadpanel.Controls.Add(Me.butOK)
        Me.numpadpanel.Controls.Add(Me.butNum0)
        Me.numpadpanel.Controls.Add(Me.butReset)
        Me.numpadpanel.Controls.Add(Me.butNum7)
        Me.numpadpanel.Controls.Add(Me.butNum8)
        Me.numpadpanel.Controls.Add(Me.butNum9)
        Me.numpadpanel.Controls.Add(Me.butNum4)
        Me.numpadpanel.Controls.Add(Me.butNum5)
        Me.numpadpanel.Controls.Add(Me.butNum6)
        Me.numpadpanel.Controls.Add(Me.butNum3)
        Me.numpadpanel.Controls.Add(Me.butNum2)
        Me.numpadpanel.Controls.Add(Me.butNum1)
        Me.numpadpanel.Location = New System.Drawing.Point(12, 51)
        Me.numpadpanel.Name = "numpadpanel"
        Me.numpadpanel.Size = New System.Drawing.Size(409, 315)
        Me.numpadpanel.TabIndex = 3
        '
        'butNum00
        '
        Me.butNum00.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum00.Appearance.Options.UseFont = True
        Me.butNum00.Location = New System.Drawing.Point(177, 228)
        Me.butNum00.Name = "butNum00"
        Me.butNum00.Size = New System.Drawing.Size(63, 62)
        Me.butNum00.TabIndex = 28
        Me.butNum00.Text = "00"
        '
        'butClose
        '
        Me.butClose.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClose.Appearance.Options.UseFont = True
        Me.butClose.Location = New System.Drawing.Point(246, 228)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(124, 62)
        Me.butClose.TabIndex = 27
        Me.butClose.Text = "Cancel"
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Location = New System.Drawing.Point(246, 92)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(124, 130)
        Me.butOK.TabIndex = 26
        Me.butOK.Text = "OK"
        '
        'butNum0
        '
        Me.butNum0.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum0.Appearance.Options.UseFont = True
        Me.butNum0.Location = New System.Drawing.Point(39, 228)
        Me.butNum0.Name = "butNum0"
        Me.butNum0.Size = New System.Drawing.Size(132, 62)
        Me.butNum0.TabIndex = 25
        Me.butNum0.Text = "0"
        '
        'butReset
        '
        Me.butReset.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butReset.Appearance.Options.UseFont = True
        Me.butReset.Location = New System.Drawing.Point(246, 24)
        Me.butReset.Name = "butReset"
        Me.butReset.Size = New System.Drawing.Size(124, 62)
        Me.butReset.TabIndex = 24
        Me.butReset.Text = "C"
        '
        'butNum7
        '
        Me.butNum7.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum7.Appearance.Options.UseFont = True
        Me.butNum7.Location = New System.Drawing.Point(39, 160)
        Me.butNum7.Name = "butNum7"
        Me.butNum7.Size = New System.Drawing.Size(63, 62)
        Me.butNum7.TabIndex = 23
        Me.butNum7.Text = "7"
        '
        'butNum8
        '
        Me.butNum8.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum8.Appearance.Options.UseFont = True
        Me.butNum8.Location = New System.Drawing.Point(108, 160)
        Me.butNum8.Name = "butNum8"
        Me.butNum8.Size = New System.Drawing.Size(63, 62)
        Me.butNum8.TabIndex = 22
        Me.butNum8.Text = "8"
        '
        'butNum9
        '
        Me.butNum9.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum9.Appearance.Options.UseFont = True
        Me.butNum9.Location = New System.Drawing.Point(177, 160)
        Me.butNum9.Name = "butNum9"
        Me.butNum9.Size = New System.Drawing.Size(63, 62)
        Me.butNum9.TabIndex = 21
        Me.butNum9.Text = "9"
        '
        'butNum4
        '
        Me.butNum4.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum4.Appearance.Options.UseFont = True
        Me.butNum4.Location = New System.Drawing.Point(39, 92)
        Me.butNum4.Name = "butNum4"
        Me.butNum4.Size = New System.Drawing.Size(63, 62)
        Me.butNum4.TabIndex = 20
        Me.butNum4.Text = "4"
        '
        'butNum5
        '
        Me.butNum5.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum5.Appearance.Options.UseFont = True
        Me.butNum5.Location = New System.Drawing.Point(108, 92)
        Me.butNum5.Name = "butNum5"
        Me.butNum5.Size = New System.Drawing.Size(63, 62)
        Me.butNum5.TabIndex = 19
        Me.butNum5.Text = "5"
        '
        'butNum6
        '
        Me.butNum6.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum6.Appearance.Options.UseFont = True
        Me.butNum6.Location = New System.Drawing.Point(177, 92)
        Me.butNum6.Name = "butNum6"
        Me.butNum6.Size = New System.Drawing.Size(63, 62)
        Me.butNum6.TabIndex = 18
        Me.butNum6.Text = "6"
        '
        'butNum3
        '
        Me.butNum3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum3.Appearance.Options.UseFont = True
        Me.butNum3.Location = New System.Drawing.Point(177, 24)
        Me.butNum3.Name = "butNum3"
        Me.butNum3.Size = New System.Drawing.Size(63, 62)
        Me.butNum3.TabIndex = 17
        Me.butNum3.Text = "3"
        '
        'butNum2
        '
        Me.butNum2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum2.Appearance.Options.UseFont = True
        Me.butNum2.Location = New System.Drawing.Point(108, 24)
        Me.butNum2.Name = "butNum2"
        Me.butNum2.Size = New System.Drawing.Size(63, 62)
        Me.butNum2.TabIndex = 16
        Me.butNum2.Text = "2"
        '
        'butNum1
        '
        Me.butNum1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butNum1.Appearance.Options.UseFont = True
        Me.butNum1.Location = New System.Drawing.Point(39, 24)
        Me.butNum1.Name = "butNum1"
        Me.butNum1.Size = New System.Drawing.Size(63, 62)
        Me.butNum1.TabIndex = 15
        Me.butNum1.Text = "1"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.txtcounter)
        Me.PanelControl3.Controls.Add(Me.txtprefix)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(455, 43)
        Me.PanelControl3.TabIndex = 2
        '
        'txtcounter
        '
        Me.txtcounter.Location = New System.Drawing.Point(200, 10)
        Me.txtcounter.Name = "txtcounter"
        Me.txtcounter.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtcounter.Properties.Appearance.Options.UseFont = True
        Me.txtcounter.Size = New System.Drawing.Size(217, 26)
        Me.txtcounter.TabIndex = 1
        '
        'txtprefix
        '
        Me.txtprefix.Location = New System.Drawing.Point(10, 10)
        Me.txtprefix.Name = "txtprefix"
        Me.txtprefix.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txtprefix.Properties.Appearance.Options.UseFont = True
        Me.txtprefix.Size = New System.Drawing.Size(184, 26)
        Me.txtprefix.TabIndex = 0
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 51)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(408, 315)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colkodeVC})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.RowHeight = 24
        '
        'colkodeVC
        '
        Me.colkodeVC.Caption = "Kode Voucher"
        Me.colkodeVC.FieldName = "kodevc"
        Me.colkodeVC.Name = "colkodeVC"
        Me.colkodeVC.Visible = True
        Me.colkodeVC.VisibleIndex = 0
        '
        'frminputvc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(656, 378)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frminputvc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Input Kode Voucher"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.numpadpanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.numpadpanel.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.txtcounter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtprefix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents tbKeluar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbHapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tbvcGF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbvcPR As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents txtcounter As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtprefix As DevExpress.XtraEditors.TextEdit
    Friend WithEvents colkodeVC As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbkalender As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents numpadpanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butNum00 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butReset As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butNum1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbSimpan As DevExpress.XtraEditors.SimpleButton
End Class
