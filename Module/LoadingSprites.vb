﻿Imports DevExpress.Utils

Public Class LoadingSprites
    Private LoadingSprite As WaitDialogForm

    Public Overloads Sub ShowLoading()
        LoadingSprite = New WaitDialogForm("Loading..", "Please wait")
        LoadingSprite.Show()
    End Sub

    Public Overloads Sub ShowLoading(ByVal ParentForm As Form)
        LoadingSprite = New WaitDialogForm("Loading..", "Please wait") With {.Owner = ParentForm}
        LoadingSprite.Show()
    End Sub

    Public Overloads Sub ShowLoading(ByVal Info As String, ByVal title As String)
        LoadingSprite = New WaitDialogForm(Info, title)
        LoadingSprite.Show()
    End Sub

    Public Overloads Sub ShowLoading(ByVal Info As String, ByVal title As String, ByVal ParentForm As Form)
        LoadingSprite = New WaitDialogForm(Info, title) With {.Owner = ParentForm}
        LoadingSprite.Show()
    End Sub

    Public Sub CloseLoading()
        If LoadingSprite.Created Then
            LoadingSprite.Close()
            LoadingSprite.Dispose()
        End If
    End Sub
End Class
