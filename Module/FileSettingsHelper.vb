﻿Public Class FileSettingsHelper
    '***********************************************************************************
    ' API Function
    '***********************************************************************************
    Private Declare Unicode Function WriteINIFile Lib "kernel32" Alias "WritePrivateProfileStringW" (ByVal pAppName As String, ByVal pKeyName As String, ByVal pString As String, ByVal pFileName As String) As Int32
    Private Declare Unicode Function GetINIFile Lib "kernel32" Alias "GetPrivateProfileStringW" (ByVal pAppName As String, ByVal pKeyName As String, ByVal pDefault As String, ByVal pReturned As String, ByVal pSize As Int32, ByVal pFileName As String) As Int32

    ''' <summary>
    ''' Create or update existing INI file
    ''' </summary>
    ''' <param name="FilePath">[in] The path file will be write on to.</param>
    ''' <param name="DataSection">[in] Section or Category as the data field container.</param>
    ''' <param name="DataName">[in] A data field inside the Section.</param>
    ''' <param name="DataValue">[in] Value of data field specified before.</param>
    Public Shared Sub WriteSettings(ByVal FilePath As String, ByVal DataSection As String, ByVal DataName As String, ByVal DataValue As String)
        WriteINIFile(DataSection, DataName, DataValue, FilePath)
    End Sub

    ''' <summary>
    ''' Read INI file specified by path files
    ''' </summary>
    ''' <param name="FilePath">[in] The path file will be write on to.</param>
    ''' <param name="DataSection">[in] Section or Category as the data field container.</param>
    ''' <param name="DataName">[in] A data field inside the Section.</param>
    ''' <param name="DataDefault">[in] Default value will be used if data field not exist.</param>
    ''' <returns>Return a string value.</returns>
    Public Shared Function ReadSettings(ByVal FilePath As String, ByVal DataSection As String, ByVal DataName As String, ByVal DataDefault As String) As String
        Dim Values As String = Space$(1024)
        Dim LenParamVal As Long = GetINIFile(DataSection, DataName, DataDefault, Values, Len(Values), FilePath)

        Return Left$(Values, LenParamVal)
    End Function
End Class