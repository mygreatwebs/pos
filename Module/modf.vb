﻿Imports MySql.Data.MySqlClient
Imports Laritta_POS.RFID_Tools
Imports System.ComponentModel
Imports System.IO
Imports System.Runtime.CompilerServices
Module modf

    'mySQL cloud emoney
    Public conemoney As New MySqlConnection
    Public commandemoney As New MySqlCommand

    'Variabel mySQL
    Dim xCon As MySqlConnection

    'WaitFormDialog
    Public LoadSprite As New LoadingSprites

    'Encryption
    Public Const EncryptKey As String = "01encryptitk02"
    Public Encryptor As New TripleDES(EncryptKey)


    Public ExDb As New ClassDB
    Public xSet As New DataSet
    Public SQLquery As String

    'laritta cash
    Public membertopup As String
    Public saldosebelum As Double
    Public saldosesudah As Double
    Public bataltopup As Boolean = False
    Public saldokartu As Double
    Public bayarpakaiemoney As Boolean = False
    Public jumlahpembayaranemoney As Double
    Public idmember As String = "196206180001"
    Public useemoney As Boolean = False
    Public namapemilikkartu As String = ""
    Public totaltopup As Double = 0
    Public nominalhargatopup As Double = 0
    Public istopup As Boolean = False
    Public bayarpakaikartukredit As Boolean = False
    Public passwordenkripsi As String = "J4anu$arGa4kA44La$ayLho0Sumpah"
    Public idnominaltopup As String = ""
    Public idpromo As String
    Public nominaltopupvoucher As Double = 0
    Public listitemtopup As New DataTable
    Public pakaivouchertopup As Boolean = False
    Public adatopupdenganvoucher As Boolean = False
    Public jumlahjenisbarangtopupdenganvoucher As Integer = 0
    Public adatopupdenganvoucherdanreguler As Boolean = False
    'Public adajenistopupvoucherdanreguler As Boolean = False
    Public ispromoemoney As Boolean = False

    'variable master promo
    Public custbawakartumember As Boolean = False
    Public sudahtanyabawamember As Boolean = False
    Public isDapatPromo As Boolean = False
    Public totalvalid As Double = 0
    Public isambilpaketpromo As Boolean = False
    Public cancelambilbarangpromo As Boolean = False

    'Setting
    Public id_cabang As String = My.Settings.idcabang
    Public namacabang, ketvoidpos As String

    'Connection String
    Public conn_string_local As String

    'RFID
    Public DefReaderName As String
    Public GlobalReaderContext As IntPtr
    Public MyMiFareKey As MiFareKey = New MiFareKey
    Public MyACR As ACR1222L = New ACR1222L
    Public MyCard As MiFareCard = New MiFareCard
    Public CardUID As String

    'Variabel login
    Public staff_id, staff_group, shiftLogin, shiftNow, start_modal As Integer
    Public staff_name As String
    Public status_login As Boolean = True
    Public TglShiftNow, TglShiftLogin As DateTime

    'Variabel security
    Public AllowDiskon As Boolean
    Public AllowVoid As Boolean
    'Variabel Query dan SP
    Public sp_Query, param_Query As String

    'Variabel toko
    Public qttInteger, totDisc, selected_idxRow, idCuaca, TotalDisc As Integer

    Public selected_row As DataRow
    Public isReprint, isContinue, isprintclosingx As Boolean
    Public arrayhari() As String = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"}
    Public jamts As Date
    Public selectedvoucher As Integer = 0
    Public showvirtualnum As Boolean = True
    '1 promo
    '2 khusus
    Public Sqlbeforemutasi As String
    Public SqlAftermutasi As String
    Public prosests As Boolean
    Public adadatatransfer As Boolean = False
    Public isbayarshow As Boolean = False
    Public IsGrandOpening As Boolean = True
    Public isDummy As Boolean = False
    Public TipeLogout As Integer '1-Batal,2-Logout Tanpa Masukan Modal,3- Closing & Ganti Kasir
    Public isUserCanClosing As Boolean
    Public kodemember As String = ""
    Public kodememberswap As String = ""
    Public isCekProdukPromo As Boolean = False
    Public idCust As Integer = 0
    Public idfrmconfirm_1 As Integer = 0 '0 - tidak dipilih,1 - ke menu ,2 - input bayar
    Public ispromoproces As Boolean = False
    Public isBeli5Gratis1 As Boolean = False
    Public isBeli5Gratis1MLG As Boolean = False
    Public isUndianKalender As Boolean = False
    Public isPromoKeju As Boolean = False
    Public isvalidpromokeju As Boolean = False
    Public isvalidfreekalender022017 As Boolean = False
    Public isvalidBeliRotiGratisProdukshowcase = False
    Public isPromoWholeBreadMLG = False, isPromoNewProduct As Boolean, isvalidpromokalender032017 As Boolean
    Public isPromoVoucherUndian As Boolean
    Public ishavemember As Boolean = False
    Public AktifPromo As Integer '0- tidak ada,1. procheese,2. 5 gratis 1
    Public byarpakaiVC As Boolean = False
    Public G_idpromo As Integer = 0
    Public G_ketpromo As String = ""
    Public G_kodekupon As String = ""
    Public v_pointawal As Decimal
    Public v_kuponawal As Decimal
    Public v_pointakhir As Decimal
    Public v_kuponakhir As Decimal
    Public G_kuponkalender As String = ""
    Public userVoid As String = ""
    Public isUserCanVoid As Boolean
    Public jmlPaket As Integer
    Public batalnilaivoucher As Boolean = False
    Public novoucher As String
    Public promo7rb As Boolean = Nothing
    Public konfirmasipromo7rb As Boolean = False
    Public isbolehprint As Boolean
    Public isvalidbuyonegetone As Boolean = False
    Public isinputkodemember As Boolean = False
    Public isvalidSpecialkemerdekaan As Boolean = False
    Public showlistbarangpromo As Boolean = False
    Public Sub ceksalahsaldo(membersalah As String)
        'blessing laritta cash
        If IsNothing(xSet.Tables("listblessing")) Then
            xSet.Tables.Add("listblessing")
        Else
            xSet.Tables("listblessing").Clear()
        End If
        SQLquery = String.Format("SELECT id_blessing,kode_member,nominal,jenis_blessing,date_format(tanggal_cutoff,'%Y-%m-%d %H:%i:%s') `tanggal_cutoff` FROM blessing_laritta_cash WHERE kode_member='{0}'", membersalah)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "listblessing")

        'cek list blessing dan revisi saldo
        For o = 0 To xSet.Tables("listblessing").Rows.Count - 1
            'revisi saldo
            If xSet.Tables("listblessing").Rows(o).Item("jenis_blessing").ToString = "0" Then
                SQLquery = String.Format("SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='{0}' AND tanggal_input>'{1}'", membersalah, xSet.Tables("listblessing").Rows(o).Item("tanggal_cutoff"))
                If IsNothing(xSet.Tables("membersalah")) Then
                    xSet.Tables.Add("membersalah")
                Else
                    xSet.Tables("membersalah").Clear()
                End If
                ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
                If xSet.Tables("membersalah").Rows.Count = 0 Then
                    inputsaldo(xSet.Tables("listblessing").Rows(o).Item("nominal").ToString)
                End If
            ElseIf xSet.Tables("listblessing").Rows(o).Item("jenis_blessing").ToString = "1" Then
                Dim nominalkartu As Double
                nominalkartu = readsaldo(False)
                nominalkartu = nominalkartu + xSet.Tables("listblessing").Rows(o).Item("nominal")
                inputsaldo(nominalkartu.ToString)
            End If
            SQLquery = String.Format("UPDATE blessing_laritta_cash SET isDone='1' WHERE id_blessing='{0}'", xSet.Tables("listblessing").Rows(o).Item("id_blessing"))
            ExDb.ExecData(SQLquery, conn_string_local)
        Next o

        ''orang dengan id member 197607130002
        'If membersalah = "197607130002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197607130002' AND tanggal_input>'2017-07-22 09:02:07'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("37500")
        '    End If
        'End If

        ''orang dengan id member 197605130004
        'If membersalah = "197605130004" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197607130002' AND tanggal_input>'2017-07-24 12:23:12'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 198804110002
        'If membersalah = "198804110002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198804110002' AND tanggal_input>'2017-07-24 12:23:12'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("21000")
        '    End If
        'End If

        ''orang dengan id member 197102020002
        'If membersalah = "197102020002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197102020002' AND tanggal_input>'2017-08-28 07:00:34'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 196205170001
        'If membersalah = "196205170001" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='196205170001' AND tanggal_input>'2017-08-28 11:27:54'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("147500")
        '    End If
        'End If

        ''orang dengan id member 197006080005
        'If membersalah = "197006080005" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197006080005' AND tanggal_input>'2017-09-16 07:11:22'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("84800")
        '    End If
        'End If

        ''orang dengan id member 197411190003
        'If membersalah = "197411190003" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197411190003' AND tanggal_input>'2017-09-11 09:23:01'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("62800")
        '    End If
        'End If


        ''orang dengan id member 198102210002
        'If membersalah = "198102210002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198102210002' AND tanggal_input>'2017-09-11 13:10:15'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("107600")
        '    End If
        'End If

        ''orang dengan id member 199107260003
        'If membersalah = "199107260003" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='199107260003' AND tanggal_input>'2017-09-03 13:22:13'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("195600")
        '    End If
        'End If

        ''orang dengan id member 197502080007
        'If membersalah = "197502080007" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197502080007' AND tanggal_input>'2017-09-15 20:57:01'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("159000")
        '    End If
        'End If

        ''orang dengan id member 197108210002
        'If membersalah = "197108210002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197108210002' AND tanggal_input>'2017-09-15 13:12:11'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("91000")
        '    End If
        'End If

        ''orang dengan id member 198012210006
        'If membersalah = "198012210006" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198012210006' AND tanggal_input>'2017-09-15 13:02:01'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("39250")
        '    End If
        'End If

        ''orang dengan id member 198012210006
        'If membersalah = "198012210006" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198012210006' AND tanggal_input>'2017-09-15 13:02:01'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("39250")
        '    End If
        'End If

        ''orang dengan id member 197707060001
        'If membersalah = "197707060001" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197707060001' AND tanggal_input>'2017-09-15 09:47:40'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("105000")
        '    End If
        'End If

        ''orang dengan id member 198906140007
        'If membersalah = "198906140007" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198906140007' AND tanggal_input>'2017-09-15 10:31:26'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("30000")
        '    End If
        'End If

        ''orang dengan id member 197505080009
        'If membersalah = "197505080009" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197505080009' AND tanggal_input>'2017-09-18 18:42:26'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("19000")
        '    End If
        'End If

        ''orang dengan id member 197907250008
        'If membersalah = "197907250008" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='197907250008' AND tanggal_input>'2017-09-15 21:20:40'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("93000")
        '    End If
        'End If

        ''orang dengan id member 198401130007
        'If membersalah = "198401130007" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198401130007' AND tanggal_input>'2017-09-15 08:24:29'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("3000")
        '    End If
        'End If

        ''orang dengan id member 193001010275
        'If membersalah = "193001010275" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='193001010275' AND tanggal_input>'2017-09-15 08:44:08'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("21500")
        '    End If
        'End If

        ''orang dengan id member 198105070006
        'If membersalah = "198105070006" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198105070006' AND tanggal_input>'2017-09-15 07:13:56'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("800")
        '    End If
        'End If

        ''orang dengan id member 198211170006
        'If membersalah = "198211170006" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198211170006' AND tanggal_input>'2017-09-18 16:46:57'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("20000")
        '    End If
        'End If

        ''orang dengan id member 198006200003
        'If membersalah = "198006200003" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198006200003' AND tanggal_input>'2017-09-19 11:03:39'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 198303240003
        'If membersalah = "198303240003" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198303240003' AND tanggal_input>'2017-10-01 11:12:12'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("93800")
        '    End If
        'End If

        ''orang dengan id member 196406110002
        'If membersalah = "196406110002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='196406110002' AND tanggal_input>'2017-10-04 13:13:58'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("65000")
        '    End If
        'End If

        ''orang dengan id member 199307240011
        'If membersalah = "199307240011" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='199307240011' AND tanggal_input>'2017-10-08 20:35:50'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 196602200002
        'If membersalah = "196602200002" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='196602200002' AND tanggal_input>'2017-10-11 17:32:25'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 199503120009
        'If membersalah = "199503120009" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='199503120009' AND tanggal_input>'2017-10-15 21:23:47'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("0")
        '    End If
        'End If

        ''orang dengan id member 198803020004
        'If membersalah = "198803020004" Then
        '    SQLquery = "SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='198803020004' AND tanggal_input>'2017-10-23 18:22:09'"
        '    If IsNothing(xSet.Tables("membersalah")) Then
        '        xSet.Tables.Add("membersalah")
        '    Else
        '        xSet.Tables("membersalah").Clear()
        '    End If
        '    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "membersalah")
        '    If xSet.Tables("membersalah").Rows.Count = 0 Then
        '        inputsaldo("5000")
        '    End If
        'End If
    End Sub

    Public Sub cekisvalidfreekalender0220017()
        Dim arrydt() As String = Split(My.Settings.ValidRangeFreeKalender022017, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isvalidfreekalender022017 = True
        Else
            isvalidfreekalender022017 = False
        End If
    End Sub

    Public Sub cekisvalidpromokalender032017()
        Dim arrydt() As String = Split(My.Settings.ValidRangePromoKalender032017, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isvalidpromokalender032017 = True
        Else
            isvalidpromokalender032017 = False
        End If
    End Sub

    Public Sub cekisvalidBeliRotiGratisProdukshowcase()
        Dim arrydt() As String = Split(My.Settings.ValidRangeBeliRotiGratisProdukshowcase, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isvalidBeliRotiGratisProdukshowcase = True
        Else
            isvalidBeliRotiGratisProdukshowcase = False
        End If
    End Sub

    Public Sub cekisvalidSpecialkemerdekaan()
        Dim arrydt() As String = Split(My.Settings.ValidRangeSpecialkemerdekaan, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isvalidSpecialkemerdekaan = True
        Else
            isvalidSpecialkemerdekaan = False
        End If
    End Sub

    Public Sub cekvalidundiankalender()
        Dim arrydt() As String = Split(My.Settings.ValidRangeUndianKalender, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isUndianKalender = True
        Else
            isUndianKalender = False
        End If
    End Sub

    Public Sub cekvalidBeli5Gratis1()
        Dim arrydt() As String = Split(My.Settings.ValidRangeBeli5Gratis1, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isBeli5Gratis1 = True
        Else
            isBeli5Gratis1 = False
        End If
    End Sub

    Public Function cekvalidbuyonegetone()
        Dim arrydt() As String = Split(My.Settings.ValidRangePromoBuyOneGetOne, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub cekvalidPromoWholeBreadMLG()
        Dim arrydt() As String = Split(My.Settings.ValidRangePromoWholeBreadMLG, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isPromoWholeBreadMLG = True
        Else
            isPromoWholeBreadMLG = False
        End If
    End Sub
    Public Sub cekvalidPromoNewProduct()
        Dim arrydt() As String = Split(My.Settings.ValidRangePromoNewProduct, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isPromoNewProduct = True
        Else
            isPromoNewProduct = False
        End If
    End Sub
    Public Sub cekvalidBeli5Gratis1MLG()
        If My.Settings.idWilayah <> "MLG" Then
            isBeli5Gratis1MLG = False
            Exit Sub
        End If
        Dim arrydt() As String = Split(My.Settings.ValidRangeBeli5Gratis1MLG, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isBeli5Gratis1MLG = True
        Else
            isBeli5Gratis1MLG = False
        End If
    End Sub

    Public Sub cekvalidpromokeju()
        Dim arrydt() As String = Split(My.Settings.ValidRangePromoKeju, ";")
        Dim tglservelokal As String = ExDb.Scalar("select date(now())", conn_string_local)
        If CDate(tglservelokal) >= CDate(arrydt(0)) And CDate(tglservelokal) <= CDate(arrydt(1)) Then
            isvalidpromokeju = True
        Else
            isvalidpromokeju = False
        End If
    End Sub

    Private Sub cekIsGrandOpening()
        Try
            IsGrandOpening = IIf(My.Settings("IsGrandOpening") = "1", True, False)
        Catch ex As Exception
            IsGrandOpening = False
        End Try
    End Sub

    Private Function cekadadatamutbs() As Boolean
        If IsNothing(xSet.Tables("dtmtbs")) = False Then xSet.Tables("dtmtbs").Clear()
        ExDb.ExecQuery(My.Settings.db_conn_mysql, "select count(*) from mtadj where notes like 'Mutasi TS ke BS by Software%' and DATE(createDate)='" & Format(Now(), "yyyy-MM-dd") & "' and status=0 and isvoid=0", xSet, "dtmtbs")

        If xSet.Tables("dtmtbs").Rows.Count > 0 Then

            If xSet.Tables("dtmtbs").Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Sub SetDefaultPrinting()
        Dim oPS As New System.Drawing.Printing.PrinterSettings

        For i As Integer = 0 To oPS.PaperSizes.Count - 1
            'MsgBox(oPS.PaperSizes(i).ToString)
            If oPS.PaperSizes(i).PaperName = "80(72.1) x 3276 mm" Then
                'MsgBox(oPS.PaperSizes(i).ToString)
                oPS.DefaultPageSettings.PaperSize = oPS.PaperSizes(i)

                'MsgBox(oPS.DefaultPageSettings.PaperSource.ToString)
            End If
        Next
        oPS.Copies = 2

    End Sub

    Public Sub Main()
        Dim appProc() As Process

        Dim strProcName As String
        Dim strModName As String = Process.GetCurrentProcess.MainModule.ModuleName
        strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

        appProc = Process.GetProcessesByName(strProcName)
        If appProc.Length > 1 Then
            MessageBox.Show("Applikasi sudah berjalan di Desktop.")
        Else
            conn_string_local = My.Settings.localconn

            Try
                xCon = New MySqlConnection(conn_string_local)
                xCon.Open()
                If Not xCon Is Nothing Then xCon.Close()

                SQLquery = "SELECT idCabang, namaCabang, alamatCabang, telpCabang, isProduksi FROM mcabang WHERE Inactive=0"
                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "daftarCabang")

                createPOSDataSet()
                createTabelKresek()
                createUangKasirDataSet()
                'QryHeaderNota()
                'SetDefaultPrinting()
                cekvalidundiankalender()
                cekvalidBeli5Gratis1()
                cekvalidBeli5Gratis1MLG()
                cekvalidpromokeju()
                cekisvalidfreekalender0220017()
                cekisvalidBeliRotiGratisProdukshowcase()
                cekvalidPromoWholeBreadMLG()
                cekvalidPromoNewProduct()
                cekisvalidpromokalender032017()
                cekisvalidSpecialkemerdekaan()
                adadatatransfer = cekadadatamutbs()
                jamts = ExDb.Scalar("select jamexpired from msetupitembs limit 0,1", My.Settings.db_conn_mysql)
                cekIsGrandOpening()
            Catch ex As MySqlException
                msgboxInformation("Error connecting to the server")
                Exit Sub
            End Try

            DevExpress.UserSkins.BonusSkins.Register()
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(GetType(DevExpress.UserSkins.BonusSkins).Assembly)
            DevExpress.Skins.SkinManager.Default.RegisterAssembly(GetType(DevExpress.UserSkins.OfficeSkins).Assembly)
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Caramel")
            DevExpress.Skins.SkinManager.EnableFormSkins()

            Try
                Application.Run(New mainForm)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Application.Exit()
            End Try
        End If
    End Sub

    Private Sub createUangKasirDataSet()
        xSet.Tables.Add("infouangkasir")
        xSet.Tables("infouangkasir").Columns.Add("idUang", Type.GetType("System.Decimal"))
        xSet.Tables("infouangkasir").Columns.Add("nominal", Type.GetType("System.String"))
        xSet.Tables("infouangkasir").Columns.Add("jmlh", Type.GetType("System.Decimal"))

    End Sub

    Private Sub createPOSDataSet()
        xSet.Tables.Add("setSales")
        xSet.Tables("setSales").Columns.Add("idBrg", Type.GetType("System.Decimal"))
        xSet.Tables("setSales").Columns.Add("namaBrg", Type.GetType("System.String"))
        xSet.Tables("setSales").Columns.Add("jmlh", Type.GetType("System.Decimal"))
        xSet.Tables("setSales").Columns.Add("hrg", Type.GetType("System.Decimal"))
        xSet.Tables("setSales").Columns.Add("hrgpcs", Type.GetType("System.Decimal"))
        xSet.Tables("setSales").Columns.Add("disc", Type.GetType("System.Decimal"))
    End Sub

    Private Sub createTabelKresek()
        Dim ds As New DataSet

        ExDb.ExecQuery(conn_string_local, "SHOW TABLES LIKE 'mmenukresek' ", ds, "urtkresek")
        If ds.Tables(0).Rows.Count < 1 Then

            Dim sqtcrtb As String = " CREATE TABLE `mmenukresek` (" &
                    " `idBrg` INT(11) NULL DEFAULT NULL, " &
                    " `urutan` INT(11) NULL DEFAULT NULL " &
                    ") COLLATE='utf8_general_ci' ENGINE=InnoDB "

            ExDb.ExecData(sqtcrtb, conn_string_local)

        End If
        ds.Dispose()
    End Sub
    Public Sub msgboxErrorOffline()
        MsgBox("Koneksi Offline , silahkan hubungi pihak Developer", MsgBoxStyle.Critical, "PERHATIAN!")
    End Sub
    Public Sub msgboxErrorDev()
        MsgBox("Terdapat error pada saat menyimpan data, silahkan hubungi pihak Developer", MsgBoxStyle.Critical, "PERHATIAN!")
    End Sub

    Public Sub msgboxWarning(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Exclamation, "Perhatian")
    End Sub

    Public Sub msgboxInformation(ByVal xtext As String)
        MsgBox(xtext, MsgBoxStyle.Information, "Perhatian")
    End Sub

    Public Function NextShiftChecker()
        Try
            If Not xSet.Tables("CekNextShift") Is Nothing Then xSet.Tables("CekNextShift").Clear()
            SQLquery = String.Format("SELECT COUNT(no_urut_closing) jmlShift, IFNULL(MAX(no_urut_closing),0)+1 AS NextShift, CURRENT_TIMESTAMP() AS TglShift FROM mtclosing WHERE idCabang={0} AND DATE(createDate)=DATE(CURDATE())", id_cabang)
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "CekNextShift")

            shiftNow = xSet.Tables("CekNextShift").Rows(0).Item("NextShift")
            TglShiftNow = xSet.Tables("CekNextShift").Rows(0).Item("TglShift")

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ShiftChecker()
        Try
            If Not xSet.Tables("CekShiftNow") Is Nothing Then xSet.Tables("CekShiftNow").Clear()
            'SQLquery = String.Format("SELECT COUNT(shiftKe) num, IFNULL(MAX(shiftKe),0) shiftNow, IFNULL(MAX(startTime), CURRENT_TIMESTAMP()) TglShiftNow, CURRENT_TIMESTAMP() TglShiftLogin FROM mtswitchuser WHERE idCabang={0} AND idStaff={1} AND endTime='00:00:00' AND status=1", id_cabang, staff_id)
            SQLquery = String.Format("SELECT COUNT(shiftKe) num, IFNULL(MAX(shiftKe),0) shiftNow, IFNULL(MAX(startTime), CURRENT_TIMESTAMP()) TglShiftNow, CURRENT_TIMESTAMP() TglShiftLogin FROM mtswitchuser WHERE idCabang={0} AND firstbalance<>0 AND endTime='00:00:00' AND status=1", id_cabang)
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "CekShiftNow")

            shiftNow = xSet.Tables("CekShiftNow").Rows(0).Item("shiftNow")
            TglShiftNow = xSet.Tables("CekShiftNow").Rows(0).Item("TglShiftNow")

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub GetKodemember()
        If Not xSet.Tables("kodemember") Is Nothing Then xSet.Tables("kodemember").Clear()
        ExDb.ExecQuery(conn_string_local, "select kodemember from mtposmember where idorder=" & selected_idxRow, xSet, "kodemember")
        If xSet.Tables("kodemember").Rows.Count > 0 Then kodemember = xSet.Tables("kodemember").Rows(0)(0)
        xSet.Tables.Remove("kodemember")
    End Sub

    Public Sub QryHeaderNota()
        If Not xSet.Tables("printNota") Is Nothing Then xSet.Tables("printNota").Clear()
        SQLquery = "SELECT mpm.footertext, a.numOrder, a.subTotal, a.disc, b.totalPayment, b.totalChange, a.createDate, a.isVoid AS Void, c.Nama AS namaSPG,promodeskripsi,jenis_transaksi " &
                    "FROM mtpos AS a LEFT JOIN mtbayarpos AS b ON a.idOrder=b.idOrder LEFT JOIN mstaff c ON a.updateBy=c.idStaff, mpromomesg AS mpm "

        SQLquery += String.Format("WHERE a.idOrder={0} AND a.idCabang={1}", selected_idxRow, id_cabang)

        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "printNota")

        If Not xSet.Tables("printbayar") Is Nothing Then xSet.Tables("printbayar").Clear()
        SQLquery = String.Format("select `mt2`.`idOrder` AS `idOrder`,sum(`dt`.`amount` ) AS `nom` " &
            " ,mk.idKategoriPayment  from ((`dtbayarpos` `dt` join `mtbayarpos` `mt` " &
            " on((`dt`.`idPaymentOrder` = `mt`.`idPaymentOrder`))) join `mtpos` `mt2` " &
            " on((`mt`.`idOrder` = `mt2`.`idOrder`))) left join mpayment mp on dt.idpayment=mp.idPayment inner join mkategoripayment mk on mp.idKategoriPayment=mk.idKategoriPayment " &
            " where ((`mt2`.`isVoid` = 0 OR mt2.isVoid=1) and mt2.idOrder ={0}) group by `mt2`.`idOrder`,mk.idKategoriPayment ", selected_idxRow)

        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "printbayar")

        If Not xSet.Tables("cetakanke") Is Nothing Then xSet.Tables("cetakanke").Clear()
        SQLquery = "select ifnull(max(b.printOrder),0) as counter from mtprintlog a inner join dtprintlog b on a.idprintlog=b.idprintlog inner join mform c on a.idform=c.idform where c.namaform='POS' and a.idformno=" & selected_idxRow
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "cetakanke")
        If Not xSet.Tables("cetakinfopoint") Is Nothing Then xSet.Tables("cetakinfopoint").Clear()
        SQLquery = String.Format("select pointawal,pointakhir,kuponawal,kuponakhir from mtposmember where idorder={0}", selected_idxRow)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "cetakinfopoint")
    End Sub

    Public Function TestOnline() As Boolean
        Dim testcon As MySql.Data.MySqlClient.MySqlConnection
        Try
            testcon = New MySqlConnection(My.Settings.cloudconn)
            testcon.Open()
        Catch ex As Exception
            'MsgBox(ex.Message)
            Return False
        End Try
        testcon.Close()
        Return True
    End Function

    Public Sub SimpanCetakanKe(ByVal nmform As String, varcetke As Integer)
        Dim Zcon As MySqlConnection
        Dim vartr As MySql.Data.MySqlClient.MySqlTransaction
        Dim id_prinlog As Integer = 0
        Try
            Zcon = New MySqlConnection(My.Settings.db_conn_mysql)
            Dim command As New MySqlCommand()
            Zcon.Open()
            command.Connection = Zcon
            vartr = Zcon.BeginTransaction()
            command.Transaction = vartr

            SQLquery = "INSERT INTO mtprintlog(idform,idformno) " &
                        " select idform," & selected_idxRow & " from mform where namaform='POS'"

            command.CommandText = SQLquery
            command.ExecuteNonQuery()

            If Not xSet.Tables("getNewNum") Is Nothing Then xSet.Tables("getNewNum").Clear()
            SQLquery = "SELECT MAX(idprintlog) getNewNum FROM mtprintlog "

            command.CommandText = SQLquery
            ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
            id_prinlog = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
            xSet.Tables("getNewNum").Clear()

            SQLquery = String.Format("INSERT INTO dtprintlog(idprintlog,printOrder,printBy,printDate) " &
                        " VALUES ({0},{1},{2},NOW()) ", id_prinlog, varcetke, staff_id)


            command.CommandText = SQLquery
            command.ExecuteNonQuery()
            vartr.Commit()
            Zcon.Close()

        Catch ex As Exception
            vartr.Rollback()
            'MsgBox(ex.Message)
            msgboxErrorDev()
        End Try
    End Sub

    Public Sub Get_MembercardSettings()
        'get data reader and card from settings.ini
        Dim SavedKeyA, SavedKeyB As String

        DefReaderName = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "ReaderDefaultName", "")
        If DefReaderName = "" Then
            MyMiFareKey = New MiFareKey()
        Else
            SavedKeyA = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyA", "")
            SavedKeyB = FileSettingsHelper.ReadSettings(Application.StartupPath & "\setting.ini", "Reader and Card", "KeyB", "")

            If Not SavedKeyA = "" Then SavedKeyA = Encryptor.DecryptData(SavedKeyA)
            If Not SavedKeyB = "" Then SavedKeyB = Encryptor.DecryptData(SavedKeyB)

            MyMiFareKey = New MiFareKey(SavedKeyA, SavedKeyB)
        End If

    End Sub

    Public Sub filterbarang(idbarang As String, idkategori As String)
        Try
            SQLquery = "select namaBrg FROM mbarang mb
join mkategoribrg2 mk on mk.idKategori2=mb.idKategori2
WHERE idBrg IN (" & idbarang & ") and 
mk.namakategori2 in (" & idkategori & ")
order by price asc"
            If IsNothing(xSet.Tables("listfilter")) Then
                xSet.Tables.Add("listfilter")
            Else
                xSet.Tables("listfilter").Clear()
            End If
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "listfilter")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub filterbarangdankategori(idbarang As String, idkategori As String)
        Try
            SQLquery = "select namaBrg FROM mbarang mb
join mkategoribrg2 mk on mk.idKategori2=mb.idKategori2
WHERE idBrg IN (" & idbarang & ") or 
mk.namakategori2 in (" & idkategori & ")"
            If IsNothing(xSet.Tables("listfilter")) Then
                xSet.Tables.Add("listfilter")
            Else
                xSet.Tables("listfilter").Clear()
            End If
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "listfilter")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function kategoribarang(idbarang As String)
        Try
            SQLquery = "select idKategori2 FROM mbarang mb
WHERE idBrg = '" & idbarang & "'"
            Return ExDb.Scalar(SQLquery, My.Settings.localconn)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Public Function AES_Encrypt(ByVal input As String, ByVal pass As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = Security.Cryptography.CipherMode.ECB
            Dim DESEncrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(input)
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return encrypted
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Public Function AES_Decrypt(ByVal input As String, ByVal pass As String) As String
        If input = "" Then
            Return "null"
            Exit Function
        End If

        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim decrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
            Array.Copy(temp, 0, hash, 0, 16)
            Array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = Security.Cryptography.CipherMode.ECB
            Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
            Dim Buffer As Byte() = Convert.FromBase64String(input)
            decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return decrypted
        Catch ex As Exception
            Try
                pass = "januar"
                Dim hash(31) As Byte
                Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
                Array.Copy(temp, 0, hash, 0, 16)
                Array.Copy(temp, 0, hash, 15, 16)
                AES.Key = hash
                AES.Mode = Security.Cryptography.CipherMode.ECB
                Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
                Dim Buffer As Byte() = Convert.FromBase64String(input)
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
                Return decrypted
            Catch ex2 As Exception
                Return "error"
            End Try
        End Try

    End Function

    Private Sub ReaderStarter()
        Try
            If Not MyACR.ReaderEstablished Then
                MyACR.Establish(GlobalReaderContext)
            End If
            MyACR.ReaderName = DefReaderName
            MyACR.EasyConnection()
        Catch ex As RFID_Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function KeySetupCheck() As Boolean
        If DefReaderName = "" Then
            Return False
        End If
        If IsNothing(MyMiFareKey.KeyA) Then
            Return False
        End If

        Return True
    End Function
    Private ReadOnly bw As BackgroundWorker = New BackgroundWorker()
    Public Sub clearblockread()
        If KeySetupCheck() Then
            ReaderStarter()
            If MyACR.CardConnected Then
                MyACR.ClearLCD()
                MyACR.DisplayLCD(1, "    Welcome")
                MyACR.DisplayLCD(2, " Laritta Bakery")
                MyACR.Disconnect()
            End If
            'Threading.Thread.Sleep(2000)
            If bw.IsBusy Then bw.CancelAsync()
            bw.RunWorkerAsync()
        End If
    End Sub
    Public Function readsaldo(showsaldo As Boolean)
        'read saldo kartu
        Dim saldo As Double
        Dim fusedata As String
        Dim tempdata(1) As String

        inputPayment.checktopupcard()
        MyCard.BlockRead(5, 16)
        tempdata(0) = bw_RunWorkerCompleted().ToString
        tempdata(0) = Replace(tempdata(0), ChrW(144), "")
        inputPayment.checktopupcard()
        MyCard.BlockRead(6, 16)
        tempdata(1) = bw_RunWorkerCompleted().ToString
        tempdata(1) = Replace(tempdata(1), ChrW(144), "")
        fusedata = tempdata(0) & tempdata(1)
        If fusedata = "" Or fusedata = Nothing Or fusedata.Length < 24 Then
            fusedata = "0"
        Else
            fusedata = AES_Decrypt(fusedata, passwordenkripsi)
        End If

        If showsaldo = True Then
            ReaderStarter()
            If MyACR.CardConnected Then
                MyACR.Backlight(True)
                MyACR.ClearLCD()
                MyACR.DisplayLCD(1, "Saldo anda:")
                If CDbl(fusedata) = 0 Then
                    MyACR.DisplayLCD(2, "Rp. 0.-")
                Else
                    MyACR.DisplayLCD(2, "Rp. " & Format(CDbl(fusedata), "#,###") & ".-")
                End If
                MyACR.Disconnect()
            End If
        End If

        Return CDbl(fusedata)
    End Function




    Public Function readidmemeber()
        'read kode member
        Dim kodemb As String
        If inputPayment.checktopupcard() = True Then
            MyCard.BlockRead(4, 16)
            kodemb = Replace(bw_RunWorkerCompleted().ToString, ChrW(144), "")
        Else
            kodemb = ""
        End If

        Return kodemb
    End Function

    Private Function bw_RunWorkerCompleted()
        'done
        Dim TempKodeMember As String = ""
        For i = 0 To 16
            If MyCard.RecvBuff(i) = &H0 Then Continue For
            TempKodeMember = TempKodeMember + Convert.ToChar(MyCard.RecvBuff(i))
        Next
        MyCard.Disconnect()
        'kodemember.EditValue = Trim(TempKodeMember) 'Godspeed..
        Return Trim(TempKodeMember)
    End Function

    Public Function splitenkripsi(text As String)
        Dim arrayenkripsi(1) As String
        Dim ch As Char
        arrayenkripsi(0) = ""
        arrayenkripsi(1) = ""
        For i = 0 To text.Length - 1
            ch = text(i)
            If i >= 16 Then
                arrayenkripsi(1) = arrayenkripsi(1) & ch.ToString
            Else
                arrayenkripsi(0) = arrayenkripsi(0) & ch.ToString
            End If
        Next i
        Return arrayenkripsi
    End Function

    Public Function inputsaldo(saldo As String)
        Try
            ''saldo = 22000
            'passwordenkripsi = "J4anu$arGa4kA44La$ayLho0Sumpah"
            'passwordenkripsi = "januar"
            Dim hasilsplit(1) As String
            Dim hasilenkripsi As String = AES_Encrypt(saldo, passwordenkripsi)
            Array.Copy(splitenkripsi(hasilenkripsi), hasilsplit, 2)
            'ReaderStarter()
            If inputPayment.checktopupcard() Then
                MyCard.BlockWrite(5, hasilsplit(0))
            End If
            If inputPayment.checktopupcard() Then
                MyCard.BlockWrite(6, hasilsplit(1))
            End If
            readsaldo(True)
            Return True
        Catch ex As Exception
            readsaldo(True)
            MessageBox.Show(ex.Message)
            Return False

        End Try
    End Function

    Public Function checktopupcard()
        Get_MembercardSettings()
        'If Not MyMiFareKey.Stats Then Return

        If Not MyMiFareKey.Stats Then
            Return False
            Exit Function
        End If

        Dim state As String = ""

        Try
            state = "Reader"
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            'StatusMessage.Text = state & " is ready"

            'try to connect to card
            state = "Card"
            MyCard.Connect(GlobalReaderContext, DefReaderName)
            'StatusMessage.Text = state & " is ready"
        Catch ex As RFID_Exception
            'StatusMessage.Text = state & " not found"
        End Try

        Try

            '============================================================using sector 1 as hq, trailer=7, block=4,5,6
            'try to load DEFAULT keys to buffer
            state = "Load Default Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.DefaultKey)

            'try to authenticate with DEFAULT keys into trailer sector 2 (block7)
            state = "Auth"
            MyCard.Authentication(7, MiFareKey.KeyType.TypeA)

            'if success > do binarywrite | if no > skip

            'write secret keys to trailer sector
            MyCard.BinaryKeys_Write(1, MyMiFareKey.KeyA, MyMiFareKey.KeyB)
            '============================================================
        Catch ex As Exception
            'StatusMessage.Text = "Default key not accepted"
        End Try


        Try
            'try to load custom key B (to write purpose)
            state = "Load Custom Keys"
            MyCard.LoadKeys(MiFareKey.KeyType.TypeB, MyMiFareKey.KeyB)

            'try to authenticate with CUSTOM keys into block 4 where keys are going to be stored
            state = "Auth"
            MyCard.Authentication(4, MiFareKey.KeyType.TypeB)

            '============================================================
            'get uid
            state = "UID"
            CardUID = MyCard.GetUID()
            Return True
            'StatusMessage.Text = "Card is ready"
        Catch ex As Exception
            'StatusMessage.Text = "Card is not recognized"
        End Try
    End Function

    Public Function cekpernahpakailarittacash(membercode As String)
        Dim idtransaksiemoney As String = ""
        SQLquery = String.Format("SELECT id_trans_emoney FROM mtrans_emoney WHERE kode_member='{0}'", membercode)
        idtransaksiemoney = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
        If IsNothing(idtransaksiemoney) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function cekpernahbeliitem(kodecust As String, kodebarang As String)
        Dim idbarang As String = ""
        SQLquery = String.Format("SELECT pd.idBrg FROM mbst_posm pm JOIN mbst_posd pd ON pm.id_pos=pd.id_pos WHERE pm.idCust='{0}' AND pd.idBrg='{1}' AND pm.isVoid=0", kodecust, kodebarang)
        idbarang = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
        If IsNothing(idbarang) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function cekpernahdapatpromo(kodecust As String, kodebarang As String)
        Dim idbarang As String = ""
        SQLquery = String.Format("SELECT pd.idBrg FROM mbst_posm pm JOIN mbst_posd pd ON pm.id_pos=pd.id_pos WHERE pm.idCust='{0}' AND pd.idBrg='{1}' AND pm.isVoid=0", kodecust, kodebarang)
        idbarang = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
        If IsNothing(idbarang) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function checktopup(checkall As Boolean) As Boolean
        Try
            Dim syaratreturn As Boolean = False

            If checkall = True Then
                If listitemtopup.Columns.Count = 0 Then
                    listitemtopup.Columns.Add("idBrg")
                Else
                    listitemtopup.Clear()
                End If
            End If

            For i = 0 To FormPOSx.GridView1.RowCount - 1
                Dim kategori As String = kategoribrg(FormPOSx.GridView1.GetRowCellValue(i, "idBrg")).ToString.ToUpper
                If kategori = "TOP UP" Then
                    istopup = True
                    If checkall = True Then
                        listitemtopup.Rows.Add(FormPOSx.GridView1.GetRowCellValue(i, "idBrg"))
                    Else
                        syaratreturn = True
                    End If

                    If i = FormPOSx.GridView1.RowCount - 1 Then
                        syaratreturn = True
                    End If

                    If syaratreturn Then
                        Return True
                    End If

                End If
            Next i
            Return False
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Function

    Public Function checkvalidvouchertoptup() As Boolean
        'Dim syaratreturn As Boolean = False
        For i = 0 To FormPOSx.GridView1.RowCount - 1
            Dim kategori As String = kategoribrg(FormPOSx.GridView1.GetRowCellValue(i, "idBrg")).ToString.ToUpper
            If kategori = "TOP UP" Then
                If FormPOSx.GridView1.GetRowCellValue(i, "idBrg").ToString = xSet.Tables("datavoucher").Rows(0).Item("idBrg").ToString Then
                    Return True
                    Exit Function
                End If

            End If
        Next i
        Return False

    End Function


    Public Function kategoribrg(idbrg As String)
        Dim x As String = "SELECT mk.namaKategori2 FROM mkategoribrg2 mk 
JOIN mbarang mb ON mb.idKategori2=mk.idKategori2
WHERE mb.idBrg='" & idbrg & "'"
        Dim hasil As String = ExDb.Scalar(x, conn_string_local)
        Return hasil
    End Function

    Dim Zcon As MySqlConnection
    Dim conupdatecloud As New MySqlConnection
    Public Sub updatedataofflineemoney(syncJurnal As Boolean)
        Try
            If TestOnline() Then
                'koneksi ke cloud
                conupdatecloud = New MySqlConnection(My.Settings.cloudconn)
                Dim commandupdatecloud As New MySqlCommand
                Dim vartrupdatecloud As MySql.Data.MySqlClient.MySqlTransaction
                conupdatecloud.Open()
                commandupdatecloud.Connection = conupdatecloud
                vartrupdatecloud = conupdatecloud.BeginTransaction()

                'koneksi ke local

                Dim vartr As MySql.Data.MySqlClient.MySqlTransaction
                Zcon = New MySqlConnection(My.Settings.db_conn_mysql)
                Dim command As New MySqlCommand()
                Zcon.Open()
                command.Connection = Zcon
                vartr = Zcon.BeginTransaction()
                command.Transaction = vartr

                'cek jurnal offline
                SQLquery = "SELECT mp.idOrder,numOrder,dtjurnal,mp.updateBy,CONCAT(DATE_FORMAT(CURDATE(), 'SO/%y/%m/'), LPAD(numorder, 6, '0')) `noNota`,isJurnalNotaVoid FROM app_logoffline.mt_journal_offline mt JOIN mtpos mp ON mp.idOrder=mt.idOrder"
                If IsNothing(xSet.Tables("listjurnaloffline")) = True Then
                    xSet.Tables.Add("listjurnaloffline")
                Else
                    xSet.Tables("listjurnaloffline").Clear()
                End If
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listjurnaloffline")

                'update mtrans e-money
                SQLquery = "SELECT * FROM app_logoffline.mtrans_emoney mt"
                If IsNothing(xSet.Tables("listupdatemtrans")) = True Then
                    xSet.Tables.Add("listupdatemtrans")
                Else
                    xSet.Tables("listupdatemtrans").Clear()
                End If
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listupdatemtrans")

                If xSet.Tables("listupdatemtrans").Rows.Count = 0 And xSet.Tables("listjurnaloffline").Rows.Count = 0 Then
                    Exit Sub
                End If

                For i = 0 To xSet.Tables("listupdatemtrans").Rows.Count - 1
                    'update mtrans e-money
                    SQLquery = String.Format("INSERT INTO mtrans_emoney (idOrder, idCabang, tanggal_input, kode_member, isVoid, jenis_transaksi,nominal) VALUES ('{0}','{1}',now(),'{2}','0',1,{3})", xSet.Tables("listupdatemtrans").Rows(i).Item("idOrder"), xSet.Tables("listupdatemtrans").Rows(i).Item("idCabang"), xSet.Tables("listupdatemtrans").Rows(i).Item("kode_member"), xSet.Tables("listupdatemtrans").Rows(i).Item("nominal"))
                    commandupdatecloud.CommandText = SQLquery
                    commandupdatecloud.ExecuteNonQuery()

                    'update promo offline
                    If IsNothing(xSet.Tables("listpromooffline")) = True Then
                        xSet.Tables.Add("listpromooffline")
                    Else
                        xSet.Tables("listpromooffline").Clear()
                    End If
                    SQLquery = String.Format("SELECT * FROM app_logoffline.topup_offline where idOrder={0}", xSet.Tables("listupdatemtrans").Rows(i).Item("idOrder"))
                    ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listpromooffline")

                    For k = 0 To xSet.Tables("listpromooffline").Rows.Count - 1
                        SQLquery = String.Format("INSERT INTO dtrans_emoney_promo (id_trans_emoney, idpromo, isvoid) VALUES (LAST_INSERT_ID(),'{0}',0)", xSet.Tables("listpromoemoney").Rows(k).Item("idpromo"))
                        commandupdatecloud.CommandText = SQLquery
                        commandupdatecloud.ExecuteNonQuery()
                    Next k

                    'update dtrans e-money
                    'SQLquery = String.Format("INSERT INTO mbst_demoney (id_trans_emoney,idOrder, nominal, isVoid) VALUES (LAST_INSERT_ID(),'{0}',{1},'0')", xSet.Tables("listupdatedtrans").Rows(i).Item(0), xSet.Tables("listupdatedtrans").Rows(i).Item(1))
                    'commandupdatecloud.CommandText = SQLquery
                    'commandupdatecloud.ExecuteNonQuery()
                Next i


                'SQLquery = "SELECT * FROM app_logoffline.mbst_demoney md"
                'If IsNothing(xSet.Tables("listupdatedtrans")) = True Then
                '    xSet.Tables.Add("listupdatedtrans")
                'Else
                '    xSet.Tables("listupdatedtrans").Clear()
                'End If
                'ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listupdatedtrans")



                'update saldo topup offline
                If IsNothing(xSet.Tables("listtoptupoffline")) = True Then
                    xSet.Tables.Add("listtoptupoffline")
                Else
                    xSet.Tables("listtoptupoffline").Clear()
                End If

                SQLquery = "SELECT * FROM app_logoffline.topup_offline"
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listtoptupoffline")

                For i = 0 To xSet.Tables("listtoptupoffline").Rows.Count - 1
                    If IsNothing(xSet.Tables("listitemtransaksipos")) = True Then
                        xSet.Tables.Add("listitemtransaksipos")
                    Else
                        xSet.Tables("listitemtransaksipos").Clear()
                    End If

                    SQLquery = String.Format("SELECT mp.idOrder,subTotal-mp.disc `totalNominal`,quantity,idBrg,price,dp.totalPrice `totalPriceDetail`,date_format(createDate,'%Y%m%d%k%i%s') `createDate`,dp.disc `detaildisc`,totalQTY,CONCAT(DATE_FORMAT(CURDATE(), 'SO/%y/%m/'), LPAD(numorder, 6, '0')) NoNota FROM mtpos mp JOIN dtpos dp ON mp.idOrder=dp.idOrder WHERE mp.idOrder='{0}'", xSet.Tables("listtoptupoffline").Rows(i).Item("idOrder"))
                    ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listitemtransaksipos")

                    'insert ke mbst_posm
                    SQLquery = String.Format("SELECT idCust FROM mcustomer WHERE kode_member='{0}'", xSet.Tables("listtoptupoffline").Rows(i).Item("kode_member"))
                    Dim kodecustsync As String = ExDb.Scalar(SQLquery, My.Settings.cloudconn)

                    SQLquery = String.Format("INSERT INTO mbst_posm(id_pos, idCabang, tgl_pos, nomor_nota, idCust, total_qty, total_price, isVoid, tgl_input, point_gained, stamp_gained, use_promo, use_gift)
	VALUES ({0}, {1}, {2}, '{3}', {4}, {5}, {6}, 0, now(), 0, 0, 0, 0)",
xSet.Tables("listtoptupoffline").Rows(i).Item("idOrder"),
                                             My.Settings.idcabang,
                                             xSet.Tables("listitemtransaksipos").Rows(0).Item("createDate"),
                         xSet.Tables("listitemtransaksipos").Rows(0).Item("NoNota"),
                    kodecustsync,
xSet.Tables("listitemtransaksipos").Rows(0).Item("totalQTY"),
                                             xSet.Tables("listitemtransaksipos").Rows(0).Item("totalNominal")
)
                    commandupdatecloud.CommandText = SQLquery
                    commandupdatecloud.ExecuteNonQuery()

                    'insert ke mbst_posd
                    For j = 0 To xSet.Tables("listitemtransaksipos").Rows.Count - 1
                        SQLquery = String.Format("INSERT INTO mbst_posd(id_pos, idCabang, idBrg, quantity, price, disc, totalPrice) VALUES ({0},{1},{2},{3},{4},{5},{6})",
                           xSet.Tables("listtoptupoffline").Rows(i).Item("idOrder"),
                                     My.Settings.idcabang,
                                              xSet.Tables("listitemtransaksipos").Rows(j).Item("idBrg"),
                                              xSet.Tables("listitemtransaksipos").Rows(j).Item("quantity"),
                                                 xSet.Tables("listitemtransaksipos").Rows(j).Item("price"),
                                                 xSet.Tables("listitemtransaksipos").Rows(j).Item("detaildisc"),
                                                 xSet.Tables("listitemtransaksipos").Rows(j).Item("totalPriceDetail")
                        )
                        commandupdatecloud.CommandText = SQLquery
                        commandupdatecloud.ExecuteNonQuery()
                    Next j
                Next i

                'update saldo e-money di cloud (mcustomer)
                SQLquery = "SELECT * FROM app_logoffline.saldo_offline so where kode_member<>'" & kodemember & "'"
                If IsNothing(xSet.Tables("listupdatesaldo")) = True Then
                    xSet.Tables.Add("listupdatesaldo")
                Else
                    xSet.Tables("listupdatesaldo").Clear()
                End If
                ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listupdatesaldo")
                For i = 0 To xSet.Tables("listupdatesaldo").Rows.Count - 1
                    SQLquery = String.Format("UPDATE mcustomer m SET m.emoney={0} WHERE m.kode_member='{1}'", xSet.Tables("listupdatesaldo").Rows(i).Item(1), xSet.Tables("listupdatesaldo").Rows(i).Item(0))
                    commandupdatecloud.CommandText = SQLquery
                    commandupdatecloud.ExecuteNonQuery()
                Next i

                'update point otomatis

                SQLquery = "select a.numOrder Nota,date(a.createDate) TglNota,b.namaCust Pelanggan,b.kodekartu Kartu,b.idCust,a.idOrder,'false' UsingManual from mtpos a inner join app_logoffline.mlogaddpoint b on a.idOrder =b.idOrder and a.isvoid=0"

                If IsNothing(xSet.Tables("PointOffline")) = False Then
                    xSet.Tables("PointOffline").Clear()
                Else
                    xSet.Tables.Add("PointOffline")
                End If

                ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "PointOffline")

                'proses update otomatis
                Dim doneproses As Boolean
                Dim rowpos As Integer = 1
                Dim idcutomersync As String = ""
                For Each dtrow As DataRow In xSet.Tables("PointOffline").Rows
                    SQLquery = String.Format("SELECT mc.idCust FROM mcustomer mc WHERE mc.kode_member='{0}'", dtrow("Kartu"))
                    idcutomersync = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                    If CekUsingManual(CInt(My.Settings.idcabang), dtrow("idOrder")) Then
                        SQLquery = String.Format("delete from app_logoffline.mlogaddpoint where idOrder={0} and kodekartu='{1}'", dtrow("idOrder"), dtrow("Kartu"))
                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()
                    Else
                        doneproses = TambahPointCloudSync(dtrow("idOrder").ToString, idcutomersync)
                        If doneproses = False Then
                            MsgBox("Tambah Point pada baris ke " & rowpos.ToString & " Gagal !")
                            Exit Sub
                        End If
                        SQLquery = String.Format("delete from app_logoffline.mlogaddpoint where idOrder={0} and kodekartu={1}", dtrow("idOrder"), dtrow("Kartu"))
                        command.CommandText = SQLquery
                        command.ExecuteNonQuery()

                    End If
                    rowpos += 1
                Next dtrow
                If syncJurnal Then
                    'upload jurnal offline
                    For k = 0 To xSet.Tables("listjurnaloffline").Rows.Count - 1
                        Dim keterangan As String

                        If xSet.Tables("listjurnaloffline").Rows(k).Item("isJurnalNotaVoid").ToString = "0" Then
                            keterangan = "Penjualan Retail"
                        Else
                            keterangan = "Void Penjualan Retail Nomor: " & xSet.Tables("listjurnaloffline").Rows(k).Item("noNota").ToString
                        End If
                        SQLquery = String.Format("CALL {4}.insert_journal(DATE(now()),'12','{0}','{1}' ,'{5}',NULL,'{2}',now(),'{3}')", xSet.Tables("listjurnaloffline").Rows(k).Item("idOrder"), xSet.Tables("listjurnaloffline").Rows(k).Item("numOrder"), xSet.Tables("listjurnaloffline").Rows(k).Item("updateBy"), xSet.Tables("listjurnaloffline").Rows(k).Item("dtjurnal"), My.Settings.db_finance, keterangan)
                        commandupdatecloud.CommandText = SQLquery
                        commandupdatecloud.ExecuteNonQuery()
                    Next k
                End If
                vartrupdatecloud.Commit()

                    'delete data mtrans
                    SQLquery = "DELETE FROM app_logoffline.mtrans_emoney"
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()

                    'delete data dtrans
                    'SQLquery = "DELETE FROM app_logoffline.mbst_demoney"
                    'command.CommandText = SQLquery
                    'command.ExecuteNonQuery()

                    'delete saldo offline
                    SQLquery = "DELETE FROM app_logoffline.saldo_offline"
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()

                    'delete topup offline
                    SQLquery = "DELETE FROM app_logoffline.topup_offline"
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()

                    'delete promo offline
                    SQLquery = "DELETE FROM app_logoffline.dtrans_emoney_promo"
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()

                'delete jurnal offline
                If syncJurnal Then
                    SQLquery = "DELETE FROM app_logoffline.mt_journal_offline"
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()
                End If


                vartr.Commit()
                End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Zcon.Dispose()
            conupdatecloud.Dispose()

        End Try
    End Sub

    Private Function CekUsingManual(ByVal idCab As Integer, ByVal idpos As Integer) As Boolean
        Dim retval As Integer
        retval = ExDb.Scalar("select count(id_pos) as tot  from mbst_posm where id_pos=" & idpos & " and idCabang=" & idCab, My.Settings.cloudconn)
        If retval > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Function TambahPointCloudSync(ByVal v_idOrder As String, ByVal v_idCust As String) As Boolean
        Dim XConCloud As MySqlConnection
        Dim vartrCloud As MySql.Data.MySqlClient.MySqlTransaction
        Dim adaTranCloud As Boolean = False
        XConCloud = New MySqlConnection(My.Settings.cloudconn)
        Dim commandCloud As New MySqlCommand()
        'Dim command2 As New MySqlCommand

        If Not xSet.Tables("This_mtpos") Is Nothing Then xSet.Tables("This_mtpos").Clear()
        SQLquery = String.Format("SELECT idOrder ID, totalQTY totalqtt, disc Diskon, subTotal-disc TotalTrans, createDate TglTrans, isVoid,CONCAT(DATE_FORMAT(CURDATE(), 'SO/%y/%m/'), LPAD(numorder, 6, '0')) NoNota FROM mtpos WHERE idOrder={0} ", v_idOrder)
        ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "This_mtpos")
        If xSet.Tables("This_mtpos").Rows.Count = 0 Then
            Return False
        End If

        If Not xSet.Tables("This_dtpos") Is Nothing Then xSet.Tables("This_dtpos").Clear()
        SQLquery = String.Format("SELECT a.idBrg ID, b.namaBrg Nama, a.quantity Jml, a.price Hrg, a.disc Disc, a.totalPrice Total FROM dtpos a INNER JOIN mbarang b ON a.idBrg=b.idBrg AND a.status=1 WHERE idOrder={0};", v_idOrder)
        ExDb.ExecQuery(My.Settings.db_conn_mysql, SQLquery, xSet, "This_dtpos")
        Try
            XConCloud.Open()
            commandCloud.Connection = XConCloud
            vartrCloud = XConCloud.BeginTransaction()
            commandCloud.Transaction = vartrCloud
            adaTranCloud = True

            SQLquery = String.Format("CALL InsertTransOutlet ('{0}', {1}, {2}, '{3}', '{4}', {5}, {6}, {7}, '",
                                     v_idCust,
                                     id_cabang,
                                     v_idOrder,
                                     Format(xSet.Tables("This_mtpos").Rows(0).Item("TglTrans"), "yyyy-MM-dd HH:mm:ss"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("NoNota"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("totalqtt"),
                                     xSet.Tables("This_mtpos").Rows(0).Item("TotalTrans"),
                                     staff_id)
            For Each drow As DataRow In xSet.Tables("This_dtpos").Rows
                SQLquery += String.Format("({0}, {1}, {2}, {3}, {4}, {5}, {6}), ", v_idOrder, id_cabang, drow.Item("ID"), drow.Item("Jml"), drow.Item("Hrg"), drow.Item("Disc"), drow.Item("Total"))
            Next
            SQLquery = Mid(SQLquery, 1, SQLquery.Length - 2) & "'); "
            'ExDb.ExecData(1, SQLquery)
            commandCloud.CommandText = SQLquery
            commandCloud.ExecuteNonQuery()
            SQLquery = ""
            vartrCloud.Commit()
            XConCloud.Close()
            adaTranCloud = False
            Return True
        Catch exCloud As Exception
            If adaTranCloud = True Then
                'vartrCloud.Rollback()
                MsgBox(exCloud.Message)
            End If
            Return False
        End Try
    End Function

    Public Function getAccountChild(ByVal accParent As String, ByVal dtAccSource As DataTable) As DataTable
        Dim dtAkunTemp As New DataTable
        dtAkunTemp = dtAccSource.Clone

        For x = 0 To accParent.Split(",").Length - 1
            Dim acc As String = If(accParent.IndexOf(",") <> -1, accParent.Substring(0, accParent.IndexOf(",")), accParent)
            acc.Replace(" ", "")
            getDetail(acc, dtAccSource, dtAkunTemp)
            accParent = accParent.Remove(0, accParent.IndexOf(",") + 1)
        Next

        Return dtAkunTemp
    End Function

    Function getDetail(ByVal accParent As String, ByVal dtAccSource As DataTable, ByVal dtAkunTemp As DataTable) As DataTable
        If dtAccSource.Select("accParent = " & accParent).Length = 0 Then
            Dim rows() As DataRow = dtAccSource.Select("accCode = " & accParent)
            Dim row As DataRow
            For Each row In rows
                dtAkunTemp.ImportRow(row)
            Next
            Return dtAkunTemp
        Else
            Dim rows() As DataRow = dtAccSource.Select("accParent = " & accParent)
            Dim row As DataRow
            For Each row In rows
                getDetail(row("accCode"), dtAccSource, dtAkunTemp)
            Next
        End If
    End Function
    <Extension()>
    Public Function repCommaSep(ByVal m_value As String) As String 'Replace Comma Separator
        If InStr(m_value, ",") <> 0 Then
            Return Replace(m_value, ",", ".")
        Else
            Return m_value
        End If
    End Function
    <Extension()>
    Public Function repPointSep(ByVal m_value As String) As Decimal 'Replace Point Separator
        If InStr(m_value, ".") <> 0 Then
            Return Replace(m_value, ".", ",")
        Else
            Return m_value
        End If
    End Function
End Module