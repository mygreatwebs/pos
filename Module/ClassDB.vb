﻿Imports MySql.Data.MySqlClient

Public Class ClassDB

    Dim xCom As MySqlCommand
    Dim xAdap As MySqlDataAdapter
    Dim xAdap2 As MySqlDataAdapter
    Dim xTrans As MySqlTransaction


    '****************************************************************************************
    '   Name        : ExecQuery
    '   Parameter   : SQL           (String)
    '                 xDataSet      (DataSet)
    '                 xTableName    (String)
    '   Function    : Execute SQL Languange
    '****************************************************************************************
    Public Function Scalar(ByVal x As String, ByVal constr As String)
        'untuk select single data
        Try
            Using xCon = New MySqlConnection(constr)
                xCon.Open()
                Dim a As String
                Dim command As New MySqlCommand(x, xCon)
                If IsDBNull(command.ExecuteScalar()) = True Then
                    a = ""
                Else
                    a = command.ExecuteScalar()
                End If
                xCon.Close()
                Return a
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Public Function ExecQuery2(ByVal connection_string As String, ByVal pSQL As String, ByVal pDataSet As DataSet, ByVal pTableName As String, ByVal curridx As Integer, ByVal pagesize As Integer) As DataSet
        Using xCon = New MySqlConnection(connection_string)
            xAdap = New MySqlDataAdapter(pSQL, xCon)
            xAdap.Fill(pDataSet, curridx, pagesize, pTableName)
            xAdap.Dispose()
        End Using
        sp_Query = ""
        param_Query = ""
        SQLquery = ""

        Return pDataSet
    End Function

    Public Function ExecQuery(ByVal connection_string As String, ByVal pSQL As String, ByVal pDataSet As DataSet, ByVal pTableName As String) As DataSet
        Using xCon = New MySqlConnection(connection_string)
            xAdap = New MySqlDataAdapter(pSQL, xCon)
            xAdap.SelectCommand.CommandTimeout = 0
            xAdap.Fill(pDataSet, pTableName)
            xAdap.Dispose()
        End Using
        sp_Query = ""
        param_Query = ""
        SQLquery = ""

        Return pDataSet
    End Function

    '****************************************************************************************
    '   Name        : ExecData
    '   Parameter   : SQL           (String)
    '   Function    : Execute SQL Languange For Inser, Update, Delete
    '****************************************************************************************

    Public Sub ExecData(ByVal pSQL As String, ByVal connection_string As String)
        Using xCon = New MySqlConnection(connection_string)
            Try
                xCom = New MySqlCommand(pSQL, xCon)
                xCon.Open()
                xTrans = xCon.BeginTransaction
                xCom.Transaction = xTrans
                xCom.ExecuteNonQuery()
                xTrans.Commit()
                xCon.Close()
                xCom.Dispose()
            Catch ex As Exception
                xTrans.Rollback()
                MsgBox(String.Format("Error On : {0}{1}{2}", ex.Source, Chr(13), ex.Message), MessageBoxButtons.OK, MessageBoxIcon.Error)
                Throw ex
            Finally
                xCon.Close()
            End Try
        End Using
        sp_Query = ""
        param_Query = ""
        SQLquery = ""
    End Sub

    Public Function ExecQueryWithTransaction(ByVal mcommand As MySqlCommand, ByVal pSQL As String, ByVal pDataSet As DataSet, ByVal pTableName As String) As DataSet
        Try
            'Using xCon = New MySqlConnection(connection_string)
            'xadap2.SelectCommand.Transaction = adtran
            xadap2 = New MySqlDataAdapter(mcommand)


            xadap2.SelectCommand.CommandTimeout = 0

            xadap2.Fill(pDataSet, pTableName)
            xadap2.Dispose()
            'End Using
            sp_Query = ""
            param_Query = ""
            SQLquery = ""

            Return pDataSet
        Catch ex As Exception
            If ex.Message = "Unable to connect to any of the specified MySQL hosts." Then
                msgboxInformation("Tidak dapat terhubung dengan server utama. Masuk ke mode offline.")
            Else
                MsgBox(String.Format("Error On : {0}{1}{2}{1}{3}", ex.Source, Chr(13), ex.Message, pSQL), MessageBoxButtons.OK, MessageBoxIcon.Error)
                msgboxErrorDev()
            End If
            Return Nothing
        End Try
    End Function
End Class
