﻿Public Class modalForm

    Private Sub modalForm_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        TextEdit1.Focus()
    End Sub

    Private Sub modalForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        TextEdit1.Text = 0
    End Sub

    Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton1.Click
        If TextEdit1.EditValue > 0 Then
            start_modal = TextEdit1.EditValue

            Try
                If Not NextShiftChecker() Then Throw New Exception
                shiftLogin = shiftNow
                TglShiftLogin = TglShiftNow

                SQLquery = String.Format("INSERT INTO mtswitchuser(idCabang, idStaff, shiftKe, startTime, endtime, firstBalance) " & _
                                       "VALUES({0},{1},{2},'{3}','00:00:00',{4})",
                                        id_cabang, staff_id, shiftLogin, Format(TglShiftLogin, "yyyy-MM-dd HH:mm:ss"), start_modal)

                If Not conn_string_local = My.Settings.db_conn_mysql Then
                    ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                End If
                ExDb.ExecData(SQLquery, conn_string_local)
            Catch ex As Exception
                msgboxErrorDev()
            Finally
                Close()
            End Try
        End If
    End Sub

    Private Sub TextEdit1_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles TextEdit1.Enter
        virNumpad.ShowDialog()
        TextEdit1.Text = qttInteger
        SimpleButton1.Focus()
    End Sub

    Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SimpleButton2.Click
        start_modal = 0
        Close()
    End Sub
End Class