﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class topup_confirmation
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.kodemembertxt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.idbarang = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.topup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jumlah = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Harga = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Total = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.nominaltopup = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Totalnominalcol = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.saldoawal = New DevExpress.XtraEditors.TextEdit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.kodemembertxt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.saldoawal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer1"
        Me.ShapeContainer2.Size = New System.Drawing.Size(594, 394)
        Me.ShapeContainer2.TabIndex = 6
        Me.ShapeContainer2.TabStop = False
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butOK.Location = New System.Drawing.Point(730, 430)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(114, 70)
        Me.butOK.TabIndex = 29
        Me.butOK.Text = "Ya"
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDelete.Location = New System.Drawing.Point(610, 430)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(114, 70)
        Me.butDelete.TabIndex = 28
        Me.butDelete.Text = "Batal"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(465, 36)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.ReadOnly = True
        Me.TextEdit2.Size = New System.Drawing.Size(372, 30)
        Me.TextEdit2.TabIndex = 27
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Location = New System.Drawing.Point(465, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(93, 18)
        Me.LabelControl1.TabIndex = 26
        Me.LabelControl1.Text = "Pelanggan"
        '
        'kodemembertxt
        '
        Me.kodemembertxt.Location = New System.Drawing.Point(12, 36)
        Me.kodemembertxt.Name = "kodemembertxt"
        Me.kodemembertxt.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kodemembertxt.Properties.Appearance.Options.UseFont = True
        Me.kodemembertxt.Properties.MaxLength = 12
        Me.kodemembertxt.Properties.ReadOnly = True
        Me.kodemembertxt.Size = New System.Drawing.Size(372, 30)
        Me.kodemembertxt.TabIndex = 25
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(122, 18)
        Me.LabelControl2.TabIndex = 24
        Me.LabelControl2.Text = "Kode Member"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Size = New System.Drawing.Size(594, 394)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(465, 72)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(372, 78)
        Me.MemoEdit1.TabIndex = 28
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.GridControl1)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 156)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(832, 268)
        Me.GroupControl2.TabIndex = 30
        Me.GroupControl2.Text = "List Top up"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Margin = New System.Windows.Forms.Padding(4)
        Me.GridControl1.Location = New System.Drawing.Point(2, 25)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Margin = New System.Windows.Forms.Padding(4)
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(828, 241)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.idbarang, Me.topup, Me.jumlah, Me.Harga, Me.Total, Me.nominaltopup, Me.Totalnominalcol})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowColumnResizing = False
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsFind.AllowFindPanel = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'idbarang
        '
        Me.idbarang.Caption = "idbarang"
        Me.idbarang.FieldName = "idbrg"
        Me.idbarang.Name = "idbarang"
        '
        'topup
        '
        Me.topup.Caption = "Top up"
        Me.topup.FieldName = "Top up"
        Me.topup.Name = "topup"
        Me.topup.Visible = True
        Me.topup.VisibleIndex = 0
        Me.topup.Width = 160
        '
        'jumlah
        '
        Me.jumlah.Caption = "Jumlah"
        Me.jumlah.FieldName = "Jumlah"
        Me.jumlah.Name = "jumlah"
        Me.jumlah.Visible = True
        Me.jumlah.VisibleIndex = 1
        Me.jumlah.Width = 42
        '
        'Harga
        '
        Me.Harga.Caption = "Harga"
        Me.Harga.DisplayFormat.FormatString = "n"
        Me.Harga.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Harga.FieldName = "Harga"
        Me.Harga.Name = "Harga"
        Me.Harga.Visible = True
        Me.Harga.VisibleIndex = 2
        Me.Harga.Width = 147
        '
        'Total
        '
        Me.Total.Caption = "Total"
        Me.Total.DisplayFormat.FormatString = "n2"
        Me.Total.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Total.FieldName = "Total"
        Me.Total.Name = "Total"
        Me.Total.Visible = True
        Me.Total.VisibleIndex = 3
        Me.Total.Width = 147
        '
        'nominaltopup
        '
        Me.nominaltopup.Caption = "Nominal Top up"
        Me.nominaltopup.DisplayFormat.FormatString = "n2"
        Me.nominaltopup.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.nominaltopup.FieldName = "Nominal Top up"
        Me.nominaltopup.Name = "nominaltopup"
        Me.nominaltopup.Visible = True
        Me.nominaltopup.VisibleIndex = 4
        Me.nominaltopup.Width = 147
        '
        'Totalnominalcol
        '
        Me.Totalnominalcol.Caption = "Total Top up"
        Me.Totalnominalcol.DisplayFormat.FormatString = "n2"
        Me.Totalnominalcol.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.Totalnominalcol.FieldName = "Total Top up"
        Me.Totalnominalcol.Name = "Totalnominalcol"
        Me.Totalnominalcol.Visible = True
        Me.Totalnominalcol.VisibleIndex = 5
        Me.Totalnominalcol.Width = 167
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Location = New System.Drawing.Point(12, 73)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(96, 18)
        Me.LabelControl3.TabIndex = 31
        Me.LabelControl3.Text = "Saldo Awal"
        '
        'saldoawal
        '
        Me.saldoawal.EditValue = ""
        Me.saldoawal.Location = New System.Drawing.Point(12, 97)
        Me.saldoawal.Name = "saldoawal"
        Me.saldoawal.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.saldoawal.Properties.Appearance.Options.UseFont = True
        Me.saldoawal.Properties.DisplayFormat.FormatString = "n2"
        Me.saldoawal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.saldoawal.Properties.EditFormat.FormatString = "n2"
        Me.saldoawal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.saldoawal.Properties.Mask.EditMask = "n"
        Me.saldoawal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.saldoawal.Properties.MaxLength = 12
        Me.saldoawal.Properties.ReadOnly = True
        Me.saldoawal.Size = New System.Drawing.Size(372, 30)
        Me.saldoawal.TabIndex = 32
        '
        'topup_confirmation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 510)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.saldoawal)
        Me.Controls.Add(Me.MemoEdit1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.butOK)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.butDelete)
        Me.Controls.Add(Me.kodemembertxt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "topup_confirmation"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Top up Confirmation"
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.kodemembertxt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.saldoawal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents ShapeContainer2 As PowerPacks.ShapeContainer
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents kodemembertxt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Private WithEvents ShapeContainer1 As PowerPacks.ShapeContainer
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents saldoawal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents idbarang As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents topup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jumlah As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Harga As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Total As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents nominaltopup As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Totalnominalcol As DevExpress.XtraGrid.Columns.GridColumn
End Class
