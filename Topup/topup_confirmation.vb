﻿Imports Laritta_POS.RFID_Tools
Public Class topup_confirmation

    Private Sub topup_confirmation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bataltopup = False
        GridControl1.DataSource = xSet.Tables("listopup")
        kodemembertxt.Text = membertopup
    End Sub

    Private Sub kodemember_EditValueChanged(sender As Object, e As EventArgs) Handles kodemembertxt.EditValueChanged
        MemoEdit1.Text = ""
        TextEdit2.Text = ""
        idCust = 0
        saldosebelum = 0
        Try
            SQLquery = String.Format("SELECT a.idCust, b.namaKategoriCust, a.id_tipemembership, c.nama_tipemembership, c.level_tipemembership, a.namaCust, BirthDate, homeAddress, d.nama_kota, Phone, PhoneMobile, Pekerjaan, kode_kartu, kode_kartuUID, " &
                                                 "emoney, points, stamp, sum_trans_outlet, sum_trans_order, sum_buy_outlet, sum_buy_order, a.notes, a.joinmembership_date,0 jumlahKupon " &
                                                 "FROM mcustomer a INNER JOIN mkategoricust b ON a.idKategoriCust=b.idKategoriCust INNER JOIN mbsm_tipemembership c ON a.id_tipemembership=c.id_tipemembership INNER JOIN mbsm_lockota d ON a.id_kota=d.id_kota " &
                                                 "LEFT JOIN app_kupon.bonusKupon e on a.idCust=e.idCust WHERE a.kode_member='{0}'", kodemembertxt.Text.Substring(0, 12))

            If IsNothing(xSet.Tables("infomember")) = False Then xSet.Tables("infomember").Clear()

            saldokartu = readsaldo(True)
            saldosebelum = saldokartu
            saldoawal.EditValue = saldokartu

            ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "infomember")

            If xSet.Tables("infomember").Rows.Count = 0 Then
                TextEdit2.Text = "Data Tidak Ditemukan"
            Else
                idCust = xSet.Tables("infomember").Rows(0)("idCust")
                TextEdit2.Text = xSet.Tables("infomember").Rows(0)("namaCust")
                namapemilikkartu = xSet.Tables("infomember").Rows(0)("namaCust")
                MemoEdit1.Text = xSet.Tables("infomember").Rows(0)("HomeAddress")
            End If


            'total top up
            Dim totaltopup As Double = 0
            nominalhargatopup = 0
            For i = 0 To GridView1.RowCount - 1
                totaltopup = totaltopup + GridView1.GetRowCellValue(i, "Total Top up")
                nominalhargatopup = nominalhargatopup + CDbl(GridView1.GetRowCellValue(i, "Total"))
            Next i

            'cek batas saldo
            If saldokartu + totaltopup > 1000000 Then
                bataltopup = True
                MessageBox.Show("Saldo sudah melebihi batas yang ditentukan!", "Top Up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.Close()
                Exit Sub
            End If

            ReaderStarter()
            MyACR.Backlight(True)
            MyACR.ClearLCD()
            MyACR.DisplayLCD(1, "Saldo anda:")
            If saldokartu = 0 Then
                MyACR.DisplayLCD(2, "Rp. 0,-")
            Else
                MyACR.DisplayLCD(2, "Rp. " & Format(saldokartu, "#,###") & ".-")
            End If

            MyACR.Disconnect()
        Catch ex As Exception
            If ex.HResult = -2147467259 Then
                MsgBox(ex.Message)
                TextEdit2.Text = "Koneksi Offline"
            Else
                MsgBox(ex.Message)
            End If
        End Try
    End Sub
    Private Sub ReaderStarter()
        Try
            If Not MyACR.ReaderEstablished Then
                MyACR.Establish(GlobalReaderContext)
            End If
            MyACR.ReaderName = DefReaderName
            MyACR.EasyConnection()
        Catch ex As RFID_Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        Try
            Dim result As Integer = MessageBox.Show("Apakah anda yakin akan membatalkan proses top up?", "Top up confirmation", MessageBoxButtons.YesNo)
            If result = DialogResult.Yes Then
                bataltopup = True
                Me.Close()
            ElseIf result = DialogResult.No Then
                bataltopup = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Try
            Dim result As Integer = MessageBox.Show("Apakah anda yakin akan melakukan proses top up?" & vbCrLf & "Periksa kembali data Top up sebelum melanjutakan proses!", "Top up confirmation", MessageBoxButtons.YesNo)
            If result = DialogResult.No Then
                Exit Sub
            End If

            'verifikasi kartu apakah sama dengan awal atau gak
            If kodemember <> readidmemeber().ToString Then
                MessageBox.Show("Kode member tidak sama dengan kode member yang sebelumnya!", "Top up Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                'Exit Sub
            End If

            Dim jumlahtopup As Double = 0
            For i = 0 To xSet.Tables("listopup").Rows.Count - 1
                jumlahtopup = jumlahtopup + CDbl(xSet.Tables("listopup").Rows(i).Item("Total Top up"))
            Next i
            totaltopup = jumlahtopup
            jumlahtopup = jumlahtopup + saldokartu
            'jumlahtopup = 0
            If jumlahtopup > 1000000 Then
                MessageBox.Show("Terjadi kesalahan pada Top up ini" & vbCrLf & "Silahkan melakukan top up ulang", "Top up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If inputsaldo(jumlahtopup.ToString) Then
                If CDbl(readsaldo(False)) = jumlahtopup Then
                    saldosesudah = jumlahtopup
                    MessageBox.Show("Top up berhasil dilakukan!", "Top up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Close()
                Else
                    MessageBox.Show("Top up gagal dilakukan!" & vbCrLf & "Silahkan melakukan top up ulang", "Top up confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            Else
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class