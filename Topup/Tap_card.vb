﻿Imports System.ComponentModel
Imports System.Text
Imports Laritta_POS.RFID_Tools
Imports DevExpress.Utils
Partial Public Class tap_card
    Inherits DevExpress.XtraEditors.XtraForm
    Shared Sub New()
        DevExpress.UserSkins.BonusSkins.Register()
        DevExpress.Skins.SkinManager.EnableFormSkins()
    End Sub

    Public Sub New()
        InitializeComponent()
    End Sub

    Private ReadOnly bw As BackgroundWorker = New BackgroundWorker()
    Public CurrentMember As Integer
    Public CurrentKodeKartuMember As String
    Public kodemember As String = ""
    Dim MSCount As Integer
    Dim ReaderIsBusy As Boolean
    Dim CardUID As String
    Public oldkodemember As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'RFID
        'Label1.Text = "Version V." & FileVersionInfo.GetVersionInfo(Application.ExecutablePath).FileVersion
        'Label1.Location = New Point(Me.Width - Me.Width + 20, Me.Height - 100)

        ' Timer1.Start()


        'loginForm.ShowDialog()
        Get_MembercardSettings()

            bw.WorkerSupportsCancellation = True
            bw.WorkerReportsProgress = True
            AddHandler bw.DoWork, AddressOf bw_DoWork
            AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
            AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged


        Try
            If Not KeySetupCheck() Then
                CardProgressStatus.Caption = "Membercard settings hasn't defined!"
                Return
            End If

            ReaderStarter()
            MyACR.Backlight(True)
            MyACR.DisplayLCD(1, "    Welcome")
            MyACR.DisplayLCD(2, " Laritta Bakery")
            MyACR.Disconnect()

            bw.RunWorkerAsync()

        Catch ex As Exception
            CardProgressStatus.Caption = "Reader not found!"
        End Try
        PictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch


    End Sub



    Private Function KeySetupCheck() As Boolean
        If DefReaderName = "" Then
            Return False
        End If
        If IsNothing(MyMiFareKey.KeyA) Then
            Return False
        End If

        Return True
    End Function

    Private Sub ReaderStarter()
        Try
            If Not MyACR.ReaderEstablished Then
                MyACR.Establish(GlobalReaderContext)
            End If
            MyACR.ReaderName = DefReaderName
            MyACR.EasyConnection()
        Catch ex As RFID_Exception
            CardProgressStatus.Caption = "Error.." 'OMFG?!
        End Try
    End Sub


#Region "Asynchronous Background Worker"

    Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
        On Error Resume Next
        MSCount = 10

        Do While CardProgressBar.EditValue <> 100
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
            If bw.CancellationPending Or ReaderIsBusy Then
                e.Cancel = True
                Exit Do
            End If

            System.Threading.Thread.Sleep(100)
            bw.ReportProgress(0)

            If MSCount < 10 Then MSCount += 1

            'check if the reader available
            If Not MyCard.ReaderEstablished Then MyCard.Establish(GlobalReaderContext)
            If Not MyCard.ReaderEstablished Then
                SetLabelText_ThreadSafe(CardProgressStatus, "Reader not found")             'Cant found any reader
                MSCount = 0
                Continue Do
            End If
            bw.ReportProgress(1)
            SetLabelText_ThreadSafe(CardProgressStatus, "Waiting..")                        'Successfully connected to reader

            'check is any available card
            MyCard.Connect(GlobalReaderContext, DefReaderName)
            If Not MyCard.CardConnected Then
                Continue Do
            End If
            'the shit is coming
            bw.ReportProgress(30)
            SetLabelText_ThreadSafe(CardProgressStatus, "Authenticating..")                 'Successfully connected to card

            'try to authenticate card
            MyCard.LoadKeys(MiFareKey.KeyType.TypeA, MyMiFareKey.KeyA)
            If Not MyCard.ReturnCode = 0 Then
                'shit happen
                SetLabelText_ThreadSafe(CardProgressStatus, "Load failed..")                'Failed to load keys
                MSCount = 0
                Continue Do
            Else
                bw.ReportProgress(60)
                MyCard.Authentication(4, MiFareKey.KeyType.TypeA)                          'auth block no.4 where the key stored
                If Not MyCard.ReturnCode = 0 Then
                    'shit!
                    SetLabelText_ThreadSafe(CardProgressStatus, "Authenticate failed..")    'Failed to authenticate
                    MSCount = 0
                    Continue Do
                End If
            End If
            bw.ReportProgress(75)

            'get card uid
            CardUID = MyCard.GetUID
            If CardUID = "" Then
                SetLabelText_ThreadSafe(CardProgressStatus, "Card not recognized!")         'IMPOSIBRO!
                MSCount = 0
                Return
            End If
            bw.ReportProgress(85)
            SetLabelText_ThreadSafe(CardProgressStatus, "Reading..")                        'Trying to read

            'try to get kodemember
            MyCard.BlockRead(5, 16)     'read block no.4 where the key stored
            If Not MyCard.ReturnCode = 0 Then
                'holy shit!
                SetLabelText_ThreadSafe(CardProgressStatus, "Read failed..")                'Failed to read
                MSCount = 0
                Continue Do
            Else
                bw.ReportProgress(100)
                SetLabelText_ThreadSafe(CardProgressStatus, "Success..")                    'Godspeed..
                Return
            End If
        Loop
    End Sub

    Private Sub bw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs)
        CardProgressBar.EditValue = e.ProgressPercentage
    End Sub

    Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
        If e.Cancelled = True Then
            'cancel
            CardProgressStatus.Caption = "Canceled.." 'User trespass, WTF!?
        ElseIf e.Error IsNot Nothing Then
            'error
            CardProgressStatus.Caption = "Error.." 'OMFG?!
        Else
            'done
            ReaderIsBusy = True

            Dim TempKodeMember As String = ""
            For i = 0 To 16
                If MyCard.RecvBuff(i) = &H0 Then Continue For
                TempKodeMember = TempKodeMember + Convert.ToChar(MyCard.RecvBuff(i))
            Next
            MyCard.Disconnect()
            'If showtap = False Then Exit Sub
            kodemember = Trim(TempKodeMember) 'Godspeed..
            idmember = Replace(kodemember, ChrW(144), "")

            If kodemember <> "" Then
                'Me.Visible = False
                'showtap = False
                'Me.Close()
                oldkodemember = idmember
                checktopupcard()
                MyCard.BlockRead(6, 16)

                ReaderIsBusy = True

                TempKodeMember = ""
                For i = 0 To 16
                    If MyCard.RecvBuff(i) = &H0 Then Continue For
                    TempKodeMember = TempKodeMember + Convert.ToChar(MyCard.RecvBuff(i))
                Next
                MyCard.Disconnect()
                'If showtap = False Then Exit Sub
                Dim saldo2 As String = Trim(TempKodeMember) 'Godspeed..
                saldo2 = Replace(saldo2, ChrW(144), "")
                Dim saldoenkrip As String = oldkodemember & saldo2
                Dim saldokartu As String = ""
                'saldokartu = AES_Decrypt(saldoenkrip, "test")
                saldokartu = readsaldo(False)
                'If saldokartu = "" Then
                '    saldokartu = "0"
                'End If
                ReaderStarter()
                If MyACR.CardConnected Then
                    MyACR.ClearLCD()

                    If saldokartu = "error" Then
                        MyACR.DisplayLCD(1, "Proses Gagal!")
                        MyACR.DisplayLCD(2, "Rp. 0,-")
                        saldocust.Text = "Rp. 0,-"
                    ElseIf saldokartu = "null" Or saldokartu = "0" Then
                        MyACR.DisplayLCD(1, "Saldo Anda:")
                        MyACR.DisplayLCD(2, "Rp. 0,-")
                        saldocust.Text = "Rp. 0,-"
                    Else
                        MyACR.DisplayLCD(1, "Saldo Anda:")
                        MyACR.DisplayLCD(2, "Rp. " & Format(CDbl(saldokartu), "#,###") & ".-")
                        saldocust.Text = "Rp. " & Format(CDbl(saldokartu), "#,###") & ".-"
                    End If

                    MyACR.Disconnect()
                End If
                saldokartu = ""
                Timer1.Start()

                'Detil_customer.Show()
            End If

        End If
    End Sub

#End Region

#Region "Crossthreading"

    ' The delegate
    Delegate Sub SetLabelText_Delegate(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)

    ' The delegates subroutine.
    Private Sub SetLabelText_ThreadSafe(ByVal [TargetObject] As DevExpress.XtraBars.BarStaticItem, ByVal [text] As String)
        ' InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        If MSCount >= 10 Or [text] = "Success.." Then
            If InvokeRequired Then
                Dim MyDelegate As New SetLabelText_Delegate(AddressOf SetLabelText_ThreadSafe)
                Invoke(MyDelegate, New Object() {[TargetObject], [text]})
            Else
                [TargetObject].Caption = [text]
            End If
        End If
    End Sub

    Private Sub tap_card_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        On Error Resume Next
        If bw.IsBusy Then bw.CancelAsync()


        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub tap_card_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        Me.WindowState = FormWindowState.Maximized
        'RFID

        ' Timer1.Start()



    End Sub


    'Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
    '    If showtap = False Then Exit Sub
    '    kodemember = ""
    '    Get_MembercardSettings()
    '    bw.WorkerSupportsCancellation = True
    '    bw.WorkerReportsProgress = True
    '    AddHandler bw.DoWork, AddressOf bw_DoWork
    '    AddHandler bw.ProgressChanged, AddressOf bw_ProgressChanged
    '    AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted

    '    Try
    '        If Not KeySetupCheck() Then
    '            CardProgressStatus.Caption = "Membercard settings hasn't defined!"
    '            Return
    '        End If

    '        ReaderStarter()
    '        MyACR.Backlight(True)
    '        MyACR.DisplayLCD(1, "    Welcome")
    '        MyACR.DisplayLCD(2, " Laritta Bakery")
    '        MyACR.Disconnect()

    '        bw.RunWorkerAsync()
    '    Catch ex As Exception
    '        CardProgressStatus.Caption = "Reader not found!"
    '    End Try
    'End Sub





#End Region

    Public Sub mulai()
        Try
            Me.Visible = True

            kodemember = ""
            Threading.Thread.Sleep(2000)

            If KeySetupCheck() Then
                ReaderStarter()
                If MyACR.CardConnected Then
                    MyACR.ClearLCD()
                    MyACR.DisplayLCD(1, "    Welcome")
                    MyACR.DisplayLCD(2, " Laritta Bakery")
                    saldocust.Text = "Tap Kartu"
                    MyACR.Disconnect()
                End If
                If ReaderIsBusy Then
                    CardUID = ""
                    CardProgressBar.EditValue = 0
                    ReaderIsBusy = False
                    CardProgressStatus.Caption = "Waiting.."
                    If bw.IsBusy Then bw.CancelAsync()
                    bw.RunWorkerAsync()
                End If
            End If
            'Show()
            'Timer1.Start()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        mulai()
        Timer1.Stop()
    End Sub

    Private Sub close_Click(sender As Object, e As EventArgs) Handles closebtn.Click
        Me.Close()
    End Sub

    Private Sub tap_card_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Timer1.Stop()
        bw.Dispose()
        ReaderStarter()
        MyACR.ClearLCD()
        MyACR.Backlight(False)
        MyACR.Disconnect()
        MyCard.Disconnect()
    End Sub
End Class
