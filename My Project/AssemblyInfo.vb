﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Laritta POS")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("LarittaIT")> 
<Assembly: AssemblyProduct("Laritta POS")>
<Assembly: AssemblyCopyright("Copyright ©  2017")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6267976c-8c65-4ec2-a163-ef08e97a3b18")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.7.8.7")>
<Assembly: AssemblyFileVersion("1.7.8.7")>

<Assembly: NeutralResourcesLanguageAttribute("")> 