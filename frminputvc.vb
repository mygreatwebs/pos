﻿Public Class frminputvc
    Dim tempkodevc As New DataTable
    Dim haspushed As Boolean = False
    Private Sub initdata()
        If IsNothing(xSet.Tables("kodevoucher")) Then
            'create dummy data
            ExDb.ExecQuery(My.Settings.db_conn_mysql, "select 'Dummy' kdVC", xSet, "kodevoucher")
            'xSet.Tables("kodevoucher").Clear()
        End If

        If xSet.Tables("kodevoucher").Rows.Count > 0 Then
            tempkodevc = xSet.Tables("kodevoucher").Copy
        End If
    End Sub

    Private Sub tbvcPR_Click(sender As Object, e As EventArgs) Handles tbvcPR.Click
        txtprefix.Text = "PR/" & Format(Now(), "yy/MM/")
    End Sub

    Private Sub tbvcGF_Click(sender As Object, e As EventArgs) Handles tbvcGF.Click
        txtprefix.Text = "GF/" & Format(Now(), "yy/MM/")
    End Sub

    Private Sub txtcounter_GotFocus(sender As Object, e As EventArgs) Handles txtcounter.GotFocus
        virNumpad.ShowDialog(Me)
        txtcounter.Text = qttInteger
    End Sub

    Private Sub frminputvc_Load(sender As Object, e As EventArgs) Handles Me.Load
        initdata()
        GridControl1.DataSource = xSet.Tables("tempkodevoucher")
    End Sub
    Private Function cekTotal()
        Try
            If CInt(txtcounter.EditValue) >= 99999999 Then
                txtcounter.Text = 99999999
                Return False
            End If
            If hasPushed = False Then hasPushed = True
            Return True
        Catch ex As Exception
            txtcounter.Text = 99999999
            Return False
        End Try
    End Function

    Private Sub tbKeluar_Click(sender As Object, e As EventArgs) Handles tbKeluar.Click
        xSet.Tables("kodevoucher").Dispose()
        tempkodevc.Dispose()
        Close()
    End Sub

    Private Sub butNum1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum1.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 1
        hasPushed = True
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum2.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 2
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum3.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 3
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum4_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum4.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 4
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum5_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum5.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 5
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum6_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum6.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 6
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum7_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum7.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 7
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum8_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum8.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 8
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum9_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum9.Click
        If hasPushed = False Then
            If txtcounter.Text = 1 Then txtcounter.Text = 0
        End If
        txtcounter.Text = txtcounter.Text * 10 + 9
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum0_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum0.Click
        txtcounter.Text = txtcounter.Text * 10
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butNum00_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butNum00.Click
        txtcounter.Text = txtcounter.Text * 100
        If Not cekTotal() Then Exit Sub
    End Sub

    Private Sub butReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butReset.Click
        txtcounter.Text = 0
    End Sub

    Private Sub butOK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butOK.Click
        Dim kdvc As String = ""
        If txtcounter.Text.Length > 5 Then
            MsgBox("Digit KodeVoucher Terlalu panjang, maximal 5 digit")
            txtcounter.Text = 0
            Exit Sub
        End If
        qttInteger = txtcounter.Text
        kdvc = String.Format("{0:00000}", Val(qttInteger))
        tempkodevc.Rows.Add(New Object() {0, txtprefix.Text & kdvc})
        numpadpanel.Visible = False
    End Sub

    Private Sub butClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClose.Click
        qttInteger = 0
        numpadpanel.Visible = False
    End Sub

    Private Sub tbkalender_Click(sender As Object, e As EventArgs) Handles tbkalender.Click
        tempkodevc.Rows.Add(New Object() {0, "kalender"})
        numpadpanel.Visible = False
    End Sub

    Private Sub tbSimpan_Click(sender As Object, e As EventArgs) Handles tbSimpan.Click
        tempkodevc.AcceptChanges()

        Dim mreader As DataTableReader = tempkodevc.CreateDataReader
        xSet.Tables("kodevoucher").Load(mreader)
        tempkodevc.Dispose()
        Close()
    End Sub

    Private Sub tbHapus_Click(sender As Object, e As EventArgs) Handles tbHapus.Click
        GridView1.DeleteSelectedRows()
    End Sub

End Class