﻿Public Class frminputuangkasir
    Dim isSaved As Boolean

    Private Function f_cint(ByVal m_val As String) As Integer
        On Error GoTo ErrRaise
        If m_val = "" Then Return 0
        Return CDbl(m_val)
        Exit Function
ErrRaise:
        Return 0
    End Function

    Private Sub txt100Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt100Ribu.EditValueChanged
        TextEdit7.EditValue = f_cint(txt100Ribu.EditValue) * 100000
        totaluanglembar()
    End Sub

    Private Sub txt50Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt50Ribu.EditValueChanged
        TextEdit8.EditValue = f_cint(txt50Ribu.EditValue) * 50000
        totaluanglembar()
    End Sub

    Private Sub txt20Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt20Ribu.EditValueChanged

        TextEdit9.EditValue = f_cint(txt20Ribu.EditValue) * 20000
        totaluanglembar()
    End Sub

    Private Sub txt10Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt10Ribu.EditValueChanged

        TextEdit10.EditValue = f_cint(txt10Ribu.EditValue) * 10000
        totaluanglembar()
    End Sub

    Private Sub txt5Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt5Ribu.EditValueChanged

        TextEdit11.EditValue = f_cint(txt5Ribu.EditValue) * 5000
        totaluanglembar()
    End Sub

    Private Sub txt1Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt1Ribu.EditValueChanged

        TextEdit12.EditValue = f_cint(txt1Ribu.EditValue) * 1000
        totaluanglembar()
    End Sub
    Private Sub totaluanglembar()
        TextEdit13.EditValue = TextEdit7.EditValue + TextEdit8.EditValue + TextEdit9.EditValue + TextEdit10.EditValue + TextEdit11.EditValue + TextEdit12.EditValue + TextEdit1.EditValue
    End Sub

    Private Sub txt1RibuLogam_EditValueChanged(sender As Object, e As EventArgs) Handles txt1RibuLogam.EditValueChanged

        TextEdit20.EditValue = f_cint(txt1RibuLogam.EditValue) * 1000
        totaluanglogam()
    End Sub

    Private Sub txt500Logam_EditValueChanged(sender As Object, e As EventArgs) Handles txt500Logam.EditValueChanged
        TextEdit19.EditValue = f_cint(txt500Logam.EditValue) * 500
        totaluanglogam()
    End Sub

    Private Sub txt200Logam_EditValueChanged(sender As Object, e As EventArgs) Handles txt200Logam.EditValueChanged
        TextEdit18.EditValue = f_cint(txt200Logam.EditValue) * 200
        totaluanglogam()
    End Sub

    Private Sub txt100Logam_EditValueChanged(sender As Object, e As EventArgs) Handles txt100Logam.EditValueChanged
        TextEdit17.EditValue = f_cint(txt100Logam.EditValue) * 100
        totaluanglogam()
    End Sub

    Private Sub txt50Logam_EditValueChanged(sender As Object, e As EventArgs) Handles txt50Logam.EditValueChanged
        TextEdit16.EditValue = f_cint(txt50Logam.EditValue) * 50
        totaluanglogam()
    End Sub

    Private Sub txt25Logam_EditValueChanged(sender As Object, e As EventArgs) Handles txt25Logam.EditValueChanged
        TextEdit15.EditValue = f_cint(txt25Logam.EditValue) * 25
        totaluanglogam()
    End Sub

    Private Sub totaluanglogam()
        TextEdit14.EditValue = f_cint(TextEdit20.EditValue) + f_cint(TextEdit19.EditValue) + f_cint(TextEdit18.EditValue) + f_cint(TextEdit17.EditValue) + f_cint(TextEdit16.EditValue) + f_cint(TextEdit15.EditValue)
    End Sub


    Private Sub TextEdit13_EditValueChanged(sender As Object, e As EventArgs) Handles TextEdit13.EditValueChanged
        TextEdit27.EditValue = f_cint(TextEdit13.EditValue) + f_cint(TextEdit14.EditValue)
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Close()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If TextEdit27.EditValue = 0 Then
            Exit Sub
        End If
        xSet.Tables("infouangkasir").Clear()
        xSet.Tables("infouangkasir").Rows.Add(1, 100000, f_cint(txt100Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(2, 50000, f_cint(txt50Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(3, 20000, f_cint(txt20Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(4, 10000, f_cint(txt10Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(5, 5000, f_cint(txt5Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(13, 2000, f_cint(txt2Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(6, 1000, f_cint(txt1Ribu.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(7, 1000, f_cint(txt1RibuLogam.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(8, 500, f_cint(txt500Logam.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(9, 200, f_cint(txt200Logam.EditValue))
        xSet.Tables("infouangkasir").Rows.Add(10, 100, f_cint(txt100Logam.EditValue))
        'MsgBox(xSet.Tables("infouangkasir").Rows.Count.ToString)
        isSaved = True
        Close()
    End Sub

    Private Sub frminputuangkasir_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub frminputuangkasir_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If isSaved = False Then e.Cancel = True
    End Sub

    Private Sub TextEdit14_EditValueChanged(sender As Object, e As EventArgs) Handles TextEdit14.EditValueChanged
        TextEdit27.EditValue = f_cint(TextEdit13.EditValue) + f_cint(TextEdit14.EditValue)
    End Sub

    Private Sub txt2Ribu_EditValueChanged(sender As Object, e As EventArgs) Handles txt2Ribu.EditValueChanged
        TextEdit1.EditValue = f_cint(txt2Ribu.EditValue) * 2000
        totaluanglembar()
    End Sub


End Class