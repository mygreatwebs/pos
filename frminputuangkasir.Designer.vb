﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frminputuangkasir
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txt100Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txt50Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.txt20Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.txt10Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.txt5Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.txt1Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.txt2Ribu = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit13 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit12 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit14 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit15 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit16 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit17 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit18 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit19 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit20 = New DevExpress.XtraEditors.TextEdit()
        Me.txt25Logam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.txt50Logam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.txt100Logam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.txt200Logam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.txt500Logam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.txt1RibuLogam = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit27 = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txt100Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt50Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt20Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt10Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt5Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt2Ribu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt25Logam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt50Logam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt100Logam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt200Logam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt500Logam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1RibuLogam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(37, 36)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 16)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "100.000"
        '
        'txt100Ribu
        '
        Me.txt100Ribu.EditValue = ""
        Me.txt100Ribu.Location = New System.Drawing.Point(98, 33)
        Me.txt100Ribu.Name = "txt100Ribu"
        Me.txt100Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt100Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt100Ribu.Properties.MaxLength = 5
        Me.txt100Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt100Ribu.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(204, 34)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Lembar ="
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(37, 62)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "50.000"
        '
        'txt50Ribu
        '
        Me.txt50Ribu.EditValue = ""
        Me.txt50Ribu.Location = New System.Drawing.Point(98, 59)
        Me.txt50Ribu.Name = "txt50Ribu"
        Me.txt50Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt50Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt50Ribu.Properties.MaxLength = 5
        Me.txt50Ribu.Properties.NullText = "0"
        Me.txt50Ribu.Properties.NullValuePrompt = "0"
        Me.txt50Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt50Ribu.TabIndex = 4
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(37, 88)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl6.TabIndex = 6
        Me.LabelControl6.Text = "20.000"
        '
        'txt20Ribu
        '
        Me.txt20Ribu.EditValue = ""
        Me.txt20Ribu.Location = New System.Drawing.Point(98, 85)
        Me.txt20Ribu.Name = "txt20Ribu"
        Me.txt20Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt20Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt20Ribu.Properties.MaxLength = 5
        Me.txt20Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt20Ribu.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(37, 114)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl8.TabIndex = 9
        Me.LabelControl8.Text = "10.000"
        '
        'txt10Ribu
        '
        Me.txt10Ribu.EditValue = ""
        Me.txt10Ribu.Location = New System.Drawing.Point(98, 111)
        Me.txt10Ribu.Name = "txt10Ribu"
        Me.txt10Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt10Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt10Ribu.Properties.MaxLength = 5
        Me.txt10Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt10Ribu.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl10.Location = New System.Drawing.Point(37, 140)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 16)
        Me.LabelControl10.TabIndex = 12
        Me.LabelControl10.Text = "5.000"
        '
        'txt5Ribu
        '
        Me.txt5Ribu.EditValue = ""
        Me.txt5Ribu.Location = New System.Drawing.Point(98, 137)
        Me.txt5Ribu.Name = "txt5Ribu"
        Me.txt5Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt5Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt5Ribu.Properties.MaxLength = 5
        Me.txt5Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt5Ribu.TabIndex = 13
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.Location = New System.Drawing.Point(37, 192)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 16)
        Me.LabelControl12.TabIndex = 15
        Me.LabelControl12.Text = "1.000"
        '
        'txt1Ribu
        '
        Me.txt1Ribu.EditValue = ""
        Me.txt1Ribu.Location = New System.Drawing.Point(98, 189)
        Me.txt1Ribu.Name = "txt1Ribu"
        Me.txt1Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt1Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt1Ribu.Properties.MaxLength = 5
        Me.txt1Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt1Ribu.TabIndex = 16
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(266, 32)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit7.Properties.Mask.EditMask = "n"
        Me.TextEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit7.Properties.ReadOnly = True
        Me.TextEdit7.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit7.TabIndex = 18
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.Location = New System.Drawing.Point(372, 33)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl13.TabIndex = 19
        Me.LabelControl13.Text = "Rupiah"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(204, 63)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "Lembar ="
        '
        'TextEdit8
        '
        Me.TextEdit8.Location = New System.Drawing.Point(266, 61)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit8.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit8.Properties.Mask.EditMask = "n"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit8.Properties.ReadOnly = True
        Me.TextEdit8.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit8.TabIndex = 21
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(372, 62)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl3.TabIndex = 22
        Me.LabelControl3.Text = "Rupiah"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl9.Location = New System.Drawing.Point(204, 89)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl9.TabIndex = 23
        Me.LabelControl9.Text = "Lembar ="
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(266, 87)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit9.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit9.Properties.Mask.EditMask = "n"
        Me.TextEdit9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit9.Properties.ReadOnly = True
        Me.TextEdit9.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit9.TabIndex = 24
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(372, 88)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl7.TabIndex = 25
        Me.LabelControl7.Text = "Rupiah"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LabelControl41)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl42)
        Me.GroupControl1.Controls.Add(Me.txt2Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl43)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.TextEdit13)
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.TextEdit12)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.TextEdit11)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.TextEdit10)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.TextEdit9)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.TextEdit8)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.TextEdit7)
        Me.GroupControl1.Controls.Add(Me.txt1Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.txt5Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.txt10Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.txt20Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.txt50Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.txt100Ribu)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(461, 247)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Uang Kertas"
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl41.Location = New System.Drawing.Point(372, 164)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl41.TabIndex = 42
        Me.LabelControl41.Text = "Rupiah"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(266, 163)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit1.Properties.Mask.EditMask = "n"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 33
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl42.Location = New System.Drawing.Point(204, 165)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl42.TabIndex = 40
        Me.LabelControl42.Text = "Lembar ="
        '
        'txt2Ribu
        '
        Me.txt2Ribu.EditValue = ""
        Me.txt2Ribu.Location = New System.Drawing.Point(98, 164)
        Me.txt2Ribu.Name = "txt2Ribu"
        Me.txt2Ribu.Properties.Mask.EditMask = "\d+"
        Me.txt2Ribu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt2Ribu.Properties.MaxLength = 5
        Me.txt2Ribu.Size = New System.Drawing.Size(100, 20)
        Me.txt2Ribu.TabIndex = 16
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl43.Location = New System.Drawing.Point(37, 167)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(32, 16)
        Me.LabelControl43.TabIndex = 38
        Me.LabelControl43.Text = "2.000"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl19.Location = New System.Drawing.Point(372, 215)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl19.TabIndex = 37
        Me.LabelControl19.Text = "Rupiah"
        '
        'TextEdit13
        '
        Me.TextEdit13.Location = New System.Drawing.Point(266, 214)
        Me.TextEdit13.Name = "TextEdit13"
        Me.TextEdit13.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit13.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit13.Properties.Mask.EditMask = "n"
        Me.TextEdit13.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit13.Properties.ReadOnly = True
        Me.TextEdit13.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit13.TabIndex = 36
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl20.Location = New System.Drawing.Point(204, 216)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(29, 16)
        Me.LabelControl20.TabIndex = 35
        Me.LabelControl20.Text = "Total"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl17.Location = New System.Drawing.Point(372, 189)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl17.TabIndex = 34
        Me.LabelControl17.Text = "Rupiah"
        '
        'TextEdit12
        '
        Me.TextEdit12.Location = New System.Drawing.Point(266, 188)
        Me.TextEdit12.Name = "TextEdit12"
        Me.TextEdit12.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit12.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit12.Properties.Mask.EditMask = "n"
        Me.TextEdit12.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit12.Properties.ReadOnly = True
        Me.TextEdit12.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit12.TabIndex = 33
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl18.Location = New System.Drawing.Point(204, 190)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl18.TabIndex = 32
        Me.LabelControl18.Text = "Lembar ="
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl15.Location = New System.Drawing.Point(372, 138)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl15.TabIndex = 31
        Me.LabelControl15.Text = "Rupiah"
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(266, 137)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit11.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit11.Properties.Mask.EditMask = "n"
        Me.TextEdit11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit11.Properties.ReadOnly = True
        Me.TextEdit11.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit11.TabIndex = 30
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl16.Location = New System.Drawing.Point(204, 139)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl16.TabIndex = 29
        Me.LabelControl16.Text = "Lembar ="
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl11.Location = New System.Drawing.Point(372, 111)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl11.TabIndex = 28
        Me.LabelControl11.Text = "Rupiah"
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(266, 110)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit10.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit10.Properties.Mask.EditMask = "n"
        Me.TextEdit10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit10.Properties.ReadOnly = True
        Me.TextEdit10.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit10.TabIndex = 27
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl14.Location = New System.Drawing.Point(204, 112)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(56, 16)
        Me.LabelControl14.TabIndex = 26
        Me.LabelControl14.Text = "Lembar ="
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LabelControl21)
        Me.GroupControl2.Controls.Add(Me.TextEdit14)
        Me.GroupControl2.Controls.Add(Me.LabelControl22)
        Me.GroupControl2.Controls.Add(Me.LabelControl23)
        Me.GroupControl2.Controls.Add(Me.TextEdit15)
        Me.GroupControl2.Controls.Add(Me.LabelControl24)
        Me.GroupControl2.Controls.Add(Me.LabelControl25)
        Me.GroupControl2.Controls.Add(Me.TextEdit16)
        Me.GroupControl2.Controls.Add(Me.LabelControl26)
        Me.GroupControl2.Controls.Add(Me.LabelControl27)
        Me.GroupControl2.Controls.Add(Me.TextEdit17)
        Me.GroupControl2.Controls.Add(Me.LabelControl28)
        Me.GroupControl2.Controls.Add(Me.LabelControl29)
        Me.GroupControl2.Controls.Add(Me.TextEdit18)
        Me.GroupControl2.Controls.Add(Me.LabelControl30)
        Me.GroupControl2.Controls.Add(Me.LabelControl31)
        Me.GroupControl2.Controls.Add(Me.TextEdit19)
        Me.GroupControl2.Controls.Add(Me.LabelControl32)
        Me.GroupControl2.Controls.Add(Me.LabelControl33)
        Me.GroupControl2.Controls.Add(Me.TextEdit20)
        Me.GroupControl2.Controls.Add(Me.txt25Logam)
        Me.GroupControl2.Controls.Add(Me.LabelControl34)
        Me.GroupControl2.Controls.Add(Me.txt50Logam)
        Me.GroupControl2.Controls.Add(Me.LabelControl35)
        Me.GroupControl2.Controls.Add(Me.txt100Logam)
        Me.GroupControl2.Controls.Add(Me.LabelControl36)
        Me.GroupControl2.Controls.Add(Me.txt200Logam)
        Me.GroupControl2.Controls.Add(Me.LabelControl37)
        Me.GroupControl2.Controls.Add(Me.txt500Logam)
        Me.GroupControl2.Controls.Add(Me.LabelControl38)
        Me.GroupControl2.Controls.Add(Me.LabelControl39)
        Me.GroupControl2.Controls.Add(Me.txt1RibuLogam)
        Me.GroupControl2.Controls.Add(Me.LabelControl40)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 265)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(461, 203)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Uang Logam"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl21.Location = New System.Drawing.Point(372, 143)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl21.TabIndex = 37
        Me.LabelControl21.Text = "Rupiah"
        '
        'TextEdit14
        '
        Me.TextEdit14.Location = New System.Drawing.Point(266, 142)
        Me.TextEdit14.Name = "TextEdit14"
        Me.TextEdit14.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit14.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit14.Properties.Mask.EditMask = "n"
        Me.TextEdit14.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit14.Properties.ReadOnly = True
        Me.TextEdit14.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit14.TabIndex = 36
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl22.Location = New System.Drawing.Point(204, 144)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(29, 16)
        Me.LabelControl22.TabIndex = 35
        Me.LabelControl22.Text = "Total"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl23.Location = New System.Drawing.Point(39, 166)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl23.TabIndex = 34
        Me.LabelControl23.Text = "Rupiah"
        Me.LabelControl23.Visible = False
        '
        'TextEdit15
        '
        Me.TextEdit15.Location = New System.Drawing.Point(35, 166)
        Me.TextEdit15.Name = "TextEdit15"
        Me.TextEdit15.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit15.TabIndex = 33
        Me.TextEdit15.Visible = False
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl24.Location = New System.Drawing.Point(35, 166)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl24.TabIndex = 32
        Me.LabelControl24.Text = "Biji ="
        Me.LabelControl24.Visible = False
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl25.Location = New System.Drawing.Point(84, 169)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl25.TabIndex = 31
        Me.LabelControl25.Text = "Rupiah"
        Me.LabelControl25.Visible = False
        '
        'TextEdit16
        '
        Me.TextEdit16.Location = New System.Drawing.Point(37, 167)
        Me.TextEdit16.Name = "TextEdit16"
        Me.TextEdit16.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit16.TabIndex = 30
        Me.TextEdit16.Visible = False
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl26.Location = New System.Drawing.Point(39, 167)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl26.TabIndex = 29
        Me.LabelControl26.Text = "Biji ="
        Me.LabelControl26.Visible = False
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl27.Location = New System.Drawing.Point(372, 111)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl27.TabIndex = 28
        Me.LabelControl27.Text = "Rupiah"
        '
        'TextEdit17
        '
        Me.TextEdit17.Location = New System.Drawing.Point(266, 110)
        Me.TextEdit17.Name = "TextEdit17"
        Me.TextEdit17.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit17.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit17.Properties.Mask.EditMask = "n"
        Me.TextEdit17.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit17.Properties.ReadOnly = True
        Me.TextEdit17.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit17.TabIndex = 27
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl28.Location = New System.Drawing.Point(204, 112)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl28.TabIndex = 26
        Me.LabelControl28.Text = "Biji ="
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl29.Location = New System.Drawing.Point(372, 88)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl29.TabIndex = 25
        Me.LabelControl29.Text = "Rupiah"
        '
        'TextEdit18
        '
        Me.TextEdit18.Location = New System.Drawing.Point(266, 87)
        Me.TextEdit18.Name = "TextEdit18"
        Me.TextEdit18.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit18.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit18.Properties.Mask.EditMask = "n"
        Me.TextEdit18.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit18.Properties.ReadOnly = True
        Me.TextEdit18.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit18.TabIndex = 24
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl30.Location = New System.Drawing.Point(204, 89)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl30.TabIndex = 23
        Me.LabelControl30.Text = "Biji ="
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl31.Location = New System.Drawing.Point(372, 62)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl31.TabIndex = 22
        Me.LabelControl31.Text = "Rupiah"
        '
        'TextEdit19
        '
        Me.TextEdit19.Location = New System.Drawing.Point(266, 61)
        Me.TextEdit19.Name = "TextEdit19"
        Me.TextEdit19.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit19.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit19.Properties.Mask.EditMask = "n"
        Me.TextEdit19.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit19.Properties.ReadOnly = True
        Me.TextEdit19.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit19.TabIndex = 21
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl32.Location = New System.Drawing.Point(204, 63)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl32.TabIndex = 20
        Me.LabelControl32.Text = "Biji ="
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl33.Location = New System.Drawing.Point(372, 33)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(39, 16)
        Me.LabelControl33.TabIndex = 19
        Me.LabelControl33.Text = "Rupiah"
        '
        'TextEdit20
        '
        Me.TextEdit20.Location = New System.Drawing.Point(266, 32)
        Me.TextEdit20.Name = "TextEdit20"
        Me.TextEdit20.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit20.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit20.Properties.Mask.EditMask = "n"
        Me.TextEdit20.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit20.Properties.ReadOnly = True
        Me.TextEdit20.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit20.TabIndex = 18
        '
        'txt25Logam
        '
        Me.txt25Logam.Location = New System.Drawing.Point(37, 166)
        Me.txt25Logam.Name = "txt25Logam"
        Me.txt25Logam.Size = New System.Drawing.Size(100, 20)
        Me.txt25Logam.TabIndex = 16
        Me.txt25Logam.Visible = False
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl34.Location = New System.Drawing.Point(37, 167)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(14, 16)
        Me.LabelControl34.TabIndex = 15
        Me.LabelControl34.Text = "25"
        Me.LabelControl34.Visible = False
        '
        'txt50Logam
        '
        Me.txt50Logam.Location = New System.Drawing.Point(37, 163)
        Me.txt50Logam.Name = "txt50Logam"
        Me.txt50Logam.Size = New System.Drawing.Size(100, 20)
        Me.txt50Logam.TabIndex = 13
        Me.txt50Logam.Visible = False
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl35.Location = New System.Drawing.Point(37, 166)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(14, 16)
        Me.LabelControl35.TabIndex = 12
        Me.LabelControl35.Text = "50"
        Me.LabelControl35.Visible = False
        '
        'txt100Logam
        '
        Me.txt100Logam.EditValue = ""
        Me.txt100Logam.Location = New System.Drawing.Point(98, 111)
        Me.txt100Logam.Name = "txt100Logam"
        Me.txt100Logam.Properties.Mask.EditMask = "\d+"
        Me.txt100Logam.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt100Logam.Properties.MaxLength = 5
        Me.txt100Logam.Size = New System.Drawing.Size(100, 20)
        Me.txt100Logam.TabIndex = 10
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl36.Location = New System.Drawing.Point(37, 114)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(21, 16)
        Me.LabelControl36.TabIndex = 9
        Me.LabelControl36.Text = "100"
        '
        'txt200Logam
        '
        Me.txt200Logam.EditValue = ""
        Me.txt200Logam.Location = New System.Drawing.Point(98, 85)
        Me.txt200Logam.Name = "txt200Logam"
        Me.txt200Logam.Properties.Mask.EditMask = "\d+"
        Me.txt200Logam.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt200Logam.Properties.MaxLength = 5
        Me.txt200Logam.Size = New System.Drawing.Size(100, 20)
        Me.txt200Logam.TabIndex = 7
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl37.Location = New System.Drawing.Point(37, 88)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(21, 16)
        Me.LabelControl37.TabIndex = 6
        Me.LabelControl37.Text = "200"
        '
        'txt500Logam
        '
        Me.txt500Logam.EditValue = ""
        Me.txt500Logam.Location = New System.Drawing.Point(98, 59)
        Me.txt500Logam.Name = "txt500Logam"
        Me.txt500Logam.Properties.Mask.EditMask = "\d+"
        Me.txt500Logam.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt500Logam.Properties.MaxLength = 5
        Me.txt500Logam.Size = New System.Drawing.Size(100, 20)
        Me.txt500Logam.TabIndex = 4
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl38.Location = New System.Drawing.Point(37, 62)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(21, 16)
        Me.LabelControl38.TabIndex = 3
        Me.LabelControl38.Text = "500"
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl39.Location = New System.Drawing.Point(204, 34)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(30, 16)
        Me.LabelControl39.TabIndex = 2
        Me.LabelControl39.Text = "Biji ="
        '
        'txt1RibuLogam
        '
        Me.txt1RibuLogam.EditValue = ""
        Me.txt1RibuLogam.Location = New System.Drawing.Point(98, 33)
        Me.txt1RibuLogam.Name = "txt1RibuLogam"
        Me.txt1RibuLogam.Properties.Mask.EditMask = "\d+"
        Me.txt1RibuLogam.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.txt1RibuLogam.Properties.MaxLength = 5
        Me.txt1RibuLogam.Size = New System.Drawing.Size(100, 20)
        Me.txt1RibuLogam.TabIndex = 1
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl40.Location = New System.Drawing.Point(37, 36)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(28, 16)
        Me.LabelControl40.TabIndex = 0
        Me.LabelControl40.Text = "1000"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(370, 480)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(77, 36)
        Me.SimpleButton1.TabIndex = 2
        Me.SimpleButton1.Text = "Simpan"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(287, 480)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(77, 36)
        Me.SimpleButton2.TabIndex = 3
        Me.SimpleButton2.Text = "Batal"
        Me.SimpleButton2.Visible = False
        '
        'TextEdit27
        '
        Me.TextEdit27.Location = New System.Drawing.Point(75, 488)
        Me.TextEdit27.Name = "TextEdit27"
        Me.TextEdit27.Properties.DisplayFormat.FormatString = "n0"
        Me.TextEdit27.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.TextEdit27.Properties.Mask.EditMask = "n"
        Me.TextEdit27.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit27.Properties.ReadOnly = True
        Me.TextEdit27.Size = New System.Drawing.Size(197, 20)
        Me.TextEdit27.TabIndex = 37
        '
        'frminputuangkasir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(490, 528)
        Me.Controls.Add(Me.TextEdit27)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frminputuangkasir"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "  "
        CType(Me.txt100Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt50Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt20Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt10Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt5Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt2Ribu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.TextEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt25Logam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt50Logam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt100Logam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt200Logam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt500Logam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1RibuLogam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit27.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt100Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt50Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt20Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt10Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt5Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit13 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit12 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit15 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit16 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit17 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit18 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit19 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit20 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt25Logam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt50Logam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt100Logam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt200Logam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt500Logam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt1RibuLogam As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit27 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txt2Ribu As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
End Class
