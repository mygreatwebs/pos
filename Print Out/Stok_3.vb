﻿Public Class Stok_3

    Private Sub Stok3_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        XrLabel1.Text = "Cabang " & xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("namaCabang")
        XrLabel2.Text = Format(Now, "dd-MM-yyyy HH:mm")
        If Trim$(SqlAftermutasi) = "" Then
            Print_stok2TableAdapter1.Adapter.SelectCommand.CommandText = Sqlbeforemutasi
        Else
            Print_stok2TableAdapter1.Adapter.SelectCommand.CommandText = SqlAftermutasi
        End If
        
        Print_stok2TableAdapter1.ClearBeforeFill = True
        Print_stok2TableAdapter1.Fill(DataSetPOS1.print_stok2, id_cabang)
    End Sub
End Class