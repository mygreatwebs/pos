﻿Public Class Stok2



    Private Sub Stok2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        XrLabel1.Text = "Cabang " & xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("namaCabang")
        XrLabel2.Text = Format(Now, "dd-MM-yyyy HH:mm")

        Print_stok2TableAdapter1.Adapter.SelectCommand.CommandText = "SELECT namaBrg, namaKategori2, SUM( IFNULL(qttIN,0)) AS qttIN, SUM( IFNULL(qttOUT,0)) AS qttOUT, SUM( IFNULL(qttIN,0)) + SUM(IFNULL(qttOUT,0)) AS Balance " & _
                                " FROM mbarang a INNER  JOIN (SELECT        idBrg, quantity AS qttIN, 0 AS qttOUT " & _
                               " FROM            tstok a WHERE        (idCabang = @idCabang) AND (isExpired = 0) AND (isVoid = 0) AND (quantity > 0)  " & _
                               " UNION ALL " & _
                               " SELECT        idBrg, 0 AS qttIN, quantity AS qttOUT FROM tstok b  " & _
                               " WHERE        (idCabang = @idCabang) AND (isExpired = 0) AND (isVoid = 0) AND (quantity < 0)) b_1 ON a.idBrg = b_1.idBrg LEFT OUTER JOIN " & _
                               " mkategoribrg2 c ON a.idKategori2 = c.idKategori2 WHERE  (a.isToko = 1) AND (a.inactive=0) GROUP BY a.idBrg " & _
                               " ORDER BY c.namaKategori2, a.namaBrg "

        Print_stok2TableAdapter1.ClearBeforeFill = True
        Print_stok2TableAdapter1.Fill(DataSetPOS1.print_stok2, id_cabang)

    End Sub


End Class