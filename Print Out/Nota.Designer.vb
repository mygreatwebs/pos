﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Nota
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Nota))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.labelisReprint = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelisVoid = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.labelNoNota = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelTglJam = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.gojekflag = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.labelFooter = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.lblarittacash = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel26 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.LbVC = New DevExpress.XtraReports.UI.XRLabel()
        Me.LbCC = New DevExpress.XtraReports.UI.XRLabel()
        Me.LbCash = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelSPG = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelChange = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelPayment = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.LabelDisc = New DevExpress.XtraReports.UI.XRLabel()
        Me.labelSubTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.DataSetPOS1 = New Laritta_POS.DataSetPOS()
        Me.Print_dtposTableAdapter1 = New Laritta_POS.DataSetPOSTableAdapters.print_dtposTableAdapter()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Xrpointawal = New DevExpress.XtraReports.UI.XRLabel()
        Me.Xrpointakhir = New DevExpress.XtraReports.UI.XRLabel()
        Me.Xrkuponawal = New DevExpress.XtraReports.UI.XRLabel()
        Me.Xrkuponakhir = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter3 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.kodemembertxtnota = New DevExpress.XtraReports.UI.XRLabel()
        Me.pemiliksaldo = New DevExpress.XtraReports.UI.XRLabel()
        Me.namamember = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.saldosesudahtxt = New DevExpress.XtraReports.UI.XRLabel()
        Me.saldosebelumtxt = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.parsaldosesudah = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetPOS1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.Detail.HeightF = 22.92!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrTable1
        '
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(262.0!, 22.92!)
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell2, Me.XrTableCell3, Me.XrTableCell4})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_dtpos.namaBrg")})
        Me.XrTableCell1.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.StylePriority.UseFont = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "XrTableCell1"
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 1.2543553968045533R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_dtpos.quantity", "{0:n0}")})
        Me.XrTableCell2.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.StylePriority.UseFont = False
        Me.XrTableCell2.StylePriority.UseTextAlignment = False
        Me.XrTableCell2.Text = "XrTableCell2"
        Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell2.Weight = 0.41811846209800879R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_dtpos.price", "{0:n0}")})
        Me.XrTableCell3.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.StylePriority.UseFont = False
        Me.XrTableCell3.StylePriority.UseTextAlignment = False
        Me.XrTableCell3.Text = "[price]"
        Me.XrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell3.Weight = 0.57491290007329521R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "print_dtpos.totalPrice", "{0:n0}")})
        Me.XrTableCell4.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.StylePriority.UseFont = False
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:n0}"
        Me.XrTableCell4.Summary = XrSummary1
        Me.XrTableCell4.Text = "XrTableCell4"
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell4.Weight = 0.75261324102414318R
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 25.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 40.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.labelisReprint, Me.labelisVoid, Me.XrLine1, Me.labelNoNota, Me.labelTglJam, Me.XrLabel1, Me.XrPictureBox1, Me.gojekflag})
        Me.PageHeader.HeightF = 184.375!
        Me.PageHeader.Name = "PageHeader"
        '
        'labelisReprint
        '
        Me.labelisReprint.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.labelisReprint.Name = "labelisReprint"
        Me.labelisReprint.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelisReprint.SizeF = New System.Drawing.SizeF(79.16667!, 23.0!)
        Me.labelisReprint.StylePriority.UseTextAlignment = False
        Me.labelisReprint.Text = "Reprint"
        Me.labelisReprint.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'labelisVoid
        '
        Me.labelisVoid.LocationFloat = New DevExpress.Utils.PointFloat(182.8333!, 0!)
        Me.labelisVoid.Name = "labelisVoid"
        Me.labelisVoid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelisVoid.SizeF = New System.Drawing.SizeF(79.16667!, 23.0!)
        Me.labelisVoid.StylePriority.UseTextAlignment = False
        Me.labelisVoid.Text = "Nota Void"
        Me.labelisVoid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine1
        '
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 181.6667!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(262.0!, 2.0!)
        '
        'labelNoNota
        '
        Me.labelNoNota.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.labelNoNota.LocationFloat = New DevExpress.Utils.PointFloat(60.9583!, 158.6667!)
        Me.labelNoNota.Name = "labelNoNota"
        Me.labelNoNota.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelNoNota.SizeF = New System.Drawing.SizeF(201.0417!, 23.0!)
        Me.labelNoNota.StylePriority.UseFont = False
        Me.labelNoNota.StylePriority.UseTextAlignment = False
        Me.labelNoNota.Text = "Nomor Nota"
        Me.labelNoNota.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'labelTglJam
        '
        Me.labelTglJam.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.labelTglJam.LocationFloat = New DevExpress.Utils.PointFloat(60.9583!, 135.6667!)
        Me.labelTglJam.Name = "labelTglJam"
        Me.labelTglJam.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelTglJam.SizeF = New System.Drawing.SizeF(201.0417!, 23.0!)
        Me.labelTglJam.StylePriority.UseFont = False
        Me.labelTglJam.StylePriority.UseTextAlignment = False
        Me.labelTglJam.Text = "Tanggal; waktu"
        Me.labelTglJam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(9.999967!, 86.625!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(252.0!, 49.04165!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Alamat" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Telp"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(10.00002!, 23.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(252.0!, 63.625!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'gojekflag
        '
        Me.gojekflag.Image = CType(resources.GetObject("gojekflag.Image"), System.Drawing.Image)
        Me.gojekflag.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.MiddleCenter
        Me.gojekflag.LocationFloat = New DevExpress.Utils.PointFloat(0!, 144.9168!)
        Me.gojekflag.Name = "gojekflag"
        Me.gojekflag.SizeF = New System.Drawing.SizeF(60.9583!, 36.74994!)
        '
        'XrLine2
        '
        Me.XrLine2.LineWidth = 2
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0!, 185.2917!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(262.0!, 2.0!)
        '
        'labelFooter
        '
        Me.labelFooter.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.labelFooter.LocationFloat = New DevExpress.Utils.PointFloat(8.738296!, 210.5835!)
        Me.labelFooter.Multiline = True
        Me.labelFooter.Name = "labelFooter"
        Me.labelFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelFooter.SizeF = New System.Drawing.SizeF(252.0!, 35.91669!)
        Me.labelFooter.StylePriority.UseFont = False
        Me.labelFooter.StylePriority.UseTextAlignment = False
        Me.labelFooter.Text = "Thank You"
        Me.labelFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(43.85162!, 166.2917!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(92.70834!, 18.0!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Change"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLine5
        '
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(46.37504!, 80.16669!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(215.6251!, 6.166687!)
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(45.11331!, 62.16669!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(92.70833!, 18.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "Payment"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLine4
        '
        Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(45.11331!, 38.00001!)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.SizeF = New System.Drawing.SizeF(215.625!, 6.166679!)
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(45.11331!, 44.16669!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(92.70833!, 18.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Total"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(46.37504!, 20.00001!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(92.70834!, 18.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "Disc."
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(46.37497!, 2.000014!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(92.70831!, 18.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Sub Total"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLine3
        '
        Me.XrLine3.LineWidth = 2
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(262.0!, 2.0!)
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.lblarittacash, Me.XrLabel26, Me.XrLabel4, Me.XrLine6, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.LbVC, Me.LbCC, Me.LbCash, Me.labelSPG, Me.labelChange, Me.labelPayment, Me.labelTotal, Me.LabelDisc, Me.labelSubTotal, Me.XrLine3, Me.XrLabel5, Me.XrLabel6, Me.XrLabel7, Me.XrLine4, Me.XrLabel8, Me.XrLine5, Me.XrLabel9, Me.labelFooter, Me.XrLine2})
        Me.GroupFooter1.HeightF = 269.5001!
        Me.GroupFooter1.KeepTogether = True
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'lblarittacash
        '
        Me.lblarittacash.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblarittacash.LocationFloat = New DevExpress.Utils.PointFloat(139.0834!, 140.3334!)
        Me.lblarittacash.Name = "lblarittacash"
        Me.lblarittacash.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblarittacash.SizeF = New System.Drawing.SizeF(102.5!, 18.0!)
        Me.lblarittacash.StylePriority.UseFont = False
        Me.lblarittacash.StylePriority.UseTextAlignment = False
        Me.lblarittacash.Text = "999999"
        Me.lblarittacash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel26
        '
        Me.XrLabel26.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel26.LocationFloat = New DevExpress.Utils.PointFloat(46.37516!, 140.3334!)
        Me.XrLabel26.Name = "XrLabel26"
        Me.XrLabel26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel26.SizeF = New System.Drawing.SizeF(92.70833!, 18.0!)
        Me.XrLabel26.StylePriority.UseFont = False
        Me.XrLabel26.StylePriority.UseTextAlignment = False
        Me.XrLabel26.Text = "  Laritta Cash"
        Me.XrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(8.738065!, 246.5001!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(252.0003!, 23.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine6
        '
        Me.XrLine6.LocationFloat = New DevExpress.Utils.PointFloat(45.11336!, 160.1251!)
        Me.XrLine6.Name = "XrLine6"
        Me.XrLine6.SizeF = New System.Drawing.SizeF(215.625!, 6.166672!)
        '
        'XrLabel12
        '
        Me.XrLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(46.37505!, 122.3334!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(92.70834!, 18.00001!)
        Me.XrLabel12.StylePriority.UseFont = False
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "  Voucher"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel11
        '
        Me.XrLabel11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(46.37505!, 104.3334!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(92.70834!, 17.99999!)
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "  Debit/CC"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel10
        '
        Me.XrLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(46.37504!, 86.33337!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(92.70834!, 17.99999!)
        Me.XrLabel10.StylePriority.UseFont = False
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "  Cash"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'LbVC
        '
        Me.LbVC.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbVC.LocationFloat = New DevExpress.Utils.PointFloat(139.0833!, 122.3334!)
        Me.LbVC.Name = "LbVC"
        Me.LbVC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LbVC.SizeF = New System.Drawing.SizeF(102.5!, 18.0!)
        Me.LbVC.StylePriority.UseFont = False
        Me.LbVC.StylePriority.UseTextAlignment = False
        Me.LbVC.Text = "999999"
        Me.LbVC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'LbCC
        '
        Me.LbCC.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbCC.LocationFloat = New DevExpress.Utils.PointFloat(139.0834!, 104.3334!)
        Me.LbCC.Name = "LbCC"
        Me.LbCC.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LbCC.SizeF = New System.Drawing.SizeF(102.5001!, 18.0!)
        Me.LbCC.StylePriority.UseFont = False
        Me.LbCC.StylePriority.UseTextAlignment = False
        Me.LbCC.Text = "999999"
        Me.LbCC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'LbCash
        '
        Me.LbCash.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbCash.LocationFloat = New DevExpress.Utils.PointFloat(139.0834!, 86.33337!)
        Me.LbCash.Name = "LbCash"
        Me.LbCash.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LbCash.SizeF = New System.Drawing.SizeF(102.5001!, 18.0!)
        Me.LbCash.StylePriority.UseFont = False
        Me.LbCash.StylePriority.UseTextAlignment = False
        Me.LbCash.Text = "999999"
        Me.LbCash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'labelSPG
        '
        Me.labelSPG.Font = New System.Drawing.Font("Verdana", 8.0!)
        Me.labelSPG.LocationFloat = New DevExpress.Utils.PointFloat(8.738272!, 187.5835!)
        Me.labelSPG.Name = "labelSPG"
        Me.labelSPG.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelSPG.SizeF = New System.Drawing.SizeF(252.0!, 23.0!)
        Me.labelSPG.StylePriority.UseFont = False
        Me.labelSPG.StylePriority.UseTextAlignment = False
        Me.labelSPG.Text = "Nama SPG"
        Me.labelSPG.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'labelChange
        '
        Me.labelChange.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelChange.LocationFloat = New DevExpress.Utils.PointFloat(136.56!, 166.2917!)
        Me.labelChange.Name = "labelChange"
        Me.labelChange.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelChange.SizeF = New System.Drawing.SizeF(122.9167!, 18.0!)
        Me.labelChange.StylePriority.UseFont = False
        Me.labelChange.StylePriority.UseTextAlignment = False
        Me.labelChange.Text = "999999"
        Me.labelChange.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'labelPayment
        '
        Me.labelPayment.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelPayment.LocationFloat = New DevExpress.Utils.PointFloat(137.8216!, 62.16669!)
        Me.labelPayment.Name = "labelPayment"
        Me.labelPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelPayment.SizeF = New System.Drawing.SizeF(122.9167!, 18.0!)
        Me.labelPayment.StylePriority.UseFont = False
        Me.labelPayment.StylePriority.UseTextAlignment = False
        Me.labelPayment.Text = "999999"
        Me.labelPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'labelTotal
        '
        Me.labelTotal.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelTotal.LocationFloat = New DevExpress.Utils.PointFloat(137.8216!, 44.16669!)
        Me.labelTotal.Name = "labelTotal"
        Me.labelTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelTotal.SizeF = New System.Drawing.SizeF(122.9167!, 18.0!)
        Me.labelTotal.StylePriority.UseFont = False
        Me.labelTotal.StylePriority.UseTextAlignment = False
        Me.labelTotal.Text = "999999"
        Me.labelTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'LabelDisc
        '
        Me.LabelDisc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDisc.LocationFloat = New DevExpress.Utils.PointFloat(139.0834!, 20.00001!)
        Me.LabelDisc.Name = "LabelDisc"
        Me.LabelDisc.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.LabelDisc.SizeF = New System.Drawing.SizeF(122.9167!, 18.0!)
        Me.LabelDisc.StylePriority.UseFont = False
        Me.LabelDisc.StylePriority.UseTextAlignment = False
        Me.LabelDisc.Text = "999999"
        Me.LabelDisc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'labelSubTotal
        '
        Me.labelSubTotal.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelSubTotal.LocationFloat = New DevExpress.Utils.PointFloat(139.0833!, 2.0!)
        Me.labelSubTotal.Name = "labelSubTotal"
        Me.labelSubTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.labelSubTotal.SizeF = New System.Drawing.SizeF(122.9167!, 18.0!)
        Me.labelSubTotal.StylePriority.UseFont = False
        Me.labelSubTotal.StylePriority.UseTextAlignment = False
        Me.labelSubTotal.Text = "999999"
        Me.labelSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'DataSetPOS1
        '
        Me.DataSetPOS1.DataSetName = "DataSetPOS"
        Me.DataSetPOS1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Print_dtposTableAdapter1
        '
        Me.Print_dtposTableAdapter1.ClearBeforeFill = True
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel13, Me.Xrpointawal, Me.Xrpointakhir, Me.Xrkuponawal, Me.Xrkuponakhir, Me.XrLabel17, Me.XrLabel18, Me.XrLabel19})
        Me.GroupFooter2.HeightF = 37.29169!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'XrLabel13
        '
        Me.XrLabel13.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(152.8692!, 15.00003!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(55.0!, 15.0!)
        Me.XrLabel13.StylePriority.UseFont = False
        Me.XrLabel13.StylePriority.UsePadding = False
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Sesudah"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'Xrpointawal
        '
        Me.Xrpointawal.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Xrpointawal.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0!)
        Me.Xrpointawal.Name = "Xrpointawal"
        Me.Xrpointawal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Xrpointawal.SizeF = New System.Drawing.SizeF(52.86919!, 15.0!)
        Me.Xrpointawal.StylePriority.UseFont = False
        Me.Xrpointawal.StylePriority.UseTextAlignment = False
        Me.Xrpointawal.Text = "9999"
        Me.Xrpointawal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Xrpointakhir
        '
        Me.Xrpointakhir.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Xrpointakhir.LocationFloat = New DevExpress.Utils.PointFloat(207.8692!, 0!)
        Me.Xrpointakhir.Name = "Xrpointakhir"
        Me.Xrpointakhir.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Xrpointakhir.SizeF = New System.Drawing.SizeF(54.13078!, 15.0!)
        Me.Xrpointakhir.StylePriority.UseFont = False
        Me.Xrpointakhir.StylePriority.UseTextAlignment = False
        Me.Xrpointakhir.Text = "9999"
        Me.Xrpointakhir.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Xrkuponawal
        '
        Me.Xrkuponawal.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Xrkuponawal.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 15.00003!)
        Me.Xrkuponawal.Name = "Xrkuponawal"
        Me.Xrkuponawal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Xrkuponawal.SizeF = New System.Drawing.SizeF(52.86917!, 15.0!)
        Me.Xrkuponawal.StylePriority.UseFont = False
        Me.Xrkuponawal.StylePriority.UseTextAlignment = False
        Me.Xrkuponawal.Text = "9999"
        Me.Xrkuponawal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'Xrkuponakhir
        '
        Me.Xrkuponakhir.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Xrkuponakhir.LocationFloat = New DevExpress.Utils.PointFloat(207.8692!, 15.00003!)
        Me.Xrkuponakhir.Name = "Xrkuponakhir"
        Me.Xrkuponakhir.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.Xrkuponakhir.SizeF = New System.Drawing.SizeF(52.86917!, 15.0!)
        Me.Xrkuponakhir.StylePriority.UseFont = False
        Me.Xrkuponakhir.StylePriority.UseTextAlignment = False
        Me.Xrkuponakhir.Text = "9999"
        Me.Xrkuponakhir.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel17
        '
        Me.XrLabel17.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel17.StylePriority.UseFont = False
        Me.XrLabel17.StylePriority.UseTextAlignment = False
        Me.XrLabel17.Text = "Point Sebelum"
        Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel18
        '
        Me.XrLabel18.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(152.8692!, 0!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(55.0!, 15.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UsePadding = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "Sesudah"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel19
        '
        Me.XrLabel19.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(0!, 14.99999!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel19.StylePriority.UseFont = False
        Me.XrLabel19.StylePriority.UseTextAlignment = False
        Me.XrLabel19.Text = "Kupon Sebelum"
        Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'GroupFooter3
        '
        Me.GroupFooter3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrLabel23, Me.XrLabel24, Me.kodemembertxtnota, Me.pemiliksaldo, Me.XrLabel20, Me.XrLabel16, Me.XrLabel15, Me.XrLabel14, Me.saldosesudahtxt, Me.saldosebelumtxt, Me.XrLabel21, Me.XrLabel22, Me.XrLabel2})
        Me.GroupFooter3.HeightF = 129.1667!
        Me.GroupFooter3.Level = 2
        Me.GroupFooter3.Name = "GroupFooter3"
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(82.0!, 0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(101.7917!, 15.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Data Laritta Cash"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel23
        '
        Me.XrLabel23.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(0!, 15.0!)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel23.StylePriority.UseFont = False
        Me.XrLabel23.StylePriority.UseTextAlignment = False
        Me.XrLabel23.Text = "Kode Member"
        Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel24
        '
        Me.XrLabel24.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(82.0!, 15.0!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(11.21423!, 15.0!)
        Me.XrLabel24.StylePriority.UseFont = False
        Me.XrLabel24.StylePriority.UseTextAlignment = False
        Me.XrLabel24.Text = ":"
        Me.XrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'kodemembertxtnota
        '
        Me.kodemembertxtnota.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.kodemembertxtnota.LocationFloat = New DevExpress.Utils.PointFloat(94.02309!, 15.0!)
        Me.kodemembertxtnota.Name = "kodemembertxtnota"
        Me.kodemembertxtnota.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.kodemembertxtnota.SizeF = New System.Drawing.SizeF(166.7152!, 15.0!)
        Me.kodemembertxtnota.StylePriority.UseFont = False
        Me.kodemembertxtnota.StylePriority.UseTextAlignment = False
        Me.kodemembertxtnota.Text = "Nama Member"
        Me.kodemembertxtnota.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'pemiliksaldo
        '
        Me.pemiliksaldo.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.namamember, "Text", "")})
        Me.pemiliksaldo.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pemiliksaldo.LocationFloat = New DevExpress.Utils.PointFloat(94.02309!, 30.00003!)
        Me.pemiliksaldo.Name = "pemiliksaldo"
        Me.pemiliksaldo.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.pemiliksaldo.SizeF = New System.Drawing.SizeF(166.7152!, 15.0!)
        Me.pemiliksaldo.StylePriority.UseFont = False
        Me.pemiliksaldo.StylePriority.UseTextAlignment = False
        Me.pemiliksaldo.Text = "Nama Member"
        Me.pemiliksaldo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'namamember
        '
        Me.namamember.Name = "namamember"
        Me.namamember.ValueInfo = "Member"
        Me.namamember.Visible = False
        '
        'XrLabel20
        '
        Me.XrLabel20.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(82.0!, 60.00008!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(11.21423!, 15.0!)
        Me.XrLabel20.StylePriority.UseFont = False
        Me.XrLabel20.StylePriority.UseTextAlignment = False
        Me.XrLabel20.Text = ":"
        Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel16
        '
        Me.XrLabel16.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(82.0!, 45.00005!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(11.21423!, 15.0!)
        Me.XrLabel16.StylePriority.UseFont = False
        Me.XrLabel16.StylePriority.UseTextAlignment = False
        Me.XrLabel16.Text = ":"
        Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel15
        '
        Me.XrLabel15.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(0!, 60.00008!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Saldo Sesudah"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel14
        '
        Me.XrLabel14.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(0!, 45.00005!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel14.StylePriority.UseFont = False
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "Saldo Sebelum"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'saldosesudahtxt
        '
        Me.saldosesudahtxt.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.saldosesudahtxt.LocationFloat = New DevExpress.Utils.PointFloat(94.02309!, 60.00008!)
        Me.saldosesudahtxt.Name = "saldosesudahtxt"
        Me.saldosesudahtxt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.saldosesudahtxt.SizeF = New System.Drawing.SizeF(166.7152!, 15.0!)
        Me.saldosesudahtxt.StylePriority.UseFont = False
        Me.saldosesudahtxt.StylePriority.UseTextAlignment = False
        Me.saldosesudahtxt.Text = "9999"
        Me.saldosesudahtxt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'saldosebelumtxt
        '
        Me.saldosebelumtxt.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.saldosebelumtxt.LocationFloat = New DevExpress.Utils.PointFloat(94.02309!, 45.00005!)
        Me.saldosebelumtxt.Name = "saldosebelumtxt"
        Me.saldosebelumtxt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.saldosebelumtxt.SizeF = New System.Drawing.SizeF(166.7152!, 15.0!)
        Me.saldosebelumtxt.StylePriority.UseFont = False
        Me.saldosebelumtxt.StylePriority.UseTextAlignment = False
        Me.saldosebelumtxt.Text = "9999"
        Me.saldosebelumtxt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel21
        '
        Me.XrLabel21.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(82.0!, 30.00003!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(11.21423!, 15.0!)
        Me.XrLabel21.StylePriority.UseFont = False
        Me.XrLabel21.StylePriority.UseTextAlignment = False
        Me.XrLabel21.Text = ":"
        Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel22
        '
        Me.XrLabel22.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(0!, 30.00003!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(82.0!, 15.0!)
        Me.XrLabel22.StylePriority.UseFont = False
        Me.XrLabel22.StylePriority.UseTextAlignment = False
        Me.XrLabel22.Text = "Nama Member"
        Me.XrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(127.0591!, 82.4167!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(122.9168!, 23.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Cetakan Ke X"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'parsaldosesudah
        '
        Me.parsaldosesudah.Name = "parsaldosesudah"
        Me.parsaldosesudah.Type = GetType(Double)
        Me.parsaldosesudah.ValueInfo = "0"
        Me.parsaldosesudah.Visible = False
        '
        'Nota
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.GroupFooter1, Me.GroupFooter2, Me.GroupFooter3})
        Me.DataAdapter = Me.Print_dtposTableAdapter1
        Me.DataMember = "print_dtpos"
        Me.DataSource = Me.DataSetPOS1
        Me.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.Margins = New System.Drawing.Printing.Margins(19, 25, 25, 40)
        Me.PageHeight = 900
        Me.PageWidth = 307
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.namamember, Me.parsaldosesudah})
        Me.ShowPrintMarginsWarning = False
        Me.Version = "15.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetPOS1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents labelNoNota As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelTglJam As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents labelFooter As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents labelisReprint As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelisVoid As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelChange As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelPayment As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LabelDisc As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents labelSubTotal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents DataSetPOS1 As Laritta_POS.DataSetPOS
    Friend WithEvents Print_dtposTableAdapter1 As Laritta_POS.DataSetPOSTableAdapters.print_dtposTableAdapter
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents labelSPG As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LbVC As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LbCC As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents LbCash As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Xrpointawal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Xrpointakhir As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Xrkuponawal As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Xrkuponakhir As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupFooter3 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents namamember As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents pemiliksaldo As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents saldosesudahtxt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents saldosebelumtxt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents kodemembertxtnota As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents lblarittacash As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel26 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents gojekflag As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents parsaldosesudah As DevExpress.XtraReports.Parameters.Parameter
End Class
