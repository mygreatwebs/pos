﻿Public Class Stok

    Private Sub Stok_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        Dim oldlogin As Integer = shiftLogin
        Try
            XrLabel1.Text = "Cabang " & xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("namaCabang")
            XrLabel2.Text = Format(Now, "dd-MM-yyyy HH:mm")

            If isprintclosingx Then
                shiftLogin = selected_idxRow
                isprintclosingx = False
            End If

            SQLquery = String.Format("SELECT MIN(startTime) as minstart, MAX(endTime) as maxend FROM mtswitchuser " & _
                                     "WHERE (status = 1) AND (idCabang = {0}) AND (shiftKe = {1}) AND (DATE(startTime)='{2}')",
                                     id_cabang,
                                     1,
                                     Format(TglShiftNow, "yyyy-MM-dd"))

            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "GETTIMES")
            Dim startDate As String = Format(xSet.Tables("GETTIMES").Rows(0).Item("minstart"), "yyyy-MM-dd HH:mm:ss")
            Dim endDate As String = Format(xSet.Tables("GETTIMES").Rows(0).Item("minstart"), "yyyy-MM-dd 23:59:59")
            Print_stokTableAdapter1.Adapter.SelectCommand.CommandText = "SELECT a.namaBrg, c.namaKategori2, IFNULL(b_1.qttIN, 0) AS qttIN, IFNULL(b_1.qttOUT, 0) AS qttOUT, IFNULL(b_1.qttIN, 0) + IFNULL(b_1.qttOUT, 0) AS Balance " & _
                               " FROM            mbarang a LEFT OUTER JOIN " & _
                               " (SELECT        a.idBrg, a.qttIN, a.qttOUT " & _
                               " FROM            dtclosing a LEFT OUTER JOIN " & _
                               " mtclosing b ON a.idClosing = b.idClosing " & _
                               " WHERE       (b.no_urut_closing = @idClosing) AND (b.idCabang = @idCabang) AND (b.createDate BETWEEN @startDate AND @endDate)) b_1 ON  " & _
                               " a.idBrg = b_1.idBrg LEFT OUTER JOIN mkategoribrg2 c ON a.idKategori2 = c.idKategori2  WHERE (a.isToko = 1) AND (a.isToko=1) AND (a.Inactive = 0) " & _
                               " and (-1*IFNULL(b_1.qttOUT, 0)) + IFNULL(b_1.qttIN, 0)<>0 GROUP BY a.idBrg ORDER BY c.namaKategori2, a.namaBrg  "

            Print_stokTableAdapter1.ClearBeforeFill = True

            Print_stokTableAdapter1.Fill(DataSetPOS1.print_stok, shiftLogin, id_cabang, startDate, endDate)
        Catch ex As Exception
            msgboxWarning("Stock tidak dapat di print karena perbedaan tanggal shift" + ex.Message)
        Finally
            xSet.Tables.Remove("GETTIMES")
        End Try
        shiftLogin = oldlogin
    End Sub

    Private Sub Stok_PrintProgress(sender As Object, e As DevExpress.XtraPrinting.PrintProgressEventArgs) Handles Me.PrintProgress

    End Sub
End Class