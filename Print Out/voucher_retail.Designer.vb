﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class voucher_retail
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(voucher_retail))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.KodeBenefit = New DevExpress.XtraReports.UI.XRLabel()
        Me.kodevoucher = New DevExpress.XtraReports.Parameters.Parameter()
        Me.RewardName = New DevExpress.XtraReports.UI.XRLabel()
        Me.reward = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.JenisBenefit = New DevExpress.XtraReports.UI.XRLabel()
        Me.tipevoucher = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.expvoucher = New DevExpress.XtraReports.Parameters.Parameter()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.DataCabang = New DevExpress.XtraReports.UI.XRLabel()
        Me.alamat = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.LabelFooter = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.cetakanke = New DevExpress.XtraReports.Parameters.Parameter()
        Me.RealTimeSource1 = New DevExpress.Data.RealTimeSource()
        Me.keteranganVoucher = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.KodeBenefit, Me.RewardName, Me.XrLabel2, Me.XrLabel3, Me.XrLabel4, Me.XrLabel5, Me.JenisBenefit, Me.XrLabel7, Me.XrLabel8, Me.XrLabel1})
        Me.Detail.Dpi = 254.0!
        Me.Detail.HeightF = 343.7061!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel9
        '
        Me.XrLabel9.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.keteranganVoucher, "Text", "")})
        Me.XrLabel9.Dpi = 254.0!
        Me.XrLabel9.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0!, 291.6003!)
        Me.XrLabel9.Multiline = True
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(714.0!, 52.10574!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'KodeBenefit
        '
        Me.KodeBenefit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.kodevoucher, "Text", "")})
        Me.KodeBenefit.Dpi = 254.0!
        Me.KodeBenefit.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.KodeBenefit.LocationFloat = New DevExpress.Utils.PointFloat(221.8752!, 84.2187!)
        Me.KodeBenefit.Name = "KodeBenefit"
        Me.KodeBenefit.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.KodeBenefit.SizeF = New System.Drawing.SizeF(490.5623!, 58.42!)
        Me.KodeBenefit.StylePriority.UseFont = False
        Me.KodeBenefit.StylePriority.UsePadding = False
        Me.KodeBenefit.StylePriority.UseTextAlignment = False
        Me.KodeBenefit.Text = "Kode Benefit"
        Me.KodeBenefit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'kodevoucher
        '
        Me.kodevoucher.Name = "kodevoucher"
        Me.kodevoucher.Visible = False
        '
        'RewardName
        '
        Me.RewardName.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.reward, "Text", "")})
        Me.RewardName.Dpi = 254.0!
        Me.RewardName.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.RewardName.LocationFloat = New DevExpress.Utils.PointFloat(221.875!, 142.6387!)
        Me.RewardName.Name = "RewardName"
        Me.RewardName.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.RewardName.SizeF = New System.Drawing.SizeF(490.5625!, 90.54166!)
        Me.RewardName.StylePriority.UseFont = False
        Me.RewardName.StylePriority.UsePadding = False
        Me.RewardName.StylePriority.UseTextAlignment = False
        Me.RewardName.Text = "Nama Reward"
        Me.RewardName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'reward
        '
        Me.reward.Name = "reward"
        Me.reward.Visible = False
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 254.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(23.43752!, 84.2187!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(171.9792!, 58.42!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UsePadding = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "Code"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 254.0!
        Me.XrLabel3.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(23.43752!, 142.6387!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(171.9792!, 58.42!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UsePadding = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "Reward"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 254.0!
        Me.XrLabel4.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(195.4167!, 84.2187!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(26.45836!, 58.41999!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UsePadding = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = ":"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 254.0!
        Me.XrLabel5.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(195.4167!, 142.6387!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(26.45836!, 58.42001!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UsePadding = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = ":"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'JenisBenefit
        '
        Me.JenisBenefit.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.tipevoucher, "Text", "")})
        Me.JenisBenefit.Dpi = 254.0!
        Me.JenisBenefit.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.JenisBenefit.LocationFloat = New DevExpress.Utils.PointFloat(221.8752!, 0!)
        Me.JenisBenefit.Name = "JenisBenefit"
        Me.JenisBenefit.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.JenisBenefit.SizeF = New System.Drawing.SizeF(490.5623!, 84.2187!)
        Me.JenisBenefit.StylePriority.UseFont = False
        Me.JenisBenefit.StylePriority.UsePadding = False
        Me.JenisBenefit.StylePriority.UseTextAlignment = False
        Me.JenisBenefit.Text = "VOUCHER BUY 1 GET 1 FREE"
        Me.JenisBenefit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'tipevoucher
        '
        Me.tipevoucher.Name = "tipevoucher"
        Me.tipevoucher.Visible = False
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 254.0!
        Me.XrLabel7.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(23.43752!, 0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(171.9792!, 58.42!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UsePadding = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Type"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 254.0!
        Me.XrLabel8.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(195.4168!, 0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(26.45836!, 58.41999!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UsePadding = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = ":"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.expvoucher, "Text", "")})
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(145.1039!, 233.1803!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(543.8961!, 58.42001!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UsePadding = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Valid until 15 June 2017"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'expvoucher
        '
        Me.expvoucher.Name = "expvoucher"
        Me.expvoucher.Visible = False
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 64.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 63.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.DataCabang, Me.XrLine1, Me.XrPictureBox1, Me.XrPageInfo1})
        Me.ReportHeader.Dpi = 254.0!
        Me.ReportHeader.HeightF = 383.6458!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'DataCabang
        '
        Me.DataCabang.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.alamat, "Text", "")})
        Me.DataCabang.Dpi = 254.0!
        Me.DataCabang.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.DataCabang.LocationFloat = New DevExpress.Utils.PointFloat(0!, 175.6601!)
        Me.DataCabang.Multiline = True
        Me.DataCabang.Name = "DataCabang"
        Me.DataCabang.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DataCabang.SizeF = New System.Drawing.SizeF(714.0001!, 124.5658!)
        Me.DataCabang.StylePriority.UseFont = False
        Me.DataCabang.StylePriority.UseTextAlignment = False
        Me.DataCabang.Text = "Alamat" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Telp"
        Me.DataCabang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'alamat
        '
        Me.alamat.Name = "alamat"
        Me.alamat.ValueInfo = "Jl. Bratang Binangun No. 21 - Surabaya"
        Me.alamat.Visible = False
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 254.0!
        Me.XrLine1.LineWidth = 5
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 358.6458!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(714.0!, 13.22919!)
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 254.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(183.3333!, 0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(362.2675!, 161.6075!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 254.0!
        Me.XrPageInfo1.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.XrPageInfo1.Format = "{0:dddd, MMMM d, yyyy}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(64.22898!, 300.2259!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(624.771!, 58.41998!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2, Me.LabelFooter, Me.XrLabel6})
        Me.ReportFooter.Dpi = 254.0!
        Me.ReportFooter.HeightF = 227.9651!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 254.0!
        Me.XrLine2.LineWidth = 5
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0!, 0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(714.0002!, 13.22919!)
        '
        'LabelFooter
        '
        Me.LabelFooter.Dpi = 254.0!
        Me.LabelFooter.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFooter.LocationFloat = New DevExpress.Utils.PointFloat(0!, 13.22917!)
        Me.LabelFooter.Multiline = True
        Me.LabelFooter.Name = "LabelFooter"
        Me.LabelFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.LabelFooter.SizeF = New System.Drawing.SizeF(714.0!, 156.3158!)
        Me.LabelFooter.StylePriority.UseFont = False
        Me.LabelFooter.StylePriority.UseTextAlignment = False
        Me.LabelFooter.Text = "Mohon membawa bukti print ini pada saat melakukan penukaran" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Terima kasih" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "www.la" &
    "rittabakery.com"
        Me.LabelFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.cetakanke, "Text", "")})
        Me.XrLabel6.Dpi = 254.0!
        Me.XrLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(221.8752!, 169.545!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 3, 0, 254.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(492.125!, 58.42001!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UsePadding = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "cetakan"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'cetakanke
        '
        Me.cetakanke.Name = "cetakanke"
        Me.cetakanke.ValueInfo = "cetakan ke-1"
        Me.cetakanke.Visible = False
        '
        'RealTimeSource1
        '
        Me.RealTimeSource1.DisplayableProperties = Nothing
        Me.RealTimeSource1.UseWeakEventHandler = True
        '
        'keteranganVoucher
        '
        Me.keteranganVoucher.Name = "keteranganVoucher"
        Me.keteranganVoucher.Visible = False
        '
        'voucher_retail
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.ReportFooter})
        Me.Dpi = 254.0!
        Me.Margins = New System.Drawing.Printing.Margins(32, 34, 64, 63)
        Me.PageHeight = 1194
        Me.PageWidth = 780
        Me.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.kodevoucher, Me.reward, Me.alamat, Me.cetakanke, Me.tipevoucher, Me.expvoucher, Me.keteranganVoucher})
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ShowPrintMarginsWarning = False
        Me.SnapGridSize = 80.645!
        Me.Version = "15.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents KodeBenefit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents kodevoucher As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents RewardName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents reward As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents JenisBenefit As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents DataCabang As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents alamat As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents LabelFooter As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents RealTimeSource1 As DevExpress.Data.RealTimeSource
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents cetakanke As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents tipevoucher As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents expvoucher As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents keteranganVoucher As DevExpress.XtraReports.Parameters.Parameter
End Class
