﻿Imports MySql.Data.MySqlClient

Public Class Nota

    Dim pagectr As Long = 0
    Dim cetke As Integer = 0

    Private Sub Nota_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        If isDummy Then Exit Sub
        labelisReprint.Visible = isReprint

        XrLabel1.Text = String.Format("{0}{1}Telp : {2}", xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("alamatCabang"), vbCrLf, xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("telpCabang"))
        labelTglJam.Text = Format(Now, "dd-MM-yyyy HH:mm")
        'printnota
        labelNoNota.Text = String.Format("No : SO/{0}/{1}/{2}",
                                      Format(Year(xSet.Tables("printNota").Rows(0).Item("createDate")), "00").Substring(2, 2),
                                      Format(Month(xSet.Tables("printNota").Rows(0).Item("createDate")), "00"),
                                      Format(xSet.Tables("printNota").Rows(0).Item("numOrder"), "000000"))

        labelisVoid.Visible = xSet.Tables("printNota").Rows(0).Item("Void")
        labelFooter.Text = xSet.Tables("printNota").Rows(0).Item("footertext")
        labelSubTotal.Text = String.Format("{0:n0}", xSet.Tables("printNota").Rows(0).Item("subTotal"))
        LabelDisc.Text = String.Format("{0:n0}", xSet.Tables("printNota").Rows(0).Item("disc"))
        labelTotal.Text = String.Format("{0:n0}", xSet.Tables("printNota").Rows(0).Item("subTotal") - xSet.Tables("printNota").Rows(0).Item("disc"))
        labelPayment.Text = String.Format("{0:n0}", xSet.Tables("printNota").Rows(0).Item("totalPayment"))
        labelChange.Text = String.Format("{0:n0}", xSet.Tables("printNota").Rows(0).Item("totalChange"))
        labelSPG.Text = "SPG : " & xSet.Tables("printNota").Rows(0).Item("namaSPG")
        XrLabel4.Text = xSet.Tables("printNota").Rows(0).Item("promodeskripsi")

        'aktif or non aktif gojek
        If xSet.Tables("printNota").Rows(0).Item("jenis_transaksi").ToString = "0" Then
            gojekflag.Visible = False
        Else
            gojekflag.Visible = True
        End If

        'member emoney
        kodemembertxtnota.Text = "-"
        saldosebelumtxt.Text = "-"
        saldosesudahtxt.Text = "-"
        pemiliksaldo.Text = "-"
        If namapemilikkartu = "" Then
            namapemilikkartu = "-"
        End If

        If ishavemember = True Then
            kodemembertxtnota.Text = kodemember
        End If

        If useemoney = True Then
            If saldosebelum = 0 Then
                saldosebelumtxt.Text = "Rp.0,-"
            Else
                saldosebelumtxt.Text = "Rp." & Format(saldosebelum, "#,###") & ",-"
            End If

            If saldosesudah = 0 Then
                saldosesudahtxt.Text = "Rp.0,-"
            Else
                saldosesudahtxt.Text = "Rp." & Format(saldosesudah, "#,###") & ",-"
            End If

            'pemiliksaldo.Text = namapemilikkartu
            kodemembertxtnota.Text = kodemember
        End If
        'print bayar
        If xSet.Tables("printbayar").Select("idKategoriPayment=1").Count > 0 Then
            LbCash.Text = String.Format("{0:n0}", xSet.Tables("printbayar").Select("idKategoriPayment=1")(0)(1))
        Else
            LbCash.Text = 0
        End If

        If xSet.Tables("printbayar").Select("idKategoriPayment=2").Count > 0 Then
            LbCC.Text = String.Format("{0:n0}", xSet.Tables("printbayar").Select("idKategoriPayment=2")(0)(1))
        Else
            LbCC.Text = 0
        End If

        If xSet.Tables("printbayar").Select("idKategoriPayment=3").Count > 0 Then
            LbVC.Text = String.Format("{0:n0}", xSet.Tables("printbayar").Select("idKategoriPayment=3")(0)(1))
        Else
            LbVC.Text = 0
        End If

        If xSet.Tables("printbayar").Select("idKategoriPayment=6").Count > 0 Then
            lblarittacash.Text = String.Format("{0:n0}", xSet.Tables("printbayar").Select("idKategoriPayment=6")(0)(1))
        Else
            lblarittacash.Text = 0.ToString
        End If

        'cetakan ke
        If IsNothing(xSet.Tables("cetakanke")) Then
            cetke = 1
        Else
            If xSet.Tables("cetakanke").Rows.Count = 0 Then
                cetke = 1
            Else
                cetke = xSet.Tables("cetakanke").Rows(0)("counter")
                cetke = cetke + 1
            End If
        End If
        If IsNothing(xSet.Tables("cetakinfopoint")) Then
            Xrpointawal.Text = ""
            Xrpointakhir.Text = ""
            Xrkuponawal.Text = ""
            Xrkuponakhir.Text = ""
        Else
            If xSet.Tables("cetakinfopoint").Rows.Count = 0 Then
                Xrpointawal.Text = ""
                Xrpointakhir.Text = Int((xSet.Tables("printNota").Rows(0).Item("subTotal") - xSet.Tables("printNota").Rows(0).Item("disc")) / 2500)
                Xrkuponawal.Text = ""
                Xrkuponakhir.Text = Int(Int((xSet.Tables("printNota").Rows(0).Item("subTotal") - xSet.Tables("printNota").Rows(0).Item("disc")) / 2500) / 10)
            Else
                Xrpointawal.Text = Format(Int(xSet.Tables("cetakinfopoint").Rows(0)("pointawal")), "#.###")
                Xrpointakhir.Text = Format(Int(xSet.Tables("cetakinfopoint").Rows(0)("pointakhir")), "#.###")
                Xrkuponawal.Text = Format(Int(xSet.Tables("cetakinfopoint").Rows(0)("kuponawal")), "#.###")
                Xrkuponakhir.Text = Format(Int(xSet.Tables("cetakinfopoint").Rows(0)("kuponakhir")), "#.###")
            End If
        End If
        XrLabel2.Text = "Cetakan Ke  : " & cetke
        'kodemembertxtnota.Text = kodemember
        'GroupFooter2.Visible = Not (Trim(kodemember) = "")
        'kodemember = ""
        Print_dtposTableAdapter1.ClearBeforeFill = True
        Print_dtposTableAdapter1.Fill(DataSetPOS1.print_dtpos, selected_idxRow)

        'PageHeight = 470 + (DataSetPOS1.print_dtpos.Rows.Count * Detail.HeightF)
        'MsgBox(PageHeight)
        'PageHeight = (TopMargin.HeightF + PageHeader.HeightF) + (DataSetPOS1.print_dtpos.Rows.Count * Detail.HeightF) + (GroupFooter1.HeightF + BottomMargin.HeightF) + 165
        'MsgBox(PageHeight)
    End Sub

    Private Sub TopMargin_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles TopMargin.BeforePrint
        pagectr = pagectr + 1
        'If pagectr > 1 Then
        'Margins.Top = 0.21
        'End If
    End Sub

    Private Sub PageHeader_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles PageHeader.BeforePrint
        If pagectr > 1 Then e.Cancel = True
    End Sub

    Private Sub Nota_PrintProgress(sender As Object, e As DevExpress.XtraPrinting.PrintProgressEventArgs) Handles Me.PrintProgress
        If e.PageIndex = Me.Pages.Count - 1 Then
            SimpanCetakanKe("POS", cetke)
        End If
    End Sub

End Class