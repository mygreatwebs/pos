﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class promo10rbmalang
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colnamaBrg = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colkategori = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coljmlh = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colidBrg = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.price = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.PanelControl1.Appearance.Options.UseBackColor = True
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.PanelControl1.Controls.Add(Me.butDelete)
        Me.PanelControl1.Controls.Add(Me.butOK)
        Me.PanelControl1.Controls.Add(Me.GridControl1)
        Me.PanelControl1.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(499, 406)
        Me.PanelControl1.TabIndex = 2
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDelete.Location = New System.Drawing.Point(252, 321)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(114, 70)
        Me.butDelete.TabIndex = 33
        Me.butDelete.Text = "Tidak"
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butOK.Location = New System.Drawing.Point(372, 321)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(114, 70)
        Me.butOK.TabIndex = 32
        Me.butOK.Text = "Ya"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 5)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(474, 310)
        Me.GridControl1.TabIndex = 31
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colnamaBrg, Me.price, Me.colkategori, Me.coljmlh, Me.colidBrg})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.RowHeight = 20
        '
        'colnamaBrg
        '
        Me.colnamaBrg.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnamaBrg.AppearanceCell.Options.UseFont = True
        Me.colnamaBrg.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnamaBrg.AppearanceHeader.Options.UseFont = True
        Me.colnamaBrg.Caption = "Barang"
        Me.colnamaBrg.FieldName = "namaBrg"
        Me.colnamaBrg.Name = "colnamaBrg"
        Me.colnamaBrg.OptionsColumn.AllowEdit = False
        Me.colnamaBrg.Visible = True
        Me.colnamaBrg.VisibleIndex = 0
        Me.colnamaBrg.Width = 227
        '
        'colkategori
        '
        Me.colkategori.Caption = "Kategori"
        Me.colkategori.FieldName = "namaKategori2"
        Me.colkategori.Name = "colkategori"
        Me.colkategori.OptionsColumn.AllowEdit = False
        Me.colkategori.Width = 115
        '
        'coljmlh
        '
        Me.coljmlh.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.coljmlh.AppearanceCell.Options.UseFont = True
        Me.coljmlh.AppearanceCell.Options.UseTextOptions = True
        Me.coljmlh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.coljmlh.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.coljmlh.AppearanceHeader.Options.UseFont = True
        Me.coljmlh.AppearanceHeader.Options.UseTextOptions = True
        Me.coljmlh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.coljmlh.Caption = "Qty"
        Me.coljmlh.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.coljmlh.FieldName = "jmlh"
        Me.coljmlh.Name = "coljmlh"
        Me.coljmlh.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "jmlh", "{0:0.##}")})
        Me.coljmlh.Visible = True
        Me.coljmlh.VisibleIndex = 2
        Me.coljmlh.Width = 66
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Mask.EditMask = "###.###,###,##;"
        Me.RepositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        Me.RepositoryItemTextEdit1.NullText = "0"
        '
        'colidBrg
        '
        Me.colidBrg.FieldName = "idBrg"
        Me.colidBrg.Name = "colidBrg"
        '
        'price
        '
        Me.price.Caption = "Harga"
        Me.price.FieldName = "price"
        Me.price.Name = "price"
        Me.price.Visible = True
        Me.price.VisibleIndex = 1
        '
        'promo10rbmalang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 429)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "promo10rbmalang"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Promo BRI"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colnamaBrg As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colkategori As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coljmlh As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colidBrg As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents price As DevExpress.XtraGrid.Columns.GridColumn
End Class
