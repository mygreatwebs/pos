﻿Public Class promo10rbmalang
    Private Sub promo10rbmalang_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If IsNothing(xSet.Tables("listbarang")) Then
                xSet.Tables.Add("listbarang")
                xSet.Tables("listbarang").Columns.Add("idBrg")
                xSet.Tables("listbarang").Columns.Add("namaBrg")
                xSet.Tables("listbarang").Columns.Add("namaKategori2")
                xSet.Tables("listbarang").Columns.Add("jmlh")
                xSet.Tables("listbarang").Columns.Add("price")
            End If
            SQLquery = "SELECT
  mb.idBrg,
  mb.namaBrg,
  mk.namaKategori2,
mb.price,
  0 `jmlh`
FROM mbarang mb
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2
WHERE mb.Inactive='0' "
            xSet.Tables("listbarang").Clear()
            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarang")
            GridControl1.DataSource = xSet.Tables("listbarang")

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        Me.Close()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Try
            'Dim jumlahpotongan As Integer = FormPOSx.GridView1.GetRowCellValue(letakrowpromo, "jmlh")

            'deletepromo(False, jumlahpotongan)
            If CDbl(FormPOSx.txtTotalBeli.Text) < 100000 Then
                MessageBox.Show("Promo tidak berlaku untuk pembelian dibawah 100.000 rupiah", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.Close()
                Exit Sub
            End If
            Dim countharga As Integer
            Dim countjumlah As Integer
            Dim selectedsoes() As DataRow = xSet.Tables("listbarang").Select("jmlh>0")
            Dim hargasatuan As Double
            For Each soes In selectedsoes
                countharga = countharga + soes.Item("price") * soes.Item("jmlh")
                countjumlah = countjumlah + soes.Item("jmlh")
            Next soes
            xSet.Tables("setSales").Rows.Add(1170,
                                                        "PROMO GRATIS ROTI - MALANG",
                                                        1,
                                                        0,
                                                        0,
                                                        0)
            If countharga < 10000 Then
                'MessageBox.Show("Jumlah soes tidak sesuai ketentuan kupon kalender", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                'Exit Sub
                hargasatuan = 10000 / countjumlah
                For Each soes In selectedsoes
                    xSet.Tables("setSales").Rows.Add(soes.Item("idBrg"),
                                                        soes.Item("namaBrg"),
                                                        soes.Item("jmlh"),
                                                        soes.Item("jmlh") * 0,
                                                        0,
                                                        0)
                Next soes
            Else
                countharga = 0
                Dim hargapcs As Double
                Dim hargasebelum As Double = 0
                For Each soes In selectedsoes
                    countharga = countharga + (soes.Item("price") * soes.Item("jmlh"))

                    If countharga > 10000 And hargasebelum < 10000 And hargasebelum > 0 Then
                        hargapcs = (countharga - 10000) / soes.Item("jmlh")
                        hargasebelum = countharga
                    ElseIf countharga > 10000 And hargasebelum = 0 Then
                        hargapcs = (countharga - 10000) / soes.Item("jmlh")
                        hargasebelum = countharga
                    ElseIf countharga <= 10000 Then
                        hargapcs = 0
                        hargasebelum = countharga
                    Else
                        hargapcs = soes.Item("price")
                        hargasebelum = countharga
                    End If
                    xSet.Tables("setSales").Rows.Add(soes.Item("idBrg"),
                                                        soes.Item("namaBrg"),
                                                        soes.Item("jmlh"),
                                                        soes.Item("jmlh") * hargapcs,
                                                        hargapcs,
                                                        0)
                Next soes
            End If

            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class