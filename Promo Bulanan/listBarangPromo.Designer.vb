﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class listBarangPromo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.idrewardsub = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.idBrg = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.namaBrg = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.namaKategori2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jmlhsub = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.idrewardgrid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.namaRewardgrid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.max_paket = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jenis_rewardgrid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jumlah_paket_itemgrid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.harga_paketgrid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.butDelete = New DevExpress.XtraEditors.SimpleButton()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.idreward = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.namaReward = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.harga_paket = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jenis_reward = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.jumlah_paket_item = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.butOK = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.idrewardsub, Me.idBrg, Me.namaBrg, Me.namaKategori2, Me.jmlhsub})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'idrewardsub
        '
        Me.idrewardsub.Caption = "idrewardsub"
        Me.idrewardsub.FieldName = "idreward"
        Me.idrewardsub.Name = "idrewardsub"
        '
        'idBrg
        '
        Me.idBrg.Caption = "idBrg"
        Me.idBrg.FieldName = "idBrg"
        Me.idBrg.Name = "idBrg"
        '
        'namaBrg
        '
        Me.namaBrg.Caption = "Nama Barang"
        Me.namaBrg.FieldName = "namaBrg"
        Me.namaBrg.Name = "namaBrg"
        Me.namaBrg.OptionsColumn.ReadOnly = True
        Me.namaBrg.Visible = True
        Me.namaBrg.VisibleIndex = 0
        '
        'namaKategori2
        '
        Me.namaKategori2.Caption = "Kategori"
        Me.namaKategori2.FieldName = "namaKategori2"
        Me.namaKategori2.Name = "namaKategori2"
        Me.namaKategori2.OptionsColumn.ReadOnly = True
        Me.namaKategori2.Visible = True
        Me.namaKategori2.VisibleIndex = 1
        '
        'jmlhsub
        '
        Me.jmlhsub.Caption = "Jumlah"
        Me.jmlhsub.ColumnEdit = Me.RepositoryItemTextEdit2
        Me.jmlhsub.FieldName = "jmlh"
        Me.jmlhsub.Name = "jmlhsub"
        Me.jmlhsub.Visible = True
        Me.jmlhsub.VisibleIndex = 2
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        Me.RepositoryItemTextEdit2.NullText = "0"
        '
        'GridControl1
        '
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        GridLevelNode1.LevelTemplate = Me.GridView1
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(5, 4)
        Me.GridControl1.MainView = Me.GridView2
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(791, 439)
        Me.GridControl1.TabIndex = 31
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView1})
        '
        'GridView2
        '
        Me.GridView2.Appearance.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView2.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView2.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView2.Appearance.Row.Options.UseFont = True
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.idrewardgrid, Me.namaRewardgrid, Me.max_paket, Me.jenis_rewardgrid, Me.jumlah_paket_itemgrid, Me.harga_paketgrid})
        Me.GridView2.GridControl = Me.GridControl1
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.RowHeight = 20
        '
        'idrewardgrid
        '
        Me.idrewardgrid.Caption = "idreward"
        Me.idrewardgrid.FieldName = "idreward"
        Me.idrewardgrid.Name = "idrewardgrid"
        '
        'namaRewardgrid
        '
        Me.namaRewardgrid.Caption = "Nama Reward"
        Me.namaRewardgrid.FieldName = "namaReward"
        Me.namaRewardgrid.Name = "namaRewardgrid"
        Me.namaRewardgrid.Visible = True
        Me.namaRewardgrid.VisibleIndex = 0
        '
        'max_paket
        '
        Me.max_paket.Caption = "max_paket"
        Me.max_paket.FieldName = "max_paket"
        Me.max_paket.Name = "max_paket"
        '
        'jenis_rewardgrid
        '
        Me.jenis_rewardgrid.Caption = "jenis_rewardgrid"
        Me.jenis_rewardgrid.FieldName = "jenis_reward"
        Me.jenis_rewardgrid.Name = "jenis_rewardgrid"
        '
        'jumlah_paket_itemgrid
        '
        Me.jumlah_paket_itemgrid.Caption = "jumlah_paket_itemgrid"
        Me.jumlah_paket_itemgrid.FieldName = "jumlah_paket_item"
        Me.jumlah_paket_itemgrid.Name = "jumlah_paket_itemgrid"
        '
        'harga_paketgrid
        '
        Me.harga_paketgrid.Caption = "Harga Paket"
        Me.harga_paketgrid.DisplayFormat.FormatString = "n2"
        Me.harga_paketgrid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.harga_paketgrid.FieldName = "harga_paket"
        Me.harga_paketgrid.Name = "harga_paketgrid"
        Me.harga_paketgrid.Visible = True
        Me.harga_paketgrid.VisibleIndex = 1
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.PanelControl2.Appearance.Options.UseBackColor = True
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
        Me.PanelControl2.Controls.Add(Me.butDelete)
        Me.PanelControl2.Controls.Add(Me.LookUpEdit1)
        Me.PanelControl2.Controls.Add(Me.butOK)
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(805, 524)
        Me.PanelControl2.TabIndex = 43
        '
        'butDelete
        '
        Me.butDelete.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butDelete.Appearance.Options.UseFont = True
        Me.butDelete.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butDelete.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butDelete.Location = New System.Drawing.Point(562, 449)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(114, 70)
        Me.butDelete.TabIndex = 35
        Me.butDelete.Text = "Tidak"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.EditValue = "[Pilih Reward]"
        Me.LookUpEdit1.Location = New System.Drawing.Point(5, 489)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.NullText = "[Pilih Reward]"
        Me.LookUpEdit1.Properties.View = Me.GridLookUpEdit1View
        Me.LookUpEdit1.Size = New System.Drawing.Size(322, 26)
        Me.LookUpEdit1.TabIndex = 42
        Me.LookUpEdit1.Visible = False
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.idreward, Me.namaReward, Me.harga_paket, Me.jenis_reward, Me.jumlah_paket_item})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'idreward
        '
        Me.idreward.Caption = "idreward"
        Me.idreward.FieldName = "idreward"
        Me.idreward.Name = "idreward"
        '
        'namaReward
        '
        Me.namaReward.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.namaReward.AppearanceCell.Options.UseFont = True
        Me.namaReward.Caption = "Nama Reward"
        Me.namaReward.FieldName = "namaReward"
        Me.namaReward.Name = "namaReward"
        Me.namaReward.Visible = True
        Me.namaReward.VisibleIndex = 0
        '
        'harga_paket
        '
        Me.harga_paket.Caption = "harga_paket"
        Me.harga_paket.FieldName = "harga_paket"
        Me.harga_paket.Name = "harga_paket"
        '
        'jenis_reward
        '
        Me.jenis_reward.Caption = "jenis_reward"
        Me.jenis_reward.FieldName = "jenis_reward"
        Me.jenis_reward.Name = "jenis_reward"
        '
        'jumlah_paket_item
        '
        Me.jumlah_paket_item.Caption = "jumlah_paket_item"
        Me.jumlah_paket_item.FieldName = "jumlah_paket_item"
        Me.jumlah_paket_item.Name = "jumlah_paket_item"
        '
        'butOK
        '
        Me.butOK.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butOK.Appearance.Options.UseFont = True
        Me.butOK.Image = Global.Laritta_POS.My.Resources.Resources.tick_32
        Me.butOK.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.butOK.Location = New System.Drawing.Point(682, 449)
        Me.butOK.Name = "butOK"
        Me.butOK.Size = New System.Drawing.Size(114, 70)
        Me.butOK.TabIndex = 34
        Me.butOK.Text = "Ya"
        '
        'listBarangPromo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 548)
        Me.Controls.Add(Me.PanelControl2)
        Me.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "listBarangPromo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "listBarangPromo"
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butDelete As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents idrewardgrid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents namaRewardgrid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents max_paket As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jenis_rewardgrid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jumlah_paket_itemgrid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents harga_paketgrid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents idrewardsub As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents idBrg As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents namaBrg As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents namaKategori2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jmlhsub As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents idreward As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents namaReward As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents harga_paket As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jenis_reward As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents jumlah_paket_item As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
End Class
