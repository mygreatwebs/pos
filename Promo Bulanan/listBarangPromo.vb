﻿Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraGrid.Views.Base
Public Class listBarangPromo
    Dim namapromo As String = ""
    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        cancelambilbarangpromo = True
        Me.Close()
    End Sub

    Public Sub callme(query As String, promoname As String)
        Try
            If IsNothing(xSet.Tables("listbarangpromo")) Then
                xSet.Tables.Add("listbarangpromo")
                xSet.Tables("listbarangpromo").Columns.Add("idBrg")
                xSet.Tables("listbarangpromo").Columns.Add("namaBrg")
                xSet.Tables("listbarangpromo").Columns.Add("namaKategori2")
                xSet.Tables("listbarangpromo").Columns.Add("jmlh")
            Else
                xSet.Tables("listbarangpromo").Clear()
            End If
            namapromo = promoname
            ExDb.ExecQuery(My.Settings.localconn, query, xSet, "listbarangpromo")
            GridControl1.DataSource = xSet.Tables("listbarangpromo")
            Me.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub GenerateReward(query As String, idpromo As String)
        Try
            cancelambilbarangpromo = False
            LookUpEdit1.EditValue = Nothing
            If IsNothing(xSet.Relations("DaftarBarang")) = False Then
                xSet.Relations.Remove("DaftarBarang")
                xSet.EnforceConstraints = False
            End If

            If IsNothing(xSet.Tables("listreward")) Then
                xSet.Tables.Add("listreward")
                xSet.Tables("listreward").Columns.Add("idreward", Type.GetType("System.Decimal"))
                xSet.Tables("listreward").Columns.Add("namaReward")
                xSet.Tables("listreward").Columns.Add("max_paket")
                xSet.Tables("listreward").Columns.Add("jenis_reward")
                xSet.Tables("listreward").Columns.Add("jumlah_paket_item")
                xSet.Tables("listreward").Columns.Add("harga_paket")
            Else
                xSet.Tables("listreward").Clear()
            End If

            If IsNothing(xSet.Tables("listbarangpromo")) Then
                xSet.Tables.Add("listbarangpromo")
                xSet.Tables("listbarangpromo").Columns.Add("idreward", Type.GetType("System.Decimal"))
                xSet.Tables("listbarangpromo").Columns.Add("idBrg")
                xSet.Tables("listbarangpromo").Columns.Add("namaBrg")
                xSet.Tables("listbarangpromo").Columns.Add("namaKategori2")
                xSet.Tables("listbarangpromo").Columns.Add("jmlh")
                xSet.Tables("listbarangpromo").Columns.Add("harga")
            Else
                xSet.Tables("listbarangpromo").Clear()
            End If

            If IsNothing(xSet.Tables("setSalesX")) Then
                xSet.Tables.Add("setSalesX")
                xSet.Tables("setSalesX").Columns.Add("idBrg", Type.GetType("System.Decimal"))
                xSet.Tables("setSalesX").Columns.Add("namaBrg", Type.GetType("System.String"))
                xSet.Tables("setSalesX").Columns.Add("jmlh", Type.GetType("System.Decimal"))
                xSet.Tables("setSalesX").Columns.Add("hrg", Type.GetType("System.Decimal"))
                xSet.Tables("setSalesX").Columns.Add("hrgpcs", Type.GetType("System.Decimal"))
                xSet.Tables("setSalesX").Columns.Add("disc", Type.GetType("System.Decimal"))
            Else
                xSet.Tables("setSalesX").Clear()
            End If

            ExDb.ExecQuery(My.Settings.localconn, query, xSet, "listreward")
            SQLquery = String.Format("SELECT mrp.idreward,idBrg,namaBrg,mk.namaKategori2,if(jenis_harga=0,drp.harga,mb.price-drp.harga) `harga` FROM m_reward_promo mrp
 JOIN d_reward_promo drp ON mrp.idreward=drp.idreward
JOIN mbarang mb ON drp.iditem=if(jenis_item=0,mb.idKategori2,mb.idBrg)
  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2 WHERE mb.inactive=0 AND mrp.idpromo='{0}'", idpromo)

            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarangpromo")

            xSet.EnforceConstraints = True
            Dim keyColumn As DataColumn = xSet.Tables("listreward").Columns("idreward")
            Dim foreignKeyColumn As DataColumn = xSet.Tables("listbarangpromo").Columns("idreward")
            xSet.Relations.Add("DaftarBarang", keyColumn, foreignKeyColumn)
            gridPrepare()

            'LookUpEdit1.Properties.DataSource = xSet.Tables("listreward")
            'LookUpEdit1.Properties.DisplayMember = "namaReward"
            'LookUpEdit1.Properties.ValueMember = "idreward"

            GridControl1.DataSource = xSet.Tables("listreward")
            GridControl1.ForceInitialize()

            Me.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub gridPrepare()
        GridControl1.LevelTree.Nodes.Add("DaftarBarang", GridView1)
        GridView1.PopulateColumns(xSet.Tables("listbarangpromo"))
        GridView1.OptionsView.ShowAutoFilterRow = True
        Dim repoHarga As RepositoryItemTextEdit = New RepositoryItemTextEdit
        Dim repojumlah As RepositoryItemTextEdit = New RepositoryItemTextEdit
        GridControl1.RepositoryItems.Add(repoHarga)
        GridControl1.RepositoryItems.Add(repojumlah)

        'hide grouping
        GridView1.OptionsView.ShowGroupPanel = False

        'hide Columns
        GridView1.Columns("idreward").Visible = False
        GridView1.Columns("idBrg").Visible = False

        'change columns caption
        GridView1.Columns("namaBrg").Caption = "Nama Barang"
        GridView1.Columns("namaKategori2").Caption = "Kategori Barang"

        'change editable columns
        GridView1.Columns("namaBrg").OptionsColumn.ReadOnly = True
        GridView1.Columns("namaKategori2").OptionsColumn.ReadOnly = True

        'repository jumlah
        repojumlah.NullText = "0"
        repojumlah.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        repojumlah.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        GridView1.Columns("jmlh").Caption = "Jumlah"
        GridView1.Columns("jmlh").ColumnEdit = repojumlah

        'repository Harga
        repoHarga.NullText = "0"
        repoHarga.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        repoHarga.DisplayFormat.FormatString = "n2"
        repoHarga.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        repoHarga.Mask.EditMask = "n2"
        repoHarga.ReadOnly = True

        GridView1.Columns("harga").Caption = "Harga"
        GridView1.Columns("harga").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns("harga").DisplayFormat.FormatString = "n2"
        GridView1.Columns("harga").ColumnEdit = repoHarga
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Try
            Dim hargatotal As Double
            Dim jumlahlegal As Integer = 0
            Select Case namapromo
                Case "promokemerdekaan"

                    Dim countwholebread As Integer = 0
                    Dim countcookies As Integer = 0
                    Dim countpastrypie As Integer = 0

                    Dim selectedbrg() As DataRow = xSet.Tables("listbarangpromo").Select("jmlh>0")

                    For Each brg In selectedbrg
                        If brg.Item("namaKategori2").ToString = "COOKIES" Then
                            countcookies = countcookies + CInt(brg.Item("jmlh"))
                        End If

                        If brg.Item("namaKategori2").ToString = "PASTRY & PIE" Then
                            countpastrypie = countpastrypie + CInt(brg.Item("jmlh"))
                        End If

                        If brg.Item("namaKategori2").ToString = "WHOLE BREAD" Then
                            countwholebread = countwholebread + CInt(brg.Item("jmlh"))
                        End If
                    Next brg

                    If countpastrypie > 0 And countpastrypie Mod 4 > 0 Then
                        MessageBox.Show("Jumlah item PASTRY & PIE harus kelipatan 4 dan maksimal 8 item!", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If

                    If countwholebread > 5 Then
                        MessageBox.Show("Jumlah item WHOLE BREAD tidak sesuai ketentuan!", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If

                    If countcookies > 3 Then
                        MessageBox.Show("Jumlah item COOKIES tidak sesuai ketentuan!", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If

                    If countpastrypie > 0 And countpastrypie / 4 > 2 Then
                        MessageBox.Show("Jumlah item PASTRY & PIE tidak sesuai ketentuan!", "System Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Exit Sub
                    End If

                    Dim hargabarangpromo As Double

                    For Each brg In selectedbrg
                        If brg.Item("namaKategori2").ToString = "COOKIES" Then
                            hargabarangpromo = 45000
                        ElseIf brg.Item("namaKategori2").ToString = "PASTRY & PIE" Then
                            hargabarangpromo = 4250
                        ElseIf brg.Item("namaKategori2").ToString = "WHOLE BREAD" Then
                            hargabarangpromo = 8000
                        End If

                        xSet.Tables("setSales").Rows.Add(brg.Item("idBrg"),
                                                    brg.Item("namaBrg"),
                                                    brg.Item("jmlh"),
                                                    CInt(brg.Item("jmlh")) * hargabarangpromo,
                                                    hargabarangpromo,
                                                    0)
                    Next brg
                    showlistbarangpromo = False
                    Me.Close()
                Case "0"
                    Dim jumlahitemterpilih As Integer = 0
                    jumlahlegal = 0
                    hargatotal = 0
                    If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                        jumlahlegal = totalvalid \ xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value")
                        If IsDBNull(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket")) = False Then
                            If jumlahlegal > GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket") Then
                                jumlahlegal = GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket")
                            End If
                        End If

                        jumlahlegal = jumlahlegal * GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item")

                    ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then

                    End If

                    Dim selectedbrg() As DataRow = xSet.Tables("listbarangpromo").Select("jmlh>0")

                    For Each brg In selectedbrg
                        jumlahitemterpilih = jumlahitemterpilih + CInt(brg.Item("jmlh"))
                    Next brg

                    If jumlahitemterpilih > jumlahlegal Then
                        Exit Sub
                    ElseIf jumlahitemterpilih Mod CInt(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item")) > 0 Then
                        Exit Sub
                    Else
                        For Each brg In selectedbrg
                            xSet.Tables("setSales").Rows.Add(brg.Item("idBrg"),
                                                    brg.Item("namaBrg"),
                                                    brg.Item("jmlh"),
                                                    0,
                                                    0,
                                                    0)
                        Next brg
                    End If
                    jumlahpaket = (jumlahitemterpilih / CInt(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item")))
                    hargatotal = jumlahpaket * CDbl(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "harga_paket"))

                    xSet.Tables("setSales").Rows.Add(1346,
                                                   "PAKET PRODUK",
                                                   1,
                                                   hargatotal,
                                                   hargatotal / jumlahpaket,'harga per pcs
                                                   0)
                    FormPOSx.calculateDiscTotal()
                    inputPayment.callMe(CInt(FormPOSx.txtTotalBeli.EditValue) + CInt(FormPOSx.txtDisc.EditValue), CInt(FormPOSx.txtDisc.EditValue), FormPOSx.GridView1.Columns("jmlh").SummaryItem.SummaryValue, FormPOSx.GridView1.Columns("namaBrg").SummaryItem.SummaryValue, CInt(FormPOSx.txtDiscPercent.EditValue), False)
                    isambilpaketpromo = True
                    Me.Close()
                Case "1"
                    Dim jumlahitemterpilih As Integer = 0
                    jumlahlegal = 0
                    Dim selectedbrg() As DataRow = xSet.Tables("listbarangpromo").Select("jmlh>0")

                    If IsDBNull(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item")) = False Then
                        If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                            jumlahlegal = totalvalid \ xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value")
                            If IsDBNull(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket")) = False Then
                                If jumlahlegal > GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket") Then
                                    jumlahlegal = GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "max_paket")
                                End If
                            End If

                            jumlahlegal = jumlahlegal * GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item")

                        ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then

                        End If


                        For Each brg In selectedbrg
                            jumlahitemterpilih = jumlahitemterpilih + CInt(brg.Item("jmlh"))
                        Next brg

                        If jumlahitemterpilih > jumlahlegal Then
                            Exit Sub
                        End If
                    End If


                    For Each brg In selectedbrg
                        xSet.Tables("setSales").Rows.Add(brg.Item("idBrg"),
                                                    brg.Item("namaBrg"),
                                                   brg.Item("jmlh"),
                                                   brg.Item("harga") * brg.Item("jmlh"),
                                                   brg.Item("harga"),
                                                   0)
                    Next brg

                    FormPOSx.calculateDiscTotal()
                    inputPayment.callMe(CInt(FormPOSx.txtTotalBeli.EditValue) + CInt(FormPOSx.txtDisc.EditValue), CInt(FormPOSx.txtDisc.EditValue), FormPOSx.GridView1.Columns("jmlh").SummaryItem.SummaryValue, FormPOSx.GridView1.Columns("namaBrg").SummaryItem.SummaryValue, CInt(FormPOSx.txtDiscPercent.EditValue), False)
                    isambilpaketpromo = True
                    Me.Close()

            End Select

            'save grouping
            hargatotal = 0
            jumlahlegal = 0

            xSet.Tables("setSalesX").Clear()
            If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='7'").Count > 0 Then
                jumlahlegal = totalvalid \ xSet.Tables("listparameterpromotopup").Select("idparameter_promo='11'").FirstOrDefault()("value")
                If xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'").Count > 0 Then
                    If jumlahlegal > xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'").FirstOrDefault()("value") Then
                        jumlahlegal = xSet.Tables("listparameterpromotopup").Select("idparameter_promo='10'").FirstOrDefault()("value")
                    End If
                End If
            ElseIf xSet.Tables("listparameterpromotopup").Select("idparameter_promo='26'").Count > 0 Then

            End If

            For k = 0 To xSet.Tables("listreward").Rows.Count - 1
                Dim jumlahbaranglegal As Integer


                Dim jumlahitemterpilih As Integer = 0
                If xSet.Tables("listreward").Rows(k).Item("jenis_reward").ToString = "0" Then

                    jumlahbaranglegal = jumlahlegal * xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item")

                    Dim selectedbrg() As DataRow = xSet.Tables("listbarangpromo").Select("jmlh>'0' and idreward='" & xSet.Tables("listreward").Rows(k).Item("idreward").ToString & "'")
                    If selectedbrg.Count = 0 Then
                        Continue For
                    End If
                    For Each brg In selectedbrg
                        jumlahitemterpilih = jumlahitemterpilih + CInt(brg.Item("jmlh"))
                    Next brg

                    If jumlahitemterpilih > jumlahbaranglegal Then
                        Exit Sub
                    ElseIf jumlahitemterpilih Mod CInt(xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item")) > 0 Then
                        Exit Sub
                    Else
                        For Each brg In selectedbrg
                            xSet.Tables("setSalesX").Rows.Add(brg.Item("idBrg"),
                                                    brg.Item("namaBrg"),
                                                    brg.Item("jmlh"),
                                                    0,
                                                    0,
                                                    0)
                        Next brg
                    End If
                    jumlahpaket = (jumlahitemterpilih / CInt(xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item")))
                    hargatotal = jumlahpaket * CDbl(xSet.Tables("listreward").Rows(k).Item("harga_paket"))

                    xSet.Tables("setSalesX").Rows.Add(1346,
                                                  "PAKET PRODUK",
                                                  1,
                                                  hargatotal,
                                                  hargatotal / jumlahpaket,'harga per pcs
                                                  0)

                    jumlahlegal = jumlahlegal - ((jumlahitemterpilih) / xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item"))
                ElseIf xSet.Tables("listreward").Rows(k).Item("jenis_reward").ToString = "1" Then

                    jumlahbaranglegal = jumlahlegal * xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item")

                    Dim selectedbrg() As DataRow = xSet.Tables("listbarangpromo").Select("jmlh>'0' and idreward='" & xSet.Tables("listreward").Rows(k).Item("idreward").ToString & "'")

                    If selectedbrg.Count = 0 Then
                        Continue For
                    End If

                    For Each brg In selectedbrg
                        jumlahitemterpilih = jumlahitemterpilih + CInt(brg.Item("jmlh"))
                    Next brg

                    If jumlahitemterpilih > jumlahbaranglegal Then
                        Exit Sub
                    End If

                    For Each brg In selectedbrg
                        xSet.Tables("setSalesX").Rows.Add(brg.Item("idBrg"),
                                                    brg.Item("namaBrg"),
                                                   brg.Item("jmlh"),
                                                   brg.Item("harga") * brg.Item("jmlh"),
                                                   brg.Item("harga"),
                                                   0)
                    Next brg

                    jumlahpaket = (jumlahitemterpilih / CInt(xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item")))

                    jumlahlegal = jumlahlegal - (jumlahitemterpilih / xSet.Tables("listreward").Rows(k).Item("jumlah_paket_item"))

                End If
                If jumlahlegal < 0 Then
                    Exit Sub
                End If
            Next k

            For o = 0 To xSet.Tables("setSalesX").Rows.Count - 1
                xSet.Tables("setSales").Rows.Add(xSet.Tables("setSalesX").Rows(o).Item("idBrg"),
                                                  xSet.Tables("setSalesX").Rows(o).Item("namaBrg"),
                                                  xSet.Tables("setSalesX").Rows(o).Item("jmlh"),
                                                  xSet.Tables("setSalesX").Rows(o).Item("hrg"),
                                                  xSet.Tables("setSalesX").Rows(o).Item("hrgpcs"),
                                                  xSet.Tables("setSalesX").Rows(o).Item("disc"))
            Next o

            FormPOSx.calculateDiscTotal()
            inputPayment.callMe(CInt(FormPOSx.txtTotalBeli.EditValue) + CInt(FormPOSx.txtDisc.EditValue), CInt(FormPOSx.txtDisc.EditValue), FormPOSx.GridView1.Columns("jmlh").SummaryItem.SummaryValue, FormPOSx.GridView1.Columns("namaBrg").SummaryItem.SummaryValue, CInt(FormPOSx.txtDiscPercent.EditValue), False)
            isambilpaketpromo = True
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Dim jumlahpaket As Integer
    Private Sub LookUpEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles LookUpEdit1.EditValueChanged
        '        Try
        '            If IsNothing(xSet.Tables("listbarangpromo")) Then
        '                xSet.Tables.Add("listbarangpromo")
        '                xSet.Tables("listbarangpromo").Columns.Add("idBrg")
        '                xSet.Tables("listbarangpromo").Columns.Add("namaBrg")
        '                xSet.Tables("listbarangpromo").Columns.Add("namaKategori2")
        '                xSet.Tables("listbarangpromo").Columns.Add("jmlh")
        '            Else
        '                xSet.Tables("listbarangpromo").Clear()
        '            End If

        '            If IsNothing(xSet.Tables("listreward")) Then
        '                Exit Sub
        '            End If

        '            namapromo = GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jenis_reward").ToString

        '            jumlahpaket = CInt(GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jumlah_paket_item"))

        '            If GridLookUpEdit1View.GetRowCellValue(GridLookUpEdit1View.FocusedRowHandle, "jenis_reward").ToString = "0" Then
        '                SQLquery = String.Format("SELECT idBrg,namaBrg,mk.namaKategori2 FROM d_reward_promo drp
        'JOIN mbarang mb ON drp.iditem=if(jenis_item=0,mb.idKategori2,mb.idBrg)
        '  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2 WHERE idreward='{0}' and mb.inactive=0", LookUpEdit1.EditValue)
        '                GridView1.Columns("harga").Visible = False
        '            Else
        '                SQLquery = String.Format("SELECT idBrg,namaBrg,mk.namaKategori2,if(jenis_harga=0,drp.harga,mb.price-drp.harga) `harga` FROM d_reward_promo drp
        'JOIN mbarang mb ON drp.iditem=if(jenis_item=0,mb.idKategori2,mb.idBrg)
        '  JOIN mkategoribrg2 mk ON mk.idKategori2=mb.idKategori2 WHERE idreward='{0}' and mb.inactive=0", LookUpEdit1.EditValue)
        '                GridView1.Columns("harga").Visible = True
        '            End If

        '            ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "listbarangpromo")
        '            GridControl1.DataSource = xSet.Tables("listbarangpromo")

        '        Catch ex As Exception
        '            MessageBox.Show(ex.Message)
        '        End Try
    End Sub

    Private Sub listBarangPromo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GridView2_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView2.RowCellClick
        Try
            Dim view As GridView = sender
            If view Is Nothing Then
                Return
            End If
            Dim expanded As Boolean = view.GetMasterRowExpanded(GridView2.FocusedRowHandle)
            If expanded = False Then
                view.ExpandMasterRow(GridView2.FocusedRowHandle)
            Else
                view.CollapseMasterRow(GridView2.FocusedRowHandle)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class