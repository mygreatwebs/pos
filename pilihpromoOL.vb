﻿Imports MySql.Data.MySqlClient

Public Class pilihpromoOL

    Private Function cekpromoultah() As Boolean
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where date(TglMulai)>='2017-04-10' and date(TglAkhir)<='2017-04-30' and idpromo={1}", Format(Now(), "yyyy-MM-dd"), G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Expired !")
            Return False
            Exit Function
        End If
        Return True
    End Function

    Private Function cekvalidpromo() As Boolean
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where date(TglMulai)<=DATE(now()) and date(TglAkhir)>=DATE(now())and idpromo={1}", Format(Now(), "yyyy-MM-dd"), G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Expired !")
            Return False
            Exit Function
        End If
        Return True
    End Function

    Private Function cekpromoLoveMonday() As Boolean
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where '{0}' >= date(TglMulai) and '{0}' <= date(TglAkhir) and idpromo={1}", Format(Now(), "yyyy-MM-dd"), G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Expired !")
            Return False
            Exit Function
        End If
        SQLquery = String.Format("select count(*) from app_promo.mpromo where aktif=1 and idpromo={0}", G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Tidak Aktif !")
            Return False
            Exit Function
        End If
        Dim totqty As Integer = 0
        If Now().DayOfWeek <> 1 And G_idpromo = 1 Then
            MsgBox("Penukaran hanya bisa dilakukan hari Senen ")
            Return False
            Exit Function
        End If
        If Trim(txbJumlah.Text).ToString.Length <> 7 Then
            Return False
            Exit Function
        End If

        If xSet.Tables("itempromo").Select("namaBrg like '%upin%' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaBrg like '%upin%'")(0)("jmlh") > 1 Then
                MsgBox("Qty donat upin/ipin untuk promo ini tidak boleh lebih dari 1")
                Return False
                Exit Function
            End If
        End If

        If xSet.Tables("itempromo").Select("namaBrg like '%ipin%' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaBrg like '%ipin%'")(0)("jmlh") > 1 Then
                MsgBox("Qty donat upin/ipin untuk promo ini tidak boleh lebih dari 1")
                Return False
                Exit Function
            End If
        End If
        If xSet.Tables("itempromo").Select("namaBrg like '%upin%' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaBrg like '%ipin%' and jmlh>0").Count > 0 Then
                MsgBox("Input Free Item Salah !, Pilih Salah Satu Donat Upin atau Ipin")
                Return False
                Exit Function
            End If
        End If
        If xSet.Tables("itempromo").Select("namaBrg like '%ipin%' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaBrg like '%upin%' and jmlh>0").Count > 0 Then
                MsgBox("Input Free Item Salah !, Pilih Salah Satu Donat Upin atau Ipin")
                Return False
                Exit Function
            End If
        End If

        If xSet.Tables("itempromo").Select("namaKategori2='DOUGHNUT' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaKategori2<>'DOUGHNUT' and jmlh>0").Count > 0 Then
                MsgBox("Input Free Item Salah !, Terdapat Item diluar Kategori DOUGHNUT")
                Return False
                Exit Function
            End If
            For Each row As DataRow In xSet.Tables("itempromo").Select("namaKategori2='DOUGHNUT' and jmlh>0")
                totqty += row("jmlh")
            Next
            If totqty <> 3 Then
                MsgBox("Input Free Item Salah !, Jumlah Free Item harus 3 Item ")
                Return False
                Exit Function
            End If
        End If

        If xSet.Tables("itempromo").Select("namaKategori2='BREAD' and jmlh>0").Count > 0 Then
            If xSet.Tables("itempromo").Select("namaKategori2<>'BREAD' and jmlh>0").Count > 0 Then
                MsgBox("Input Free Item Salah !, Terdapat Item Diluar Kagetori Bread")
                Return False
                Exit Function
            End If
            For Each row As DataRow In xSet.Tables("itempromo").Select("namaKategori2='BREAD' and jmlh>0")
                totqty += row("jmlh")
            Next
            If totqty <> 3 Then
                MsgBox("Input Free Item Salah !, Jumlah Free Item harus 3 Item ")
                Return False
                Exit Function
            End If
        End If
        Return True
    End Function

    Private Function CekPromoMidleMonth() As Boolean
        Dim totqty As Integer = 0
        If Trim(txbJumlah.Text).ToString.Length <> 7 Then
            Return False
            Exit Function
        End If
        'cek expired promo
        Dim vartemp As Integer
        SQLquery = String.Format("select count(*) from app_promo.mpromo where '{0}' >= date(TglMulai) and '{0}' <= date(TglAkhir) and idpromo={1}", Format(Now(), "yyyy-MM-dd"), G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Expired !")
            Return False
            Exit Function
        End If
        SQLquery = String.Format("select count(*) from app_promo.mpromo where aktif=1 and idpromo={0}", G_idpromo)
        vartemp = ExDb.Scalar(SQLquery, conn_string_local)
        If vartemp = 0 Then
            MsgBox("Promo sudah Tidak Aktif !")
            Return False
            Exit Function
        End If
        'jumlah hadiah cookies cman1
        If xSet.Tables("itempromo").Rows.Count > 0 Then
            For Each row As DataRow In xSet.Tables("itempromo").Select("jmlh>0")
                totqty += row("jmlh")
            Next
            If totqty <> 1 Then
                MsgBox("Input Free Item Salah !, Jumlah Free Item harus 1 Item ")
                Return False
                Exit Function
            End If
        End If
        Return True
    End Function

    Private Function cekKodeKuponOnline(ByVal v_kodekupon As String) As Boolean
        Dim jmlada As Integer
        If TestOnline() Then
            jmlada = ExDb.Scalar("select count(*) from app_promo.mpromokupon where upper(concat(prefix,numvoucher))='" & UCase(v_kodekupon) & "' and '" & Format(Now(), "yyyy-MM-dd") & "' between tglmulai and tglakhir ", My.Settings.cloudconn)
            If jmlada = 0 Then
                MsgBox("Kode Kupon tidak ada pada Database, Mohon Cek Kembali Inputan Anda !")
                Return False
            End If
            jmlada = ExDb.Scalar("select count(*) from app_promo.mt_tukar_kupon where upper(kodekupon)='" & UCase(v_kodekupon) & "'", My.Settings.cloudconn)
            If jmlada > 0 Then
                MsgBox("Kode Kupon sudah pernah ditukarkan !")
                Return False
            End If
        End If
        'cek lokal
        jmlada = ExDb.Scalar("select count(*) from app_logoffline.mt_tukar_kuponoffline where upper(kodekupon)='" & UCase(v_kodekupon) & "'", My.Settings.db_conn_mysql)
        If jmlada > 0 Then
            MsgBox("Kode Kupon sudah pernah ditukarkan !")
            Return False
        End If
        Return True
    End Function

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        Dim isCekKodeKupon As Boolean
        Select Case G_idpromo
            Case 1 'Love Monday
                If cekpromoLoveMonday() = False Then Exit Sub
            Case 2
                If CekPromoMidleMonth() = False Then Exit Sub
            Case 8
                If cekpromoultah() = False Then Exit Sub
            Case > 8
                If cekvalidpromo() = False Then Exit Sub
        End Select
        'If xSet.Tables("itempromo").Select("jmlh>0").Count = 0 Then Exit Sub
        'isCekKodeKupon = cekKodeKuponOnline(txbJumlah.Text)
        'If isCekKodeKupon = False Then Exit Sub
        'For Each row As DataRow In xSet.Tables("itempromo").Select("jmlh>0")
        '    totqty += row("jmlh")
        'Next
        'If totqty <> 3 Then Exit Sub

        if isnothing(xset.tables("setsales")) then exit sub
        for each row as datarow in xset.tables("itempromo").select("jmlh>0")
            'xset.tables("setsalesswap").rows.add(drow.item("idbrg"),
            '                                          drow.item("namabrg"),
            '                                          drow.item("jmlh"),
            '                                          drow.item("hrg"),
            '                                          drow.item("hrgpcs"),
            '                                          drow.item("disc"))

            xset.tables("setsales").rows.add(row("idbrg"),
                                                row("namabrg"), row("jmlh"), 0, row("hrg"), 100)
        next
        G_kodekupon = txbJumlah.Text
        G_ketpromo = LabelControl2.Text
        Close()

        AktifPromo = 3
    End Sub
    Private Sub infopromo()
        Dim strtemp As String = txbJumlah.Text
        Dim prefixpromo As String = Mid(strtemp, 2, 1)
        Dim prefixmedsos As String = Mid(strtemp, 1, 1)

        If prefixmedsos.ToString.ToUpper = "F" Then
            prefixmedsos = "Facebook"
        ElseIf prefixmedsos.ToString.ToUpper = "I" Then
            prefixmedsos = "Instagram"
        ElseIf prefixmedsos.ToString.ToUpper = "J" Then
            prefixmedsos = "Instagram Jawa POS"
        Else
            prefixmedsos = "Other Medsos"
        End If

        If IsNothing(xSet.Tables("infopromo")) = False Then xSet.Tables("infopromo").Clear()
        SQLquery = "select idpromo,namapromo from app_promo.mpromo where prefixkupon='" & prefixpromo & "'"
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "infopromo")
        If xSet.Tables("infopromo").Rows.Count > 0 Then
            prefixpromo = xSet.Tables("infopromo").Rows(0)("namapromo")
            LabelControl2.Text = prefixpromo
            LabelControl3.Text = prefixmedsos
            G_idpromo = xSet.Tables("infopromo").Rows(0)("idpromo")
        Else
            G_idpromo = -1
            MsgBox("Promo Tidak Ditemukan ")
        End If
        xSet.Tables.Remove("infopromo")
        'isi item  promonya di urutkan harga terendah
        SQLquery = "select  a.idbrg,namaBrg,namaKategori2,0 jmlh,price hrg from mbarang  a left join app_promo.dtpromoitem b on a.idbrg=b.idbrg left join mkategoribrg2 c on a.idKategori2=c.idkategori2  where b.idpromo=" & G_idpromo & " order by price ASC"
        If IsNothing(xSet.Tables("itempromo")) = False Then xSet.Tables("itempromo").Clear()
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "itempromo")
        GridControl1.DataSource = xSet.Tables("itempromo")

    End Sub

    Private Sub txbJumlah_TextChanged(sender As Object, e As EventArgs) Handles txbJumlah.TextChanged
        If Trim(txbJumlah.Text).ToString.Length = 7 Then
            infopromo()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub pilihpromoOL_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("itempromo")
        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub pilihpromoOL_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        AktifPromo = 0
        G_ketpromo = ""
        Close()
    End Sub

    Private Sub pilihpromoOL_Load(sender As Object, e As EventArgs) Handles Me.Load
        PanelControl1.Appearance.BackColor = Color.OrangeRed
        PanelControl1.Appearance.BackColor2 = Color.OrangeRed
        PanelControl1.Appearance.Options.UseBackColor = True

        PanelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        PanelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
    End Sub
End Class