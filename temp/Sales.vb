﻿
Public Class Sales
    Dim totkoreksi As Double
    Private Sub Sales_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles Me.BeforePrint
        Dim totDiskon As Double = 0

        NamaCbg.Text = "Cabang " & xSet.Tables("daftarCabang").Select("idCabang=" & id_cabang)(0).Item("namaCabang")
        TglNow.Text = Format(Now, "dd-MM-yyyy HH:mm")

        SQLquery = String.Format("SELECT MIN(startTime) as minstart, MAX(endTime) as maxend, CURDATE() DateNow FROM mtswitchuser " & _
                                 "WHERE (status = 1) AND (idCabang = {0}) AND (shiftKe = {1}) AND (DATE(startTime)='{2}')",
                                 id_cabang,
                                 shiftLogin,
                                 Format(TglShiftNow, "yyyy-MM-dd"))
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "GETTIMES")
        Dim startDate As String = Format(xSet.Tables("GETTIMES").Rows(0).Item("minstart"), "yyyy-MM-dd HH:mm:ss")
        Dim endDate As String = String.Format("{0} {1}", Format(xSet.Tables("GETTIMES").Rows(0).Item("DateNow"), "yyyy-MM-dd"),
                                              xSet.Tables("GETTIMES").Rows(0).Item("maxend"))

        Print_salesTableAdapter1.ClearBeforeFill = True
        Print_salesTableAdapter1.Fill(DataSetPOS1.print_sales, id_cabang, shiftLogin, startDate, endDate)

        'SQLquery = String.Format("SELECT IFNULL(SUM(BREAD),0) AS BREAD, IFNULL(SUM(BS),0) AS BS, IFNULL(SUM(PTHTLR),0) AS PTHTLR, IFNULL(SUM(MINUMAN),0) AS MINUMAN, " & _
        '            "IFNULL(SUM(BREAD)+SUM(BS)+SUM(PTHTLR)+SUM(MINUMAN),0) AS TOTAL FROM ( SELECT totalPrice AS BREAD, 0 BS, " & _
        '            "0 PTHTLR, 0 MINUMAN FROM mtpos a  LEFT JOIN dtpos b ON a.idOrder=b.idOrder AND b.status=1 " & _
        '            "LEFT JOIN mbarang c ON b.idBrg=c.idBrg LEFT JOIN mkategoribrg2 d ON c.idKategori2=d.idKategori2 " & _
        '            "WHERE b.idBrg NOT IN (SELECT idBrg FROM mbarang  WHERE namabrg LIKE '% BS' OR idBrg=139 " & _
        '            "OR idKategori2 NOT IN (1,2,3,4,5,6,7,8,11,12)) AND a.isVoid=0 AND a.status=1 AND a.idCabang={0} AND a.createDate " & _
        '            "BETWEEN '{1}' AND '{2}' UNION ALL SELECT 0 BREAD, totalPrice AS BS, 0 PTHTLR, 0 MINUMAN FROM mtpos a " & _
        '            "LEFT JOIN dtpos b ON a.idOrder=b.idOrder AND b.status=1 LEFT JOIN mbarang c ON b.idBrg=c.idBrg " & _
        '            "LEFT JOIN mkategoribrg2 d ON c.idKategori2=d.idKategori2 WHERE b.idBrg IN (SELECT idBrg FROM mbarang " & _
        '            "WHERE namabrg LIKE '% BS') AND a.isVoid=0 AND a.status=1 AND a.idCabang={0} AND a.createDate BETWEEN '{1}' AND '{2}' " & _
        '            "UNION ALL SELECT 0 BREAD, 0 AS BS, totalPrice AS PTHTLR, 0 MINUMAN FROM mtpos a  LEFT JOIN dtpos b " & _
        '            "ON a.idOrder=b.idOrder AND b.status=1 LEFT JOIN mbarang c ON b.idBrg=c.idBrg LEFT JOIN mkategoribrg2 d " & _
        '            "ON c.idKategori2=d.idKategori2 WHERE b.idBrg=139 AND a.isVoid=0 AND a.status=1 AND a.idCabang={0}  AND a.createDate " & _
        '            "BETWEEN '{1}' AND '{2}' UNION ALL SELECT 0 BREAD, 0 AS BS, 0 AS PTHTLR, totalPrice AS MINUMAN " & _
        '            "FROM mtpos a  LEFT JOIN dtpos b ON a.idOrder=b.idOrder AND b.status=1 LEFT JOIN mbarang c ON b.idBrg=c.idBrg " & _
        '            "LEFT JOIN mkategoribrg2 d ON c.idKategori2=d.idKategori2 WHERE b.idBrg IN (SELECT idBrg FROM mbarang " & _
        '            "WHERE idKategori2 IN (9,10)) AND a.isVoid=0 AND a.status=1 AND a.idCabang={0} AND a.createDate BETWEEN '{1}' AND '{2}' ) a",
        '            id_cabang, startDate, endDate)
        If My.Settings.idWilayah = "MLG" Then
            totDiskon = ExDb.Scalar(String.Format("select ifnull(sum(disc),0) tot from mtpos where isvoid=0 and  createDate BETWEEN '{1}' AND '{2}' and idcabang={0}", id_cabang, startDate, endDate), My.Settings.db_conn_mysql)
        End If
        SQLquery = String.Format("select  ifnull(sum((b.quantity * c.price)),0)    from mtadj a left join dtadj b on a.idadj=b.idAdj left join mbarang c on b.idBrg = c.idBrg " & _
                        "where b.idJnsAdj =7 and a.notes  not in ('Mutasi TS ke BS by Software (-)')  and createDate BETWEEN '{1}' AND '{2}' and idcabang={0}", id_cabang, startDate, endDate)

        totkoreksi = ExDb.Scalar(SQLquery, My.Settings.db_conn_mysql)

        SQLquery = String.Format("SELECT Tgl, SUM(BREAD) BREAD, SUM(BS) BS, SUM(PTHTLR) PTHTLR, SUM(MINUMAN) MINUMAN, SUM(LAIN2) LAIN2, SUM(disc) DISC, SUM(DiscItem) DISCITEM FROM (" & _
                    "SELECT DATE(a.createDate) Tgl, CASE WHEN c.idKategori2 IN (1,3,4,5,6,7,8,11,12) THEN b.quantity*b.price ELSE 0 END AS BREAD, " & _
                    "CASE WHEN c.idKategori2=2 AND b.idBrg<>139 THEN b.quantity*b.price ELSE 0 END AS BS, CASE WHEN b.idBrg=139 THEN b.quantity*b.price ELSE 0 END AS PTHTLR, " & _
                    "CASE WHEN c.idKategori2 IN (9,10) THEN b.quantity*b.price ELSE 0 END MINUMAN, CASE WHEN c.idKategori2>12 THEN b.quantity*b.price ELSE 0 END LAIN2, " & _
                    "b.totalPrice*a.discPercent/100 disc, b.quantity*b.disc DiscItem FROM mtpos a  INNER JOIN dtpos b ON a.idOrder=b.idOrder AND b.status=1 INNER JOIN mbarang c ON b.idBrg=c.idBrg " & _
                    "INNER JOIN mkategoribrg2 d ON c.idKategori2=d.idKategori2 WHERE a.isVoid=0 AND a.status=1 AND a.idCabang={0} " & _
                    "AND a.createDate BETWEEN '{1}' AND '{2}' ) A GROUP BY Tgl ", id_cabang, startDate, endDate)

        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getKategoriSales")

        totalRoti.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("BREAD"))
        totalBS.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("BS"))
        totalMinuman.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("MINUMAN"))
        totalPthTelur.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("PTHTLR"))
        totalDisc.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("DISC"))
        If My.Settings.idWilayah = "MLG" Then totalDisc.Text = String.Format("{0:n0}", totDiskon)
        totalDiscItem.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("DISCITEM"))
        totalLain.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("LAIN2"))
        totalPendapatan.Text = String.Format("{0:n0}", xSet.Tables("getKategoriSales").Rows(0).Item("BREAD") +
                                             xSet.Tables("getKategoriSales").Rows(0).Item("BS") +
                                             xSet.Tables("getKategoriSales").Rows(0).Item("MINUMAN") +
                                             xSet.Tables("getKategoriSales").Rows(0).Item("PTHTLR") +
                                             xSet.Tables("getKategoriSales").Rows(0).Item("LAIN2") -
                                             xSet.Tables("getKategoriSales").Rows(0).Item("DISCITEM") -
                                             totalDisc.Text)

        XrLabel34.Text = Format(totkoreksi, "#,##")
        XrLabel35.Text = Format(CDbl(totalPendapatan.Text) + totkoreksi, "#,##")
        If xSet.Tables("infouangkasir").Rows.Count > 0 Then
            For Each dtrow As DataRow In xSet.Tables("infouangkasir").Rows
                Select Case dtrow("nominal")
                    Case 100000
                        XrLabel39.Text = dtrow("jmlh")
                        XrLabel37.Text = Format(dtrow("jmlh") * 100000, "#,###")
                    Case 50000
                        XrLabel40.Text = dtrow("jmlh")
                        XrLabel41.Text = Format(dtrow("jmlh") * 50000, "#,###")
                    Case 20000
                        XrLabel43.Text = dtrow("jmlh")
                        XrLabel44.Text = Format(dtrow("jmlh") * 20000, "#,###")
                    Case 10000
                        XrLabel46.Text = dtrow("jmlh")
                        XrLabel47.Text = Format(dtrow("jmlh") * 10000, "#,###")
                    Case 5000
                        XrLabel49.Text = dtrow("jmlh")
                        XrLabel50.Text = Format(dtrow("jmlh") * 5000, "#,###")
                    Case 1000
                        If dtrow("idUang") = 6 Then
                            XrLabel52.Text = dtrow("jmlh")
                            XrLabel53.Text = Format(dtrow("jmlh") * 1000, "#,###")
                        Else
                            XrLabel56.Text = dtrow("jmlh")
                            XrLabel58.Text = Format(dtrow("jmlh") * 1000, "#,###")
                        End If
                    Case 500
                        XrLabel61.Text = dtrow("jmlh")
                        XrLabel60.Text = Format(dtrow("jmlh") * 500, "#,###")
                    Case 200
                        XrLabel64.Text = dtrow("jmlh")
                        XrLabel63.Text = Format(dtrow("jmlh") * 200, "#,###")
                    Case 100
                        XrLabel67.Text = dtrow("jmlh")
                        XrLabel66.Text = Format(dtrow("jmlh") * 100, "#,###")
                End Select
            Next
            Dim totalall As Double
            For Each dtrow As DataRow In xSet.Tables("infouangkasir").Rows
                totalall += dtrow("jmlh") * dtrow("nominal")
            Next
            XrLabel68.Text = Format(totalall, "#,###")
            'XrLabel70.Text = Format(totalall, "#,###")
        End If
        PageHeight = 550 + DataSetPOS1.print_sales.Rows.Count * Detail.HeightF

        xSet.Tables.Remove("GETTIMES")
        xSet.Tables.Remove("getKategoriSales")
    End Sub

    Private Sub XrLabel14_AfterPrint(sender As Object, e As EventArgs) Handles XrLabel14.AfterPrint
        'Dim xtot As Double = CDbl(XrLabel14.Text) + totkoreksi
        'XrLabel1.Text = Format(totkoreksi, "#,##")
        'XrLabel22.Text = Format(xtot, "#,##")
        'XrLabel72.Text = Format(CDbl(XrLabel70.Text) - CDbl(XrLabel14.Text) - CDbl(XrLabel6.Text), "#,###")
    End Sub
End Class