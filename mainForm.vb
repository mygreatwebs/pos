﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports DevExpress.XtraPrinting
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports MySql.Data.MySqlClient

Public Class mainForm

    Dim xLoc As Point
    Dim numOrder As Integer

    Dim RetryClosing As Integer
    Dim ProgressMsg As String
    Dim isNotaVoid As Boolean
    Dim isClosed As Boolean
    'Dim printspeednota As XtraReport = New Nota
    'Dim toolspeed As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printspeednota)
    Private Function Begin_Closing()
        'cek user dapat closing apa tidak
        isUserCanClosing = False
        If IsNothing(xSet.Tables("CekUserShiftNow")) = False Then xSet.Tables("CekUserShiftNow").Clear()
        SQLquery = String.Format("SELECT COUNT(shiftKe) num, IFNULL(MAX(shiftKe),0) shiftNow, DATE(MAX(startTime)) TglShiftNow, CURDATE() TglShiftLogin FROM mtswitchuser WHERE idCabang={0} AND idStaff={1} AND endTime='00:00:00' AND status=1", id_cabang, staff_id)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "CekUserShiftNow")
        If xSet.Tables("CekUserShiftNow").Rows(0).Item("num") = 0 Then
            MsgBox("User " & staff_name & " tidak boleh melakukan closing ")
            Return False
        End If
        xSet.Tables.Remove("CekUserShiftNow")
        isUserCanClosing = True
        confirmClosing.ShowDialog()
        If idCuaca = 0 Then Return False

        Dim LastQuery As String = ""
        ProgressMsg = ""

        Try
            SQLquery = "SELECT idClosing as selectedID, createDate FROM mtclosing WHERE DATE(createDate)=CURDATE() AND no_urut_closing=" & shiftLogin
            ProgressMsg &= "Mencoba mengambil daftar closing" & vbCrLf
            LastQuery = SQLquery
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "cekClosing")
            If xSet.Tables("cekClosing").Rows.Count = 0 Then
                'STEP 1 :insert ke mtclosing
                SQLquery = String.Format("INSERT INTO mtclosing(idCabang, idStaff, no_urut_closing, idCuaca, createDate) VALUES({0},{1},{2},{3},CURRENT_TIMESTAMP())",
                                         id_cabang, staff_id, shiftLogin, idCuaca)
                ProgressMsg &= "STEP 1 :insert ke mtclosing" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecData(SQLquery, conn_string_local)
                'inject ke Table mtswitchuser yang usernya bukan user closing
                SQLquery = String.Format("INSERT INTO mtswitchuser(idCabang, idStaff, shiftKe, startTime, endtime, firstBalance) " & _
                                         "select idcabang,updateby,{0},'{1}','00:00:00',0 from mtpos where shiftke={0} and date(createdate)='{2}' and updateby<>{3} group by idcabang,updateby",
                                                shiftLogin, Format(TglShiftLogin, "yyyy-MM-dd HH:mm:ss"), Format(TglShiftLogin, "yyyy-MM-dd"), staff_id)
                ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                'STEP 2 :cek semua id yg belum logout.
                SQLquery = String.Format("SELECT idSwitch, idStaff FROM mtswitchuser WHERE idCabang={0} AND shiftKe={1} AND DATE(startTime)='{2}' AND endTime='00:00:00'",
                                         id_cabang, shiftLogin, Format(TglShiftNow, "yyyy-MM-dd"))
                ProgressMsg &= "cek semua id yg belum logout" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "unlogoutID")


                'STEP 3 :total semua transaksi yg telah di lakukan masing2 id
                For Each rows As DataRow In xSet.Tables("unlogoutID").Rows
                    SQLquery = String.Format("SELECT IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-1))))-SUM(b.totalChange),0) AS CASH, " & _
                                "IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-2)))),0) as CC, " & _
                                "IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-3)))),0) as VOC " & _
                                "FROM mtpos a LEFT JOIN ( " & _
                                "SELECT x.idOrder, y.idPayment, y.amount, CASE WHEN y.idPayment=1 THEN x.totalChange ELSE 0 END AS totalChange, x.totalOrder " & _
                                "FROM mtbayarpos x INNER JOIN dtbayarpos y ON x.idPaymentOrder=y.idPaymentOrder AND y.status=1 " & _
                                ") b ON a.idOrder=b.idOrder  " & _
                                "INNER JOIN mpayment d ON b.idPayment=d.idPayment WHERE DATE(a.createDate)='{3}' " & _
                                "AND a.isVoid=0 AND a.idCabang={0} AND a.shiftKe={1} AND a.updateBy={2}; ",
                                id_cabang, shiftLogin, rows.Item("idStaff"), Format(TglShiftNow, "yyyy-MM-dd"))
                    ProgressMsg &= "Get total semua transaksi yg telah di lakukan masing2 id" & vbCrLf
                    LastQuery = SQLquery
                    ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "totalEachID")
                    'STEP 4 :update mtswitchuser dengan total
                    SQLquery = String.Format("UPDATE mtswitchuser SET endTime=CURTIME(), lastbalance={4}, cash={5}, creditCard={6}, " & _
                                             "voucher={7} WHERE idStaff={1} AND idSwitch={2} AND idCabang={0} AND " & _
                                             "DATE(startTime)=DATE('{3}'); ",
                                             id_cabang, rows.Item("idStaff"), rows.Item("idSwitch"), Format(TglShiftNow, "yyyy-MM-dd"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CASH") + xSet.Tables("totalEachID").Rows(0).Item("CC") + xSet.Tables("totalEachID").Rows(0).Item("VOC"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CASH"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CC"),
                                             xSet.Tables("totalEachID").Rows(0).Item("VOC"))
                    Try
                        If Not conn_string_local = My.Settings.db_conn_mysql Then
                            ProgressMsg &= "STEP 4 :update mtswitchuser dengan total" & vbCrLf
                            LastQuery = SQLquery
                            ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                        End If
                    Catch ex As Exception

                    Finally
                        ProgressMsg &= "STEP 4 :update mtswitchuser dengan total" & vbCrLf
                        LastQuery = SQLquery
                        ExDb.ExecData(SQLquery, conn_string_local)
                    End Try
                    xSet.Tables("totalEachID").Clear()
                Next

                Dim numOrder As Integer
                'SELECT NO ID CLOSING YANG BARU SAJA DI INSERT KE MTCLOSING
                SQLquery = "SELECT DISTINCT MAX(idClosing) AS getNewNum FROM mtclosing WHERE DATE(createDate)=DATE(CURDATE()) AND idCabang=" & id_cabang
                ProgressMsg &= "Select no.id closing yang baru saja di insert ke mtclosing" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                numOrder = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                xSet.Tables("getNewNum").Clear()

                'STEP 5 : insert hasil closing sebagai beginning balance untuk shift selanjutnya
                SQLquery = String.Format("INSERT INTO dtclosing(idclosing, idBrg, qttIN, qttOUT) " & _
                            "SELECT {0}, a.idBrg, SUM( IFNULL(a.qttIN,0) ) AS qttIN, SUM( IFNULL(a.qttOUT,0) ) AS qttOUT FROM ( " & _
                            "SELECT idBrg, quantity AS qttIN, 0 AS qttOUT FROM tstok a WHERE (idCabang = {1}) AND (isExpired =0) AND (isVoid =0) AND (quantity >0) UNION ALL " & _
                            "SELECT idBrg, 0 AS qttIN, quantity AS qttOUT FROM tstok b WHERE (idCabang = {1}) AND (isExpired =0) AND (isVoid =0) AND (quantity <0) " & _
                            ")a INNER JOIN mbarang b ON a.idBrg = b.idBrg WHERE isToko=1 GROUP BY a.idBrg ", numOrder, id_cabang)
                ProgressMsg &= "STEP 5 : insert hasil closing sebagai beginning balance untuk shift selanjutnya" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecData(SQLquery, conn_string_local)

                'STEP 6 :Update semua tabel status=1
                SQLquery = String.Format("UPDATE mtspb SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtadj SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtpos SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtbayarpos SET status=1 WHERE DATE(updateDate)=DATE('{0}'); " & _
                            "UPDATE tstok SET isExpired=1; ", Format(TglShiftNow, "yyyy-MM-dd"))
                ProgressMsg &= "STEP 6 :Update semua tabel status=1" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecData(SQLquery, conn_string_local)

                'STEP 7 :INSERT stok hasil closing ke table tstok untuk perhitungan kartu stok
                SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) " & _
                                         "SELECT 5, {0}, {1}, idBrg, qttIN+qttOUT FROM dtclosing WHERE idClosing={1}", id_cabang, numOrder)
                ProgressMsg &= "STEP 7 :INSERT stok hasil closing ke table tstok untuk perhitungan kartu stok" & vbCrLf
                LastQuery = SQLquery
                ExDb.ExecData(SQLquery, conn_string_local)

                'STEP 8 :print data sales dan stok
                selected_idxRow = shiftLogin
                Using printout As New Sales
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        tool.Print()
                        'tool.ShowPreviewDialog()
                    End Using
                End Using
                Using printout As New Sales
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        'tool.Print()
                        tool.ShowPreviewDialog()
                    End Using
                End Using
                Using printout As New Stok
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        tool.Print()
                        tool.ShowPreviewDialog()
                    End Using
                End Using
            Else
                PanelControl1.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            ProgressMsg &= "Last query : " & LastQuery & vbCrLf

            'BERDASARKAN PENGALAMAN, CLOSING ERROR KRN INSERT KE MTCLOSING SUKSES TAPI DTCLOSING KOSONG
            If xSet.Tables("GetLastClosing") IsNot Nothing Then xSet.Tables("GetLastClosing").Clear()
            SQLquery = "SELECT MAX(idClosing) idClosing FROM mtclosing WHERE DATE(createdate)=CURDATE() AND no_urut_closing=" & shiftLogin
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "GetLastClosing")

            If xSet.Tables("GetLastClosing").Rows.Count > 0 Then
                ProgressMsg &= "Last ID dari mtclosing = " & xSet.Tables("GetLastClosing").Rows(0).Item(0) & vbCrLf

                If xSet.Tables("GetLastDTClosing") IsNot Nothing Then xSet.Tables("GetLastDTClosing").Clear()
                SQLquery = "SELECT idClosing FROM dtclosing WHERE idClosing=" & xSet.Tables("GetLastClosing").Rows(0).Item(0)
                ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "GetLastDTClosing")

                If xSet.Tables("GetLastDTClosing").Rows.Count = 0 Then
                    ProgressMsg &= "Tidak terdapat record di dtclosing" & vbCrLf
                    SQLquery = "DELETE FROM mtclosing WHERE idClosing=" & xSet.Tables("GetLastClosing").Rows(0).Item(0)
                    ExDb.ExecData(SQLquery, conn_string_local)
                Else
                    ProgressMsg &= "Terdapat record di dtclosing" & vbCrLf
                End If
            Else
                ProgressMsg &= "Last ID untuk tanggal " & Format(Now, "dd-MM-yyyy") & ", shift ke-" & shiftLogin & " tidak ada" & vbCrLf
            End If

            ProgressMsg &= "Error messages : " & ex.ToString()

            Clipboard.SetDataObject(ProgressMsg)
            IO.File.AppendAllText(My.Computer.FileSystem.SpecialDirectories.Desktop & "\Err" & Format(Now, "yyyyMMdd-HHmmss") & ".txt", Clipboard.GetText())

            xSet.Tables("cekClosing").Clear()
            xSet.Tables("unlogoutID").Clear()
            Return False
        End Try

        xSet.Tables("cekClosing").Clear()
        If IsNothing(xSet.Tables("unlogoutID")) = False Then xSet.Tables("unlogoutID").Clear()

        Return True
    End Function

    Private Function Begin_ClosingOptm()
        Dim Zcon As MySqlConnection
        Dim vartr As MySql.Data.MySqlClient.MySqlTransaction
        Dim adatrans As Boolean = False
        'cek user dapat closing apa tidak
        isUserCanClosing = False

        If IsNothing(xSet.Tables("CekUserShiftNow")) = False Then xSet.Tables("CekUserShiftNow").Clear()
        SQLquery = String.Format("SELECT COUNT(shiftKe) num, IFNULL(MAX(shiftKe),0) shiftNow, DATE(MAX(startTime)) TglShiftNow, CURDATE() TglShiftLogin FROM mtswitchuser WHERE idCabang={0} AND idStaff={1} AND endTime='00:00:00' AND status=1", id_cabang, staff_id)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "CekUserShiftNow")
        If xSet.Tables("CekUserShiftNow").Rows(0).Item("num") = 0 Then
            MsgBox("User " & staff_name & " tidak boleh melakukan closing ")
            Return False
        End If
        xSet.Tables.Remove("CekUserShiftNow")
        isUserCanClosing = True
        frminputuangkasir.ShowDialog()
        confirmClosing.ShowDialog()
        If idCuaca = 0 Then Return False

        Dim LastQuery As String = ""
        ProgressMsg = ""

        Try
            Zcon = New MySqlConnection(My.Settings.db_conn_mysql)
            Dim command As New MySqlCommand()
            'Dim command2 As New MySqlCommand
            Zcon.Open()
            command.Connection = Zcon
            vartr = Zcon.BeginTransaction()
            command.Transaction = vartr
            adatrans = True
            SQLquery = "SELECT idClosing as selectedID, createDate FROM mtclosing WHERE DATE(createDate)=CURDATE() AND no_urut_closing=" & shiftLogin
            ProgressMsg &= "Mencoba mengambil daftar closing" & vbCrLf
            LastQuery = SQLquery
            'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "cekClosing")
            command.CommandText = SQLquery
            ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "cekClosing")
            If xSet.Tables("cekClosing").Rows.Count = 0 Then
                'STEP 1 :insert ke mtclosing
                SQLquery = String.Format("INSERT INTO mtclosing(idCabang, idStaff, no_urut_closing, idCuaca, createDate) VALUES({0},{1},{2},{3},CURRENT_TIMESTAMP())",
                                         id_cabang, staff_id, shiftLogin, idCuaca)
                ProgressMsg &= "STEP 1 :insert ke mtclosing" & vbCrLf
                LastQuery = SQLquery
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
                'inject ke Table mtswitchuser yang usernya bukan user closing
                SQLquery = String.Format("INSERT INTO mtswitchuser(idCabang, idStaff, shiftKe, startTime, endtime, firstBalance) " & _
                                         "select idcabang,updateby,{0},'{1}','00:00:00',0 from mtpos where shiftke={0} and date(createdate)='{2}' and updateby<>{3} group by idcabang,updateby",
                                                shiftLogin, Format(TglShiftLogin, "yyyy-MM-dd HH:mm:ss"), Format(TglShiftLogin, "yyyy-MM-dd"), staff_id)
                'ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
                'STEP 2 :cek semua id yg belum logout.
                SQLquery = String.Format("SELECT idSwitch, idStaff FROM mtswitchuser WHERE idCabang={0} AND shiftKe={1} AND DATE(startTime)='{2}' AND endTime='00:00:00'",
                                         id_cabang, shiftLogin, Format(TglShiftNow, "yyyy-MM-dd"))
                ProgressMsg &= "cek semua id yg belum logout" & vbCrLf
                LastQuery = SQLquery
                command.CommandText = SQLquery
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "unlogoutID")
                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "unlogoutID")


                'STEP 3 :total semua transaksi yg telah di lakukan masing2 id
                For Each rows As DataRow In xSet.Tables("unlogoutID").Rows
                    SQLquery = String.Format("SELECT IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-1))))-SUM(b.totalChange),0) AS CASH, " &
                                "IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-2)))),0) as CC, " &
                                "IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-3)))),0) as VOC, " &
                                "IFNULL(SUM(b.amount*(1-abs(sign(d.idKategoriPayment-6)))),0) as EMONEY " &
                                "FROM mtpos a LEFT JOIN ( " &
                                "SELECT x.idOrder, y.idPayment, y.amount, CASE WHEN y.idPayment=1 THEN x.totalChange ELSE 0 END AS totalChange, x.totalOrder " &
                                "FROM mtbayarpos x INNER JOIN dtbayarpos y ON x.idPaymentOrder=y.idPaymentOrder AND y.status=1 " &
                                ") b ON a.idOrder=b.idOrder  " &
                                "INNER JOIN mpayment d ON b.idPayment=d.idPayment WHERE DATE(a.createDate)='{3}' " &
                                "AND a.isVoid=0 AND a.idCabang={0} AND a.shiftKe={1} AND a.updateBy={2}; ",
                                id_cabang, shiftLogin, rows.Item("idStaff"), Format(TglShiftNow, "yyyy-MM-dd"))
                    ProgressMsg &= "Get total semua transaksi yg telah di lakukan masing2 id" & vbCrLf
                    LastQuery = SQLquery
                    'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "totalEachID")
                    command.CommandText = SQLquery
                    ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "totalEachID")
                    'STEP 4 :update mtswitchuser dengan total
                    SQLquery = String.Format("UPDATE mtswitchuser SET endTime=CURTIME(), lastbalance={4}, cash={5}, creditCard={6}, " &
                                             "voucher={7},larittacash={8} WHERE idStaff={1} AND idSwitch={2} AND idCabang={0} AND " &
                                             "DATE(startTime)=DATE('{3}'); ",
                                             id_cabang, rows.Item("idStaff"), rows.Item("idSwitch"), Format(TglShiftNow, "yyyy-MM-dd"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CASH") + xSet.Tables("totalEachID").Rows(0).Item("CC") + xSet.Tables("totalEachID").Rows(0).Item("VOC"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CASH"),
                                             xSet.Tables("totalEachID").Rows(0).Item("CC"),
                                             xSet.Tables("totalEachID").Rows(0).Item("VOC"), xSet.Tables("totalEachID").Rows(0).Item("EMONEY"))
                    'Try
                    'If Not conn_string_local = My.Settings.db_conn_mysql Then
                    'ProgressMsg &= "STEP 4 :update mtswitchuser dengan total" & vbCrLf
                    'LastQuery = SQLquery
                    'ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
                    'End If
                    'Catch ex As Exception

                    'Finally
                    ProgressMsg &= "STEP 4 :update mtswitchuser dengan total" & vbCrLf
                    LastQuery = SQLquery
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()
                    'ExDb.ExecData(SQLquery, conn_string_local)
                    'End Try
                    xSet.Tables("totalEachID").Clear()
                Next

                Dim numOrder As Integer
                'SELECT NO ID CLOSING YANG BARU SAJA DI INSERT KE MTCLOSING
                SQLquery = "SELECT DISTINCT MAX(idClosing) AS getNewNum FROM mtclosing WHERE DATE(createDate)=DATE(CURDATE()) AND idCabang=" & id_cabang
                ProgressMsg &= "Select no.id closing yang baru saja di insert ke mtclosing" & vbCrLf
                LastQuery = SQLquery
                'ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getNewNum")
                command.CommandText = SQLquery
                ExDb.ExecQueryWithTransaction(command, SQLquery, xSet, "getNewNum")
                numOrder = xSet.Tables("getNewNum").Rows(0).Item(0).ToString
                xSet.Tables("getNewNum").Clear()

                'STEP 5 : insert hasil closing sebagai beginning balance untuk shift selanjutnya
                SQLquery = String.Format("INSERT INTO dtclosing(idclosing, idBrg, qttIN, qttOUT) " & _
                            "SELECT {0}, a.idBrg, SUM( IFNULL(a.qttIN,0) ) AS qttIN, SUM( IFNULL(a.qttOUT,0) ) AS qttOUT FROM ( " & _
                            "SELECT idBrg, quantity AS qttIN, 0 AS qttOUT FROM tstok a WHERE (idCabang = {1}) AND (isExpired =0) AND (isVoid =0) AND (quantity >0) UNION ALL " & _
                            "SELECT idBrg, 0 AS qttIN, quantity AS qttOUT FROM tstok b WHERE (idCabang = {1}) AND (isExpired =0) AND (isVoid =0) AND (quantity <0) " & _
                            ")a INNER JOIN mbarang b ON a.idBrg = b.idBrg WHERE isToko=1 GROUP BY a.idBrg ", numOrder, id_cabang)
                ProgressMsg &= "STEP 5 : insert hasil closing sebagai beginning balance untuk shift selanjutnya" & vbCrLf
                LastQuery = SQLquery
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()

                'STEP 6 :Update semua tabel status=1
                SQLquery = String.Format("UPDATE mtspb SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtadj SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtpos SET status=1 WHERE DATE(createDate)=DATE('{0}'); " & _
                            "UPDATE mtbayarpos SET status=1 WHERE DATE(updateDate)=DATE('{0}'); " & _
                            "UPDATE tstok SET isExpired=1; ", Format(TglShiftNow, "yyyy-MM-dd"))
                ProgressMsg &= "STEP 6 :Update semua tabel status=1" & vbCrLf
                LastQuery = SQLquery
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()

                'STEP 7 :INSERT stok hasil closing ke table tstok untuk perhitungan kartu stok
                SQLquery = String.Format("INSERT INTO tstok(idJenisTrans, idCabang, idTrans, idBrg, quantity) " & _
                                         "SELECT 5, {0}, {1}, idBrg, qttIN+qttOUT FROM dtclosing WHERE idClosing={1}", id_cabang, numOrder)
                ProgressMsg &= "STEP 7 :INSERT stok hasil closing ke table tstok untuk perhitungan kartu stok" & vbCrLf
                LastQuery = SQLquery
                'ExDb.ExecData(SQLquery, conn_string_local)
                command.CommandText = SQLquery
                command.ExecuteNonQuery()
                'simpan input uang kasir sebelum closing
                For Each drow As DataRow In xSet.Tables("infouangkasir").Rows
                    SQLquery = String.Format("insert into dtinfouangkasir (idClosing,idPecahan,JmlLembar) VALUES ({0},{1},{2})", numOrder, drow("idUang"), drow("jmlh"))
                    command.CommandText = SQLquery
                    command.ExecuteNonQuery()
                Next
                'xSet.Tables("infouangkasir").Clear()
                vartr.Commit()
                Zcon.Close()
                adatrans = False
                'STEP 8 :print data sales dan stok
                selected_idxRow = shiftLogin
                Using printout As New Sales
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        tool.Print()
                    End Using
                End Using
                Using printout As New Sales
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        'tool.Print()
                        tool.ShowPreviewDialog()
                    End Using
                End Using
                'sync jurnal offline
                updatedataofflineemoney(True)
                Using printout As New Stok
                    Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                        tool.Print()
                        tool.ShowPreviewDialog()
                    End Using
                End Using

            Else
                PanelControl1.Visible = False
            End If
        Catch ex As Exception
            If adatrans = True Then
                vartr.Rollback()
                Zcon.Close()
            End If
            MsgBox(ex.Message)

            xSet.Tables("cekClosing").Clear()
            xSet.Tables("unlogoutID").Clear()
            Return False
        End Try

        xSet.Tables("cekClosing").Clear()
        If IsNothing(xSet.Tables("unlogoutID")) = False Then xSet.Tables("unlogoutID").Clear()

        Return True
    End Function
    Private Sub mainForm_GotFocus(ByVal sender As Object, ByVal e As EventArgs) Handles Me.GotFocus
        If status_login = False Then
            Close()
        ElseIf status_login = True And staff_name = "" Then
            Visible = True
            Return
        ElseIf status_login = True And staff_name = "setting pengaturan" Then
            Visible = True

            butInput.Enabled = False
            butClosing.Enabled = False
            butShift.Enabled = False

            PanelControl1.Visible = False
            PanelControl2.Visible = True
            Return
        Else
            Visible = True

            butInput.Enabled = True
            butClosing.Enabled = True
            butShift.Enabled = True

            PanelControl1.Visible = True
            PanelControl2.Visible = False
            PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
        End If
    End Sub

    Private Sub mainForm_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        AppsVersionControl.Text = My.Application.Info.Version.ToString

        PanelControl1.Visible = False
        loginForm.ShowDialog()
    End Sub

    Private Sub mainForm_SizeChanged(ByVal sender As Object, ByVal e As EventArgs) Handles Me.SizeChanged
        If Size.Width > 750 Then
            xLoc.X = (Screen.PrimaryScreen.WorkingArea.Width - PanelControl1.Width) / 2
            xLoc.Y = (Screen.PrimaryScreen.WorkingArea.Height - PanelControl1.Height) / 2
            If PanelControl1.Location = xLoc Then
                Exit Sub
            End If
            PanelControl1.Location = xLoc
            xLoc.X = (Screen.PrimaryScreen.WorkingArea.Width - PanelControl2.Width) / 2
            xLoc.Y = (Screen.PrimaryScreen.WorkingArea.Height - PanelControl2.Height) / 2
            PanelControl2.Location = xLoc
        End If
    End Sub

    Private Sub butInput_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butInput.Click
        Visible = False
        FormPOSx.ShowDialog()
        Visible = True
    End Sub

    Private Sub butSettings_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butSettings.Click
        PanelControl2.Visible = True
        PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
    End Sub

    Private Sub butShift_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butShift.Click
        'formContinue.callMe("Tombol Ya untuk Closing" & vbCrLf & "Tombol Tidak untuk Logout sementara ")
        frmpilihanlogout.ShowDialog()
        Select Case TipeLogout
            Case 1
                Exit Sub
            Case 2
                GoTo nextline
            Case 3
                '                If isContinue Then
                RetryClosing = 1
                While RetryClosing <= 5
                    'If Not Begin_Closing() Then
                    If Not Begin_ClosingOptm() Then
                        If isUserCanClosing = False Then Exit While
                        RetryClosing += 1
                        msgboxInformation("Mencoba melakukan closing lagi. Percobaan ke:" & RetryClosing)
                    Else
                        Exit While
                    End If
                End While
                '                End If
        End Select

nextline:
        staff_id = 0
        staff_group = 0
        staff_name = ""

        PanelControl1.Visible = False
        loginForm.ShowDialog()
    End Sub

    Private Sub butClosing_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butClosing.Click
        formContinue.callMe("Yakin ingin melakukan Closing?")
        If isContinue Then
            RetryClosing = 1
            While RetryClosing <= 5
                'If Not Begin_Closing() Then
                If Not Begin_ClosingOptm() Then
                    If isUserCanClosing = False Then Exit While
                    RetryClosing += 1
                    msgboxInformation("Mencoba melakukan closing lagi. Percobaan ke:" & RetryClosing)
                Else
                    Exit While
                End If
            End While

            Close()
        End If
    End Sub

    Private Sub butBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butBack.Click
        If staff_id = 0 Then
            PanelControl2.Visible = False
            loginForm.ShowDialog()
        Else
            txtNoNota.Text = ""
            If Not xSet.Tables("getDatamtPos") Is Nothing Then xSet.Tables("getDatamtPos").Clear()
            If Not xSet.Tables("gridOrder") Is Nothing Then xSet.Tables("gridOrder").Clear()

            PanelControl2.Visible = False

            PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default
        End If
    End Sub


    Private Sub butVoid_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butVoid.Click
        If isNotaVoid = True Then Exit Sub
        If isClosed Then
            MsgBox("Tidak Dapat di Void Krn Sudah di closing !")
            Exit Sub
        End If

        'cek jurnal offline
        SQLquery = String.Format("SELECT dtjurnal FROM app_logoffline.mt_journal_offline WHERE isJurnalNotaVoid=0 AND idOrder='{0}'", numOrder)
        Dim dtjurnaloffline As String = ExDb.Scalar(SQLquery, conn_string_local)

        If TestOnline() = False And IsNothing(dtjurnaloffline) Then
            MessageBox.Show("Nota belum bisa divoid karena koneksi offline!", "Void Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        'variable id trans emoney
        Dim idtransemoney As String
        'cek apakah ada item top up atau transaksi dengan e-money
        Dim adatopup As Boolean = False
        Dim adametodeemoney As Boolean = False
        If IsNothing(xSet.Tables("datapembayaranemoney")) Then
            xSet.Tables.Add("datapembayaranemoney")
        Else
            xSet.Tables("datapembayaranemoney").Clear()
        End If

        SQLquery = String.Format("SELECT db.amount FROM mtbayarpos mb JOIN dtbayarpos db ON mb.idPaymentOrder=db.idPaymentOrder WHERE mb.idOrder='{0}' AND db.idPayment='28'", numOrder)
        ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "datapembayaranemoney")

        If xSet.Tables("datapembayaranemoney").Rows.Count > 0 Then
            adametodeemoney = True
        End If

        For i = 0 To xSet.Tables("gridOrder").Rows.Count - 1
            filterbarang(xSet.Tables("gridOrder").Rows(i).Item(0).ToString, "'TOP UP'")
            If xSet.Tables("listfilter").Rows.Count > 0 Then
                adatopup = True
                Exit For
            End If
        Next i

        If (adatopup = True Or adametodeemoney = True) And TestOnline() = False Then
            MsgBox("Tidak Dapat Void Nota ini saat koneksi offline!")
            Exit Sub
        End If

        If adatopup = True Or adametodeemoney = True Then
            If checktopupcard() = False Then
                MsgBox("Untuk melakukan void ini, diperlukan kartu member terkait!")
                Exit Sub
            ElseIf checktopupcard() = True Then
                kodemember = readidmemeber().ToString
                SQLquery = String.Format("SELECT kode_member
FROM
  mtrans_emoney me
WHERE idCabang='{0}' AND me.idOrder='{1}' and jenis_transaksi=1", My.Settings.idcabang, numOrder)

                If kodemember <> ExDb.Scalar(SQLquery, My.Settings.cloudconn).ToString Then
                    MsgBox("Untuk melakukan void ini, diperlukan kartu member terkait!")
                    Exit Sub
                End If

            End If
        End If


        If AllowVoid = False Then
            frmvoiduser.ShowDialog()
            If isUserCanVoid = False Then Exit Sub
        End If
        If AllowVoid = True Then userVoid = staff_id

        If GridView1.DataRowCount > 0 Then
            formContinue.callMe2(String.Format("Yakin No Nota {0} akan di Void ?", txtNoNota.Text))
            If Not isContinue Then
                Exit Sub
            End If

            If Trim(ketvoidpos) = "" Then
                MessageBox.Show("Keterangan Void Wajib Di isi", "POS Laritta")
                Exit Sub
            ElseIf ketvoidpos.Length <= 10 Then
                MessageBox.Show("Keterangan Void kurang dari 10 Karakter", "POS Laritta")
                Exit Sub
            End If
            'void bayar trus void nota
            On Error GoTo ErrRaise

            SQLquery = String.Format("UPDATE mtbayarpos set isVoid = 1 where idOrder ={0};", numOrder)
            SQLquery = SQLquery & String.Format("UPDATE mtpos set isVoid = 1 where idOrder ={0};", numOrder)
            SQLquery = SQLquery & String.Format("UPDATE tstok set isVoid = 1 where idTrans ={0}  and idJenisTrans=4;", numOrder)
            SQLquery = SQLquery & String.Format("insert into mketvoidpos (idOrder,keterangan,uservoid,tglvoid) VALUES ({0},'{1}',{2},current_timestamp())", numOrder, ketvoidpos, userVoid)
            ExDb.ExecData(SQLquery, conn_string_local)



            'cek data point dan e-money dicloud
            If TestOnline() = True Then
                SQLquery = String.Format("select count(*) from mbst_getpointandstamp where id_transaksi={0} and id_cabang={1}", numOrder, id_cabang)
                Dim jmlD As Integer = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                SQLquery = ""
                If jmlD > 0 Then
                    SQLquery = String.Format("update mbst_getpointandstamp set isvoid=1,updateby={2} where id_transaksi={0} and id_cabang={1}", numOrder, id_cabang, staff_id)
                    ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                End If

                'update offline e-money dulu kemudian update online transaksi yang di void

                updatedataofflineemoney(False)

                'update online

                'update void mtrans
                SQLquery = String.Format("SELECT id_trans_emoney
FROM
  mtrans_emoney me
WHERE idCabang='{0}' AND me.idOrder='{1}' and jenis_transaksi=1", My.Settings.idcabang, numOrder)
                idtransemoney = ExDb.Scalar(SQLquery, My.Settings.cloudconn)
                SQLquery = String.Format("UPDATE mtrans_emoney SET isVoid=1 WHERE id_trans_emoney='{0}'", idtransemoney)
                ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                ''update dtrans_emoney
                'SQLquery = String.Format("UPDATE mbst_demoney set isVoid=1 WHERE id_trans_emoney='{0}'", idtransemoney)
                'ExDb.ExecData(SQLquery, My.Settings.cloudconn)


                If IsNothing(dtjurnaloffline) Then
                    'jurnal pembalik void
                    SQLquery = String.Format("SELECT mj.idJournal,transNumber,idCOA,type,nominal,idTransaction,orderNum FROM {1}.mt_journal mj
JOIN {1}.dt_journal dj ON mj.idJournal=dj.idJournal
WHERE isVoid=0 AND idTransaction='{0}' ORDER BY DATE(createDate)", numOrder, My.Settings.db_finance)

                    If IsNothing(xSet.Tables("datajurnalpembalik")) Then
                        xSet.Tables.Add("datajurnalpembalik")
                    Else
                        xSet.Tables("datajurnalpembalik").Clear()
                    End If
                    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "datajurnalpembalik")

                    'jurnal pos (grouping)
                    SQLquery = String.Format("SELECT mj.idJournal FROM {1}.mt_journal mj
JOIN {1}.dt_journal dj ON mj.idJournal=dj.idJournal
WHERE isVoid=0 AND idTransaction='{0}' GROUP BY mj.idJournal ORDER BY DATE(createDate) LIMIT 2", numOrder, My.Settings.db_finance)

                    If IsNothing(xSet.Tables("groupingjurnal")) Then
                        xSet.Tables.Add("groupingjurnal")
                    Else
                        xSet.Tables("groupingjurnal").Clear()
                    End If
                    ExDb.ExecQuery(My.Settings.cloudconn, SQLquery, xSet, "groupingjurnal")

                    'membalik jurnal
                    Dim stringdtjurnal As String = ""
                    For y = 0 To xSet.Tables("groupingjurnal").Rows.Count - 1
                        For Each row As DataRow In xSet.Tables("datajurnalpembalik").Select("idJournal='" & xSet.Tables("groupingjurnal").Rows(y).Item("idJournal") & "'")
                            Dim type As String
                            If row("type").ToString = "1" Then
                                type = "0"
                            Else
                                type = "1"
                            End If
                            stringdtjurnal = stringdtjurnal & row("orderNum") & "//" & row("idCOA") & "//" & type & "//" & row("nominal")
                            stringdtjurnal = stringdtjurnal & "|"
                        Next row
                        SQLquery = String.Format("CALL {4}.insert_journal(DATE(now()),'12','{0}','{1}' ,'Void Penjualan Retail Nomor: {5}',NULL,'{2}',now(),'{3}')", numOrder, CInt(Microsoft.VisualBasic.Right(txtNoNota.Text, 6)), staff_id, stringdtjurnal.Substring(0, stringdtjurnal.Count - 1), My.Settings.db_finance, nomornota)
                        ExDb.ExecData(SQLquery, My.Settings.cloudconn)
                        stringdtjurnal = ""
                    Next y
                Else
                    'input jurnal offline
                    Dim splitdtjurnal() As String = dtjurnaloffline.Split("|")
                    Dim hasiljurnalpembalik As String = ""
                    For Each dtjurnalsplit As String In splitdtjurnal
                        Dim splitdata() As String = dtjurnalsplit.Split("//")
                        If splitdata(4).ToString = "0" Then
                            splitdata(4) = "1"
                        Else
                            splitdata(4) = "0"
                        End If
                        hasiljurnalpembalik = hasiljurnalpembalik & splitdata(0).ToString & "//" & splitdata(2).ToString & "//" & splitdata(4).ToString & "//" & splitdata(6).ToString & "|"
                    Next dtjurnalsplit
                    hasiljurnalpembalik = hasiljurnalpembalik.Substring(0, hasiljurnalpembalik.Length - 1)
                    SQLquery = String.Format("INSERT INTO app_logoffline.mt_journal_offline (idOrder, dtjurnal, isJurnalNotaVoid) VALUES ('{0}','{1}','1')", numOrder, hasiljurnalpembalik)
                    ExDb.ExecData(SQLquery, My.Settings.localconn)
                End If

            Else
                If IsNothing(dtjurnaloffline) = False Then
                    'input jurnal offline
                    Dim splitdtjurnal() As String = dtjurnaloffline.Split("|")
                    Dim hasiljurnalpembalik As String = ""
                    For Each dtjurnalsplit As String In splitdtjurnal
                        Dim splitdata() As String = dtjurnalsplit.Split("//")
                        If splitdata(4).ToString = "0" Then
                            splitdata(4) = "1"
                        Else
                            splitdata(4) = "0"
                        End If
                        hasiljurnalpembalik = hasiljurnalpembalik & splitdata(0).ToString & "//" & splitdata(2).ToString & "//" & splitdata(4).ToString & "//" & splitdata(6).ToString & "|"
                    Next dtjurnalsplit

                    hasiljurnalpembalik = hasiljurnalpembalik.Substring(0, hasiljurnalpembalik.Length - 1)
                    SQLquery = String.Format("INSERT INTO app_logoffline.mt_journal_offline (idOrder, dtjurnal, isJurnalNotaVoid) VALUES ('{0}','{1}','1')", numOrder, hasiljurnalpembalik)
                    ExDb.ExecData(SQLquery, My.Settings.localconn)
                End If

                SQLquery = String.Format("update app_logoffline.mlogaddpoint set void=1 where idOrder={0} ", numOrder)
                ExDb.ExecData(SQLquery, My.Settings.db_conn_mysql)
            End If

            GridControl1.DataSource = Nothing
            txtNoNota.Text = ""
            ketvoidpos = ""
            userVoid = 0
            isUserCanVoid = False
            isReprint = True
            selected_idxRow = numOrder
            QryHeaderNota()

            If adametodeemoney Or adatopup Then
                Dim tambahsaldo As Double = 0
                saldosebelum = 0
                saldosesudah = 0
                If adametodeemoney = True Then
                    'menambah saldo (void nota yang menggunakan metode pembayaran e-money)

                    For i = 0 To xSet.Tables("datapembayaranemoney").Rows.Count - 1
                        tambahsaldo = tambahsaldo + CDbl(xSet.Tables("datapembayaranemoney").Rows(i).Item(0))
                    Next i

                    saldokartu = CDbl(readsaldo(False))
                    saldosebelum = saldokartu
                    saldokartu = saldokartu + tambahsaldo
                    inputsaldo(saldokartu.ToString)
                    saldosesudah = saldokartu
                    'SQLquery = "SELECT sum(nominal) `saldosebelum` FROM mtrans_emoney me JOIN mbst_demoney md ON me.idOrder=md.idOrder WHERE me.isvoid=0 AND md.isVoid=0 AND kode_member='198208010002' and md.idOrder<>'{0}'"
                ElseIf adatopup = True Then
                    'mengurangi saldo (void nota yang ada top up nya)
                    SQLquery = String.Format("SELECT nominal FROM mtrans_emoney me WHERE kode_member='{1}' and me.id_trans_emoney='{0}'", idtransemoney, kodemember)
                    tambahsaldo = CDbl(ExDb.Scalar(SQLquery, My.Settings.cloudconn))
                    saldokartu = CDbl(readsaldo(False))
                    saldosebelum = saldokartu
                    saldokartu = saldokartu - tambahsaldo
                    inputsaldo(saldokartu.ToString)
                    saldosesudah = saldokartu
                End If

                SQLquery = String.Format("SELECT namaCust FROM mcustomer WHERE kode_member='{0}'", kodemember)
                namapemilikkartu = ExDb.Scalar(SQLquery, My.Settings.cloudconn)

                'update e-money di mcustomer
                SQLquery = String.Format("UPDATE mcustomer m SET m.emoney={0} WHERE m.kode_member='{1}'", saldokartu, kodemember)
                ExDb.ExecData(SQLquery, My.Settings.cloudconn)
            End If

            MessageBox.Show("Data telah di Void", "POS Laritta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            useemoney = True
            Using printout As New Nota
                Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                    printout.Parameters("namamember").Value = namapemilikkartu

                    tool.Print()
                    'tool.ShowPreviewDialog()
                End Using
            End Using
        End If
        useemoney = False
        saldokartu = 0
        Exit Sub
ErrRaise:
        MessageBox.Show("Proses Void Gagal !", "Informasi")
        isUserCanVoid = False
        userVoid = 0
        Exit Sub
    End Sub

    Dim nomornota As String
    Private Sub butCari_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butCari.Click
        GridControl1.DataSource = Nothing
        GridView1.PopulateColumns()

        Dim Tahun As String = ""
        Dim Bulan As String = ""
        Dim Nomer As Integer = 0
        nomornota = txtNoNota.Text
        isNotaVoid = False
        'AND isVoid = 0 
        Try
            Tahun = "20" & Mid(txtNoNota.Text, 4, 2)
            Bulan = Mid(txtNoNota.Text, 7, 2)
            Nomer = Microsoft.VisualBasic.Right(txtNoNota.Text, 6)
            SQLquery = String.Format("SELECT idOrder,isVoid,status FROM mtpos WHERE YEAR(createdate) ='{0}' AND MONTH(createdate) ='{1}' AND numorder ={2} ", Tahun, Bulan, Nomer)
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDatamtPos")

            If xSet.Tables("getDatamtPos").Rows.Count > 0 Then
                numOrder = xSet.Tables("getDatamtPos").Rows(0).Item("idOrder")
                isNotaVoid = xSet.Tables("getDatamtPos").Rows(0).Item("isVoid")
                isClosed = xSet.Tables("getDatamtPos").Rows(0).Item("status")
                If isNotaVoid Then
                    PictureEdit1.Visible = True
                Else
                    PictureEdit1.Visible = False
                End If
            Else
                numOrder = 0
            End If
            If Not xSet.Tables("getDatamtPos") Is Nothing Then xSet.Tables("getDatamtPos").Clear()
            If Not xSet.Tables("gridOrder") Is Nothing Then xSet.Tables.Remove("gridOrder")
        Catch ex As Exception
            Return
        End Try

        If numOrder > 0 Then
            SQLquery = String.Format("SELECT a.idBrg, b.namaBrg Nama, a.quantity Qty, a.price Harga, a.discpercent Disc, a.totalprice Total FROM dtpos a INNER JOIN mbarang b ON a.idBrg=b.idBrg WHERE b.inactive=0 and idOrder ={0} ORDER BY namaBrg ASC", numOrder)
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "gridOrder")

            GridControl1.DataSource = xSet.Tables("gridOrder").DefaultView

            For Each coll As DataColumn In xSet.Tables("gridOrder").Columns
                With GridView1.Columns(coll.ColumnName)
                    If coll.ColumnName = "idBrg" Then
                        .Visible = False
                    ElseIf coll.ColumnName = "Nama" Then
                        .OptionsColumn.AllowEdit = False
                    ElseIf coll.ColumnName = "quantity" Then
                        .OptionsColumn.AllowEdit = False
                        .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        .DisplayFormat.FormatString = "{0:n0}"
                    ElseIf coll.ColumnName = "Harga" Then
                        .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        .DisplayFormat.FormatString = "{0:n0}"
                    ElseIf coll.ColumnName = "Disc" Then
                        .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        .DisplayFormat.FormatString = "{0:n2}"
                    ElseIf coll.ColumnName = "Total" Then
                        .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        .DisplayFormat.FormatString = "{0:n0}"
                        '.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                        .SummaryItem.SetSummary(DevExpress.Data.SummaryItemType.Sum, "{0:n0}")
                    End If
                    .AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
                End With
            Next

            GridView1.BestFitColumns()
            'butVoid.Enabled = (AllowVoid = True And isNotaVoid = False)
        Else
            GridControl1.DataSource = Nothing
        End If
    End Sub

    Private Sub butReprint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butReprint.Click
        isReprint = True
        selected_idxRow = numOrder
        QryHeaderNota()
        GetKodemember()
        'MsgBox("a")
        Using printout As New Nota
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                'For i As Integer = 0 To tool.PrinterSettings.PaperSizes.Count - 1
                'If tool.PrinterSettings.PaperSizes(i).PaperName = "80(72.1) x 3276 mm" Then
                'tool.PrinterSettings.DefaultPageSettings.PaperSize = tool.PrinterSettings.PaperSizes(i)
                'End If
                'Next i
                'AddHandler tool.PrintingSystem.EndPrint, AddressOf PrintingSystem_EndPrint
                'tool.ShowPreviewDialog()
                tool.Print()
            End Using
            'toolspeed.Print()

            'MsgBox("b")
            'printspeednota.Dispose()
            'printspeednota = New Nota
            'toolspeed.Dispose()
            'toolspeed = New DevExpress.XtraReports.UI.ReportPrintTool(printspeednota)
        End Using
    End Sub


    Private Sub printingSystem_StartPrint(ByVal sender As Object, ByVal e As PrintDocumentEventArgs)

        For i As Integer = 0 To e.PrintDocument.PrinterSettings.PaperSizes.Count - 1
            If e.PrintDocument.PrinterSettings.PaperSizes(i).PaperName = "80(72.1) x 3276 mm" Then
                'MsgBox("test a")
                e.PrintDocument.DefaultPageSettings.PaperSize = e.PrintDocument.PrinterSettings.PaperSizes(i)

            End If
        Next i
        'For i = 0 To printDoc.PrinterSettings.PaperSizes.Count - 1
        'pkSize = printDoc.PrinterSettings.PaperSizes.Item(i)
        'comboPaperSize.Items.Add(pkSize)
        'Next
        'For i As Integer = 0 To e.PrintDocument.PrinterSettings.PrinterResolutions.Count - 1
        'If e.PrintDocument.PrinterSettings.PrinterResolutions(i).Kind = PrinterResolutionKind.High Then
        'e.PrintDocument.DefaultPageSettings.PrinterResolution = e.PrintDocument.PrinterSettings.PrinterResolutions(i)
        'Exit For
        'End If
        'Next i
    End Sub
    Private Sub butPrintStok_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butPrintStok.Click
        Using printout As New Stok2
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                tool.ShowPreviewDialog()
            End Using
        End Using
    End Sub

    Private Sub butPrintClosing_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butPrintClosing.Click
        virNumpad.ShowDialog()
        selected_idxRow = qttInteger
        isprintclosingx = True
        Using printout As New Stok
            Using tool As DevExpress.XtraReports.UI.ReportPrintTool = New DevExpress.XtraReports.UI.ReportPrintTool(printout)
                tool.ShowPreviewDialog()
            End Using
        End Using
    End Sub

    Private Sub butCekSales_Click(ByVal sender As Object, ByVal e As EventArgs) Handles butCekSales.Click
        GridControl1.DataSource = Nothing
        GridView1.PopulateColumns()

        Try
            If Not xSet.Tables("getDataStartTime") Is Nothing Then xSet.Tables("getDataStartTime").Clear()
            SQLquery = String.Format("SELECT startTime FROM mtswitchuser a WHERE a.idCabang={0} AND a.idStaff={1} AND DATE(a.startTime)=CURDATE() AND endTime='00:00:00'", id_cabang, staff_id)
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "getDataStartTime")

            If Not xSet.Tables("gridOrder") Is Nothing Then xSet.Tables.Remove("gridOrder")
            SQLquery = String.Format("SELECT namaKategoriPayment Kategori, IFNULL(SUM(jml+chargeFee),0) Total FROM mkategoripayment a " & _
                        "LEFT JOIN ( " & _
                        "SELECT c.idKategoriPayment, CASE WHEN b.idPayment=1 THEN amount-totalChange ELSE amount END AS jml, chargeFee " & _
                        "FROM mtbayarpos a " & _
                        "INNER JOIN dtbayarpos b ON a.idPaymentOrder=b.idPaymentOrder AND b.status=1 " & _
                        "INNER JOIN mpayment c ON b.idPayment=c.idPayment " & _
                        "WHERE a.isVoid=0 AND a.idCabang={0} AND (a.updateDate BETWEEN '{1}' AND CURRENT_TIMESTAMP()) " & _
                        ") b ON a.idKategoriPayment=b.idKategoriPayment " & _
                        "WHERE a.idKategoriPayment IN (1,2,3) " & _
                        "GROUP BY a.idKategoriPayment ", id_cabang, Format(xSet.Tables("getDataStartTime").Rows(0).Item(0), "yyyy-MM-dd HH:mm:ss"))
            ExDb.ExecQuery(conn_string_local, SQLquery, xSet, "gridOrder")

            GridControl1.DataSource = xSet.Tables("gridOrder").DefaultView

            For Each coll As DataColumn In xSet.Tables("gridOrder").Columns
                With GridView1.Columns(coll.ColumnName)
                    If coll.ColumnName = "Total" Then
                        .DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                        .DisplayFormat.FormatString = "{0:n0}"
                        .SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                        .SummaryItem.DisplayFormat = "{0:n0}"
                    End If
                    .AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
                End With
            Next

            GridView1.BestFitColumns()
        Catch ex As Exception
            Return
        End Try
    End Sub

    Private Sub AppsVersionControl_DoubleClick(sender As Object, e As EventArgs)
        FrmAbout.ShowDialog()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        reprintnotaclosing.ShowDialog()
    End Sub

    Private Sub reprintopenform_Click(sender As Object, e As EventArgs) Handles reprintopenform.Click
        Dim x As New reprint_voucher
        x.ShowDialog()
    End Sub

    Private Sub txtNoNota_EditValueChanged(sender As Object, e As EventArgs) Handles txtNoNota.EditValueChanged

    End Sub

    Private Sub LabelControl1_Click(sender As Object, e As EventArgs) Handles LabelControl1.Click

    End Sub
End Class