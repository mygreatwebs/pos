﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainForm
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainForm))
        Me.SplashScreenManager1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.Laritta_POS.WaitForm1), True, True)
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.reprintopenform = New DevExpress.XtraEditors.SimpleButton()
        Me.butCekSales = New DevExpress.XtraEditors.SimpleButton()
        Me.butPrintClosing = New DevExpress.XtraEditors.SimpleButton()
        Me.butPrintStok = New DevExpress.XtraEditors.SimpleButton()
        Me.butBack = New DevExpress.XtraEditors.SimpleButton()
        Me.AppsVersionControl = New DevExpress.XtraEditors.LabelControl()
        Me.butCari = New DevExpress.XtraEditors.SimpleButton()
        Me.butVoid = New DevExpress.XtraEditors.SimpleButton()
        Me.butReprint = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.txtNoNota = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.butInput = New DevExpress.XtraEditors.SimpleButton()
        Me.butClosing = New DevExpress.XtraEditors.SimpleButton()
        Me.butSettings = New DevExpress.XtraEditors.SimpleButton()
        Me.butShift = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoNota.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplashScreenManager1
        '
        Me.SplashScreenManager1.ClosingDelay = 500
        '
        'PanelControl2
        '
        Me.PanelControl2.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelControl2.Appearance.Options.UseFont = True
        Me.PanelControl2.Controls.Add(Me.PictureEdit1)
        Me.PanelControl2.Controls.Add(Me.SimpleButton1)
        Me.PanelControl2.Controls.Add(Me.PanelControl3)
        Me.PanelControl2.Controls.Add(Me.butCari)
        Me.PanelControl2.Controls.Add(Me.butVoid)
        Me.PanelControl2.Controls.Add(Me.butReprint)
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Controls.Add(Me.txtNoNota)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(-3, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(754, 476)
        Me.PanelControl2.TabIndex = 6
        Me.PanelControl2.Visible = False
        '
        'PictureEdit1
        '
        Me.PictureEdit1.EditValue = Global.Laritta_POS.My.Resources.Resources.void
        Me.PictureEdit1.Location = New System.Drawing.Point(164, 125)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Size = New System.Drawing.Size(207, 154)
        Me.PictureEdit1.TabIndex = 101
        Me.PictureEdit1.Visible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Document_Print_32
        Me.SimpleButton1.Location = New System.Drawing.Point(353, 423)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(207, 46)
        Me.SimpleButton1.TabIndex = 100
        Me.SimpleButton1.Text = "Reprint Nota Closing"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.reprintopenform)
        Me.PanelControl3.Controls.Add(Me.butCekSales)
        Me.PanelControl3.Controls.Add(Me.butPrintClosing)
        Me.PanelControl3.Controls.Add(Me.butPrintStok)
        Me.PanelControl3.Controls.Add(Me.butBack)
        Me.PanelControl3.Controls.Add(Me.AppsVersionControl)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelControl3.Location = New System.Drawing.Point(566, 2)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(186, 472)
        Me.PanelControl3.TabIndex = 7
        '
        'reprintopenform
        '
        Me.reprintopenform.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.reprintopenform.Appearance.Options.UseFont = True
        Me.reprintopenform.Enabled = False
        Me.reprintopenform.Image = CType(resources.GetObject("reprintopenform.Image"), System.Drawing.Image)
        Me.reprintopenform.Location = New System.Drawing.Point(10, 283)
        Me.reprintopenform.Name = "reprintopenform"
        Me.reprintopenform.Size = New System.Drawing.Size(166, 85)
        Me.reprintopenform.TabIndex = 101
        Me.reprintopenform.Text = "Re-print Voucher"
        '
        'butCekSales
        '
        Me.butCekSales.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butCekSales.Appearance.Options.UseFont = True
        Me.butCekSales.Image = Global.Laritta_POS.My.Resources.Resources.buy_48
        Me.butCekSales.Location = New System.Drawing.Point(10, 192)
        Me.butCekSales.Name = "butCekSales"
        Me.butCekSales.Size = New System.Drawing.Size(166, 85)
        Me.butCekSales.TabIndex = 8
        Me.butCekSales.Text = "Lihat" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "keuangan"
        Me.butCekSales.Visible = False
        '
        'butPrintClosing
        '
        Me.butPrintClosing.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPrintClosing.Appearance.Options.UseFont = True
        Me.butPrintClosing.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Document_Print_Preview_48
        Me.butPrintClosing.Location = New System.Drawing.Point(10, 101)
        Me.butPrintClosing.Name = "butPrintClosing"
        Me.butPrintClosing.Size = New System.Drawing.Size(166, 85)
        Me.butPrintClosing.TabIndex = 6
        Me.butPrintClosing.Text = "Print Closing" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ke-X"
        '
        'butPrintStok
        '
        Me.butPrintStok.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butPrintStok.Appearance.Options.UseFont = True
        Me.butPrintStok.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Printer_48
        Me.butPrintStok.Location = New System.Drawing.Point(10, 10)
        Me.butPrintStok.Name = "butPrintStok"
        Me.butPrintStok.Size = New System.Drawing.Size(166, 85)
        Me.butPrintStok.TabIndex = 5
        Me.butPrintStok.Text = "Print Stok"
        '
        'butBack
        '
        Me.butBack.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butBack.Appearance.Options.UseFont = True
        Me.butBack.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Emblem_Symbolic_Link_48
        Me.butBack.Location = New System.Drawing.Point(10, 374)
        Me.butBack.Name = "butBack"
        Me.butBack.Size = New System.Drawing.Size(166, 85)
        Me.butBack.TabIndex = 7
        Me.butBack.Text = "Tutup"
        '
        'AppsVersionControl
        '
        Me.AppsVersionControl.Location = New System.Drawing.Point(140, 456)
        Me.AppsVersionControl.Name = "AppsVersionControl"
        Me.AppsVersionControl.Size = New System.Drawing.Size(36, 13)
        Me.AppsVersionControl.TabIndex = 100
        Me.AppsVersionControl.Text = "1.0.0.0"
        '
        'butCari
        '
        Me.butCari.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.butCari.Appearance.Options.UseFont = True
        Me.butCari.Location = New System.Drawing.Point(345, 5)
        Me.butCari.Name = "butCari"
        Me.butCari.Size = New System.Drawing.Size(92, 40)
        Me.butCari.TabIndex = 2
        Me.butCari.Text = "Cari"
        '
        'butVoid
        '
        Me.butVoid.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butVoid.Appearance.Options.UseFont = True
        Me.butVoid.Image = Global.Laritta_POS.My.Resources.Resources.delete_32
        Me.butVoid.Location = New System.Drawing.Point(117, 423)
        Me.butVoid.Name = "butVoid"
        Me.butVoid.Size = New System.Drawing.Size(102, 46)
        Me.butVoid.TabIndex = 4
        Me.butVoid.Text = "Void"
        '
        'butReprint
        '
        Me.butReprint.Appearance.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.butReprint.Appearance.Options.UseFont = True
        Me.butReprint.Image = Global.Laritta_POS.My.Resources.Resources.Gnome_Document_Print_32
        Me.butReprint.Location = New System.Drawing.Point(9, 423)
        Me.butReprint.Name = "butReprint"
        Me.butReprint.Size = New System.Drawing.Size(102, 46)
        Me.butReprint.TabIndex = 3
        Me.butReprint.Text = "Reprint"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(9, 51)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(551, 366)
        Me.GridControl1.TabIndex = 99
        Me.GridControl1.TabStop = False
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.HeaderPanel.Options.UseFont = True
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Verdana", 11.25!)
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.EnableAppearanceOddRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.RowHeight = 20
        '
        'txtNoNota
        '
        Me.txtNoNota.Location = New System.Drawing.Point(103, 12)
        Me.txtNoNota.Name = "txtNoNota"
        Me.txtNoNota.Properties.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoNota.Properties.Appearance.Options.UseFont = True
        Me.txtNoNota.Properties.Mask.EditMask = "SO/99/99/999999"
        Me.txtNoNota.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtNoNota.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.txtNoNota.Size = New System.Drawing.Size(236, 24)
        Me.txtNoNota.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(9, 15)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(88, 18)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "No. Nota :"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.butInput)
        Me.PanelControl1.Controls.Add(Me.butClosing)
        Me.PanelControl1.Controls.Add(Me.butSettings)
        Me.PanelControl1.Controls.Add(Me.butShift)
        Me.PanelControl1.Location = New System.Drawing.Point(161, 85)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(426, 238)
        Me.PanelControl1.TabIndex = 7
        '
        'butInput
        '
        Me.butInput.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butInput.Appearance.Options.UseFont = True
        Me.butInput.Appearance.Options.UseTextOptions = True
        Me.butInput.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.butInput.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.butInput.Image = Global.Laritta_POS.My.Resources.Resources.buy_48
        Me.butInput.ImageLocation = DevExpress.XtraEditors.ImageLocation.BottomRight
        Me.butInput.Location = New System.Drawing.Point(5, 5)
        Me.butInput.Name = "butInput"
        Me.butInput.Size = New System.Drawing.Size(205, 111)
        Me.butInput.TabIndex = 0
        Me.butInput.Text = "Input Order"
        '
        'butClosing
        '
        Me.butClosing.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butClosing.Appearance.Options.UseFont = True
        Me.butClosing.Appearance.Options.UseTextOptions = True
        Me.butClosing.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.butClosing.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.butClosing.Image = Global.Laritta_POS.My.Resources.Resources.warning_48
        Me.butClosing.ImageLocation = DevExpress.XtraEditors.ImageLocation.BottomRight
        Me.butClosing.Location = New System.Drawing.Point(216, 122)
        Me.butClosing.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.butClosing.LookAndFeel.UseDefaultLookAndFeel = False
        Me.butClosing.Name = "butClosing"
        Me.butClosing.Size = New System.Drawing.Size(205, 111)
        Me.butClosing.TabIndex = 3
        Me.butClosing.Text = "Tutup toko"
        '
        'butSettings
        '
        Me.butSettings.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butSettings.Appearance.Options.UseFont = True
        Me.butSettings.Appearance.Options.UseTextOptions = True
        Me.butSettings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.butSettings.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.butSettings.Image = Global.Laritta_POS.My.Resources.Resources.address_48
        Me.butSettings.ImageLocation = DevExpress.XtraEditors.ImageLocation.BottomRight
        Me.butSettings.Location = New System.Drawing.Point(216, 5)
        Me.butSettings.Name = "butSettings"
        Me.butSettings.Size = New System.Drawing.Size(205, 111)
        Me.butSettings.TabIndex = 1
        Me.butSettings.Text = "Pengaturan"
        '
        'butShift
        '
        Me.butShift.Appearance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.butShift.Appearance.Options.UseFont = True
        Me.butShift.Appearance.Options.UseTextOptions = True
        Me.butShift.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.butShift.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
        Me.butShift.Image = Global.Laritta_POS.My.Resources.Resources.clock_48
        Me.butShift.ImageLocation = DevExpress.XtraEditors.ImageLocation.BottomRight
        Me.butShift.Location = New System.Drawing.Point(5, 122)
        Me.butShift.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.butShift.LookAndFeel.UseDefaultLookAndFeel = False
        Me.butShift.Name = "butShift"
        Me.butShift.Size = New System.Drawing.Size(205, 111)
        Me.butShift.TabIndex = 2
        Me.butShift.Text = "Ganti Kasir"
        '
        'mainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 481)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "mainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoNota.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butCekSales As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butPrintClosing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butPrintStok As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butBack As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents AppsVersionControl As DevExpress.XtraEditors.LabelControl
    Friend WithEvents butCari As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butVoid As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butReprint As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtNoNota As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents butInput As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butClosing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butSettings As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butShift As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents reprintopenform As DevExpress.XtraEditors.SimpleButton
End Class
