﻿Imports MySql.Data.MySqlClient

Public Class freeprodshowcase

    Private Sub isiGrid(ByVal isotherproduct As Boolean)
        If isotherproduct Then
            SQLquery = "select  a.idbrg,namaBrg,namaKategori2,0 jmlh,price hrg from mbarang  a  left join mkategoribrg2 c on a.idKategori2=c.idkategori2  where c.namaKategori2 not in ('PASTRY & PIE','PUDDING','TART SLICE','BS','COOKIES','BOX','D & K','MERCHANDISE','BIAYA TAMBAHAN','LAIN-LAIN','FREE PRODUCT','PRODUCT PROMO') order by price ASC"
        Else
            SQLquery = "select  a.idbrg,namaBrg,namaKategori2,0 jmlh,price hrg from mbarang  a  left join mkategoribrg2 c on a.idKategori2=c.idkategori2  where c.namaKategori2 in ('PASTRY & PIE','PUDDING','TART SLICE') order by price ASC"
        End If

        If IsNothing(xSet.Tables("itemprod")) = False Then xSet.Tables("itemprod").Clear()
        ExDb.ExecQuery(My.Settings.localconn, SQLquery, xSet, "itemprod")
        GridControl1.DataSource = xSet.Tables("itemprod")

    End Sub

    Public Sub CallMe()
        isiGrid(False)
        Me.ShowDialog()
    End Sub

    Private Sub butOK_Click(sender As Object, e As EventArgs) Handles butOK.Click
        'For Each row As DataRow In xSet.Tables("itempromo").Select("jmlh>0")
        '    totqty += row("jmlh")
        'Next
        'If totqty <> 3 Then Exit Sub

        If IsNothing(xSet.Tables("setSales")) Then Exit Sub
        For Each row As DataRow In xSet.Tables("itemprod").Select("jmlh>0")
            'xSet.Tables("setSalesSwap").Rows.Add(drow.Item("idBrg"),
            '                                          drow.Item("namaBrg"),
            '                                          drow.Item("jmlh"),
            '                                          drow.Item("hrg"),
            '                                          drow.Item("hrgpcs"),
            '                                          drow.Item("disc"))

            xSet.Tables("setSales").Rows.Add(row("idBrg"),
                                                row("namaBrg"), row("jmlh"), 0, row("hrg"), 100)
        Next

        Close()

        AktifPromo = 6
    End Sub


    Private Sub freeprodshowcase_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        On Error Resume Next
        xSet.Tables.Remove("itemprod")
        GC.Collect()
        GC.WaitForPendingFinalizers()
    End Sub

    Private Sub freeprodshowcase_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Dispose()
    End Sub

    Private Sub butDelete_Click(sender As Object, e As EventArgs) Handles butDelete.Click
        AktifPromo = 0
        G_ketpromo = ""
        Close()
    End Sub

    Private Sub freeprodshowcase_Load(sender As Object, e As EventArgs) Handles Me.Load
        PanelControl1.Appearance.BackColor = Color.PaleVioletRed
        PanelControl1.Appearance.BackColor2 = Color.PaleVioletRed
        PanelControl1.Appearance.Options.UseBackColor = True

        PanelControl1.LookAndFeel.UseDefaultLookAndFeel = False
        PanelControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
    End Sub

    Private Sub tbprodshowcase_Click(sender As Object, e As EventArgs) Handles tbprodshowcase.Click
        isiGrid(False)
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        isiGrid(True)
    End Sub
End Class